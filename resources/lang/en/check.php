<?php

return [
    'basket_wrong' => 'Error! Empty picking basket! ',
    'outbound_wrong' => 'the outbound is wrong, please check',
    'picking_has_abnormal' => 'Error! Bounded abnormal orders have to be closed first.',
    'check_log_params_wrong' => 'there are something wrong with the params',
    'abnormal_created' => 'the abnormal has been created',
    'picking_failed' => 'Picking Failed. Please put the basket to the Abnormal Area.',

    'task_not_exists' => 'the check task not exists',
    'outbound_not_completed' => 'Error! Order status is “:state” that can’t go through.',
    'empty_basket' => 'Error! Empty picking basket!',
    'outbound_checking' => 'Error! The outbound is being checked. ',
    'wrong_sku' => 'Wrong SKU! Try again or confirm the scanning?',
];
