<?php

return [
    'baskets_empty' => 'Please fill in the blank',
    'baskets_wrong' => 'Error! Basket is not recorded in the system',
    'picking_task_wrong' => 'picking task wrong',
    'picking_busy' => 'picking is busy, please try again after a moment',
    'picking_log_params_wrong' => 'there are something wrong with the params',
    'baskets_already_bound' => 'baskets already bound',

    'task_not_exists' => 'picking task not exists',
    'no_outbound_available' => 'No outbound available',
];
