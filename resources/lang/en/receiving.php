<?php

return [
    'receiving_finished' => 'Error! This pre-receiving order is completed',
    'actual_receiving_finished' => 'Error! This order is already finished. Start a new one.',
    'actual_receiving_not_exist' => 'actual receiving not exist',
    'receiving_busy' => 'receiving is busy now, please try again after a moment',
    'cart_busy' => 'cart is not available',
    'sku_code_print_is_busy' => 'print sku code is busy now, please try again after a moment',
    'pre_receiving_number_wrong' => 'pre_receiving_number is wrong',
    'sku_not_in_pre_receiving' => 'Error! SKU doesn’t exist in the PRE order, report an abnormal order.',
    'receiving_duplicated_sku_in_cart' => 'Error! Duplicated SKU in this cart, please use another cart. ',
];
