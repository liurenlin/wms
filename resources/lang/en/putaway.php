<?php

return [
    'receiving_cart_busy' => 'the receiving cart is busy now',
    'receiving_finished' => 'receiving is finished',
    'storage_location_wrong' => 'Error! Bin is not recorded in the picking area.',
    'putaway_busy' => 'the putaway is busy now, please try again after a moment',
    'wrong_sku' => 'Wrong SKU',
    'receiving_cart_empty' => 'Error! Empty Receiving Cart.',
    'receiving_cart_not_empty' => 'Error! Receiving Cart Not Empty.',
    'task_not_exist' => 'Error! The task not exist',
];
