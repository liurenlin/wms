<?php

return [
    'pre_receiving_items_wrong' => 'the items of pre-receiving are wrong',
    'outbound_items_wrong' => 'the items of outbound are wrong',
    'skus_wrong' => 'skus are wrong',
    'products_wrong' => 'products are wrong',
    'order_not_exist' => 'the order not exist',
];
