<?php

return [
    'pre_receiving_items_wrong' => '预录入库单 items 参数错误',
    'outbound_items_wrong' => '出库单单 items 参数错误',
    'skus_wrong' => 'skus 参数错误',
    'products_wrong' => 'products 参数错误',
];
