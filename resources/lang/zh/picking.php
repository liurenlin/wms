<?php

return [
    'baskets_empty' => '拣货篮子不能为空',
    'baskets_wrong' => '拣货篮子错误',
    'picking_task_wrong' => '拣货任务错误',
    'picking_busy' => '拣货繁忙，请稍后重试',
    'picking_log_params_wrong' => '参数错误，请检查',
    'baskets_already_bound' => '已经绑定过篮子',

    'task_not_exists' => '拣货任务不存在',
    'no_outbound_available' => '暂无出库单可用',
];
