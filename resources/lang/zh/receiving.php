<?php

return [
    'receiving_finished' => '收货已完成',
    'actual_receiving_finished' => '实际入库单已经完成',
    'actual_receiving_not_exist' => '实际入库单不存在',
    'receiving_busy' => '收货忙，请稍后重试',
    'cart_busy' => '收货车已被占用',
    'sku_code_print_is_busy' => '打印标签忙，请稍后重试',
    'pre_receiving_number_wrong' => 'pre_receiving_number 错误，请检查',
    'sku_not_in_pre_receiving' => 'SKU 不在预录入库单中，报告异常',
    'receiving_duplicated_sku_in_cart' => '存在相同 SKU 在同一辆车中，请检查',
];
