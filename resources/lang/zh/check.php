<?php

return [
    'basket_wrong' => '拣货篮子错误，请检查',
    'outbound_wrong' => '出库单错误，请检查',
    'picking_has_abnormal' => '拣货存在异常',
    'check_log_params_wrong' => '参数错误，请检查',
    'abnormal_created' => '异常已经创建，请勿重复提交',
    'picking_failed' => '拣货失败，请将篮子放到异常区',


    'task_not_exists' => '复核任务不存在',
    'outbound_not_completed' => '订单状态是 “:state” ，不能继续下去',
    'empty_basket' => '拣货篮子为空',
    'outbound_checking' => '出库单在检查中',
    'wrong_sku' => 'SKU 错误，请确认或者重新检查',
];
