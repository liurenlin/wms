<?php

return [
    'receiving_cart_busy' => '当前收货车已有上架任务',
    'receiving_finished' => '收货已完成',
    'storage_location_wrong' => '库位不在拣货区',
    'putaway_busy' => '上架忙，请稍后重试',
    'wrong_sku' => 'SKU 错误，请检查',
    'receiving_cart_empty' => '当前收货车为空',
    'receiving_cart_not_empty' => '当前收货车不为空',
    'task_not_exist' => '入库任务不存在',
];
