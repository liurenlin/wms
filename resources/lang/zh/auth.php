<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => '登录信息错误',
    'throttle' => '尝试过多，请稍后重试',

    'login.fail' => 'User/Password 不正确',
    'logout.fail' => '退出登录失败',
    'warehouse_not_found' => '用户归属仓库无法确认',

    'forbidden' => '禁止访问',


];
