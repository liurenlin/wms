<?php

namespace Tests\Feature\BasicData\ReceivingCart;

use App\Models\ReceivingCart;
use App\Models\Warehouse;
use Tests\Feature\BaseFeatureTestCase;

class CreateTest extends BaseFeatureTestCase
{
    public function testCreateSuccess()
    {
        $warehouse = factory(Warehouse::class)->create();
        $latestReceivingCart = ReceivingCart::withoutTrashed()->latest('id')->first();
        if (!$latestReceivingCart) {
            $startCodeNum = 1;
        } else {
            $startCodeNum = (int) substr($latestReceivingCart->cart_code, -3);
            $startCodeNum += 1;
        }
        $amount = random_int(1, 10);
        $data = [
            'warehouse_id' => $warehouse->id,
            'amount' => $amount,
        ];

        $response = $this->login()->header()->json('POST', 'admin/api/receiving-carts/batch', $data);

        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'amount',
            ],
        ]);
        for ($i = 0; $i < $amount; $i++) {
            $code = $startCodeNum;
            if ($startCodeNum < 100) {
                $code = sprintf('%03s', $startCodeNum);
            }
            $carts = ReceivingCart::query()
                ->where('cart_code', 'REC-' . $code)
                ->get();
            $this->assertEquals(1, count($carts));
        }
    }

    /**
     * @dataProvider getCreateFailedData
     * @param $data
     * @param $field
     */
    public function testCreateFieldFailed($data, $field)
    {
        $warehouse = factory(Warehouse::class)->create();
        $data['warehouse_id'] = $warehouse->id;
        $response = $this->login()->header()->json('POST', 'admin/api/receiving-carts/batch', $data);

        $this->validationAssert($response, $field);

        $data = [
            'amount' => 10,
            'warehouse_id' => $warehouse->id + 1,
        ];
        $response = $this->login()->header()->json('POST', 'admin/api/receiving-carts/batch', $data);

        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);

    }

    public function getCreateFailedData()
    {
        return $this->generateIntegerFieldData([], 'amount', 1, 99);
    }
}
