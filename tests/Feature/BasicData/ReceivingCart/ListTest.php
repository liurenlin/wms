<?php

namespace Tests\Feature\BasicData\ReceivingCart;

use App\Models\ReceivingCart;
use Tests\Feature\BaseFeatureTestCase;

class ListTest extends BaseFeatureTestCase
{
    public function testList(){
        factory(ReceivingCart::class)->create();
        $response = $this->login()->header()->json('GET', 'admin/api/receiving-carts?page=1&per_page=5');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'carts' => [
                    [
                        'id',
                        'code',
                        'created_at',
                    ]
                ],
            ],
        ]);
    }
}
