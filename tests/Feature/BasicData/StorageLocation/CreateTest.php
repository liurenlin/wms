<?php

namespace Tests\Feature\BasicData\StorageLocation;

use App\Models\StorageArea;
use App\Models\StorageLocation;
use App\Models\Warehouse;
use Tests\Feature\BaseFeatureTestCase;

class CreateTest extends BaseFeatureTestCase
{
    public function testCreateSuccess()
    {
        $warehouse = factory(Warehouse::class)->create();
        $area = factory(StorageArea::class)->create();
        $startPath = random_int(1, 10);
        $endPath = random_int($startPath, 11);
        $shelfQty = random_int(1, 10);
        $layerQty = random_int(1, 5);
        $binQty = random_int(1, 5);
        $data = [
            'path_start' => $startPath,
            'path_end' => $endPath,
            'shelves_quantity' => $shelfQty,
            'layers_per_shelf' => $layerQty,
            'bins_per_layer' => $binQty,
            'warehouse_id' => $warehouse->id,
            'area_id' => $area->id
        ];
        $response = $this->login()->header()->json('POST', 'admin/api/storage-locations', $data);

        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        for ($pathCode = $startPath; $pathCode <= $startPath; $pathCode++) {
            for ($shelfCode = 1; $shelfCode <= $shelfQty; $shelfCode++) {
                for ($layerCode = 0; $layerCode < $layerQty; $layerCode++) {
                    for ($binCode = 1; $binCode <= $binQty; $binCode++) {
                        $code = $area->area_code . sprintf('%02s', $pathCode)
                            . '-' . sprintf('%02s', $shelfCode) . '-'
                            . $layerCode . $binCode;
                        $locations = StorageLocation::query()
                            ->where('warehouse_id', $warehouse->id)
                            ->where('storage_location_code', $code)
                            ->get();
                        $this->assertEquals(1, count($locations));
                    }
                }
            }
        }
    }

    /**
     * @dataProvider getCreateFailedData
     * @param $data
     * @param $field
     */
    public function testCreateFieldFailed($data, $field)
    {
        $warehouse = factory(Warehouse::class)->create();
        $area = factory(StorageArea::class)->create();
        $data['warehouse_id'] = $warehouse->id;
        $data['area_id'] = $area;
        $response = $this->login()->header()->json('POST', 'admin/api/storage-locations', $data);

        $this->validationAssert($response, $field);
    }

    public function getCreateFailedData()
    {
        $startPath = random_int(1, 10);
        $endPath = random_int($startPath + 1, 11);
        $shelfQty = random_int(1, 10);
        $layerQty = random_int(1, 5);
        $binQty = random_int(1, 5);
        $data = [
            'path_start' => $startPath,
            'path_end' => $endPath,
            'shelves_quantity' => $shelfQty,
            'layers_per_shelf' => $layerQty,
            'bins_per_layer' => $binQty,
        ];
        $fields = [
            ['path_start', 1, 10],
            ['path_end', 2, 11],
            ['shelves_quantity', 1, 10],
            ['layers_per_shelf', 1, 5],
            ['bins_per_layer', 1, 5],
        ];
        $failedData = [];
        foreach ($fields as $field) {
            $fieldData = $this->generateIntegerFieldData($data, $field[0], $field[1], $field[2]);
            $failedData = array_merge($failedData, $fieldData);
        }
        // end less start
        $tmpData = $data;
        $tmpData['path_end'] = random_int(1, 10);;
        $tmpData['path_start'] = random_int($tmpData['path_end'] + 1, 11);
        $failedData[] = [$tmpData, 'path_end'];
        return $failedData;
    }

    public function testCreateNotExistFailed()
    {
        $warehouse = factory(Warehouse::class)->create();
        $area = factory(StorageArea::class)->create();
        $startPath = random_int(1, 10);
        $endPath = random_int($startPath + 1, 11);
        $shelfQty = random_int(1, 10);
        $layerQty = random_int(1, 5);
        $binQty = random_int(1, 5);
        $data = [
            'path_start' => $startPath,
            'path_end' => $endPath,
            'shelves_quantity' => $shelfQty,
            'layers_per_shelf' => $layerQty,
            'bins_per_layer' => $binQty,
            'warehouse_id' => $warehouse->id + 1,
            'area_id' => $area->id
        ];
        $response = $this->login()->header()->json('POST', 'admin/api/storage-locations', $data);

        $response->assertOk();

        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $data['warehouse_id'] = $warehouse->id;
        $data['area_id'] = $area->id + 1;
        $response = $this->login()->header()->json('POST', 'admin/api/storage-locations', $data);

        $response->assertOk();

        $response->assertJsonStructure([
            'code',
            'message',
        ]);
    }
}
