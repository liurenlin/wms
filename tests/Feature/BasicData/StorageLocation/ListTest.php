<?php

namespace Tests\Feature\BasicData\StorageLocation;

use App\Consts\StorageTypeConst;
use App\Models\StorageLocation;
use Tests\Feature\BaseFeatureTestCase;

class ListTest extends BaseFeatureTestCase
{
    public function testList(){
        factory(StorageLocation::class)->create();
        $response = $this->login()->header()->json('GET', 'admin/api/storage-locations?page=1&per_page=5');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'storage_locations' => [
                    [
                        'id',
                        'code',
                        'type',
                        'created_at',
                    ]
                ],
            ],
        ]);
    }

    public function testTypeList()
    {
        $types = StorageTypeConst::getTypes();
        $type = $types[random_int(0, count($types) - 1)];
        factory(StorageLocation::class)->create([
            'storage_type' => $type,
        ]);
        $response = $this->login()->header()->json('GET', 'admin/api/storage-locations?page=1&per_page=5');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'storage_locations' => [
                    [
                        'id',
                        'code',
                        'type',
                        'created_at',
                    ]
                ],
            ],
        ]);
    }
}
