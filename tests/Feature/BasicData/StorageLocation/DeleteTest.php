<?php

namespace Tests\Feature\BasicData\StorageLocation;

use App\Models\Stock;
use App\Models\StorageLocation;
use Tests\Feature\BaseFeatureTestCase;

class DeleteTest extends BaseFeatureTestCase
{
    public function testDeleteSuccess()
    {
        $location = factory(StorageLocation::class)->create();
        $response = $this->login()->header()->json('DELETE', 'admin/api/storage-locations/' . $location->id, []);

        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $count = StorageLocation::withoutTrashed()
            ->where('id', $location->id)
            ->count();
        $this->assertEquals(0, $count);
    }

    public function testNotExist()
    {
        $response = $this->login()->header()->json('DELETE', 'admin/api/storage-locations/0', []);

        $response->assertOk();
        $response->assertJson([
            'code' => 400,
            'message' => 'storage location not exist',
        ]);
    }

    public function testMultiSuccess()
    {
        $locations = factory(StorageLocation::class)->times(5)->create();
        $idArray = [];
        foreach ($locations as $location) {
            $idArray[] = $location->id;
        }
        $ids = implode(',', $idArray);
        $response = $this->login()->header()->json('DELETE', 'admin/api/storage-locations/' . $ids, []);

        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);

        $response = $this->login()->header()->json('DELETE', 'admin/api/storage-locations/' . $ids, []);

        $response->assertOk();
        $response->assertJson([
            'code' => 400,
            'message' => 'storage location not exist',
        ]);
        $count = StorageLocation::withoutTrashed()
            ->whereIn('id', $idArray)
            ->count();
        $this->assertEquals(0, $count);
    }

    public function testHasStock()
    {
        $location = factory(StorageLocation::class)->create();
        factory(Stock::class)->create([
            'bin' => $location->storage_location_code,
        ]);
        $response = $this->login()->header()->json('DELETE', 'admin/api/storage-locations/' . $location->id, []);

        $response->assertOk();
        $response->assertJson([
            'code' => 400,
            'message' => 'storage location not empty',
        ]);
    }
}
