<?php

namespace Tests\Feature\BasicData\Warehouse;

use App\Models\Warehouse;
use Illuminate\Support\Str;
use Tests\Feature\BaseFeatureTestCase;

class CreateTest extends BaseFeatureTestCase
{
    protected $warehouse;

    protected function setUp(): void
    {
        parent::setUp();
        Warehouse::unsetEventDispatcher();
    }

    public function testCreateSuccess()
    {
        $warehouse = $this->getWarehouseData();
        $response = $this->login()->header()->json('POST', 'admin/api/warehouses', [
            'name' => $warehouse->warehouse_name,
            'manager' => $warehouse->manager,
            'country' => $warehouse->country,
            'state' => $warehouse->state,
            'city' => $warehouse->city,
            'district' => $warehouse->district,
            'address' => $warehouse->address,
            'cellphone' => $warehouse->cellphone,
        ]);

        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'id'
            ],
        ]);
    }

    /**
     * @dataProvider getCreateFailedData
     * @param $data
     * @param $field
     */
    public function testCreateFailed($data, $field)
    {
        $response = $this->login()->header()->json('POST', 'admin/api/warehouses', $data);

        $this->validationAssert($response, $field);
    }

    protected function getWarehouseData()
    {
        if (!$this->warehouse) {
            $this->warehouse = factory(Warehouse::class)->make();
        }
        return $this->warehouse;
    }

    public function getCreateFailedData()
    {
        $warehouseData = [
            'name' => Str::random(50),
            'manager' => Str::random(10),
            'country' => Str::random(10),
            'state' => Str::random(10),
            'city' => Str::random(10),
            'district' => Str::random(10),
            'address' => Str::random(100),
            'cellphone' => Str::random(10),
        ];
        $fields = [
            ['name', 100],
            ['manager', 60],
            ['country', 60],
            ['address', 200],
            ['cellphone', 30],
        ];
        $warehouseFailedData = [];
        foreach ($fields as $field) {
            $failedData = $this->generateStringFieldData($warehouseData,$field[0], $field[1]);
            $warehouseFailedData = array_merge($warehouseFailedData, $failedData);
        }
        return $warehouseFailedData;
    }
}
