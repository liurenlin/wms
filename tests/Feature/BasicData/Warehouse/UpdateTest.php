<?php

namespace Tests\Feature\BasicData\Warehouse;

use App\Models\Warehouse;
use Illuminate\Support\Str;
use Tests\Feature\BaseFeatureTestCase;

class UpdateTest extends BaseFeatureTestCase
{
    public function testUpdateSuccess()
    {
        $origin = factory(Warehouse::class)->create();
        $warehouse = factory(Warehouse::class)->make();
        $response = $this->login()->header()->json('PUT', 'admin/api/warehouses/' . $origin->id, [
            'name' => $warehouse->warehouse_name,
            'manager' => $warehouse->manager,
            'country' => $warehouse->country,
            'state' => $warehouse->state,
            'city' => $warehouse->city,
            'district' => $warehouse->district,
            'address' => $warehouse->address,
            'cellphone' => $warehouse->cellphone,
        ]);

        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
    }

    /**
     * @dataProvider getUpdateFailedData
     * @param $data
     * @param $field
     */
    public function testUpdateFailed($data, $field)
    {
        $origin = factory(Warehouse::class)->create();
        $response = $this->login()->header()->json('PUT', 'admin/api/warehouses/' . $origin->id, $data);

        $this->validationAssert($response, $field);
    }

    public function getUpdateFailedData()
    {
        $warehouseData = [
            'name' => Str::random(50),
            'manager' => Str::random(10),
            'country' => Str::random(10),
            'state' => Str::random(10),
            'city' => Str::random(10),
            'district' => Str::random(10),
            'address' => Str::random(100),
            'cellphone' => Str::random(10),
        ];
        $fields = [
            ['name', 100],
            ['manager', 60],
            ['country', 60],
            ['address', 200],
            ['cellphone', 30],
        ];
        $warehouseFailedData = [];
        foreach ($fields as $field) {
            $failedData = $this->generateStringFieldData($warehouseData,$field[0], $field[1], false);
            $warehouseFailedData = array_merge($warehouseFailedData, $failedData);
        }
        return $warehouseFailedData;
    }
}
