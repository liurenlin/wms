<?php

namespace Tests\Feature\BasicData\Warehouse;

use App\Consts\ResponseConst;
use App\Models\Warehouse;
use Illuminate\Support\Facades\Log;
use Tests\Feature\BaseFeatureTestCase;

class ListTest extends BaseFeatureTestCase
{
    public function testList(){
        factory(Warehouse::class)->create();
        $response = $this->login()->header()->json('GET', 'admin/api/warehouses?page=1&per_page=5');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'warehouses' => [
                    [
                        'id',
                        'name',
                        'country',
                        'code',
                        'address',
                        'manager',
                        'cellphone',
                    ]
                ],
            ],
        ]);
    }
}
