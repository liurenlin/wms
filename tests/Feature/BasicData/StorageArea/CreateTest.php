<?php

namespace Tests\Feature\BasicData\StorageArea;

use App\Consts\StorageTypeConst;
use App\Models\StorageArea;
use Tests\Feature\BaseFeatureTestCase;

class CreateTest extends BaseFeatureTestCase
{
    protected $chars = [
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M'
        , 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
    ];

    public function testCreateSuccess()
    {
        $type = StorageArea::STORAGE_TYPE_PICKING;
        $num = random_int(1, 9);
        $code = '';
        for ($i = 0; $i < $num; $i++) {
            $code .= $this->chars[random_int(0, count($this->chars) - 1)];
        }
        factory(StorageArea::class)->create([
            'area_code' => $code,
            'storage_type' => $type,
        ]);
        $response = $this->login()->header()->json('POST', 'admin/api/storage-areas', [
            'type' => $type,
        ]);

        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'id'
            ],
        ]);
        $area = StorageArea::query()
            ->latest('id')
            ->first();
        $this->assertEquals($area->area_code, (++$code));

        factory(StorageArea::class)->create([
            'area_code' => ++$code,
            'storage_type' => 2,
        ]);
        $response = $this->login()->header()->json('POST', 'admin/api/storage-areas', [
            'type' => $type,
        ]);

        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'id'
            ],
        ]);
        $area = StorageArea::query()
            ->latest('id')
            ->first();
        $this->assertEquals($area->area_code, (++$code));
    }

    /**
     * @dataProvider getCreateFailedData
     * @param $data
     * @param $field
     */
    public function testCreateFailed($data, $field)
    {
        $response = $this->login()->header()->json('POST', 'admin/api/storage-areas', $data);

        $this->validationAssert($response, $field);
    }

    public function getCreateFailedData()
    {
        $failedData = [];
        $field = 'type';
        // no  type
        $failedData[] = [
            [], $field
        ];
        // no integer
        $failedData[] = [
            ['type' => chr(rand(97,122))], $field
        ];
        // error integer
        $failedData[] = [
            ['type' => 0], $field
        ];
        // 5, 2, 6
        $failedData[] = [
            ['type' => random_int(10, 100)], $field
        ];
        return $failedData;
    }
}
