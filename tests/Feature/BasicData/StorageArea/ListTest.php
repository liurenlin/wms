<?php

namespace Tests\Feature\BasicData\StorageArea;

use App\Models\StorageArea;
use Tests\Feature\BaseFeatureTestCase;

class ListTest extends BaseFeatureTestCase
{
    public function testList(){
        factory(StorageArea::class)->create();
        $response = $this->login()->header()->json('GET', 'admin/api/storage-areas?page=1&per_page=5');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'areas' => [
                    [
                        'id',
                        'storage_type',
                        'area_code',
                        'created_at',
                    ]
                ],
            ],
        ]);
    }
}
