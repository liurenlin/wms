<?php

namespace Tests\Feature\BasicData\PickingBasket;

use App\Models\Basket;
use App\Models\ReceivingCart;
use App\Models\Warehouse;
use Tests\Feature\BaseFeatureTestCase;

class CreateTest extends BaseFeatureTestCase
{
    public function testCreateSuccess()
    {
        $warehouse = factory(Warehouse::class)->create();
        $latestBasket = Basket::withoutTrashed()->latest('id')->first();
        if (!$latestBasket) {
            $startCodeNum = 1;
        } else {
            $startCodeNum = (int) substr($latestBasket->basket_code, -3);
            $startCodeNum += 1;
        }
        $amount = random_int(1, 100);
        $data = [
            'warehouse_id' => $warehouse->id,
            'amount' => $amount,
        ];

        $response = $this->login()->header()->json('POST', 'admin/api/picking-baskets/batch', $data);

        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'amount',
            ],
        ]);
        for ($i = 0; $i < $amount; $i++) {
            $code = $startCodeNum;
            if ($startCodeNum < 100) {
                $code = sprintf('%03s', $startCodeNum);
            }
            $carts = Basket::query()
                ->where('basket_code', 'BAS-' . $code)
                ->get();
            $this->assertEquals(1, count($carts));
        }
    }

    /**
     * @dataProvider getCreateFailedData
     * @param $data
     * @param $field
     */
    public function testCreateFieldFailed($data, $field)
    {
        $warehouse = factory(Warehouse::class)->create();
        $data['warehouse_id'] = $warehouse->id;
        $response = $this->login()->header()->json('POST', 'admin/api/picking-baskets/batch', $data);

        $this->validationAssert($response, $field);

        $data = [
            'amount' => 10,
            'warehouse_id' => $warehouse->id + 1,
        ];
        $response = $this->login()->header()->json('POST', 'admin/api/picking-baskets/batch', $data);

        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);

    }

    public function getCreateFailedData()
    {
        return $this->generateIntegerFieldData([], 'amount', 1, 99);
    }
}
