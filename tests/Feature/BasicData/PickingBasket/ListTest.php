<?php

namespace Tests\Feature\BasicData\PickingBasket;

use App\Models\Basket;
use Carbon\Carbon;
use Tests\Feature\BaseFeatureTestCase;

class ListTest extends BaseFeatureTestCase
{
    public function testList(){
        $basket = factory(Basket::class)->create();
        $response = $this->login()->header()->json('GET', 'admin/api/picking-baskets?page=1&per_page=5');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'baskets' => [
                    [
                        'id',
                        'basket_code',
                        'created_at',
                    ]
                ],
                'page_bean' => [
                    'total',
                    'page',
                    'per_page',
                ],
            ],
        ]);
        $response = $this->login()->header()->json('GET', 'admin/api/picking-baskets?page=1&per_page=5&basket_code=' . $basket->basket_code);
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'baskets' => [
                    [
                        'id',
                        'basket_code',
                        'created_at',
                    ]
                ],
                'page_bean' => [
                    'total',
                    'page',
                    'per_page',
                ],
            ],
        ]);
        $response->assertJson([
            'code' => 200,
            'message' => 'success',
            'data' => [
                'baskets' => [
                    [
                        'id' => $basket->id,
                        'basket_code' => $basket->basket_code,
                        'created_at' => Carbon::parse($basket->created_at)->format('Y-m-d H:i:s'),
                    ]
                ],
                'page_bean' => [
                    'total' => 1,
                    'page' => 1,
                    'per_page' => 5,
                ],
            ],
        ]);
    }
}
