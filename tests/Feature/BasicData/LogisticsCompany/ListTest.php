<?php

namespace Tests\Feature\BasicData\LogisticsCompany;

use App\Models\LogisticsCompany;
use Tests\Feature\BaseFeatureTestCase;

class ListTest extends BaseFeatureTestCase
{
    public function testList(){
        factory(LogisticsCompany::class)->create();
        $response = $this->login()->header()->json('GET', 'admin/api/logistics-companies?page=1&per_page=5');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'logistics_companies' => [
                    [
                        'id',
                        'code',
                        'name',
                        'contact',
                        'cellphone',
                        'sender',
                        'address',
                        'type',
                        'sender_phone',
                    ]
                ],
                'page_bean' => [
                    'total',
                    'page',
                    'per_page',
                ],
            ],
        ]);
    }
}
