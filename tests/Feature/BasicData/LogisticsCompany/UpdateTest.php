<?php

namespace Tests\Feature\BasicData\LogisticsCompany;

use App\Models\LogisticsCompany;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Tests\Feature\BaseFeatureTestCase;
use Tests\TestCase;

class UpdateTest extends BaseFeatureTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        LogisticsCompany::unsetEventDispatcher();
    }

    public function testUpdateSuccess()
    {
        $company = factory(LogisticsCompany::class)->create();
        $update = [
            'sender' => Str::random(23),
            'sender_phone' => Str::random(26),
            'address' => Str::random(125),
        ];
        $response = $this->login()->header()->json('PUT', 'admin/api/logistics-companies/' . $company->id, $update);
        Log::debug('zqq:' . json_encode($response, JSON_PRETTY_PRINT));
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message']);
        $company = LogisticsCompany::find($company->id);
        $this->assertEquals($update['sender'], $company->sender);
        $this->assertEquals($update['sender_phone'], $company->telephone);
        $this->assertEquals($update['address'], $company->address);
    }
}
