<?php

namespace Tests\Feature\BasicData\Sku;

use App\Models\ProductSku;
use Tests\Feature\BaseFeatureTestCase;

class ListTest extends BaseFeatureTestCase
{
    public function testList()
    {
        factory(ProductSku::class)->create();
        $response = $this->login()->header()->json('GET', 'admin/api/skus?page=1&per_page=5');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'skus' => [
                    [
                        'id',
                        'sku_code',
                        'spu_code',
                        'variant',
                        'image_url',
                        'weight',
                        'width',
                        'height',
                        'length',
                    ]
                ],
                'page_bean' => [
                    'total',
                    'page',
                    'per_page',
                ],
            ],
        ]);
    }
}
