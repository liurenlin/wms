<?php

namespace Tests\Feature\BasicData\Sku;

use App\Models\ProductSku;
use Tests\Feature\BaseFeatureTestCase;

class UpdateWeightAndVolumeTest extends BaseFeatureTestCase
{
    public function testUpdateSuccess()
    {
        $sku = factory(ProductSku::class)->create();
        $update = [
            'weight' => random_int(1, 10000) / 100,
            'width' => random_int(1, 10000) / 100,
            'height' => random_int(1, 10000) / 100,
            'length' => random_int(1, 10000) / 100,
        ];
        $response = $this->login()->header()->json('PUT', 'admin/api/skus/' . $sku->id, $update);
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message'
        ]);
        $sku = ProductSku::find($sku->id);
        $this->assertEquals($update['weight'], $sku->weight);
        $this->assertEquals($update['width'], $sku->width);
        $this->assertEquals($update['height'], $sku->height);
        $this->assertEquals($update['length'], $sku->length);

        $response = $this->login()->header()->json('PUT', 'admin/api/skus/0', $update);
        $response->assertOk();
        $response->assertJson([
            'code' => 400,
            'message' => 'sku not exist'
        ]);
    }

    /**
     * @dataProvider getUpdateFailedData
     * @param $data
     * @param $field
     */
    public function testUpdateFailed($data, $field)
    {
        $sku = factory(ProductSku::class)->create();
        $response = $this->login()->header()->json('PUT', 'admin/api/skus/' . $sku->id, $data);
        $this->validationAssert($response, $field);
    }

    public function getUpdateFailedData()
    {
        $result = [];
        $update = [
            'weight' => random_int(1, 10000) / 100,
            'width' => random_int(1, 10000) / 100,
            'height' => random_int(1, 10000) / 100,
            'length' => random_int(1, 10000) / 100,
        ];
        foreach ($update as $field => $data) {
            $fieldData = $this->generateIntegerFieldData($update, $field, 0, null, false);
            $result = array_merge($result, $fieldData);
        }
        return $result;
    }
}
