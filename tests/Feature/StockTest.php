<?php

namespace Tests\Feature;

use App\Consts\ResponseConst;
use App\Models\Basket;
use App\Models\Outbound;
use App\Models\OutboundFrozenItem;
use App\Models\OutboundItem;
use App\Models\PickingTask;
use App\Models\PickingTaskBasket;
use App\Models\PickingTaskItem;
use App\Models\Stock;
use App\Models\StorageArea;
use App\Models\StorageLocation;
use App\Models\Warehouse;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class StockTest extends TestCase
{
    use DatabaseTransactions;

    public function testIndex()
    {
        // 未登录
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->json('GET', 'api/stocks');
        $response->assertUnauthorized();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_UNAUTHORIZED,
        ]);

        // 模拟用户
        $user = factory(User::class)->create();

        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->json('GET', 'api/stocks');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_FAIL,
        ]);

        // 生成必须元素
        $warehouse = factory(Warehouse::class)->create();
        // 库位
        $storageLocation = factory(StorageLocation::class)->create([
            'warehouse_id' => $warehouse->id,
            'storage_type' => StorageArea::STORAGE_TYPE_PICKING,
        ]);
        $skuCode = 'SKU0001';
        $quantity = 10;
        // 库存
        $stock = factory(Stock::class)->create([
            'warehouse_id' => $warehouse->id,
            'sku_code' => $skuCode,
            'bin' => $storageLocation->storage_location_code,
            'storage_type' => $storageLocation->storage_type,
            'quantity' => $quantity,
        ]);
        // 出库单
        $outbound = factory(Outbound::class)->create([
            'warehouse_id' => $warehouse->id,
            'is_need_intercept' => Outbound::IS_NEED_INTERCEPT_FALSE,
            'state' => Outbound::STATE_FROZEN,
        ]);
        $outboundItem = factory(OutboundItem::class)->create([
            'outbound_id' => $outbound->id,
            'sku_code' => $skuCode,
            'quantity' => $quantity,
        ]);
        factory(OutboundFrozenItem::class)->create([
            'outbound_id' => $outbound->id,
            'outbound_item_id' => $outboundItem->id,
            'sku_code' => $skuCode,
            'frozen_bin' => $storageLocation->storage_location_code,
            'frozen_quantity' => $quantity,
        ]);
        // 生成拣货篮子
        $basket = factory(Basket::class)->create([
            'warehouse_id' => $warehouse->id,
            'state' => Basket::STATE_FREE,
        ]);

        // 参数正确
        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
            'warehouse-id' => $warehouse->id,
        ])->json('GET', 'api/stocks', [
            'bin' => $storageLocation->storage_location_code,
        ]);

        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'list' => [
                    [
                        'bin',
                        'sku_code',
                        'quantity',
                    ]
                ],
            ],
        ]);

        $response->assertJsonFragment([
            'code' => ResponseConst::CODE_SUCCESS,
            'message' => __('common.success'),
        ]);

        $response->assertJsonFragment([
            'sku_code' => $skuCode,
            'quantity' => $quantity,
            'bin' => (string) $storageLocation->storage_location_code,
        ]);
    }
}
