<?php

namespace Tests\Feature;

use App\Consts\ResponseConst;
use App\Models\Resource;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductSku;
use App\Models\ProductSkuImage;
use App\Models\ReceivingCart;
use App\Models\ReceivingCartItem;
use App\Models\Stock;
use App\Models\StorageArea;
use App\Models\StorageLocation;
use App\Models\Warehouse;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class PutawayTest extends TestCase
{
    use DatabaseTransactions;

    public function testStorePutawayTask()
    {
        // 未登录
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->json('POST', 'api/putaway-tasks');

        $response->assertUnauthorized();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_UNAUTHORIZED,
        ]);

        // 模拟用户
        $user = factory(User::class)->create();

        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->json('POST', 'api/putaway-tasks');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_FAIL,
        ]);

        // 生成必须元素
        $resource = factory(Resource::class)->create();
        $warehouse = factory(Warehouse::class)->create();
        // 库位
        factory(StorageLocation::class)->create([
            'warehouse_id' => $warehouse->id,
            'storage_type' => StorageArea::STORAGE_TYPE_PICKING,
            'is_empty' => StorageLocation::IS_EMPTY_TRUE,
            'is_suggest_lock' => StorageLocation::IS_SUGGEST_LOCK_FALSE,
        ]);
        $product = factory(Product::class)->create();
        factory(ProductImage::class, 2)->create([
            'product_id' => $product->id,
            'resource_id' => $resource->id,
        ]);
        $productSku1 = factory(ProductSku::class)->create([
            'product_id' => $product->id,
        ]);
        factory(ProductSkuImage::class, 2)->create([
            'product_sku_id' => $productSku1->id,
            'resource_id' => $resource->id,
        ]);
        // 生成收货车
        $receivingCart = factory(ReceivingCart::class)->create([
            'warehouse_id' => $warehouse->id,
            'state' => 0,
        ]);
        // 生成收货车 item
        factory(ReceivingCartItem::class)->create([
            'warehouse_id' => $warehouse->id,
            'receiving_cart_id' => $receivingCart->id,
            'sku_code' => $productSku1->sku_code,
            'quantity' => 10,
        ]);
        // 收货暂存区
        factory(Stock::class)->create([
            'warehouse_id' => $warehouse->id,
            'sku_code' => $productSku1->sku_code,
            'quantity' => 10,
            'storage_type' => StorageArea::STORAGE_TYPE_RECEIVING,
            'bin' => '',
            'frozen_quantity' => 0,
            'unfrozen_quantity' => 0,
        ]);


        // 参数正确
        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
            'warehouse-id' => $warehouse->id,
        ])->json('POST', 'api/putaway-tasks', [
            'receiving_cart_code' => $receivingCart->cart_code,
        ]);
        $response->assertOk();

        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'id',
                'task_number',
                'skus' => [
                    [
                        'code',
                        'suggest_bin',
                        'quantity',
                        'images',
                    ]
                ],
            ]
        ]);

        $response->assertJsonFragment([
            'code' => ResponseConst::CODE_SUCCESS,
            'message' => __('common.success'),
        ]);

        $response->assertJsonFragment([
            'code' => $productSku1->sku_code,
        ]);
        $response->assertJsonFragment([
            'quantity' => 10,
        ]);
    }

    public function testStorePutawayLog()
    {
        // 未登录
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->json('POST', 'api/putaway-logs');

        $response->assertUnauthorized();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_UNAUTHORIZED,
        ]);

        // 模拟用户
        $user = factory(User::class)->create();

        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->json('POST', 'api/putaway-logs');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_FAIL,
        ]);

        // 生成必须元素
        $resource = factory(Resource::class)->create();
        $warehouse = factory(Warehouse::class)->create();
        // 库位
        factory(StorageLocation::class)->create([
            'warehouse_id' => $warehouse->id,
            'storage_type' => StorageArea::STORAGE_TYPE_PICKING,
            'is_empty' => StorageLocation::IS_EMPTY_TRUE,
            'is_suggest_lock' => StorageLocation::IS_SUGGEST_LOCK_FALSE,
        ]);
        $product = factory(Product::class)->create();
        factory(ProductImage::class, 2)->create([
            'product_id' => $product->id,
            'resource_id' => $resource->id,
        ]);
        $productSku1 = factory(ProductSku::class)->create([
            'product_id' => $product->id,
        ]);
        factory(ProductSkuImage::class, 2)->create([
            'product_sku_id' => $productSku1->id,
            'resource_id' => $resource->id,
        ]);
        // 生成收货车
        $receivingCart = factory(ReceivingCart::class)->create([
            'warehouse_id' => $warehouse->id,
            'state' => 0,
        ]);
        // 生成收货车 item
        factory(ReceivingCartItem::class)->create([
            'warehouse_id' => $warehouse->id,
            'receiving_cart_id' => $receivingCart->id,
            'sku_code' => $productSku1->sku_code,
            'quantity' => 10,
        ]);
        // 收货暂存区
        factory(Stock::class)->create([
            'warehouse_id' => $warehouse->id,
            'sku_code' => $productSku1->sku_code,
            'quantity' => 10,
            'storage_type' => StorageArea::STORAGE_TYPE_RECEIVING,
            'bin' => '',
            'frozen_quantity' => 0,
            'unfrozen_quantity' => 0,
        ]);
        // 领取任务
        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
            'warehouse-id' => $warehouse->id,
        ])->json('POST', 'api/putaway-tasks', [
            'receiving_cart_code' => $receivingCart->cart_code,
        ]);

        $responseContent = $response->getOriginalContent();

        // 执行任务
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
            'warehouse-id' => $warehouse->id,
        ])->json('POST', 'api/putaway-logs', [
            'putaway_task_id' => $responseContent['data']['id'],
            'bin' => $responseContent['data']['skus'][0]['suggest_bin'],
            'receiving_cart_code' => $receivingCart->cart_code,
            'sku_code' => $responseContent['data']['skus'][0]['code'],
            'quantity' => $responseContent['data']['skus'][0]['quantity'],
        ]);

        $response->assertOk();

        $response->assertJsonStructure([
            'code',
            'message',
        ]);

        $response->assertJsonFragment([
            'code' => ResponseConst::CODE_SUCCESS,
            'message' => __('common.success'),
        ]);
    }
}
