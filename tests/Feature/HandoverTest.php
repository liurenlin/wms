<?php

namespace Tests\Feature;

use App\Consts\ResponseConst;
use App\Models\Basket;
use App\Models\CheckTask;
use App\Models\CheckTaskItem;
use App\Models\LogisticsCompany;
use App\Models\Outbound;
use App\Models\OutboundItem;
use App\Models\PickingTask;
use App\Models\PickingTaskBasket;
use App\Models\Role;
use App\Models\Stock;
use App\Models\StorageArea;
use App\Models\UserRole;
use App\Models\Warehouse;
use App\Models\Weigh;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class HandoverTest extends TestCase
{
    use DatabaseTransactions;

    public function testLogisticsCompanies()
    {
        // 未登录
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->json('GET', 'api/logistics-companies');

        $response->assertUnauthorized();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_UNAUTHORIZED,
        ]);

        // 模拟用户
        $user = factory(User::class)->create();

        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->json('GET', 'api/logistics-companies');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_FAIL,
        ]);

        // 生成必须元素
        $warehouse = factory(Warehouse::class)->create();
        // 生成用户角色
        $role = factory(Role::class)->create();
        $userRole = factory(UserRole::class)->create([
            'warehouse_id' => $warehouse->id,
            'user_id' => $user->id,
            'role_id' => $role->id,
        ]);
        $logisticsCompany = factory(LogisticsCompany::class)->create();

        // 参数正确
        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
            'warehouse-id' => $warehouse->id,
        ])->json('GET', 'api/logistics-companies');
        $response->assertOk();

        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'companies' => [
                    [
                        'id',
                        'name',
                    ]
                ]
            ]
        ]);

        $response->assertJsonFragment([
            'code' => ResponseConst::CODE_SUCCESS,
            'message' => __('common.success'),
        ]);

        $response->assertJsonFragment([
            'id' => $logisticsCompany->id,
            'name' => $logisticsCompany->company_name,
        ]);
    }

    public function testStoreHandover()
    {
        // 未登录
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->json('POST', 'api/handovers');

        $response->assertUnauthorized();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_UNAUTHORIZED,
        ]);

        // 模拟用户
        $user = factory(User::class)->create();

        // 生成必须元素
        $logisticsNumber = 'LOG00001';
        $quantity = 1;
        $signature = 'https://signature.png';
        $warehouse = factory(Warehouse::class)->create();
        // 生成物流公司
        $logisticsCompany = factory(LogisticsCompany::class)->create();
        // 出库单
        $outbound = factory(Outbound::class)->create([
            'warehouse_id' => $warehouse->id,
            'is_need_intercept' => Outbound::IS_NEED_INTERCEPT_FALSE,
            'logistics_company_id' => $logisticsCompany->id,
            'logistics_number' => $logisticsNumber,
            'state' => Outbound::STATE_WEIGHED
        ]);
        // 出库单子项
        $outboundItem = factory(OutboundItem::class)->create([
            'outbound_id' => $outbound->id,
        ]);
        // 生成拣货篮子
        $basket = factory(Basket::class)->create([
            'warehouse_id' => $warehouse->id,
            'state' => Basket::STATE_BUSY,
        ]);

        // 生成拣货任务
        $pickingTask = factory(PickingTask::class)->create([
            'warehouse_id' => $warehouse->id,
            'user_id' => $user->id,
        ]);
        $pickingTaskBasket = factory(PickingTaskBasket::class)->create([
            'picking_task_id' => $pickingTask->id,
            'outbound_number' => $outbound->outbound_number,
            'basket_code' => $basket->basket_code,
        ]);

        // 生成称重记录
        $weigh = factory(Weigh::class)->create([
            'warehouse_id' => $warehouse->id,
            'basket_code' => $basket->basket_code,
            'outbound_number' => $outbound->outbound_number,
            'picking_task_id' => $pickingTask->id,
            'logistics_company_id' => $logisticsCompany->id,
            'logistics_number' => $logisticsNumber,
        ]);
        // 出库暂存区库存
        factory(Stock::class)->create([
            'warehouse_id' => $warehouse->id,
            'sku_code' => $outboundItem->sku_code,
            'quantity' => $outboundItem->quantity,
            'storage_type' => StorageArea::STORAGE_TYPE_OUTBOUND,
            'bin' => '',
            'frozen_quantity' => 0,
            'unfrozen_quantity' => 0,
        ]);

        // 生成交接记录
        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
            'warehouse-id' => $warehouse->id,
        ])->json('POST', 'api/handovers', [
            'logistics_company_id' => $logisticsCompany->id,
            'logistics_numbers' => json_encode([
                [
                    'logistics_number' => $logisticsNumber,
                    'quantity' => $quantity,
                ]
            ]),
            'signature' => $signature,
        ]);

        $response->assertOk();

        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'id',
            ]
        ]);

        $response->assertJsonFragment([
            'code' => ResponseConst::CODE_SUCCESS,
            'message' => __('common.success'),
        ]);
    }
}
