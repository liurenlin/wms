<?php

namespace Tests\Feature;

use App\Consts\ResponseConst;
use App\Models\Basket;
use App\Models\Outbound;
use App\Models\OutboundFrozenItem;
use App\Models\OutboundItem;
use App\Models\PickingTask;
use App\Models\PickingTaskBasket;
use App\Models\PickingTaskItem;
use App\Models\Stock;
use App\Models\StorageArea;
use App\Models\StorageLocation;
use App\Models\Warehouse;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class PickingTest extends TestCase
{
    use DatabaseTransactions;

//    public function testStorePickingTask()
//    {
//        // 未登录
//        $response = $this->withHeaders([
//            'X-Requested-With' => 'XMLHttpRequest',
//        ])->json('POST', 'api/picking-tasks');
//
//        $response->assertUnauthorized();
//        $response->assertJsonStructure([
//            'code',
//            'message',
//        ]);
//        $response->assertJson([
//            'code' => ResponseConst::CODE_UNAUTHORIZED,
//        ]);
//
//        // 模拟用户
//        $user = factory(User::class)->create();
//
//        $this->actingAs($user, 'api');
//        $response = $this->withHeaders([
//            'X-Requested-With' => 'XMLHttpRequest',
//        ])->json('POST', 'api/picking-tasks');
//        $response->assertOk();
//        $response->assertJsonStructure([
//            'code',
//            'message',
//        ]);
//        $response->assertJson([
//            'code' => ResponseConst::CODE_FAIL,
//        ]);
//
//        // 生成必须元素
//        $warehouse = factory(Warehouse::class)->create();
//        // 库位
//        $storageLocation = factory(StorageLocation::class)->create([
//            'warehouse_id' => $warehouse->id,
//        ]);
//        $skuCode = 'SKU0001';
//        $quantity = 10;
//        // 库存
//        $stock = factory(Stock::class)->create([
//            'warehouse_id' => $warehouse->id,
//            'storage_type' => StorageArea::STORAGE_TYPE_PICKING,
//            'sku_code' => $skuCode,
//            'bin' => $storageLocation->storage_location_code,
//            'quantity' => $quantity,
//            'frozen_quantity' => $quantity,
//        ]);
//        // 出库单
//        $outbound = factory(Outbound::class)->create([
//            'warehouse_id' => $warehouse->id,
//            'is_need_intercept' => Outbound::IS_NEED_INTERCEPT_FALSE,
//            'state' => Outbound::STATE_FROZEN,
//        ]);
//        $outboundItem = factory(OutboundItem::class)->create([
//            'outbound_id' => $outbound->id,
//            'sku_code' => $skuCode,
//            'quantity' => $quantity,
//        ]);
//        factory(OutboundFrozenItem::class)->create([
//            'outbound_id' => $outbound->id,
//            'outbound_item_id' => $outboundItem->id,
//            'sku_code' => $skuCode,
//            'frozen_bin' => $storageLocation->storage_location_code,
//            'frozen_quantity' => $quantity,
//        ]);
//        // 生成拣货篮子
//        $basket = factory(Basket::class)->create([
//            'warehouse_id' => $warehouse->id,
//            'state' => Basket::STATE_FREE,
//        ]);
//
//        // 参数正确
//        $baskets = json_encode([['code' => $basket->basket_code]]);
//        $this->actingAs($user, 'api');
//        $response = $this->withHeaders([
//            'X-Requested-With' => 'XMLHttpRequest',
//            'warehouse-id' => $warehouse->id,
//        ])->json('POST', 'api/picking-tasks', [
//            'baskets' => $baskets,
//        ]);
//        $response->assertOk();
//
//        $response->assertJsonStructure([
//            'code',
//            'message',
//            'data' => [
//                'id',
//                'task_items' => [
//                    [
//                        'sku' => [
//                            'code',
//                        ],
//                        'basket' => [
//                            'code',
//                        ],
//                        'suggest_bin',
//                        'quantity',
//                        'id',
//                    ],
//                ],
//            ],
//        ]);
//
//        $response->assertJsonFragment([
//            'code' => ResponseConst::CODE_SUCCESS,
//            'message' => __('common.success'),
//        ]);
//
//        $response->assertJsonFragment([
//            'code' => $skuCode,
//            'quantity' => $quantity,
//        ]);
//        $response->assertJsonFragment([
//            'code' => $basket->basket_code,
//        ]);
//    }

    public function testStorePickingLog()
    {
        // 未登录
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->json('POST', 'api/picking-logs');

        $response->assertUnauthorized();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_UNAUTHORIZED,
        ]);

        // 模拟用户
        $user = factory(User::class)->create();

        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->json('POST', 'api/picking-logs');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_FAIL,
        ]);

        // 生成必须元素
        $warehouse = factory(Warehouse::class)->create();
        // 库位
        $storageLocation = factory(StorageLocation::class)->create([
            'warehouse_id' => $warehouse->id,
        ]);
        $skuCode = 'SKU0001';
        $quantity = 10;
        // 库存
        factory(Stock::class)->create([
            'warehouse_id' => $warehouse->id,
            'sku_code' => $skuCode,
            'quantity' => $quantity,
            'storage_type' => StorageArea::STORAGE_TYPE_PICKING,
            'bin' => $storageLocation->storage_location_code,
            'frozen_quantity' => $quantity,
            'unfrozen_quantity' => 0,
        ]);
        // 出库单
        $outbound = factory(Outbound::class)->create([
            'warehouse_id' => $warehouse->id,
            'is_need_intercept' => Outbound::IS_NEED_INTERCEPT_FALSE,
            'state' => Outbound::STATE_FROZEN,
        ]);
        $outboundItem = factory(OutboundItem::class)->create([
            'outbound_id' => $outbound->id,
            'sku_code' => $skuCode,
            'quantity' => $quantity,
            'is_frozen' => OutboundItem::IS_FROZEN_TRUE,
        ]);
        factory(OutboundFrozenItem::class)->create([
            'outbound_id' => $outbound->id,
            'outbound_item_id' => $outboundItem->id,
            'sku_code' => $skuCode,
            'frozen_bin' => $storageLocation->storage_location_code,
            'frozen_quantity' => $quantity,
        ]);
        // 生成拣货篮子
        $basket = factory(Basket::class)->create([
            'warehouse_id' => $warehouse->id,
            'state' => Basket::STATE_BUSY,
        ]);

        // 生成拣货任务
        $pickingTask = factory(PickingTask::class)->create([
            'warehouse_id' => $warehouse->id,
            'user_id' => $user->id,
            'task_type' => PickingTask::TASK_TYPE_PDA,
            'state' => PickingTask::STATE_IN_PROGRESS,
        ]);
        $pickingTaskBasket = factory(PickingTaskBasket::class)->create([
            'picking_task_id' => $pickingTask->id,
            'outbound_number' => $outbound->outbound_number,
            'basket_code' => $basket->basket_code,
        ]);
        $pickingTaskItem = factory(PickingTaskItem::class)->create([
            'picking_task_basket_id' => $pickingTaskBasket->id,
            'sku_code' => $skuCode,
            'suggest_bin' => $storageLocation->storage_location_code,
            'quantity' => $quantity,
            'picking_quantity' => 0,
        ]);

        // 生成任务日志
        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
            'warehouse-id' => $warehouse->id,
        ])->json('POST', 'api/picking-logs', [
            'picking_task_item_id' => $pickingTaskItem->id,
            'picking_task_item_id' => $pickingTaskItem->id,
            'sku_code' => $skuCode,
            'quantity' => $quantity,
            'bin' => (string) $storageLocation->storage_location_code,
        ]);

        $response->assertOk();

        $response->assertJsonStructure([
            'code',
            'message',
        ]);

        $response->assertJsonFragment([
            'code' => ResponseConst::CODE_SUCCESS,
            'message' => __('common.success'),
        ]);
    }

//    public function testPickintTaskList()
//    {
//        // 未登录
//        $response = $this->withHeaders([
//            'X-Requested-With' => 'XMLHttpRequest',
//        ])->json('GET', 'api/picking-tasks');
//
//        $response->assertUnauthorized();
//        $response->assertJsonStructure([
//            'code',
//            'message',
//        ]);
//        $response->assertJson([
//            'code' => ResponseConst::CODE_UNAUTHORIZED,
//        ]);
//
//        // 模拟用户
//        $user = factory(User::class)->create();
//
//        $this->actingAs($user, 'api');
//        $response = $this->withHeaders([
//            'X-Requested-With' => 'XMLHttpRequest',
//        ])->json('GET', 'api/picking-tasks');
//        $response->assertOk();
//        $response->assertJsonStructure([
//            'code',
//            'message',
//        ]);
//        $response->assertJson([
//            'code' => ResponseConst::CODE_FAIL,
//        ]);
//
//        // 生成必须元素
//        $warehouse = factory(Warehouse::class)->create();
//        // 库位
//        $storageLocation = factory(StorageLocation::class)->create([
//            'warehouse_id' => $warehouse->id,
//        ]);
//        $skuCode = 'SKU0001';
//        $quantity = 10;
//        // 库存
//        $stock = factory(Stock::class)->create([
//            'warehouse_id' => $warehouse->id,
//            'sku_code' => $skuCode,
//            'bin' => $storageLocation->storage_location_code,
//            'quantity' => $quantity,
//        ]);
//        // 出库单
//        $outbound = factory(Outbound::class)->create([
//            'warehouse_id' => $warehouse->id,
//            'is_need_intercept' => Outbound::IS_NEED_INTERCEPT_FALSE,
//            'state' => Outbound::STATE_FROZEN,
//        ]);
//        $outboundItem = factory(OutboundItem::class)->create([
//            'outbound_id' => $outbound->id,
//            'sku_code' => $skuCode,
//            'quantity' => $quantity,
//        ]);
//        factory(OutboundFrozenItem::class)->create([
//            'outbound_id' => $outbound->id,
//            'outbound_item_id' => $outboundItem->id,
//            'sku_code' => $skuCode,
//            'frozen_bin' => $storageLocation->storage_location_code,
//            'frozen_quantity' => $quantity,
//        ]);
//        // 生成拣货篮子
//        $basket = factory(Basket::class)->create([
//            'warehouse_id' => $warehouse->id,
//            'state' => Basket::STATE_BUSY,
//        ]);
//
//        // 生成拣货任务
//        $pickingTask = factory(PickingTask::class)->create([
//            'warehouse_id' => $warehouse->id,
//            'user_id' => $user->id,
//            'state' => PickingTask::STATE_IN_PROGRESS,
//        ]);
//        $pickingTaskBasket = factory(PickingTaskBasket::class)->create([
//            'picking_task_id' => $pickingTask->id,
//            'outbound_number' => $outbound->outbound_number,
//            'basket_code' => $basket->basket_code,
//        ]);
//        $pickingTaskItem = factory(PickingTaskItem::class)->create([
//            'picking_task_basket_id' => $pickingTaskBasket->id,
//            'sku_code' => $skuCode,
//            'suggest_bin' => $storageLocation->storage_location_code,
//            'quantity' => $quantity,
//            'picking_quantity' => 0,
//        ]);
//
//        $this->actingAs($user, 'api');
//        $response = $this->withHeaders([
//            'X-Requested-With' => 'XMLHttpRequest',
//            'warehouse-id' => $warehouse->id,
//        ])->json('GET', 'api/picking-tasks');
//        $response->assertOk();
//        $response->assertJsonStructure([
//            'code',
//            'message',
//            'data' => [
//                'list' => [
//                    [
//                        'id',
//                        'task_number',
//                        'has_bind_basket',
//                        'created_at',
//                        'outbounds' => [
//                            [
//                                'id',
//                                'sku_quantity',
//                            ]
//                        ]
//                    ]
//                ]
//            ]
//        ]);
//
//        $response->assertJsonFragment([
//            'code' => ResponseConst::CODE_SUCCESS,
//            'message' => __('common.success'),
//        ]);
//    }
//
//    public function testPickingBasketAvailable()
//    {
//        // 未登录
//        $response = $this->withHeaders([
//            'X-Requested-With' => 'XMLHttpRequest',
//        ])->json('GET', 'api/picking-basket-available');
//
//        $response->assertUnauthorized();
//        $response->assertJsonStructure([
//            'code',
//            'message',
//        ]);
//        $response->assertJson([
//            'code' => ResponseConst::CODE_UNAUTHORIZED,
//        ]);
//
//        // 模拟用户
//        $user = factory(User::class)->create();
//
//        $this->actingAs($user, 'api');
//        $response = $this->withHeaders([
//            'X-Requested-With' => 'XMLHttpRequest',
//        ])->json('GET', 'api/picking-basket-available');
//        $response->assertOk();
//        $response->assertJsonStructure([
//            'code',
//            'message',
//        ]);
//        $response->assertJson([
//            'code' => ResponseConst::CODE_FAIL,
//        ]);
//
//        // 生成必须元素
//        $warehouse = factory(Warehouse::class)->create();
//        // 生成拣货篮子
//        $basket = factory(Basket::class)->create([
//            'warehouse_id' => $warehouse->id,
//            'state' => Basket::STATE_BUSY,
//        ]);
//
//        $this->actingAs($user, 'api');
//        $response = $this->withHeaders([
//            'X-Requested-With' => 'XMLHttpRequest',
//            'warehouse-id' => $warehouse->id,
//        ])->json('GET', 'api/picking-basket-available', [
//            'basket_code' => $basket->basket_code,
//        ]);
//
//        $response->assertOk();
//        $response->assertJsonStructure([
//            'code',
//            'message',
//        ]);
//
//        $response->assertJsonFragment([
//            'code' => ResponseConst::CODE_FAIL,
//        ]);
//
//        // 生成拣货篮子
//        $basket = factory(Basket::class)->create([
//            'warehouse_id' => $warehouse->id,
//            'state' => Basket::STATE_FREE,
//        ]);
//
//        $this->actingAs($user, 'api');
//        $response = $this->withHeaders([
//            'X-Requested-With' => 'XMLHttpRequest',
//            'warehouse-id' => $warehouse->id,
//        ])->json('GET', 'api/picking-basket-available', [
//            'basket_code' => $basket->basket_code,
//        ]);
//
//        $response->assertOk();
//        $response->assertJsonStructure([
//            'code',
//            'message',
//        ]);
//
//        $response->assertJsonFragment([
//            'code' => ResponseConst::CODE_SUCCESS,
//            'message' => __('common.success'),
//        ]);
//    }
//
//    public function testUpdatePickingTaskBaskets()
//    {
//        // 模拟用户
//        $user = factory(User::class)->create();
//        // 生成必须元素
//        $warehouse = factory(Warehouse::class)->create();
//        // 库位
//        $storageLocation = factory(StorageLocation::class)->create([
//            'warehouse_id' => $warehouse->id,
//        ]);
//        $skuCode = 'SKU0001';
//        $quantity = 10;
//        // 库存
//        $stock = factory(Stock::class)->create([
//            'warehouse_id' => $warehouse->id,
//            'sku_code' => $skuCode,
//            'bin' => $storageLocation->storage_location_code,
//            'quantity' => $quantity,
//        ]);
//        // 出库单
//        $outbound = factory(Outbound::class)->create([
//            'warehouse_id' => $warehouse->id,
//            'is_need_intercept' => Outbound::IS_NEED_INTERCEPT_FALSE,
//            'state' => Outbound::STATE_FROZEN,
//        ]);
//        $outboundItem = factory(OutboundItem::class)->create([
//            'outbound_id' => $outbound->id,
//            'sku_code' => $skuCode,
//            'quantity' => $quantity,
//        ]);
//        factory(OutboundFrozenItem::class)->create([
//            'outbound_id' => $outbound->id,
//            'outbound_item_id' => $outboundItem->id,
//            'sku_code' => $skuCode,
//            'frozen_bin' => $storageLocation->storage_location_code,
//            'frozen_quantity' => $quantity,
//        ]);
//        // 生成拣货篮子
//        $basket = factory(Basket::class)->create([
//            'warehouse_id' => $warehouse->id,
//            'state' => Basket::STATE_FREE,
//        ]);
//
//        // 生成拣货任务
//        $pickingTask = factory(PickingTask::class)->create([
//            'warehouse_id' => $warehouse->id,
//            'user_id' => $user->id,
//        ]);
//        $pickingTaskBasket = factory(PickingTaskBasket::class)->create([
//            'picking_task_id' => $pickingTask->id,
//            'basket_code' => '',
//            'outbound_number' => $outbound->outbound_number,
//        ]);
//        $pickingTaskItem = factory(PickingTaskItem::class)->create([
//            'picking_task_basket_id' => $pickingTaskBasket->id,
//            'sku_code' => $skuCode,
//            'suggest_bin' => $storageLocation->storage_location_code,
//            'quantity' => $quantity,
//            'picking_quantity' => 0,
//        ]);
//
//        // 未登录
//        $response = $this->withHeaders([
//            'X-Requested-With' => 'XMLHttpRequest',
//        ])->json('PUT', 'api/picking-tasks/' . $pickingTask->id . '/baskets');
//
//        $response->assertUnauthorized();
//        $response->assertJsonStructure([
//            'code',
//            'message',
//        ]);
//        $response->assertJson([
//            'code' => ResponseConst::CODE_UNAUTHORIZED,
//        ]);
//
//        $this->actingAs($user, 'api');
//        $response = $this->withHeaders([
//            'X-Requested-With' => 'XMLHttpRequest',
//        ])->json('PUT', 'api/picking-tasks/' . $pickingTask->id . '/baskets');
//        $response->assertOk();
//        $response->assertJsonStructure([
//            'code',
//            'message',
//        ]);
//        $response->assertJson([
//            'code' => ResponseConst::CODE_FAIL,
//        ]);
//
//        $baskets = json_encode([
//            [
//                'code' => $basket->basket_code,
//                'outbound_id' => $outbound->id,
//            ]
//        ]);
//        $this->actingAs($user, 'api');
//        $response = $this->withHeaders([
//            'X-Requested-With' => 'XMLHttpRequest',
//            'warehouse-id' => $warehouse->id,
//        ])->json('PUT', 'api/picking-tasks/' . $pickingTask->id . '/baskets', [
//            'baskets' => $baskets,
//        ]);
//
//        $response->assertOk();
//
//        $response->assertJsonStructure([
//            'code',
//            'message',
//            'data' => [
//                'task_items' => [
//                    [
//                        'suggest_bin',
//                        'sku' => [
//                            'code',
//                        ],
//                        'basket' => [
//                            'code',
//                        ],
//                        'quantity',
//                        'id',
//                    ]
//                ]
//            ]
//        ]);
//
//        $response->assertJsonFragment([
//            'code' => ResponseConst::CODE_SUCCESS,
//            'message' => __('common.success'),
//        ]);
//    }
//
//    public function testPickingTaskShow()
//    {
//        // 模拟用户
//        $user = factory(User::class)->create();
//        // 生成必须元素
//        $warehouse = factory(Warehouse::class)->create();
//        // 库位
//        $storageLocation = factory(StorageLocation::class)->create([
//            'warehouse_id' => $warehouse->id,
//        ]);
//        $skuCode = 'SKU0001';
//        $quantity = 10;
//        // 库存
//        $stock = factory(Stock::class)->create([
//            'warehouse_id' => $warehouse->id,
//            'sku_code' => $skuCode,
//            'bin' => $storageLocation->storage_location_code,
//            'quantity' => $quantity,
//        ]);
//        // 出库单
//        $outbound = factory(Outbound::class)->create([
//            'warehouse_id' => $warehouse->id,
//            'is_need_intercept' => Outbound::IS_NEED_INTERCEPT_FALSE,
//            'state' => Outbound::STATE_FROZEN,
//        ]);
//        $outboundItem = factory(OutboundItem::class)->create([
//            'outbound_id' => $outbound->id,
//            'sku_code' => $skuCode,
//            'quantity' => $quantity,
//        ]);
//        factory(OutboundFrozenItem::class)->create([
//            'outbound_id' => $outbound->id,
//            'outbound_item_id' => $outboundItem->id,
//            'sku_code' => $skuCode,
//            'frozen_bin' => $storageLocation->storage_location_code,
//            'frozen_quantity' => $quantity,
//        ]);
//        // 生成拣货篮子
//        $basket = factory(Basket::class)->create([
//            'warehouse_id' => $warehouse->id,
//            'state' => Basket::STATE_FREE,
//        ]);
//
//        // 生成拣货任务
//        $pickingTask = factory(PickingTask::class)->create([
//            'warehouse_id' => $warehouse->id,
//            'user_id' => $user->id,
//        ]);
//        $pickingTaskBasket = factory(PickingTaskBasket::class)->create([
//            'picking_task_id' => $pickingTask->id,
//            'basket_code' => '',
//            'outbound_number' => $outbound->outbound_number,
//        ]);
//        $pickingTaskItem = factory(PickingTaskItem::class)->create([
//            'picking_task_basket_id' => $pickingTaskBasket->id,
//            'sku_code' => $skuCode,
//            'suggest_bin' => $storageLocation->storage_location_code,
//            'quantity' => $quantity,
//            'picking_quantity' => 0,
//        ]);
//
//        // 未登录
//        $response = $this->withHeaders([
//            'X-Requested-With' => 'XMLHttpRequest',
//        ])->json('GET', 'api/picking-tasks/' . $pickingTask->id);
//
//        $response->assertUnauthorized();
//        $response->assertJsonStructure([
//            'code',
//            'message',
//        ]);
//        $response->assertJson([
//            'code' => ResponseConst::CODE_UNAUTHORIZED,
//        ]);
//
//        $this->actingAs($user, 'api');
//        $response = $this->withHeaders([
//            'X-Requested-With' => 'XMLHttpRequest',
//        ])->json('GET', 'api/picking-tasks/' . $pickingTask->id);
//        $response->assertOk();
//        $response->assertJsonStructure([
//            'code',
//            'message',
//        ]);
//        $response->assertJson([
//            'code' => ResponseConst::CODE_FAIL,
//        ]);
//
//
//        $this->actingAs($user, 'api');
//        $response = $this->withHeaders([
//            'X-Requested-With' => 'XMLHttpRequest',
//            'warehouse-id' => $warehouse->id,
//        ])->json('GET', 'api/picking-tasks/' . $pickingTask->id);
//
//        $response->assertOk();
//
//        $response->assertJsonStructure([
//            'code',
//            'message',
//            'data' => [
//                'task_items' => [
//                    [
//                        'suggest_bin',
//                        'sku' => [
//                            'code',
//                        ],
//                        'basket' => [
//                            'code',
//                        ],
//                        'quantity',
//                        'id',
//                    ]
//                ]
//            ]
//        ]);
//
//        $response->assertJsonFragment([
//            'code' => ResponseConst::CODE_SUCCESS,
//            'message' => __('common.success'),
//        ]);
//    }
}
