<?php

namespace Tests\Feature\Api\Version;

use App\Consts\ResponseConst;
use App\Models\AppVersion;
use Tests\Feature\BaseFeatureTestCase;

class IndexTest extends BaseFeatureTestCase
{
    public function testIndex()
    {
        // 未登录
        $response = $this->header()->json('GET', 'api/versions');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_FAIL,
        ]);

        $appVersion = factory(AppVersion::class)->create([
            'version' => '2.0',
            'platform' => AppVersion::PLATFORM_ANDROID,
        ]);
        $querys = [
            'platform' => 'android',
            'version' => '1.0',
        ];
        $response = $this->login()->json('GET', 'api/versions?' . http_build_query($querys));
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'versions' => [
                    [
                        'version',
                        'download_url',
                        'message',
                        'platform',
                        'force',
                    ]
                ],
            ]
        ]);

        $response->assertJsonFragment([
            'version' => $appVersion->version,
        ]);
    }
}
