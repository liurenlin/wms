<?php

namespace Tests\Feature;

use App\Consts\ResponseConst;
use App\Models\Abnormal;
use App\Models\ActualReceiving;
use App\Models\PreReceiving;
use App\Models\ProductSku;
use App\Models\Warehouse;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AbnormalTest extends TestCase
{
    use DatabaseTransactions;

    public function testStoreAbnormalCode()
    {
        // 未登录
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->json('POST', 'api/abnormal-codes');

        $response->assertUnauthorized();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_UNAUTHORIZED,
        ]);

        // 模拟用户
        $user = factory(User::class)->create();

        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->json('POST', 'api/abnormal-codes');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_FAIL,
        ]);

        // 生成必须元素
        $warehouse = factory(Warehouse::class)->create();
        // 参数正确
        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
            'warehouse-id' => $warehouse->id,
        ])->json('POST', 'api/abnormal-codes');
        $response->assertOk();

        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'code',
            ],
        ]);
    }

    public function testStore()
    {
        // 未登录
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->json('POST', 'api/abnormals');

        $response->assertUnauthorized();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_UNAUTHORIZED,
        ]);

        // 模拟用户
        $user = factory(User::class)->create();

        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->json('POST', 'api/abnormals');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_FAIL,
        ]);

        // 生成必须元素
        $quantity = 1;
        $image = 'https://signature.png';
        $warehouse = factory(Warehouse::class)->create();
        $sku = factory(ProductSku::class)->create();

        $relationType = Abnormal::RELATION_TYPE_ACTUAL_RECEIVING;
        // 生成预录入库单
        $preReceiving = factory(PreReceiving::class)->create([
            'warehouse_id' => $warehouse->id,
        ]);

        $actualReceiving = factory(ActualReceiving::class)->create([
            'warehouse_id' => $warehouse->id,
            'pre_receiving_id' => $preReceiving->id,
        ]);
        $relationNumber = $actualReceiving->actual_receiving_number;

        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
            'warehouse-id' => $warehouse->id,
        ])->json('POST', 'api/abnormals', [
            'relation_type' => $relationType,
            'relation_number' => $relationNumber,
            'sku_code' => $sku->sku_code,
            'quantity' => $quantity,
            'image' => $image,
        ]);

        $response->assertOk();

        $response->assertJsonStructure([
            'code',
            'message',
        ]);

        $response->assertJsonFragment([
            'code' => ResponseConst::CODE_SUCCESS,
            'message' => __('common.success'),
        ]);
    }
}
