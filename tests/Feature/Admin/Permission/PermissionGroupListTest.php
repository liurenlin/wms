<?php

namespace Tests\Feature\Admin\Role;

use App\Models\PermissionGroup;
use App\Models\Role;
use Tests\Feature\BaseFeatureTestCase;

class PermissionGroupListTest extends BaseFeatureTestCase
{
    public function testPermissionGroupList()
    {
        $parentPermissionGroup = factory(PermissionGroup::class)->create([
            'parent_id' => 0,
        ]);
        $permissionGroup = factory(PermissionGroup::class)->create([
            'parent_id' => $parentPermissionGroup->id,
        ]);
        $response = $this->header()->json('GET', 'admin/api/permission-groups');
        $role = factory(Role::class)->create();
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'permission_groups' => [
                    [
                        'id',
                        'name',
                        'permission_groups' => [
                            [
                                'id',
                                'name',
                            ]

                        ]
                    ]
                ],
            ]
        ]);

        $response->assertJsonFragment([
            'id' => $parentPermissionGroup->id,
            'name' => $parentPermissionGroup->group_name,
        ]);

        $response->assertJsonFragment([
            'id' => $permissionGroup->id,
            'name' => $permissionGroup->group_name,
        ]);
    }
}
