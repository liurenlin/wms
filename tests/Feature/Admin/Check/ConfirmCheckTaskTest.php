<?php

namespace Tests\Feature\Admin\Check;

use App\Consts\ResponseConst;
use App\Models\Basket;
use App\Models\CheckTask;
use App\Models\CheckTaskItem;
use App\Models\Outbound;
use App\Models\OutboundFrozenItem;
use App\Models\OutboundItem;
use App\Models\PickingTask;
use App\Models\PickingTaskBasket;
use App\Models\PickingTaskItem;
use App\Models\ProductSku;
use App\Models\Stock;
use App\Models\StorageArea;
use App\Models\StorageLocation;
use App\Models\Warehouse;
use App\User;
use Tests\Feature\BaseFeatureTestCase;

class ConfirmCheckTaskTest extends BaseFeatureTestCase
{
    public function testConfirmCheckTask()
    {
        // 未登录
        $response = $this->header()->json('PUT', 'admin/api/check-tasks/1');

        $response->assertUnauthorized();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_UNAUTHORIZED,
        ]);

        // 生成必须元素
        $warehouse = factory(Warehouse::class)->create();
        // 库位
        $storageLocation = factory(StorageLocation::class)->create([
            'warehouse_id' => $warehouse->id,
        ]);
        $skuCode = 'SKU0001';
        $quantity = 10;
        $state = 0;
        // 出库单
        $outbound = factory(Outbound::class)->create([
            'warehouse_id' => $warehouse->id,
            'outbound_number' => $skuCode,
            'is_need_intercept' => Outbound::IS_NEED_INTERCEPT_FALSE,
            'state' => Outbound::STATE_FROZEN,
        ]);
        // 出库单子项
        $outboundItem = factory(OutboundItem::class)->create([
            'outbound_id' => $outbound->id,
            'sku_code' => $skuCode,
            'quantity' => $quantity,
        ]);
        factory(OutboundFrozenItem::class)->create([
            'outbound_id' => $outbound->id,
            'outbound_item_id' => $outboundItem->id,
            'sku_code' => $skuCode,
            'frozen_bin' => $storageLocation->storage_location_code,
            'frozen_quantity' => $quantity,
        ]);
        // 生成拣货篮子
        $basket = factory(Basket::class)->create([
            'warehouse_id' => $warehouse->id,
            'state' => Basket::STATE_BUSY,
        ]);

        $this->login();

        // 生成拣货任务
        $pickingTask = factory(PickingTask::class)->create([
            'warehouse_id' => $warehouse->id,
            'user_id' => $this->user->id,
        ]);
        $pickingTaskBasket = factory(PickingTaskBasket::class)->create([
            'picking_task_id' => $pickingTask->id,
            'outbound_number' => $outbound->outbound_number,
            'basket_code' => $basket->basket_code,
        ]);
        // 生成任务 item
        $pickingTaskItem = factory(PickingTaskItem::class)->create([
            'picking_task_basket_id' => $pickingTaskBasket->id,
            'sku_code' => $skuCode,
            'quantity' => $quantity,
            'picking_quantity' => $quantity,
            'state' => 1,
        ]);
        factory(Stock::class)->create([
            'warehouse_id' => $warehouse->id,
            'storage_type' => StorageArea::STORAGE_TYPE_PACKAGE,
            'bin' => '',
            'sku_code' => $skuCode,
            'quantity' => $quantity,
        ]);
        // 生成复核任务
        $checkTask = factory(CheckTask::class)->create([
            'warehouse_id' => $warehouse->id,
            'picking_task_id' => $pickingTask->id,
            'outbound_number' => $outbound->outbound_number,
            'basket_code' => $pickingTaskBasket->basket_code,
            'user_id' => $this->user->id,
            'state' => CheckTask::STATE_CREATED,
        ]);
        // 生成复核任务 item
        $checkTaskItem = factory(CheckTaskItem::class)->create([
            'check_task_id' => $checkTask->id,
            'sku_code' => $skuCode,
            'quantity' => $quantity,
            'check_quantity' => 0,
            'state' => CheckTaskItem::STATE_DEFAULT
        ]);

        $this->header([
            'warehouse_id' => $warehouse->id,
        ]);

        // 登录
        $response = $this->json('PUT', 'admin/api/check-tasks/' . $checkTask->id);
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_FAIL,
        ]);

        // 任务 confirm
        $confirmState = CheckTask::STATE_NORMAL;
        $response = $this->json('PUT', 'admin/api/check-tasks/' . $checkTask->id, [
            'state' => $confirmState,
            'skus' => json_encode([
                [
                    'code' => $skuCode,
                    'quantity' => $quantity,
                ],
            ])
        ]);

        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);

        $response->assertJsonFragment([
            'code' => ResponseConst::CODE_SUCCESS,
            'message' => __('common.success'),
        ]);

        // 测试异常
        $response = $this->json('PUT', 'admin/api/check-tasks/' . $checkTask->id, [
            'state' => $confirmState,
            'skus' => json_encode([
                [
                    'code' => $skuCode . 'X',
                    'quantity' => $quantity,
                ],
            ])
        ]);

        $response->assertOk();

        $response->assertJsonStructure([
            'code',
            'message',
        ]);

        $response->assertJsonFragment([
            'code' => ResponseConst::CODE_FAIL,
            'message' => __('check.wrong_sku'),
        ]);
    }
}
