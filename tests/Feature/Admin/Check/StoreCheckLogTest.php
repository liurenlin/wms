<?php

namespace Tests\Feature\Admin\Check;

use App\Consts\ResponseConst;
use App\Models\Basket;
use App\Models\CheckTask;
use App\Models\CheckTaskItem;
use App\Models\Outbound;
use App\Models\OutboundFrozenItem;
use App\Models\OutboundItem;
use App\Models\PickingTask;
use App\Models\PickingTaskBasket;
use App\Models\PickingTaskItem;
use App\Models\ProductSku;
use App\Models\Stock;
use App\Models\StorageArea;
use App\Models\StorageLocation;
use App\Models\Warehouse;
use App\User;
use Tests\Feature\BaseFeatureTestCase;

class StoreCheckLogTest extends BaseFeatureTestCase
{
    public function testStoreCheckLog()
    {
        // 未登录
        $response = $this->header()->json('POST', 'admin/api/check-logs');

        $response->assertUnauthorized();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_UNAUTHORIZED,
        ]);
        // 生成必须元素
        $warehouse = factory(Warehouse::class)->create();

        // 模拟用户
        $user = factory(User::class)->create();
        $this->header([
            'warehouse_id' => $warehouse->id,
        ]);

        $response = $this->login()->json('POST', 'admin/api/check-logs');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $skuCode = 'SKU0001';
        $wrongSkuCode = $skuCode . 'X';
        $response->assertJson([
            'code' => ResponseConst::CODE_FAIL,
        ]);

        $productSku = factory(ProductSku::class)->create([
            'sku_code' => $skuCode,
        ]);
        $WrongProductSku = factory(ProductSku::class)->create([
            'sku_code' => $wrongSkuCode
        ]);

        // 库位
        $storageLocation = factory(StorageLocation::class)->create([
            'warehouse_id' => $warehouse->id,
        ]);

        $quantity = 10;
        // 出库单
        $outbound = factory(Outbound::class)->create([
            'warehouse_id' => $warehouse->id,
            'outbound_number' => $skuCode,
            'is_need_intercept' => Outbound::IS_NEED_INTERCEPT_FALSE,
            'state' => Outbound::STATE_PICKING_COMPLETED,
        ]);
        // 出库单子项
        $outboundItem = factory(OutboundItem::class)->create([
            'outbound_id' => $outbound->id,
            'sku_code' => $skuCode,
            'quantity' => $quantity,
        ]);
        factory(OutboundFrozenItem::class)->create([
            'outbound_id' => $outbound->id,
            'outbound_item_id' => $outboundItem->id,
            'sku_code' => $skuCode,
            'frozen_bin' => $storageLocation->storage_location_code,
            'frozen_quantity' => $quantity,
        ]);
        // 生成拣货篮子
        $basket = factory(Basket::class)->create([
            'warehouse_id' => $warehouse->id,
            'state' => Basket::STATE_BUSY,
            'basket_code' => 'BAS-' . time(),
        ]);

        // 生成拣货任务
        $pickingTask = factory(PickingTask::class)->create([
            'warehouse_id' => $warehouse->id,
            'user_id' => $user->id,
        ]);
        $pickingTaskBasket = factory(PickingTaskBasket::class)->create([
            'picking_task_id' => $pickingTask->id,
            'outbound_number' => $outbound->outbound_number,
            'basket_code' => $basket->basket_code,
        ]);
        // 生成任务 item
        $pickingTaskItem = factory(PickingTaskItem::class)->create([
            'picking_task_basket_id' => $pickingTaskBasket->id,
            'sku_code' => $skuCode,
            'quantity' => $quantity,
            'picking_quantity' => $quantity,
            'state' => 1,
        ]);
        factory(Stock::class)->create([
            'warehouse_id' => $warehouse->id,
            'storage_type' => StorageArea::STORAGE_TYPE_PACKAGE,
            'bin' => '',
            'sku_code' => $skuCode,
            'quantity' => $quantity,
        ]);
        // 生成复核任务
        $checkTask = factory(CheckTask::class)->create([
            'warehouse_id' => $warehouse->id,
            'picking_task_id' => $pickingTask->id,
            'basket_code' => $pickingTaskBasket->basket_code,
            'user_id' => $user->id,
            'state' => CheckTask::STATE_CREATED,
        ]);
        // 生成复核任务 item
        $checkTaskItem = factory(CheckTaskItem::class)->create([
            'check_task_id' => $checkTask->id,
            'sku_code' => $skuCode,
            'quantity' => $quantity,
        ]);
        // 生成任务日志
        $response = $this->login()->json('POST', 'admin/api/check-logs', [
            'check_task_id' => $checkTask->id,
            'sku_code' => $skuCode,
            'quantity' => $quantity,
        ]);
        $response->assertOk();

        $response->assertJsonStructure([
            'code',
            'message',
        ]);

        $response->assertJsonFragment([
            'code' => ResponseConst::CODE_SUCCESS,
            'message' => __('common.success'),
        ]);

        // 生成任务日志
        $response = $this->login()->json('POST', 'admin/api/check-logs', [
            'check_task_id' => $checkTask->id,
            'sku_code' => $wrongSkuCode,
            'quantity' => $quantity,
        ]);

        $response->assertOk();

        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJsonFragment([
            'code' => ResponseConst::CODE_FAIL,
            'message' => __('check.wrong_sku'),
        ]);
    }
}
