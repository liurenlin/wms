<?php

namespace Tests\Feature\Admin\User;

use App\Consts\ResponseConst;
use App\Models\PermissionGroup;
use App\Models\Role;
use App\Models\RolePermissionGroup;
use App\Models\UserRole;
use App\User;
use Tests\Feature\BaseFeatureTestCase;

class UserListTest extends BaseFeatureTestCase
{
    public function testUserList()
    {
        // 未登录
        $response = $this->header()->json('GET', 'admin/api/users');
        $response->assertUnauthorized();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_UNAUTHORIZED,
        ]);

        $user = factory(User::class)->create();
        $role = factory(Role::class)->create();
        $parentPermissionGroup = factory(PermissionGroup::class)->create([
            'parent_id' => 0,
        ]);
        $permissionGroup = factory(PermissionGroup::class)->create([
            'parent_id' => $parentPermissionGroup->id,
        ]);
        $rolePermissionGroup = factory(RolePermissionGroup::class)->create([
            'role_id' => $role->id,
            'permission_group_id' => $permissionGroup->id,
        ]);

        factory(UserRole::class)->create([
            'warehouse_id' => 1,
            'user_id' => $user->id,
            'role_id' => $role->id,
        ]);

        $response = $this->login()->json('GET', 'admin/api/users');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'users' => [
                    [
                        'id',
                        'name',
                        'roles' => [
                            [
                                'id',
                                'name',
                            ]

                        ]
                    ]
                ],
            ]
        ]);

        $response->assertJsonFragment([
            'id' => $user->id,
            'name' => $user->name,
        ]);

        $response->assertJsonFragment([
            'id' => $role->id,
            'name' => $role->role_name,
        ]);

    }
}
