<?php

namespace Tests\Feature\Admin\User;

use App\Consts\ResponseConst;
use App\Models\PermissionGroup;
use App\Models\Role;
use App\Models\RolePermissionGroup;
use App\Models\UserRole;
use App\Models\Warehouse;
use App\User;
use Faker\Generator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Tests\Feature\BaseFeatureTestCase;

class LoginTest extends BaseFeatureTestCase
{
    public function testLogin()
    {
        $faker = app(Generator::class);
        $name = $faker->name;
        $password = $faker->password;

        $response = $this->json('POST', 'admin/api/login', [
            'name' => $name,
            'password' => $password,
        ]);
        $response->assertOk();

        $response->assertJson([
            'code' => ResponseConst::CODE_FAIL,
        ]);

        $warehouse = factory(Warehouse::class)->create();

        $user = factory(User::class)->create([
            'name' => $name,
            'password' => Hash::make($password),
        ]);
        $role = factory(Role::class)->create();
        $parentPermissionGroup = factory(PermissionGroup::class)->create([
            'parent_id' => 0,
        ]);
        $permissionGroup = factory(PermissionGroup::class)->create([
            'parent_id' => $parentPermissionGroup->id,
        ]);
        $rolePermissionGroup = factory(RolePermissionGroup::class)->create([
            'role_id' => $role->id,
            'permission_group_id' => $permissionGroup->id,
        ]);

        factory(UserRole::class)->create([
            'warehouse_id' => $warehouse->id,
            'user_id' => $user->id,
            'role_id' => $role->id,
        ]);
        $response = $this->json('POST', 'admin/api/login', [
            'name' => $name,
            'password' => $password,
        ]);
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'id',
                'name',
                'email',
                'token',
                'menus',
                'permission_groups' => [
                    [
                        'id',
                        'name',
                    ]
                ],
            ],
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_SUCCESS,
            'message' => __('common.success'),
        ]);

        $response->assertJsonFragment([
            'id' => $permissionGroup->id,
            'name' => $permissionGroup->group_name,
        ]);
    }
}
