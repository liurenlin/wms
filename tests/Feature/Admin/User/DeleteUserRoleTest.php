<?php

namespace Tests\Feature\Admin\User;

use App\Consts\ResponseConst;
use App\Models\Role;
use App\Models\UserRole;
use App\User;
use Tests\Feature\BaseFeatureTestCase;

class DeleteUserRoleTest extends BaseFeatureTestCase
{
    public function testDeleteUserRole()
    {
        // 未登录
        $user = factory(User::class)->create();
        $role = factory(Role::class)->create();
        factory(UserRole::class)->create([
            'user_id' => $user->id,
            'role_id' => $role->id,
        ]);
        $response = $this->header()->json('DELETE', 'admin/api/users/' . $user->id . '/roles/' . $role->id);
        $response->assertUnauthorized();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_UNAUTHORIZED,
        ]);

        $response = $this->login()->json('DELETE', 'admin/api/users/' . $user->id . '/roles/' . $role->id);
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);

        $exists = UserRole::query()
            ->where('user_id', $user->id)
            ->where('role_id', $role->id)
            ->exists();
        $this->assertEquals(false, $exists);
    }
}
