<?php

namespace Tests\Feature\Admin\User;

use App\Consts\ResponseConst;
use App\User;
use Illuminate\Support\Str;
use Tests\Feature\BaseFeatureTestCase;

class StoreUserTest extends BaseFeatureTestCase
{
    public function testStoreUser()
    {
        // 未登录
        $response = $this->header()->json('POST', 'admin/api/users');
        $response->assertUnauthorized();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_UNAUTHORIZED,
        ]);

        $userName = Str::random(10);
        $password = Str::random(10);

        $response = $this->login()->json('POST', 'admin/api/users', [
            'name' => $userName,
            'password' => $password,
        ]);
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'id',
            ]
        ]);
        $lastUser = User::query()->latest('id')->first();
        $response->assertJsonFragment([
            'id' => $lastUser->id,
        ]);
    }
}
