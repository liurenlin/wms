<?php

namespace Tests\Feature\Admin\User;

use App\Consts\ResponseConst;
use App\Models\Role;
use App\Models\UserRole;
use App\Models\Warehouse;
use App\User;
use Tests\Feature\BaseFeatureTestCase;

class UpdateUserTest extends BaseFeatureTestCase
{
    public function testUpdateUser()
    {
        // 未登录
        $user = factory(User::class)->create();
        $response = $this->header()->json('PUT', 'admin/api/users/' . $user->id);
        $response->assertUnauthorized();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_UNAUTHORIZED,
        ]);


        $warehouse = factory(Warehouse::class)->create();
        $role = factory(Role::class)->create();

        $this->header([
            'warehouse_id' => $warehouse->id,
        ]);

        $response = $this->login()->json('PUT', 'admin/api/users/' . $user->id, [
            'roles' => json_encode([[
                'id' => $role->id,
            ]]),
        ]);
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);

        $exists = UserRole::query()
            ->where('user_id', $user->id)
            ->where('role_id', $role->id)
            ->exists();
        $this->assertEquals(true, $exists);
    }
}
