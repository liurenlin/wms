<?php

namespace Tests\Feature\Admin;

use App\Consts\ResponseConst;
use App\Models\Basket;
use App\Models\CheckTask;
use App\Models\CheckTaskItem;
use App\Models\Outbound;
use App\Models\OutboundFrozenItem;
use App\Models\OutboundItem;
use App\Models\PickingTask;
use App\Models\PickingTaskBasket;
use App\Models\PickingTaskItem;
use App\Models\Stock;
use App\Models\StorageArea;
use App\Models\StorageLocation;
use App\Models\Warehouse;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class CheckTest extends TestCase
{
    use DatabaseTransactions;

    public function testStoreCheckTask()
    {
        // 未登录
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->json('POST', 'admin/api/check-tasks');

        $response->assertUnauthorized();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_UNAUTHORIZED,
        ]);

        // 模拟用户
        $user = factory(User::class)->create();

        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->json('POST', 'admin/api/check-tasks');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_FAIL,
        ]);

        // 生成必须元素
        $warehouse = factory(Warehouse::class)->create();
        // 库位
        $storageLocation = factory(StorageLocation::class)->create([
            'warehouse_id' => $warehouse->id,
        ]);
        $outboundNumber = 'OUT0001';
        $skuCode = 'SKU0001';
        $quantity = 10;
        // 出库单
        $outbound = factory(Outbound::class)->create([
            'warehouse_id' => $warehouse->id,
            'outbound_number' => $outboundNumber,
            'is_need_intercept' => Outbound::IS_NEED_INTERCEPT_FALSE,
            'state' => Outbound::STATE_PICKING_COMPLETED,
        ]);
        // 出库单子项
        $outboundItem = factory(OutboundItem::class)->create([
            'outbound_id' => $outbound->id,
            'sku_code' => $skuCode,
            'quantity' => $quantity,
        ]);
        factory(OutboundFrozenItem::class)->create([
            'outbound_id' => $outbound->id,
            'outbound_item_id' => $outboundItem->id,
            'sku_code' => $skuCode,
            'frozen_bin' => $storageLocation->storage_location_code,
            'frozen_quantity' => $quantity,
        ]);
        // 生成拣货篮子
        $basket = factory(Basket::class)->create([
            'warehouse_id' => $warehouse->id,
            'state' => Basket::STATE_BUSY,
            'basket_code' => 'BAS-' . time(),
        ]);

        // 生成拣货任务
        $pickingTask = factory(PickingTask::class)->create([
            'warehouse_id' => $warehouse->id,
            'user_id' => $user->id,
        ]);
        $pickingTaskBasket = factory(PickingTaskBasket::class)->create([
            'picking_task_id' => $pickingTask->id,
            'outbound_number' => $outbound->outbound_number,
            'basket_code' => $basket->basket_code,
        ]);
        // 生成任务 item
        $pickingTaskItem = factory(PickingTaskItem::class)->create([
            'picking_task_basket_id' => $pickingTaskBasket->id,
            'sku_code' => $skuCode,
            'quantity' => $quantity,
            'picking_quantity' => $quantity,
            'state' => 1,
        ]);

        // 参数正确
        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
            'warehouse-id' => $warehouse->id,
        ])->json('POST', 'admin/api/check-tasks', [
            'basket_code' => $basket->basket_code,
        ]);
        $response->assertOk();

        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'id',
                'picking_task' => [
                    'id',
                ],
            ],
        ]);

        $response->assertJsonFragment([
            'code' => ResponseConst::CODE_SUCCESS,
            'message' => __('common.success'),
        ]);

        $response->assertJsonFragment([
            'id' => $pickingTask->id,
        ]);

    }

}
