<?php

namespace Tests\Feature\Admin;

use App\Consts\ResponseConst;
use App\Models\Basket;
use App\Models\CheckTask;
use App\Models\CheckTaskItem;
use App\Models\LogisticsCompany;
use App\Models\Outbound;
use App\Models\OutboundFrozenItem;
use App\Models\OutboundItem;
use App\Models\PickingTask;
use App\Models\PickingTaskBasket;
use App\Models\PickingTaskItem;
use App\Models\Stock;
use App\Models\StorageArea;
use App\Models\StorageLocation;
use App\Models\Warehouse;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class WeighTest extends TestCase
{
    use DatabaseTransactions;

    public function testCheckOutboundAvailable()
    {
        // 未登录
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->json('GET', 'admin/api/outbound-available');
        $response->assertUnauthorized();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_UNAUTHORIZED,
        ]);

        // 模拟用户
        $user = factory(User::class)->create();

        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->json('GET', 'admin/api/outbound-available');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_FAIL,
        ]);

        // 生成必须元素
        $warehouse = factory(Warehouse::class)->create();
        // 库位
        $storageLocation = factory(StorageLocation::class)->create([
            'warehouse_id' => $warehouse->id,
        ]);
        $outboundNumber = 'OUT0001';
        $skuCode = 'SKU0001';
        $quantity = 10;
        $state = 1;
        // 出库单
        $outbound = factory(Outbound::class)->create([
            'warehouse_id' => $warehouse->id,
            'outbound_number' => $outboundNumber,
            'is_need_intercept' => Outbound::IS_NEED_INTERCEPT_FALSE,
            'state' => Outbound::STATE_CHECK_COMPLETED,
        ]);
        // 出库单子项
        $outboundItem = factory(OutboundItem::class)->create([
            'outbound_id' => $outbound->id,
            'sku_code' => $skuCode,
            'quantity' => $quantity,
        ]);
        factory(OutboundFrozenItem::class)->create([
            'outbound_id' => $outbound->id,
            'outbound_item_id' => $outboundItem->id,
            'sku_code' => $skuCode,
            'frozen_bin' => $storageLocation->storage_location_code,
            'frozen_quantity' => $quantity,
        ]);
        // 生成拣货篮子
        $basket = factory(Basket::class)->create([
            'warehouse_id' => $warehouse->id,
            'state' => Basket::STATE_BUSY,
            'basket_code' => 'BAS-' . time(),
        ]);

        // 生成拣货任务
        $pickingTask = factory(PickingTask::class)->create([
            'warehouse_id' => $warehouse->id,
            'user_id' => $user->id,
        ]);
        $pickingTaskBasket = factory(PickingTaskBasket::class)->create([
            'picking_task_id' => $pickingTask->id,
            'outbound_number' => $outbound->outbound_number,
            'basket_code' => $basket->basket_code,
        ]);
        // 生成任务 item
        $pickingTaskItem = factory(PickingTaskItem::class)->create([
            'picking_task_basket_id' => $pickingTaskBasket->id,
            'sku_code' => $skuCode,
            'quantity' => $quantity,
            'picking_quantity' => $quantity,
            'state' => 1,
        ]);

        // 参数正确
        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
            'warehouse-id' => $warehouse->id,
        ])->json('GET', 'admin/api/outbound-available', [
            'picking_basket_code' => $basket->basket_code,
        ]);
        $response->assertOk();

        $response->assertJsonStructure([
            'code',
            'message',
        ]);

        $response->assertJsonFragment([
            'code' => ResponseConst::CODE_SUCCESS,
            'message' => __('common.success'),
        ]);

    }

    public function testStoreWeigh()
    {
        // 未登录
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->json('POST', 'admin/api/weighs');
        $response->assertUnauthorized();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_UNAUTHORIZED,
        ]);

        // 模拟用户
        $user = factory(User::class)->create();

        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->json('POST', 'admin/api/weighs');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_FAIL,
        ]);

        // 生成必须元素
        $warehouse = factory(Warehouse::class)->create();
        // 库位
        $storageLocation = factory(StorageLocation::class)->create([
            'warehouse_id' => $warehouse->id,
        ]);
        $outboundNumber = 'OUT0001';
        $skuCode = 'SKU0001';
        $quantity = 10;
        $state = 0;
        // 出库单
        $outbound = factory(Outbound::class)->create([
            'warehouse_id' => $warehouse->id,
            'outbound_number' => $outboundNumber,
            'is_need_intercept' => Outbound::IS_NEED_INTERCEPT_FALSE,
            'state' => Outbound::STATE_CHECK_COMPLETED,
        ]);
        // 出库单子项
        $outboundItem = factory(OutboundItem::class)->create([
            'outbound_id' => $outbound->id,
            'sku_code' => $skuCode,
            'quantity' => $quantity,
        ]);
        factory(OutboundFrozenItem::class)->create([
            'outbound_id' => $outbound->id,
            'outbound_item_id' => $outboundItem->id,
            'sku_code' => $skuCode,
            'frozen_bin' => $storageLocation->storage_location_code,
            'frozen_quantity' => $quantity,
        ]);
        // 生成拣货篮子
        $basket = factory(Basket::class)->create([
            'warehouse_id' => $warehouse->id,
            'state' => Basket::STATE_BUSY,
            'basket_code' => 'BAS-' . time(),
        ]);

        // 生成拣货任务
        $pickingTask = factory(PickingTask::class)->create([
            'warehouse_id' => $warehouse->id,
            'user_id' => $user->id,
        ]);
        $pickingTaskBasket = factory(PickingTaskBasket::class)->create([
            'picking_task_id' => $pickingTask->id,
            'outbound_number' => $outbound->outbound_number,
            'basket_code' => $basket->basket_code,
        ]);
        // 生成任务 item
        $pickingTaskItem = factory(PickingTaskItem::class)->create([
            'picking_task_basket_id' => $pickingTaskBasket->id,
            'sku_code' => $skuCode,
            'quantity' => $quantity,
            'picking_quantity' => $quantity,
            'state' => 1,
        ]);
        // 库存
        $stock = factory(Stock::class)->create([
            'warehouse_id' => $warehouse->id,
            'storage_type' => StorageArea::STORAGE_TYPE_PACKAGE,
            'sku_code' => $skuCode,
            'quantity' => $quantity,
            'frozen_quantity' => $quantity,
        ]);
        // 生成复核任务
        $checkTask = factory(CheckTask::class)->create([
            'warehouse_id' => $warehouse->id,
            'picking_task_id' => $pickingTask->id,
            'basket_code' => $pickingTaskBasket->basket_code,
            'user_id' => $user->id,
            'state' => $state,
        ]);
        // 生成复核任务 item
        $checkTaskItem = factory(CheckTaskItem::class)->create([
            'check_task_id' => $checkTask->id,
            'sku_code' => $skuCode,
            'quantity' => $quantity,
        ]);

        // 生成物流公司
        $logisticsCompany = factory(LogisticsCompany::class)->create();

        $actualLogisticsCompany = LogisticsCompany::query()->first();

        // 生成任务日志
        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
            'warehouse-id' => $warehouse->id,
        ])->json('POST', 'admin/api/weighs', [
            'picking_basket_code' => $pickingTaskBasket->basket_code,
            'weight' => 10,
        ]);

        $response->assertOk();

        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'outbound_number',
                'logistics_company',
                'logistics_number',
            ]
        ]);

        $response->assertJsonFragment([
            'code' => ResponseConst::CODE_SUCCESS,
            'message' => __('common.success'),
        ]);

        $response->assertJsonFragment([
            'outbound_number' => $outbound->outbound_number,
            'logistics_company' => $actualLogisticsCompany->company_name,
        ]);
    }
}
