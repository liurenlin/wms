<?php

namespace Tests\Feature\Admin\Role;

use App\Consts\ResponseConst;
use App\Models\PermissionGroup;
use App\Models\Role;
use App\Models\RolePermissionGroup;
use Tests\Feature\BaseFeatureTestCase;

class RoleListTest extends BaseFeatureTestCase
{
    public function testRoleList()
    {
        // 未登录
        $response = $this->header()->json('GET', 'admin/api/roles');
        $response->assertUnauthorized();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_UNAUTHORIZED,
        ]);

        $role = factory(Role::class)->create();
        $parentPermissionGroup = factory(PermissionGroup::class)->create([
            'parent_id' => 0,
        ]);
        $permissionGroup = factory(PermissionGroup::class)->create([
            'parent_id' => $parentPermissionGroup->id,
        ]);
        $rolePermissionGroup = factory(RolePermissionGroup::class)->create([
            'role_id' => $role->id,
            'permission_group_id' => $permissionGroup->id,
        ]);

        $response = $this->login()->json('GET', 'admin/api/roles');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'roles' => [
                    [
                        'id',
                        'name',
                        'permission_groups' => [
                            [
                                'id',
                                'name',
                            ]

                        ]
                    ]
                ],
            ]
        ]);

        $response->assertJsonFragment([
            'id' => $role->id,
            'name' => $role->role_name,
        ]);

        $response->assertJsonFragment([
            'id' => $permissionGroup->id,
            'name' => $permissionGroup->group_name,
        ]);
    }
}
