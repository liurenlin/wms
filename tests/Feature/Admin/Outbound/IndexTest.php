<?php

namespace Tests\Feature\Admin\Outbound;

use App\Consts\ResponseConst;
use App\Models\Outbound;
use App\Models\OutboundItem;
use Tests\Feature\BaseFeatureTestCase;

class IndexTest extends BaseFeatureTestCase
{
    public function testIndex()
    {
        // 未登录
        $response = $this->header()->json('GET', 'admin/api/outbounds');
        $response->assertUnauthorized();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_UNAUTHORIZED,
        ]);

        $warehouseId = 1;
        $outbound = factory(Outbound::class)->create([
            'warehouse_id' => $warehouseId,
        ]);
        $skuCode = 'SKU0001';
        $quantity = 10;
        $outboundItem = factory(OutboundItem::class)->create([
            'outbound_id' => $outbound->id,
            'sku_code' => $skuCode,
            'quantity' => $quantity,
        ]);

        $response = $this->login()->json('GET', 'admin/api/outbounds');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'outbounds' => [
                    [
                        'id',
                        'outbound_number',
                        'created_at',
                        'state',
                        'platform_order' => [
                            'order_number',
                        ],
                        'quantity',
                    ]
                ],
                'page_bean' => [
                    'total',
                    'page',
                    'per_page',
                ],
            ]
        ]);

        $response->assertJsonFragment([
            'quantity' => $quantity,
        ]);

        $response->assertJsonFragment([
            'id' => $outbound->id,
            'outbound_number' => (string) $outbound->outbound_number,
        ]);
    }
}
