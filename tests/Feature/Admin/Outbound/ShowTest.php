<?php

namespace Tests\Feature\Admin\Outbound;

use App\Consts\ResponseConst;
use App\Models\Outbound;
use App\Models\OutboundItem;
use Tests\Feature\BaseFeatureTestCase;

class ShowTest extends BaseFeatureTestCase
{
    public function testShow()
    {
        // 未登录
        $response = $this->header()->json('GET', 'admin/api/outbounds/');
        $response->assertUnauthorized();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_UNAUTHORIZED,
        ]);

        $warehouseId = 1;
        $outbound = factory(Outbound::class)->create([
            'warehouse_id' => $warehouseId,
        ]);
        $skuCode = 'SKU0001';
        $quantity = 10;
        $outboundItem = factory(OutboundItem::class)->create([
            'outbound_id' => $outbound->id,
            'sku_code' => $skuCode,
            'quantity' => $quantity,
        ]);

        $response = $this->login()->json('GET', 'admin/api/outbounds/' . $outbound->id);
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'id',
                'outbound_number',
                'created_at',
                'owner',
                'platform',
                'platform_order' => [
                    'order_number',
                ],
                'receiving_country',
                'receiving_state',
                'receiving_city',
                'receiving_district',
                'receiving_address',
                'receiving_cellphone',
                'receiving_user_name',
                'skus' => [
                    [
                        'code',
                        'quantity',
                    ]
                ],
                'processes',
                'abnormals',
            ]
        ]);

        $response->assertJsonFragment([
            'id' => $outbound->id,
            'outbound_number' => (string) $outbound->outbound_number,
        ]);

        $response->assertJsonFragment([
            'skus' => [
                [
                    'code' => $skuCode,
                    'quantity' => $quantity,
                ]
            ],
        ]);
    }
}
