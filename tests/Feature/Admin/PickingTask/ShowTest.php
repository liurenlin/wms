<?php

namespace Tests\Feature\Admin\Role;

use App\Consts\ResponseConst;
use App\Models\Abnormal;
use App\Models\Outbound;
use App\Models\OutboundItem;
use App\Models\PickingTask;
use App\Models\PickingTaskBasket;
use App\Models\PickingTaskItem;
use App\Models\Warehouse;
use App\User;
use Tests\Feature\BaseFeatureTestCase;

class ShowTest extends BaseFeatureTestCase
{
    public function testUserList()
    {
        $warehouse = factory(Warehouse::class)->create();
        $outbound = factory(Outbound::class)->create([
            'warehouse_id' => $warehouse->id,
        ]);
        $skuCode = 'SKU0001';
        $quantity = 10;
        $basketCode = 'BAS-0001';
        $bin = 'BIN0001';
        $user = factory(User::class)->create();
        $outboundItem = factory(OutboundItem::class)->create([
            'outbound_id' => $outbound->id,
            'sku_code' => $skuCode,
            'quantity' => $quantity,
        ]);
        $pickingTask = factory(PickingTask::class)->create([
            'warehouse_id' => $warehouse->id,
            'user_id' => $user->id,
        ]);
        $pickingTaskBasket = factory(PickingTaskBasket::class)->create([
            'picking_task_id' => $pickingTask->id,
            'outbound_number' => $outbound->outbound_number,
            'basket_code' => $basketCode,
        ]);
        $pickingTaskItem = factory(PickingTaskItem::class)->create([
            'picking_task_basket_id' => $pickingTaskBasket->id,
            'sku_code' => $skuCode,
            'suggest_bin' => $bin,
        ]);

        $abnormals = factory(Abnormal::class)->create([
            'warehouse_id' => $warehouse->id,
            'relation_type' => Abnormal::RELATION_TYPE_PICKING,
            'relation_number' => $pickingTask->task_number,
            'report_user_id' => $user->id,
        ]);

        // 未登录
        $response = $this->header()->json('GET', 'admin/api/picking-tasks/' . $pickingTask->id);
        $response->assertUnauthorized();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_UNAUTHORIZED,
        ]);


        $response = $this->login()->json('GET', 'admin/api/picking-tasks/' . $pickingTask->id);
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'id',
                'task_number',
                'created_at',
                'operator',
                'state',
                'finished_at',
                'task_type',
                'items' => [
                    [
                        'outbound' => [
                            'id',
                            'outbound_number',
                        ],
                        'picking_basket_code'
                    ]
                ],
                'abnormals' => [
                    [
                        'id',
                        'abnormal_number',
                        'created_at',
                        'state',
                        'type',
                        'operator',
                    ]
                ]
            ]
        ]);

        $response->assertJsonFragment([
            'task_number' => $pickingTask->task_number,
        ]);

    }
}
