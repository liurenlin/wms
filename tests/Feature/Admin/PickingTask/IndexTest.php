<?php

namespace Tests\Feature\Admin\PickingTask;

use App\Consts\ResponseConst;
use App\Models\Outbound;
use App\Models\OutboundItem;
use App\Models\PickingTask;
use App\Models\PickingTaskBasket;
use App\Models\PickingTaskItem;
use App\Models\Warehouse;
use App\User;
use Tests\Feature\BaseFeatureTestCase;

class IndexTest extends BaseFeatureTestCase
{
    public function testIndex()
    {
        // 未登录
        $response = $this->header()->json('GET', 'admin/api/picking-tasks');
        $response->assertUnauthorized();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_UNAUTHORIZED,
        ]);
        $warehouse = factory(Warehouse::class)->create();
        $outbound = factory(Outbound::class)->create([
            'warehouse_id' => $warehouse->id,
        ]);
        $skuCode = 'SKU0001';
        $quantity = 10;
        $basketCode = 'BAS-0001';
        $bin = 'BIN0001';
        $user = factory(User::class)->create();
        $outboundItem = factory(OutboundItem::class)->create([
            'outbound_id' => $outbound->id,
            'sku_code' => $skuCode,
            'quantity' => $quantity,
        ]);
        $pickingTask = factory(PickingTask::class)->create([
            'warehouse_id' => $warehouse->id,
            'user_id' => $user->id,
        ]);
        $pickingTaskBasket = factory(PickingTaskBasket::class)->create([
            'picking_task_id' => $pickingTask->id,
            'outbound_number' => $outbound->outbound_number,
            'basket_code' => $basketCode,
        ]);
        $pickingTaskItem = factory(PickingTaskItem::class)->create([
            'picking_task_basket_id' => $pickingTaskBasket->id,
            'sku_code' => $skuCode,
            'suggest_bin' => $bin,
        ]);

        $response = $this->login()->json('GET', 'admin/api/picking-tasks');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'picking_tasks' => [
                    [
                        'id',
                        'task_number',
                        'created_at',
                        'operator',
                        'state',
                        'finished_at',
                        'task_type',
                    ]
                ],
                'page_bean' => [
                    'total',
                    'page',
                    'per_page',
                ]
            ]
        ]);

        $response->assertJsonFragment([
            'task_number' => $pickingTask->task_number,
        ]);

    }
}
