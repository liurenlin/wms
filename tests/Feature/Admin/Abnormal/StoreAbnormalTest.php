<?php

namespace Tests\Feature\Admin\Abnormal;

use App\Consts\ResponseConst;
use App\Models\Abnormal;
use App\Models\ActualReceiving;
use App\Models\PreReceiving;
use App\Models\ProductSku;
use App\Models\Warehouse;
use App\User;
use Tests\Feature\BaseFeatureTestCase;

class StoreAbnormalTest extends BaseFeatureTestCase
{
    public function testStoreAbnormal()
    {
        // 未登录
        $response = $this->header()->json('POST', 'admin/api/abnormals');

        $response->assertUnauthorized();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_UNAUTHORIZED,
        ]);

        // 模拟用户
        $response = $this->login()->json('POST', 'admin/api/abnormals');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_FAIL,
        ]);

        // 生成必须元素
        $quantity = 1;
        $image = 'https://signature.png';
        $warehouse = factory(Warehouse::class)->create();
        $sku = factory(ProductSku::class)->create();

        $relationType = Abnormal::RELATION_TYPE_ACTUAL_RECEIVING;
        // 生成预录入库单
        $preReceiving = factory(PreReceiving::class)->create([
            'warehouse_id' => $warehouse->id,
        ]);
        // 生成实际入库单
        $actualReceiving = factory(ActualReceiving::class)->create([
            'warehouse_id' => $warehouse->id,
            'pre_receiving_id' => $preReceiving->id,
        ]);

        $response = $this->header([
            'warehouse-id' => $warehouse->id,
        ])->login()->json('POST', 'admin/api/abnormals', [
            'relation_type' => $relationType,
            'relation_number' => $actualReceiving->actual_receiving_number,
            'sku_code' => $sku->sku_code,
            'quantity' => $quantity,
            'image' => $image,
        ]);

        $response->assertOk();

        $response->assertJsonStructure([
            'code',
            'message',
        ]);

        $response->assertJsonFragment([
            'code' => ResponseConst::CODE_SUCCESS,
            'message' => __('common.success'),
        ]);
    }
}
