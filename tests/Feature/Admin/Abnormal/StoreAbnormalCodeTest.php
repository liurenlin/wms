<?php

namespace Tests\Feature\Admin\Abnormal;

use App\Consts\ResponseConst;
use App\Models\Warehouse;
use Tests\Feature\BaseFeatureTestCase;

class StoreAbnormalCodeTest extends BaseFeatureTestCase
{
    public function testStoreAbnormalCode()
    {
        // 未登录
        $response = $this->header()->json('POST', 'admin/api/abnormal-codes');

        $response->assertUnauthorized();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_UNAUTHORIZED,
        ]);

        // 生成必须元素
        $warehouse = factory(Warehouse::class)->create();
        // 参数正确
        $response = $this->header([
            'warehouse-id' => $warehouse->id,
        ])->login()->json('POST', 'admin/api/abnormal-codes');
        $response->assertOk();

        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'code',
            ],
        ]);
    }
}
