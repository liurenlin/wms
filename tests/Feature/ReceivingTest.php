<?php

namespace Tests\Feature;

use App\Consts\ResponseConst;
use App\Models\ActualReceiving;
use App\Models\PreReceiving;
use App\Models\PreReceivingItem;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductSku;
use App\Models\ProductSkuImage;
use App\Models\ReceivingCart;
use App\Models\ReceivingCartItem;
use App\Models\Resource;
use App\Models\Stock;
use App\Models\StorageArea;
use App\Models\Warehouse;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class ReceivingTest extends TestCase
{
    use DatabaseTransactions;

    public function testStoreReceiving()
    {
        // 生成必须元素
        $resource = factory(Resource::class)->create();
        $warehouse = factory(Warehouse::class)->create();

        $product = factory(Product::class)->create();
        $productImages = factory(ProductImage::class, 2)->create([
            'product_id' => $product->id,
            'resource_id' => $resource->id,
        ]);
        $productSku1 = factory(ProductSku::class)->create([
            'product_id' => $product->id,
        ]);
        $productSku1Images = factory(ProductSkuImage::class, 2)->create([
            'product_sku_id' => $productSku1->id,
            'resource_id' => $resource->id,
        ]);
        // 生成预录入库单
        $preReceiving =  factory(PreReceiving::class)->create([
            'warehouse_id' => $warehouse->id,
            'state' => PreReceiving::STATE_CREATED
        ]);
        $quantity = 10;
        $preReceivingItem = factory(PreReceivingItem::class)->create([
            'pre_receiving_id' => $preReceiving->id,
            'sku_code' => $productSku1->sku_code,
            'quantity' => $quantity,
        ]);
        // 生成收货车
        $receivingCart = factory(ReceivingCart::class)->create([
            'warehouse_id' => $warehouse->id,
            'state' => 0,
        ]);

        // 未登录
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->json('POST', 'api/receivings', [
            'pre_receiving_number' => $preReceiving->pre_receiving_number,
        ]);
        $response->assertUnauthorized();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_UNAUTHORIZED,
        ]);

        // 模拟用户
        $user = factory(User::class)->create();

        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->json('POST', 'api/receivings');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_FAIL,
        ]);

        // 参数正确
        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
            'warehouse-id' => $warehouse->id,
        ])->json('POST', 'api/receivings', [
            'pre_receiving_number' => $preReceiving->pre_receiving_number,
        ]);
        $response->assertOk();

        $response->assertJsonStructure([
            'code',
            'message',
        ]);

        $response->assertJson([
            'code' => ResponseConst::CODE_SUCCESS,
        ]);

    }

    public function testPreReceivingShow()
    {
        // 生成必须元素
        $resource = factory(Resource::class)->create();
        $warehouse = factory(Warehouse::class)->create();

        $product = factory(Product::class)->create();
        $productImages = factory(ProductImage::class, 2)->create([
            'product_id' => $product->id,
            'resource_id' => $resource->id,
        ]);
        $productSku1 = factory(ProductSku::class)->create([
            'product_id' => $product->id,
        ]);
        $productSku1Images = factory(ProductSkuImage::class, 2)->create([
            'product_sku_id' => $productSku1->id,
            'resource_id' => $resource->id,
        ]);
        // 生成预录入库单
        $preReceiving =  factory(PreReceiving::class)->create([
            'warehouse_id' => $warehouse->id,
            'state' => PreReceiving::STATE_CREATED
        ]);
        $quantity = 10;
        $preReceivingItem = factory(PreReceivingItem::class)->create([
            'pre_receiving_id' => $preReceiving->id,
            'sku_code' => $productSku1->sku_code,
            'quantity' => $quantity,
        ]);
        // 生成收货车
        $receivingCart = factory(ReceivingCart::class)->create([
            'warehouse_id' => $warehouse->id,
            'state' => 0,
        ]);

        // 未登录
        $response = $this->withHeaders(['X-Requested-With' => 'XMLHttpRequest'])
            ->json('GET', 'api/pre-receivings/' . $preReceiving->pre_receiving_number);

        $response->assertUnauthorized();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_UNAUTHORIZED,
        ]);

        // 模拟用户
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->json('GET', 'api/pre-receivings/' . $preReceiving->pre_receiving_number);
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_FAIL,
        ]);

        // 模拟用户
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
            'warehouse-id' => $warehouse->id,
        ])->json('GET', 'api/pre-receivings/' . $preReceiving->pre_receiving_number);
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data',
        ]);

        $response->assertJsonFragment([
            'code' => ResponseConst::CODE_SUCCESS,
            'message' => __('common.success'),
        ]);

    }

    public function testPreReceivingProductShow()
    {
        // 生成必须元素
        $resource = factory(Resource::class)->create();
        $warehouse = factory(Warehouse::class)->create();

        $product = factory(Product::class)->create();
        $productImages = factory(ProductImage::class, 2)->create([
            'product_id' => $product->id,
            'resource_id' => $resource->id,
        ]);
        $productSku1 = factory(ProductSku::class)->create([
            'product_id' => $product->id,
        ]);
        $productSku1Images = factory(ProductSkuImage::class, 2)->create([
            'product_sku_id' => $productSku1->id,
            'resource_id' => $resource->id,
        ]);
        // 生成预录入库单
        $preReceiving =  factory(PreReceiving::class)->create([
            'warehouse_id' => $warehouse->id,
            'state' => PreReceiving::STATE_CREATED
        ]);
        $quantity = 10;
        $preReceivingItem = factory(PreReceivingItem::class)->create([
            'pre_receiving_id' => $preReceiving->id,
            'sku_code' => $productSku1->sku_code,
            'quantity' => $quantity,
        ]);
        // 生成收货车
        $receivingCart = factory(ReceivingCart::class)->create([
            'warehouse_id' => $warehouse->id,
            'state' => 0,
        ]);

        // 未登录
        $response = $this->withHeaders(['X-Requested-With' => 'XMLHttpRequest'])
            ->json('GET', 'api/pre-receivings/' . $preReceiving->pre_receiving_number . '/products/' . $product->id);
        $response->assertUnauthorized();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_UNAUTHORIZED,
        ]);

        // 模拟用户
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->json('GET', 'api/pre-receivings/' . $preReceiving->pre_receiving_number . '/products/' . $product->id);
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_FAIL,
        ]);

        // 模拟用户
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
            'warehouse-id' => $warehouse->id,
        ])->json('GET', 'api/pre-receivings/' . $preReceiving->pre_receiving_number . '/products/' . $product->id);

        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'id',
                'code',
                'name',
                'images' => [
                    [
                        'url',
                    ],
                ],
                'skus' => [
                    [
                        'id',
                        'code',
                        'name',
                        'attribute',
                        'images' => [
                            [
                                'url',
                            ],
                        ],
                        'quantity',
                        'print_quantity',
                    ],
                ],
            ],
        ]);

        $response->assertJsonFragment([
            'code' => ResponseConst::CODE_SUCCESS,
            'message' => __('common.success'),
        ]);

    }

    public function testStoreReceivingLog()
    {
        // 生成必须元素
        $resource = factory(Resource::class)->create();
        $warehouse = factory(Warehouse::class)->create();

        $product = factory(Product::class)->create();
        $productImages = factory(ProductImage::class, 2)->create([
            'product_id' => $product->id,
            'resource_id' => $resource->id,
        ]);
        $productSku1 = factory(ProductSku::class)->create([
            'product_id' => $product->id,
        ]);
        $productSku1Images = factory(ProductSkuImage::class, 2)->create([
            'product_sku_id' => $productSku1->id,
            'resource_id' => $resource->id,
        ]);
        // 生成预录入库单
        $preReceiving =  factory(PreReceiving::class)->create([
            'warehouse_id' => $warehouse->id,
            'state' => PreReceiving::STATE_CREATED
        ]);
        $quantity = 10;
        $preReceivingItem = factory(PreReceivingItem::class)->create([
            'pre_receiving_id' => $preReceiving->id,
            'sku_code' => $productSku1->sku_code,
            'quantity' => $quantity,
        ]);
        // 生成收货车
        $receivingCart = factory(ReceivingCart::class)->create([
            'warehouse_id' => $warehouse->id,
            'state' => 0,
        ]);
        // 生成收货车 item
        $receivingCartItems = factory(ReceivingCartItem::class)->create([
            'warehouse_id' => $warehouse->id,
            'receiving_cart_id' => $receivingCart->id,
            'sku_code' => $productSku1->sku_code,
            'quantity' => 10,
        ]);
        // 收货暂存区
        factory(Stock::class)->create([
            'warehouse_id' => $warehouse->id,
            'sku_code' => $productSku1->sku_code,
            'quantity' => 10,
            'storage_type' => StorageArea::STORAGE_TYPE_RECEIVING,
            'bin' => '',
            'frozen_quantity' => 0,
            'unfrozen_quantity' => 0,
        ]);
        // 生成实际入库单
        $actualReceiving = factory(ActualReceiving::class)->create([
            'warehouse_id' => $warehouse->id,
            'pre_receiving_id' => $preReceiving->id,
            'state' => ActualReceiving::STATE_ING,
        ]);

        // 未登录
        $response = $this->withHeaders(['X-Requested-With' => 'XMLHttpRequest'])
            ->json('POST', 'api/receiving-logs', []);

        $response->assertUnauthorized();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_UNAUTHORIZED,
        ]);

        // 模拟用户
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->json('POST', 'api/receiving-logs', [
            'actual_receiving_id' => $actualReceiving->id,
            'pre_receiving_number' => $preReceiving->pre_receiving_number,
            'sku_code' => $productSku1->sku_code,
            'quantity' => $quantity,
            'receiving_cart_code' => $receivingCart->cart_code,
        ]);
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_FAIL,
        ]);

        // 模拟用户
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
            'warehouse-id' => $warehouse->id,
        ])->json('POST', 'api/receiving-logs', [
            'actual_receiving_id' => $actualReceiving->id,
            'pre_receiving_number' => $preReceiving->pre_receiving_number,
            'sku_code' => $productSku1->sku_code,
            'quantity' => $quantity,
            'receiving_cart_code' => $receivingCart->cart_code,
        ]);

        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);

        $response->assertJsonFragment([
            'code' => ResponseConst::CODE_SUCCESS,
            'message' => __('common.success'),
        ]);
    }

    public function testStoreSkuCodePrintLog()
    {
        // 生成必须元素
        $resource = factory(Resource::class)->create();
        $warehouse = factory(Warehouse::class)->create();

        $product = factory(Product::class)->create();
        $productImages = factory(ProductImage::class, 2)->create([
            'product_id' => $product->id,
            'resource_id' => $resource->id,
        ]);
        $productSku1 = factory(ProductSku::class)->create([
            'product_id' => $product->id,
        ]);
        $productSku1Images = factory(ProductSkuImage::class, 2)->create([
            'product_sku_id' => $productSku1->id,
            'resource_id' => $resource->id,
        ]);
        // 生成预录入库单
        $preReceiving =  factory(PreReceiving::class)->create([
            'warehouse_id' => $warehouse->id,
            'state' => PreReceiving::STATE_CREATED
        ]);
        $quantity = 10;
        $preReceivingItem = factory(PreReceivingItem::class)->create([
            'pre_receiving_id' => $preReceiving->id,
            'sku_code' => $productSku1->sku_code,
            'quantity' => $quantity,
        ]);
        // 生成收货车
        $receivingCart = factory(ReceivingCart::class)->create([
            'warehouse_id' => $warehouse->id,
            'state' => 0,
        ]);
        // 生成收货车 item
        $receivingCartItems = factory(ReceivingCartItem::class)->create([
            'warehouse_id' => $warehouse->id,
            'receiving_cart_id' => $receivingCart->id,
            'sku_code' => $productSku1->sku_code,
            'quantity' => 10,
        ]);
        // 收货暂存区
        factory(Stock::class)->create([
            'warehouse_id' => $warehouse->id,
            'sku_code' => $productSku1->sku_code,
            'quantity' => 10,
            'storage_type' => StorageArea::STORAGE_TYPE_RECEIVING,
            'bin' => '',
            'frozen_quantity' => 0,
            'unfrozen_quantity' => 0,
        ]);

        // 未登录
        $response = $this->withHeaders(['X-Requested-With' => 'XMLHttpRequest'])
            ->json('POST', 'api/sku-code-print-logs');

        $response->assertUnauthorized();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_UNAUTHORIZED,
        ]);

        // 模拟用户
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->json('POST', 'api/sku-code-print-logs', [
            'pre_receiving_id' => $preReceiving->id,
            'sku_code' => $productSku1->sku_code,
            'quantity' => $quantity,
        ]);
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_FAIL,
        ]);

        // 模拟用户
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
            'warehouse-id' => $warehouse->id,
        ])->json('POST', 'api/sku-code-print-logs', [
            'pre_receiving_id' => $preReceiving->id,
            'sku_code' => $productSku1->sku_code,
            'quantity' => $quantity,
        ]);

        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);

        $response->assertJsonFragment([
            'code' => ResponseConst::CODE_SUCCESS,
            'message' => __('common.success'),
        ]);
    }

    public function testUpdateActualReceiving()
    {
        // 生成必须元素
        $resource = factory(Resource::class)->create();
        $warehouse = factory(Warehouse::class)->create();

        $product = factory(Product::class)->create();
        $productImages = factory(ProductImage::class, 2)->create([
            'product_id' => $product->id,
            'resource_id' => $resource->id,
        ]);
        $productSku1 = factory(ProductSku::class)->create([
            'product_id' => $product->id,
        ]);
        $productSku1Images = factory(ProductSkuImage::class, 2)->create([
            'product_sku_id' => $productSku1->id,
            'resource_id' => $resource->id,
        ]);
        // 生成预录入库单
        $preReceiving =  factory(PreReceiving::class)->create([
            'warehouse_id' => $warehouse->id,
            'state' => PreReceiving::STATE_CREATED
        ]);
        $quantity = 10;
        $preReceivingItem = factory(PreReceivingItem::class)->create([
            'pre_receiving_id' => $preReceiving->id,
            'sku_code' => $productSku1->sku_code,
            'quantity' => $quantity,
        ]);
        // 生成收货车
        $receivingCart = factory(ReceivingCart::class)->create([
            'warehouse_id' => $warehouse->id,
            'state' => 0,
        ]);
        // 生成收货车 item
        $receivingCartItems = factory(ReceivingCartItem::class)->create([
            'warehouse_id' => $warehouse->id,
            'receiving_cart_id' => $receivingCart->id,
            'sku_code' => $productSku1->sku_code,
            'quantity' => 10,
        ]);
        // 收货暂存区
        factory(Stock::class)->create([
            'warehouse_id' => $warehouse->id,
            'sku_code' => $productSku1->sku_code,
            'quantity' => 10,
            'storage_type' => StorageArea::STORAGE_TYPE_RECEIVING,
            'bin' => '',
            'frozen_quantity' => 0,
            'unfrozen_quantity' => 0,
        ]);

        $user = factory(User::class)->create();
        // 生成实际入库单
        $actualReceiving = factory(ActualReceiving::class)->create([
            'warehouse_id' => $warehouse->id,
            'pre_receiving_id' => $preReceiving->id,
            'user_id' => $user->id,
            'state' => ActualReceiving::STATE_ING,
        ]);
        $state = 1;
        // 未登录
        $response = $this->withHeaders(['X-Requested-With' => 'XMLHttpRequest'])
            ->json('PUT', 'api/actual-receivings/' . $actualReceiving->id, ['state' => $state]);

        $response->assertUnauthorized();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_UNAUTHORIZED,
        ]);

        // 模拟用户
        $this->actingAs($user, 'api');
        $response = $this->withHeaders(['X-Requested-With' => 'XMLHttpRequest'])
            ->json('PUT', 'api/actual-receivings/' . $actualReceiving->id, ['state' => $state]);

        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_FAIL,
        ]);

        // 模拟用户
        $this->actingAs($user, 'api');
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
            'warehouse-id' => $warehouse->id,
        ])->json('PUT', 'api/actual-receivings/' . $actualReceiving->id, ['state' => $state]);

        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);

        $response->assertJsonFragment([
            'code' => ResponseConst::CODE_SUCCESS,
            'message' => __('common.success'),
        ]);
    }

}
