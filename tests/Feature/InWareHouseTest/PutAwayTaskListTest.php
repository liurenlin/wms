<?php

namespace Tests\Feature\InWareHouseTest;

use App\Models\PutawayTask;
use Tests\Feature\BaseFeatureTestCase;

class PutAwayTaskListTest extends BaseFeatureTestCase
{
    public function testIndex()
    {
        factory(PutawayTask::class)->create();
        $response = $this->login()->header()->json('GET', 'admin/api/putaway-tasks');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'putaway_tasks' => [
                    [
                        'id',
                        'task_number',
                        'receiving_cart_code',
                        'created_at',
                        'finished_at',
                        'operator',
                    ]
                ],
                'page_bean' => [
                    'total',
                    'page',
                    'per_page',
                ],
            ],
        ]);
    }
}
