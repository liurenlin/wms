<?php

namespace Tests\Feature\InWareHouseTest;

use App\Models\Abnormal;
use App\Models\ActualReceiving;
use App\Models\PreReceiving;
use App\Models\ReceivingLog;
use Tests\Feature\BaseFeatureTestCase;

class ActualReceivingDetailTest extends BaseFeatureTestCase
{
    public function testIndex()
    {
        $actualReceiving = factory(ActualReceiving::class)->create();
        factory(Abnormal::class)->create(
            ['warehouse_id' => $actualReceiving->warehouse_id]
        );

        $preReceiving = factory(PreReceiving::class)->create(
            ['warehouse_id' => $actualReceiving->warehouse_id]
        );

        factory(ReceivingLog::class)->create(
            ['pre_receiving_id' => $preReceiving->id]
        );
        factory(Abnormal::class)->create(
            ['warehouse_id' => $actualReceiving->warehouse_id]
        );

        $response = $this->login()->header()->json('GET', 'admin/api/actual-receivings/'.$actualReceiving->id);
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'id',
                'actual_receiving_number',
                'pre_receiving_number',
                'finished_at',
                'created_at',
                'third_party_order_number',
                'third_party_order_type',
                'operator',
                'owner',
                'items',
                'receiving_logs',
                'abnormals',
            ],
        ]);
    }
}
