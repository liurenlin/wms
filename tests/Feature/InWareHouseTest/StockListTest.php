<?php

namespace Tests\Feature\InWareHouseTest;

use App\Consts\ResponseConst;
use App\Models\Abnormal;
use App\Models\Stock;
use Tests\Feature\BaseFeatureTestCase;

class StockListTest extends BaseFeatureTestCase
{
    public function testIndex()
    {
        $stocks = factory(Stock::class)->create();
        factory(Abnormal::class)->create(
            ['warehouse_id' => $stocks->warehouse_id]
        );
        $response = $this->login()->header()->json('GET', 'admin/api/stocks');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'stocks' => [
                    [
                        'id',
                        'sku_code',
                        'quantity',
                        'receiving_area',
                        'frozen_stock',
                        'unfrozen_stock',
                        'packing_area',
                    ]
                ],
                'page_bean' => [
                    'total',
                    'page',
                    'per_page',
                ],
            ],
        ]);
    }
}
