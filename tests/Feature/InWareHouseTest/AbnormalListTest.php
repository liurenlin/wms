<?php

namespace Tests\Feature\InWareHouseTest;

use App\Consts\ResponseConst;
use App\Models\Abnormal;
use Tests\Feature\BaseFeatureTestCase;

class AbnormalListTest extends BaseFeatureTestCase
{
    public function testIndex()
    {
        factory(Abnormal::class)->create();
        $response = $this->login()->header()->json('GET', 'admin/api/abnormals');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'abnormals' => [
                    [
                        'id',
                        'abnormal_number',
                        'abnormal_code',
                        'state',
                        'basket_code',
                        'abnormal_type',
                        'created_at',
                        'operator'
                    ]
                ],
                'page_bean' => [
                    'total',
                    'page',
                    'per_page',
                ],
            ],
        ]);
    }
}
