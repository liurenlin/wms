<?php

namespace Tests\Feature\InWareHouseTest;

use App\Consts\ResponseConst;
use App\Models\Abnormal;
use App\Models\PreReceiving;
use App\Models\Stock;
use Tests\Feature\BaseFeatureTestCase;

class PreReceivingListTest extends BaseFeatureTestCase
{
    public function testIndex()
    {
        factory(PreReceiving::class)->create();
        $response = $this->login()->header()->json('GET', 'admin/api/pre-receivings');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'pre_receivings' => [
                    [
                        'pre_receiving_number',
                        'created_at',
                        'third_party_order_number',
                        'type',
                        'owner',
                        'id',
                    ]
                ],
                'page_bean' => [
                    'total',
                    'page',
                    'per_page',
                ],
            ],
        ]);
    }
}
