<?php

namespace Tests\Feature\InWareHouseTest;


use App\Models\Abnormal;
use Tests\Feature\BaseFeatureTestCase;

class AbnormalDetailTest extends BaseFeatureTestCase
{
    public function testIndex()
    {
        $abnormal = factory(Abnormal::class)->create();
        $response = $this->login()->header()->json('GET', 'admin/api/abnormals/'.$abnormal->id);
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'id',
                'abnormal_number',
                'abnormal_code',
                'state',
                'relation_number',
                'created_at',
                'operator',
                'processes',
            ],
        ]);
    }
}
