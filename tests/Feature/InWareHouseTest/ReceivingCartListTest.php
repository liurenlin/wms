<?php

namespace Tests\Feature\InWareHouseTest;

use App\Models\PutawayTask;
use App\Models\ReceivingCart;
use Tests\Feature\BaseFeatureTestCase;

class ReceivingCartListTest extends BaseFeatureTestCase
{
    public function testIndex()
    {
        $receivingCart = factory(ReceivingCart::class)->create();
        factory(PutawayTask::class)->create([
            'receiving_cart_code'=>$receivingCart->cart_code
        ]);
        $response = $this->login()->header()->json('GET', 'admin/api/putaway-carts');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'receiving_carts' => [
                    [
                        'id',
                        'cart_code',
                        'created_at',
                        'operator',
                        'putaway_task'=>[
                            'task_number'
                        ]
                    ]
                ],
                'page_bean' => [
                    'total',
                    'page',
                    'per_page',
                ],
            ],
        ]);
    }
}
