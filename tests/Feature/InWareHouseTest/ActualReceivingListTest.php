<?php

namespace Tests\Feature\InWareHouseTest;

use App\Models\ActualReceiving;
use App\Models\PreReceiving;
use Tests\Feature\BaseFeatureTestCase;

class ActualReceivingListTest extends BaseFeatureTestCase
{
    public function testIndex()
    {
        $PreReceiving = factory(PreReceiving::class)->create();
        factory(ActualReceiving::class)->create([
            'pre_receiving_id'=>$PreReceiving->id
        ]);
        $response = $this->login()->header()->json('GET', 'admin/api/actual-receivings');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'actual_receivings' => [
                    [
                        'id',
                        'pre_receiving_number',
                        'acutal_receiving_number',
                        'third_party_order_number',
                        'owner',
                        'operator',
                        'created_at',
                        'finished_at',
                    ]
                ],
                'page_bean' => [
                    'total',
                    'page',
                    'per_page',
                ],
            ],
        ]);
    }
}
