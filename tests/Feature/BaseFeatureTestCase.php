<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Support\Str;
use Tests\TestCase;

class BaseFeatureTestCase extends TestCase
{
    use DatabaseTransactions;

    protected $user;

    protected function login(){
        $this->user = factory(User::class)->create();
        $this->actingAs($this->user, 'api');
        return $this;
    }

    protected function header(array $headers = []){
        $headers = array_merge($headers, [
            'X-Requested-With' => 'XMLHttpRequest',
        ]);
        $this->withHeaders($headers);
        return $this;
    }

    protected function validationAssert(TestResponse $response, $field, $code = 422)
    {
        $response->assertStatus($code);
        $response->assertJsonStructure([
            'message',
            'errors' => [
                $field
            ],
        ]);
    }

    protected function generateStringFieldData(array $data, $field, $length = null, $required = true)
    {
        $returnData = [];
        if ($required) {
            // field not exist
            $data[$field] = '';
            $returnData[] = [
                $data, $field
            ];
        }
        if (!is_null($length)) {
            // length
            $data[$field] = Str::random($length + random_int(1, 100));
            $returnData[] = [
                $data, $field
            ];
        }
        return $returnData;
    }

    protected function generateIntegerFieldData(array $data, $field, $start, $end = null, $required = true)
    {
        $returnData = [];
        if ($required) {
            // field not exist
            $data[$field] = '';
            $returnData[] = [
                $data, $field
            ];
        }
        // no integer
        $data[$field] = Str::random(5);
        $returnData[] = [
            $data, $field
        ];
        // less than start
        $data[$field] = random_int($start - 10000, $start - 1);
        $returnData[] = [
            $data, $field
        ];

        if (!is_null($end)) {
            // greater than end
            $data[$field] = random_int($end + 1, $end + 10000);
            $returnData[] = [
                $data, $field
            ];
        }
        return $returnData;
    }
}
