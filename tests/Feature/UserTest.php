<?php

namespace Tests\Feature;

use App\Consts\ResponseConst;
use App\Models\UserRole;
use App\Models\Warehouse;
use App\User;
use Faker\Generator;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserTest extends TestCase
{
    use DatabaseTransactions;

    public function testLogin()
    {
        $faker = app(Generator::class);
        $name = $faker->name;
        $password = $faker->password;

        $response = $this->json('POST', 'api/login', [
            'name' => $name,
            'password' => $password,
        ]);
        $response->assertOk();

        $response->assertJson([
            'code' => ResponseConst::CODE_FAIL,
        ]);

        $warehouse = factory(Warehouse::class)->create();

        $user = factory(User::class)->create([
            'name' => $name,
            'password' => Hash::make($password),
        ]);
        factory(UserRole::class)->create([
            'warehouse_id' => $warehouse->id,
            'user_id' => $user->id,
        ]);
        $response = $this->json('POST', 'api/login', [
            'name' => $name,
            'password' => $password,
        ]);
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
            'data' => [
                'id',
                'token',
                'warehouses' => [
                    [
                        'id',
                        'name',
                    ]
                ]
            ],
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_SUCCESS,
            'message' => __('common.success'),
        ]);
    }

    public function testLogout()
    {
        $user = factory(User::class)->create();
        $token = JWTAuth::fromUser($user);
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', 'api/logout');
        $response->assertOk();
        $response->assertJsonStructure([
            'code',
            'message',
        ]);
        $response->assertJson([
            'code' => ResponseConst::CODE_SUCCESS,
            'message' => __('common.success'),
        ]);
    }
}
