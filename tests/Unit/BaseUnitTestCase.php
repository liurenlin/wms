<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use ReflectionException;
use ReflectionMethod;
use Tests\TestCase;

class BaseUnitTestCase extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    /**
     * @param $className
     * @param $functionName
     * @param $object
     * @param array $args
     * @return mixed
     * @throws ReflectionException
     */
    public function callProtectedFunction($className, $functionName, $object, array $args)
    {
        $function = new ReflectionMethod($className, $functionName);
        $function->setAccessible(true);
        return $function->invokeArgs($object, $args);
    }
}
