<?php

namespace Tests\Unit\Service\WarehouseService;

use App\Models\Warehouse;
use App\Services\WarehouseService;
use Illuminate\Support\Str;
use PHPUnit\Framework\TestCase;
use Tests\Unit\BaseUnitTestCase;

class GenerateWarehouseCodeTest extends BaseUnitTestCase
{
    public function testGenerateWarehouseCode(){
        $country = Str::random(14);
        $service = new WarehouseService();
        $this->assertEquals($country . '0000', $service->generateWarehouseCode($country));
        factory(Warehouse::class)->create([
            'country' => $country,
        ]);
        $this->assertEquals($country . '0001', $service->generateWarehouseCode($country));
        factory(Warehouse::class)->times(10)->create([
            'country' => $country,
        ]);
        $this->assertEquals($country . '0011', $service->generateWarehouseCode($country));
        factory(Warehouse::class)->times(100)->create([
            'country' => $country,
        ]);
        $this->assertEquals($country . '0111', $service->generateWarehouseCode($country));
        factory(Warehouse::class)->times(1000)->create([
            'country' => $country,
        ]);
        $this->assertEquals($country . '1111', $service->generateWarehouseCode($country));
    }
}
