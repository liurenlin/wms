<?php

namespace Tests\Unit\BasicData\StorageLocation;

use App\Models\StorageArea;
use App\Models\StorageLocation;
use App\Models\Warehouse;
use App\Services\StorageLocationService;
use Tests\Feature\BaseFeatureTestCase;

class BatchAddLocationsTest extends BaseFeatureTestCase
{
    public function testSuccess()
    {
        $warehouse = factory(Warehouse::class)->create();
        $area = factory(StorageArea::class)->create();
        $startPath = random_int(1, 10);
        $endPath = random_int($startPath + 1, 11);
        $shelfQty = random_int(1, 10);
        $layerQty = random_int(1, 5);
        $binQty = random_int(1, 5);
        $service = new StorageLocationService();
        $this->assertEquals(true, $service->batchAddLocations($warehouse, $area, $startPath, $endPath, $shelfQty, $layerQty, $binQty));
        for ($pathCode = $startPath; $pathCode <= $startPath; $pathCode++) {
            for ($shelfCode = 1; $shelfCode <= $shelfQty; $shelfCode++) {
                for ($layerCode = 0; $layerCode < $layerQty; $layerCode++) {
                    for ($binCode = 1; $binCode <= $binQty; $binCode++) {
                        $code = $area->area_code . sprintf('%02s', $pathCode)
                            . '-' . sprintf('%02s', $shelfCode) . '-'
                            . $layerCode . $binCode;
                        $locations = StorageLocation::withoutTrashed()
                            ->where('warehouse_id', $warehouse->id)
                            ->where('storage_location_code', $code)
                            ->get();
                        $this->assertEquals(1, count($locations));
                    }
                }
            }
        }
    }
}
