## WMS

> 仓库管理系统

### 技术

#### 后端

1. mysql

* 版本：5.7
* 字符集：utf8mb4
* 排序字符集：utf8mb4_unicode_ci

2. PHP框架

* 框架：Laravel
* 版本：6.9.0

3. Redis
4. Google Cloud Storage
5. Google Cloud Pub/Sub

#### 客户端 

`支撑操作员在库内进行，收货，入库，拣货，交接等操作`

1. Android

#### 前端

`支撑管理员后台数据查看和相关库内操作，同时支撑网页端复核和称重模块`

1. React

### 模块

| 模块 |
|---|
| Receiving |
| Picking |
| Checking |
| Weigh |
| Handover |
| Abnormal |    

### Todo List

1. 异步锁库存
2. 智能锁库（规则优先级）
3. 系统智能自愈
4. 报表


