<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::namespace('Admin')->group(function () {
    Route::post('login', 'UserController@login');
    Route::get('permission-groups', 'PermissionController@permissionGroupList');
});

Route::middleware('auth:api')->namespace('Admin')->group(function () {
    Route::post('logout', 'UserController@logout');
});

Route::middleware([
    'auth:api',
    'log.operation',
    'permission',
])->namespace('Admin')->group(function () {

    /**
     * ---------------------------
     * 用户角色管理
     * ---------------------------
     */
    Route::get('permissions', 'PermissionController@index');
    Route::post('roles', 'RoleController@store');
    Route::get('roles', 'RoleController@roleList');
    Route::put('roles/{role_id}', 'RoleController@update');
    Route::delete('roles/{role_id}', 'RoleController@destroy');
    Route::get('users', 'UserController@userList');
    Route::post('users', 'UserController@storeUser');
    Route::put('users/{user_id}', 'UserController@updateUser');
    Route::delete('users/{user_id}/roles/{role_id}', 'UserController@deleteUserRole');

    //库内管理
    Route::get('sku-info', 'StockController@GetProductSku');
    Route::get('stocks', 'StockController@stockList');
    Route::get('stock-deal-with-logs', 'StockController@stockManagement');
    // 添加库内库存处理记录
    Route::post('stock-deal-with-logs', 'StockController@storeStockDealWithLog');
    // 查询库位库存
    Route::get('storage-locations/{storage_location_code}', 'StorageLocationController@show');
    Route::get('sku-stocks', 'StockController@skuStocks');

    Route::get('abnormals', 'AbnormalController@index');
    Route::get('abnormals/{abnormal_id}', 'AbnormalController@show');
    Route::put('abnormals/{abnormal_id}', 'AbnormalController@update');

    //入库管理
    //预录入库单列表
    Route::get('pre-receivings', 'ReceivingController@preReceivingList');
    //预录入库单详情
    Route::get('pre-receivings/{pre_receiving_id}', 'ReceivingController@preReceivingDetail');
    //实际入库单列表
    Route::get('actual-receivings', 'ReceivingController@actualReceivingList');
    //实际入库单详情
    Route::get('actual-receivings/{actual_receiving_id}', 'ReceivingController@actualReceivingDetail');
    //入库任务列表
    Route::get('putaway-tasks', 'ReceivingController@putAwayTaskList');
    //入库任务详情
    Route::get('putaway-tasks/{putaway_task_id}', 'ReceivingController@putAwayTaskDetail');
    //入库收货车列表
    Route::get('putaway-carts', 'ReceivingController@putawayCartsList');
    //入库收货车详情
    Route::get('putaway-carts/{cart_id}', 'ReceivingController@putawayCartDetail');


    // 出库管理
    // 出库单
    Route::get('outbounds', 'OutboundController@index');
    Route::get('outbounds/{outbound_id}', 'OutboundController@show');
    // 拣货任务
    Route::get('picking-tasks', 'PickingTaskController@index');
    Route::put('picking-tasks', 'PickingTaskController@updateTasks');
    Route::get('picking-tasks/{picking_task_id}', 'PickingTaskController@show');
    Route::post('picking-task-assigns', 'PickingTaskController@storeAssign');
    Route::get('picking-task-details', 'PickingTaskController@details');

    Route::get('picking-task-baskets', 'PickingTaskBasketController@index');
    Route::get('picking-task-baskets/{picking_task_basket_id}', 'PickingTaskBasketController@show');

    // 复核任务
    Route::get('check-tasks', 'CheckTaskController@index');
    Route::get('check-tasks/{check_task_id}', 'CheckTaskController@show');

    // 复核
    Route::post('check-tasks', 'CheckController@storeCheckTask');
    Route::post('check-logs', 'CheckController@storeCheckLog');
    Route::put('check-tasks/{check_task_id}', 'CheckController@confirmCheckTask');

    // 称重
    Route::get('outbound-available', 'WeighController@checkOutboundAvailable');
    Route::post('weighs', 'WeighController@storeWeigh');
    Route::get('weighs', 'WeighController@index');

    // 交接
    Route::get('handovers', 'HandoverController@index');
    Route::get('handovers/{handover_id}', 'HandoverController@show');

    // warehouse
    Route::get('/warehouses', 'BasicData\WarehouseController@getList');
    Route::post('/warehouses', 'BasicData\WarehouseController@create');
    Route::put('/warehouses/{id}', 'BasicData\WarehouseController@update');

    // storage location
    Route::get('/storage-locations', 'BasicData\StorageLocationController@getList');
    Route::post('/storage-locations', 'BasicData\StorageLocationController@create');
//    Route::put('/storage-locations/{id}', 'BasicData\StorageLocationController@update');
    Route::delete('/storage-locations/{ids}', 'BasicData\StorageLocationController@delete');

    // storage area
    Route::get('/storage-areas', 'BasicData\StorageAreaController@getList');
    Route::post('/storage-areas', 'BasicData\StorageAreaController@create');
//    Route::put('/storage-areas/{id}', 'BasicData\StorageAreaController@update');

    // sku list
    Route::get('/skus', 'BasicData\ProductController@getList');
    Route::put('/skus/{id}', 'BasicData\ProductController@updateWeightAndVolume');

    // logistics company
    Route::get('/logistics-companies', 'BasicData\LogisticsCompanyController@getList');
    Route::put('/logistics-companies/{id}', 'BasicData\LogisticsCompanyController@update');

    // receiving cart
    Route::get('/receiving-carts', 'BasicData\ReceivingCartController@getList');
    Route::post('/receiving-carts/batch', 'BasicData\ReceivingCartController@batchAdd');

    // picking baskets
    Route::get('/picking-baskets', 'BasicData\PickingBasketController@getList');
    Route::post('/picking-baskets/batch', 'BasicData\PickingBasketController@batchAdd');

    // 异常
    Route::post('abnormal-codes', 'AbnormalController@storeAbnormalCode');
    Route::post('abnormals', 'AbnormalController@store');
});





