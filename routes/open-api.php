<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware([
    'open.auth.permission',
])->namespace('Open')->group(function () {
    // 接收预录入库单推送
    Route::post('pre-receivings', 'PreReceivingController@store');
    // 接收出库单推送
    Route::post('outbounds', 'OutboundController@store');
    // 接收订单状态更新
    Route::post('orders/show', 'OrderController@show');
    Route::post('orders/update-state', 'OrderController@updateState');
    Route::post('orders/update', 'OrderController@update');
    // 接收批量推送SPU
    Route::post('batch/products', 'ProductController@batchStore');
    // 接收批量推送SKU
    Route::post('batch/skus', 'ProductController@batchStoreSku');
    // 库存查询
    Route::post('query/stocks', 'StockController@index');
});




