<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('health', function () {
    return [
        'code' => 200,
        'message' => 'success',
    ];
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('Api')->group(function () {
    Route::post('login', 'UserController@login');
    // 图片上传
    Route::post('images', 'FileController@uploadImage');
    // 版本
    Route::get('versions', 'VersionController@index');
});

Route::middleware('auth:api')->namespace('Api')->group(function () {
    Route::post('logout', 'UserController@logout');
});

Route::middleware([
    'auth:api',
    'warehouse',
])->namespace('Api')->group(function () {
    // 收货
    Route::post('receivings', 'ReceivingController@storeReceiving');
    Route::get('pre-receivings/{pre_receiving_number}', 'ReceivingController@preReceivingShow');
    Route::get('pre-receivings/{pre_receiving_number}/products/{product_id}', 'ReceivingController@preReceivingProductShow');
    Route::post('receiving-logs', 'ReceivingController@storeReceivingLog');
    Route::post('sku-code-print-logs', 'ReceivingController@storeSkuCodePrintLog');
    Route::put('actual-receivings/{actual_receiving_id}', 'ReceivingController@updateActualReceiving');

    // 入库
    Route::post('putaway-tasks', 'PutawayController@storePutawayTask');
    Route::post('putaway-logs', 'PutawayController@storePutawayLog');
    Route::put('putaway-tasks/{putaway_task_id}', 'PutawayController@updatePutawayTask');

    // 拣货
    Route::get('picking-tasks', 'PickingController@pickingTaskLit');
    Route::get('picking-basket-available', 'PickingController@pickingBasketAvailable');
    Route::get('picking-tasks/{picking_task_id}', 'PickingController@pickingTaskShow');
    Route::put('picking-tasks/{picking_task_id}/baskets', 'PickingController@updatePickingTaskBaskets');
    Route::post('picking-tasks', 'PickingController@storePickingTask');
    Route::post('picking-logs', 'PickingController@storePickingLog');

    // 交接
    Route::get('logistics-companies', 'HandoverController@logisticsCompanies');
    Route::post('handovers', 'HandoverController@storeHandover');
    Route::get('logistics-numbers/check', 'HandoverController@checkLogisticsNumber');

    // 异常
    Route::post('abnormal-codes', 'AbnormalController@storeAbnormalCode');
    Route::post('abnormals', 'AbnormalController@store');

    // 库位库存查询
    Route::get('stocks', 'StockController@index');
});
