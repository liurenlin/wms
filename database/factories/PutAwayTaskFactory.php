<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\PutawayTask;
use Faker\Generator as Faker;

$factory->define(PutawayTask::class, function (Faker $faker) {
    return [
        //
        'warehouse_id' => $faker->randomNumber(),
        'task_number' => $faker->randomNumber(),
        'receiving_cart_code' => $faker->randomNumber(),
        'state' => $faker->randomElement([0, 1, 2, 3, 4, 5]),
        'user_id' => $faker->randomNumber(),
        'created_at' => $faker->dateTime()->getTimestamp(),
        'updated_at' => $faker->dateTime()->getTimestamp(),
    ];
});
