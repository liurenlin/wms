<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\OutboundFrozenItem;
use Faker\Generator as Faker;

$factory->define(OutboundFrozenItem::class, function (Faker $faker) {
    return [
        'outbound_id' => $faker->randomNumber(),
        'outbound_item_id' => $faker->randomNumber(),
        'sku_code' => $faker->isbn13,
        'frozen_bin' => $faker->isbn13,
        'frozen_quantity' => $faker->randomNumber(),
        'created_at' => $faker->dateTime()->getTimestamp(),
        'updated_at' => $faker->dateTime()->getTimestamp(),
    ];
});
