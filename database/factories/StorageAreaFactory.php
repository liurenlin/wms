<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\StorageArea;
use Faker\Generator as Faker;
use App\Consts\StorageTypeConst;
use Illuminate\Support\Str;

$factory->define(StorageArea::class, function (Faker $faker) {
    $codes = StorageTypeConst::getTypes();
    return [
        'storage_type' => $codes[random_int(0, count($codes) - 1)],
        'area_code' => Str::random(8),
    ];
});
