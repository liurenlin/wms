<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\PickingTaskBasket;
use Faker\Generator as Faker;

$factory->define(PickingTaskBasket::class, function (Faker $faker) {
    return [
        'picking_task_id' => $faker->randomNumber(),
        'outbound_number' => $faker->isbn13,
        'basket_code' => $faker->isbn13,
        'created_at' => $faker->dateTime()->getTimestamp(),
        'updated_at' => $faker->dateTime()->getTimestamp(),
    ];
});








