<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\StorageLocation;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(StorageLocation::class, function (Faker $faker) {
    return [
        'warehouse_id' => random_int(100000, 999999),
        'storage_type' => 1,
        'storage_area_code' => Str::random(7),
        'storage_location_code' => Str::random(20),
    ];
});
