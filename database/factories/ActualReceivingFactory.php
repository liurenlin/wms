<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ActualReceiving;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(ActualReceiving::class, function (Faker $faker) {
    return [
        'warehouse_id' => $faker->randomNumber(),
        'actual_receiving_number' => Str::random(10),
        'user_id' => $faker->randomNumber(),
        'state' => $faker->randomElement([0, 1, 2, 3, 4, 5]),
        'pre_receiving_id' => $faker->randomNumber(),
        'created_at' => $faker->dateTime()->getTimestamp(),
        'updated_at' => $faker->dateTime()->getTimestamp(),
    ];
});
