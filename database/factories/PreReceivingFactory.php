<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\PreReceiving;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(PreReceiving::class, function (Faker $faker) {
    return [
        //
        'warehouse_id' => $faker->randomNumber(),
        'pre_receiving_number' => $faker->unique()->text(60),
        'state' => $faker->randomElement([0, 1, 2, 3, 4, 5]),
        'third_party_order_type' => $faker->randomElement([0, 1, 2]),
        'third_party_order_number' => $faker->randomNumber(),
        'third_party_order_created_at' => $faker->dateTime()->getTimestamp(),
        'owner_id' => $faker->randomNumber(),
        'created_at' => $faker->dateTime()->getTimestamp(),
        'updated_at' => $faker->dateTime()->getTimestamp(),
    ];
});
