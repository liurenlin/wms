<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\AppVersion;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(AppVersion::class, function (Faker $faker) {
    return [
        'version' => $faker->randomNumber(),
        'download_url' => $faker->url,
        'message' => Str::random(10),
        'platform' => $faker->randomElement([1, 2]),
        'is_force' => $faker->randomElement([0, 1]),
        'created_at' => $faker->dateTime()->getTimestamp(),
        'updated_at' => $faker->dateTime()->getTimestamp(),
    ];
});
