<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ReceivingCart;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(ReceivingCart::class, function (Faker $faker) {
    return [
        'warehouse_id' => random_int(1, 9999),
        'cart_code' => Str::random(42),
    ];
});
