<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CheckTask;
use App\Models\CheckTaskItem;
use Faker\Generator as Faker;

$factory->define(CheckTask::class, function (Faker $faker) {
    return [
        'task_number' => $faker->isbn13,
        'warehouse_id' => $faker->randomNumber(),
        'picking_task_id' => $faker->randomNumber(),
        'basket_code' => $faker->isbn13,
        'state' => $faker->randomElement([0, 1, 2]),
        'user_id' => $faker->randomNumber(),
        'created_at' => $faker->dateTime()->getTimestamp(),
        'updated_at' => $faker->dateTime()->getTimestamp(),
    ];
});

$factory->define(CheckTaskItem::class, function (Faker $faker) {
    return [
        //
    ];
});
