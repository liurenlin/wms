<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\PickingTask;
use Faker\Generator as Faker;

$factory->define(PickingTask::class, function (Faker $faker) {
    return [
        'warehouse_id' => $faker->randomNumber(),
        'task_number' => $faker->isbn13,
        'user_id' => $faker->randomNumber(),
        'state' => $faker->randomElement([0, 1, 2]),
        'finished_at' => $faker->dateTime()->getTimestamp(),
        'created_at' => $faker->dateTime()->getTimestamp(),
        'updated_at' => $faker->dateTime()->getTimestamp(),
    ];
});








