<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Warehouse;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Warehouse::class, function (Faker $faker) {
    return [
        'warehouse_name' => Str::random(50),
        'warehouse_code' => Str::random(10),
        'country' => $faker->countryCode,
        'state' => $faker->state,
        'city' => $faker->city,
        'district' => Str::random(12),
        'address' => $faker->address,
        'cellphone' => $faker->phoneNumber,
        'manager' => $faker->name,
    ];
});
