<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Basket;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Basket::class, function (Faker $faker) {
    return [
        'warehouse_id' => random_int(1, 100),
        'basket_code' => Str::random(17),
    ];
});
