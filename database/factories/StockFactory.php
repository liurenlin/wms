<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Stock;
use Faker\Generator as Faker;

$factory->define(Stock::class, function (Faker $faker) {
    return [
        'warehouse_id' => $faker->randomNumber(),
        'sku_code' => $faker->unique()->isbn13,
        'storage_type' => $faker->randomElement([1, 2, 3, 4, 5, 6]),
        'bin' => $faker->randomNumber(),
        'quantity' => $faker->randomNumber(),
        'frozen_quantity' => $faker->randomNumber(),
        'unfrozen_quantity' => $faker->randomNumber(),
        'created_at' => $faker->dateTime()->getTimestamp(),
        'updated_at' => $faker->dateTime()->getTimestamp(),
    ];
});




