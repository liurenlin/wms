<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\OutboundItem;
use Faker\Generator as Faker;

$factory->define(OutboundItem::class, function (Faker $faker) {
    return [
        'outbound_id' => $faker->randomNumber(),
        'sku_code' => $faker->isbn13,
        'quantity' => $faker->randomNumber(),
        'created_at' => $faker->dateTime()->getTimestamp(),
        'updated_at' => $faker->dateTime()->getTimestamp(),
    ];
});
