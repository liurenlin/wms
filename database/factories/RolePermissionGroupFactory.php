<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\RolePermissionGroup;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(RolePermissionGroup::class, function (Faker $faker) {
    return [
        'role_id' => $faker->randomNumber(),
        'permission_group_id' => $faker->randomNumber(),
        'created_at' => $faker->dateTime()->getTimestamp(),
        'updated_at' => $faker->dateTime()->getTimestamp(),
    ];
});
