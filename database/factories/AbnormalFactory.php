<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Abnormal;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Abnormal::class, function (Faker $faker) {
    return [
        'warehouse_id' => $faker->randomNumber(),
        'abnormal_number' => Str::random(10),
        'abnormal_type' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        'relation_type' => Str::random(10),
        'relation_number' => Str::random(10),
        'abnormal_code' => Str::random(12),
        'reason' => $faker->title,
        'state' => $faker->randomElement([0, 1, 2, 3, 4, 5]),
        'image' => $faker->randomElement([0, 1, 2, 3, 4, 5]),
        'report_user_id' => $faker->randomNumber(),
        'created_at' => $faker->dateTime()->getTimestamp(),
        'updated_at' => $faker->dateTime()->getTimestamp(),
    ];
});
