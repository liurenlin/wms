<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Weigh;
use Faker\Generator as Faker;

$factory->define(Weigh::class, function (Faker $faker) {
    return [
        'warehouse_id' => $faker->randomNumber(),
        'picking_task_id' => $faker->randomNumber(),
        'basket_code' => $faker->isbn13,
        'outbound_number' => $faker->isbn13,
        'weight' => $faker->randomFloat(2, 0, 100000),
        'logistics_company_id' => $faker->randomNumber(),
        'logistics_number' => $faker->isbn13,
        'user_id' => $faker->randomNumber(),
        'created_at' => $faker->dateTime()->getTimestamp(),
        'updated_at' => $faker->dateTime()->getTimestamp(),
    ];
});
