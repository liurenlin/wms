<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Outbound;
use Faker\Generator as Faker;

$factory->define(Outbound::class, function (Faker $faker) {
    return [
        'warehouse_id' => $faker->randomNumber(),
        'outbound_number' => $faker->unique()->randomNumber(),
        'outbound_type' => $faker->randomElement([0, 1, 2, 3, 4, 5]),
        'order_number' => $faker->isbn13,
        'order_created_at' => $faker->dateTime()->getTimestamp(),
        'currency' => $faker->randomElement(['php']),
        'total_amount' => $faker->randomFloat(2),
        'freight' => $faker->randomFloat(2),
        'receiving_country' => $faker->state,
        'receiving_region' => $faker->state,
        'receiving_state' => $faker->state,
        'receiving_city' => $faker->city,
        'receiving_district' => $faker->city,
        'receiving_address' => $faker->address,
        'receiving_postcode' => $faker->postcode,
        'receiving_cellphone' => $faker->phoneNumber,
        'receiving_user_name' => $faker->name,
        'receiving_first_name' => $faker->name,
        'receiving_last_name' => $faker->name,
        'logistics_company_id' => $faker->randomNumber(),
        'logistics_number' => $faker->isbn13,
        'is_need_intercept' => $faker->randomElement([0, 1]),
        'state' => $faker->randomElement([0, 1, 2, 3]),
        'created_at' => $faker->dateTime()->getTimestamp(),
        'updated_at' => $faker->dateTime()->getTimestamp(),
    ];
});
