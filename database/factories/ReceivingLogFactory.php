<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ReceivingLog;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(ReceivingLog::class, function (Faker $faker) {
    return [
        //
        'warehouse_id' => $faker->randomNumber(),
        'pre_receiving_id' => $faker->randomNumber(),
        'receiving_cart_code' => Str::random(12),
        'sku_code' => $faker->unique()->isbn13,
        'user_id' => $faker->randomNumber(),
        'receiving_cart_item_id' => $faker->randomNumber(),
        'created_at' => $faker->dateTime()->getTimestamp(),
        'updated_at' => $faker->dateTime()->getTimestamp(),
    ];
});
