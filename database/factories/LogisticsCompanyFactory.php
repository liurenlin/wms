<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\LogisticsCompany;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(LogisticsCompany::class, function (Faker $faker) {
    return [
        'company_name' => Str::random(54),
        'company_code' => Str::random(38),
        'contact' => $faker->name,
        'sender' => $faker->name,
        'cellphone' => $faker->phoneNumber,
        'telephone' => $faker->phoneNumber,
        'fax' => Str::random(23),
        'address' => $faker->address,
    ];
});
