<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Warehouse;
use App\Models\Resource;
use App\Models\ReceivingCart;
use App\Models\StorageLocation;
use App\Models\LogisticsCompany;
use App\Models\Basket;
use Faker\Generator as Faker;

$factory->define(Warehouse::class, function (Faker $faker) {
    return [
        'warehouse_name' => Str::random(50),
        'warehouse_code' => Str::random(10),
        'country' => $faker->countryCode,
        'state' => $faker->state,
        'city' => $faker->city,
        'district' => Str::random(12),
        'address' => $faker->address,
        'cellphone' => $faker->phoneNumber,
        'manager' => $faker->name,
        'deleted_at' => null,
        'created_at' => $faker->dateTime()->getTimestamp(),
        'updated_at' => $faker->dateTime()->getTimestamp(),
    ];
});

$factory->define(ReceivingCart::class, function (Faker $faker) {
    return [
        'cart_code' => $faker->unique()->text(60),
        'created_at' => $faker->dateTime()->getTimestamp(),
        'updated_at' => $faker->dateTime()->getTimestamp(),
    ];
});

$factory->define(Basket::class, function (Faker $faker) {
    return [
        'warehouse_id' => $faker->randomNumber(),
        'basket_code' => $faker->isbn13,
        'type' => $faker->randomElement([0, 1]),
        'state' => $faker->randomElement([0, 1]),
        'created_at' => $faker->dateTime()->getTimestamp(),
        'updated_at' => $faker->dateTime()->getTimestamp(),
    ];
});

$factory->define(Resource::class, function (Faker $faker) {
    return [
        'resource_name' => $faker->name,
        'url' => $faker->imageUrl(),
        'extro_info' => '',
        'created_at' => $faker->dateTime()->getTimestamp(),
        'updated_at' => $faker->dateTime()->getTimestamp(),
    ];
});

$factory->define(StorageLocation::class, function (Faker $faker) {
    return [
        'warehouse_id' => $faker->randomNumber(),
        'storage_type' => $faker->randomElement([1, 2, 3, 4]),
        'storage_area_code' => $faker->randomElement(['AC01', 'AC02', 'AC03']),
        'storage_location_code' => $faker->unique()->randomNumber(),
        'created_at' => $faker->dateTime()->getTimestamp(),
        'updated_at' => $faker->dateTime()->getTimestamp(),
    ];
});


$factory->define(LogisticsCompany::class, function (Faker $faker) {
    return [
        'company_name' => $faker->name,
        'cellphone' => $faker->phoneNumber,
        'telephone' => $faker->phoneNumber,
        'fax' => $faker->phoneNumber,
        'address' => $faker->address,
        'created_at' => $faker->dateTime()->getTimestamp(),
        'updated_at' => $faker->dateTime()->getTimestamp(),
    ];
});
