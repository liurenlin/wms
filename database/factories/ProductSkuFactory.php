<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ProductSku;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(ProductSku::class, function (Faker $faker) {
    return [
        'product_id' => random_int(100000, 9999999),
        'sku_code' => Str::random(13),
        'sku_name' => Str::random(48),
        'sku_attribute' => Str::random(48),
        'weight' => random_int(1, 100),
        'width' => random_int(1, 100),
        'height' => random_int(1, 100),
        'length' => random_int(1, 100),
    ];
});
