<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use App\Models\ProductSku;
use App\Models\ProductImage;
use App\Models\ProductSkuImage;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'product_name' => $faker->name,
        'product_code' => $faker->isbn13,
        'created_at' => $faker->dateTime()->getTimestamp(),
        'updated_at' => $faker->dateTime()->getTimestamp(),
    ];
});

$factory->define(ProductSku::class, function (Faker $faker) {
    return [
        'product_id' => $faker->randomNumber(),
        'sku_code' => $faker->isbn13,
        'sku_name' => $faker->name,
        'sku_attribute' => $faker->word,
        'weight' => $faker->randomFloat(2, 1, 200),
        'width' => $faker->randomFloat(2, 1, 200),
        'height' => $faker->randomFloat(2, 1, 200),
        'length' => $faker->randomFloat(2, 1, 200),
        'created_at' => $faker->dateTime()->getTimestamp(),
        'updated_at' => $faker->dateTime()->getTimestamp(),
    ];
});

$factory->define(ProductImage::class, function (Faker $faker) {
    return [
        'product_id' => $faker->randomNumber(),
        'resource_id' => $faker->randomNumber(),
        'created_at' => $faker->dateTime()->getTimestamp(),
        'updated_at' => $faker->dateTime()->getTimestamp(),
    ];
});

$factory->define(ProductSkuImage::class, function (Faker $faker) {
    return [
        'product_sku_id' => $faker->randomNumber(),
        'resource_id' => $faker->randomNumber(),
        'created_at' => $faker->dateTime()->getTimestamp(),
        'updated_at' => $faker->dateTime()->getTimestamp(),
    ];
});


