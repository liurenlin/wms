<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\PickingTaskItem;
use Faker\Generator as Faker;

$factory->define(PickingTaskItem::class, function (Faker $faker) {
    return [
        'picking_task_basket_id' => $faker->randomNumber(),
        'sku_code' => $faker->isbn13,
        'suggest_bin' => $faker->isbn13,
        'quantity' => $faker->randomNumber(),
        'picking_quantity' => $faker->randomNumber(),
        'state' => $faker->randomElement([0, 1]),
        'created_at' => $faker->dateTime()->getTimestamp(),
        'updated_at' => $faker->dateTime()->getTimestamp(),
    ];
});








