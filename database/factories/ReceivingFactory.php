<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\PreReceivingItem;
use App\Models\ActualReceiving;
use App\Models\ReceivingCartItem;
use Faker\Generator as Faker;

$factory->define(PreReceivingItem::class, function (Faker $faker) {
    return [
        'pre_receiving_id' => $faker->unique()->randomNumber(),
        'sku_code' => $faker->randomNumber(),
        'quantity' => $faker->randomNumber(),
        'created_at' => $faker->dateTime()->getTimestamp(),
        'updated_at' => $faker->dateTime()->getTimestamp(),
    ];
});

$factory->define(ActualReceiving::class, function (Faker $faker) {
    return [
        'warehouse_id' => $faker->randomNumber(),
        'actual_receiving_number' => $faker->unique()->text(60),
        'pre_receiving_id' => $faker->randomNumber(),
        'state' => $faker->randomElement([0, 1, 2]),
        'created_at' => $faker->dateTime()->getTimestamp(),
        'updated_at' => $faker->dateTime()->getTimestamp(),
    ];
});

$factory->define(ReceivingCartItem::class, function (Faker $faker) {
    return [
        'warehouse_id' => $faker->randomNumber(),
        'receiving_cart_id' => $faker->randomNumber(),
        'sku_code' => $faker->randomNumber(),
        'quantity' => $faker->randomNumber(),
        'created_at' => $faker->dateTime()->getTimestamp(),
        'updated_at' => $faker->dateTime()->getTimestamp(),
    ];
});

