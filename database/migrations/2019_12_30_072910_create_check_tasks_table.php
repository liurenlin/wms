<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCheckTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('check_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('task_number', 60)->unique()->default('');
            $table->integer('warehouse_id')->unsigned()->default(0);
            $table->integer('picking_task_id')->unsigned()->default(0);
            $table->string('basket_code', 20)->default('');
            $table->string('outbound_number', 60)->default('');
            $table->tinyInteger('state')->default(0)
                ->comment('check state 0:default 1:created 2:normal 3:abnormal');
            $table->integer('user_id')->unsigned()->default(0)->comment('operator id');
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);

            $table->index('warehouse_id');
            $table->index('picking_task_id');
            $table->index('basket_code');
            $table->index('user_id');
            $table->index('outbound_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('check_tasks');
    }
}
