<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOutboundHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outbound_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('outbound_id')->unsigned()->default(0);
            $table->text('info');
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);

            $table->index('outbound_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outbound_histories');
    }
}
