<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSkuCodePrintLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sku_code_print_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('warehouse_id')->unsigned()->default(0);
            $table->integer('pre_receiving_id')->unsigned()->default(0);
            $table->string('sku_code', 20)->default('');
            $table->integer('quantity')->default(0);
            $table->integer('user_id')->unsigned()->default(0)->comment('operator id');
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);

            $table->index('warehouse_id');
            $table->index('pre_receiving_id');
            $table->index('sku_code');
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sku_code_print_logs');
    }
}
