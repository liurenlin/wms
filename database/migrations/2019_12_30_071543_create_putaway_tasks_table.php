<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePutawayTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('putaway_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('warehouse_id')->unsigned()->default(0);
            $table->string('task_number', 60)->default('');
            $table->tinyInteger('state')->default(0)->comment('state: 0:default 1:create task 2: task finished');
            $table->string('receiving_cart_code', 60)->default('');
            $table->integer('user_id')->unsigned()->default(0)->comment('operator id');
            $table->integer('finished_at')->nullable();
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);

            $table->index('warehouse_id');
            $table->index('task_number');
            $table->index('receiving_cart_code');
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('putaway_tasks');
    }
}
