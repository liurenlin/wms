<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpenPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('open_permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('permission_name', 60)->default('');
            $table->string('route_name', 30)->default('');
            $table->string('route_path', 100)->default('');
            $table->string('request_method', 10)->default('')->comment('request method(GET,PUT,POST,DELETE...)');
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);

            $table->index('permission_name');
            $table->index(['route_name', 'route_path', 'request_method']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('open_permissions');
    }
}
