<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeighsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weighs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('warehouse_id')->unsigned()->default(0);
            $table->integer('picking_task_id')->unsigned()->default(0);
            $table->string('basket_code', 20)->default('');
            $table->string('outbound_number', 60)->default('');
            $table->decimal('weight', 10, 2)->default(0);
            $table->integer('logistics_company_id')->default(0);
            $table->string('logistics_number', 60)->default('');
            $table->integer('user_id')->unsigned()->default(0)->comment('weigh operator id');
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);

            $table->index('warehouse_id');
            $table->index(['picking_task_id', 'basket_code']);
            $table->index('logistics_company_id');
            $table->index('outbound_number');
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weighs');
    }
}
