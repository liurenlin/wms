<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePickingTaskBasketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('picking_task_baskets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('picking_task_id')->unsigned()->default(0);
            $table->string('outbound_number', 60)->default('');
            $table->string('basket_code', 20)->default('');
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);

            $table->index('picking_task_id');
            $table->index('outbound_number');
            $table->index('basket_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('picking_task_baskets');
    }
}
