<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOperationLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operation_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->default(0);
            $table->string('route_name', 255)->default('');
            $table->string('path', 255)->default('');
            $table->string('method', 10)->default('');
            $table->string('ip', 30)->default('');
            $table->string('user_agent', 255)->default('');
            $table->string('headers', 2000)->default('');
            $table->string('input', 2000)->default('');
            $table->string('query_string', 2000)->default('');
            $table->text('response_body')->nullable();
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);

            $table->index('user_id');
            $table->index('route_name');
            $table->index('path');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operation_logs');
    }
}
