<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('warehouse_id')->unsigned()->default(0);
            $table->string('sku_code', 20)->default('');
            $table->tinyInteger('storage_type')->default(0)
                ->comment('storage type: 1:receiving, 2:picking, 3:packing, 4:shipping, 5:abnormal, 6:defective');
            $table->string('bin', 20)->default('');
            $table->integer('quantity')->default(0);
            $table->integer('frozen_quantity')->default(0);
            $table->integer('unfrozen_quantity')->default(0);
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);

            $table->index('warehouse_id');
            $table->index('sku_code');
            $table->index('bin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stocks');
    }
}
