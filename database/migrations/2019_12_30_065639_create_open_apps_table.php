<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpenAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('open_apps', function (Blueprint $table) {
            $table->increments('id');
            $table->string('app_name', 100)->default('');
            $table->string('app_id', 20)->default('');
            $table->string('app_secret', 64)->default('');
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);

            $table->index('app_name');
            $table->index('app_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('open_apps');
    }
}
