<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHandoversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('handovers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('handover_number', 60)->default('');
            $table->integer('logistics_company_id')->unsigned()->default(0);
            $table->string('signature', 300)->default('');
            $table->integer('user_id')->unsigned()->default(0)->comment('handover operator id');
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);

            $table->index('handover_number');
            $table->index('logistics_company_id');
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('handovers');
    }
}
