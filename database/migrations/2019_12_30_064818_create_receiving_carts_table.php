<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReceivingCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receiving_carts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('warehouse_id')->unsigned()->default(0);
            $table->string('cart_code', 60)->default('');
            $table->tinyInteger('state')->default(0)->comment('state: 0:free 1:busy');
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);
            $table->integer('deleted_at')->nullable()->default(null);

            $table->index('warehouse_id');
            $table->index('cart_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receiving_carts');
    }
}
