<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReceivingLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receiving_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('warehouse_id')->unsigned()->default(0);
            $table->integer('pre_receiving_id')->unsigned()->default(0);
            $table->integer('actual_receiving_id')->unsigned()->default(0);
            $table->string('receiving_cart_code', 60)->default('');
            $table->string('sku_code', 20)->default('');
            $table->integer('quantity')->unsigned()->default(0);
            $table->integer('user_id')->unsigned()->default(0)->comment('receiving operator id');
            $table->integer('receiving_cart_item_id')->default(0);
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);

            $table->index('warehouse_id');
            $table->index('pre_receiving_id');
            $table->index('actual_receiving_id');
            $table->index('receiving_cart_code');
            $table->index('sku_code');
            $table->index('user_id');
            $table->index('receiving_cart_item_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receiving_logs');
    }
}
