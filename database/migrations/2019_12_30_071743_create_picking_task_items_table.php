<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePickingTaskItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('picking_task_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('picking_task_basket_id')->unsigned()->default(0);
            $table->string('sku_code', 20)->default('');
            $table->string('suggest_bin', 20)->default('');
            $table->integer('quantity')->default(0);
            $table->integer('picking_quantity')->default(0);
            $table->tinyInteger('state')->default(0)
                ->comment('picking state 0:unstarted 1:normal 2:abnormal');
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);

            $table->index('picking_task_basket_id');
            $table->index('sku_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('picking_task_items');
    }
}
