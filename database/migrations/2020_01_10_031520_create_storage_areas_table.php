<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStorageAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('storage_areas', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('storage_type')->default(0)
                ->comment('storage type: 1:receiving, 2:picking, 3:packing, 4:shipping, 5:abnormal, 6:defective');
            $table->string('area_code', 10)->default('');
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);
            $table->integer('deleted_at')->nullable()->default(null);

            $table->index('area_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('storage_areas');
    }
}
