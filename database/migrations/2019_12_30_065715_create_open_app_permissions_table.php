<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpenAppPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('open_app_permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('open_app_id')->unsigned()->default(0);
            $table->integer('open_permission_id')->unsigned()->default(0);
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);

            $table->index('open_app_id');
            $table->index('open_permission_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('open_app_permissions');
    }
}
