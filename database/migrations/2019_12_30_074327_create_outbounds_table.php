<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOutboundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outbounds', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('warehouse_id')->unsigned()->default(0);
            $table->string('outbound_number', 60)->default('');
            $table->tinyInteger('outbound_type')->default(0)
                ->comment('outbound type: 1:order');
            $table->string('order_number', 60)->default('');
            $table->integer('order_created_at')->default(0);
            $table->string('currency', 10)->default('');
            $table->decimal('total_amount', 10, 0)->default(0);
            $table->decimal('freight', 10, 0)->default(0);
            $table->string('receiving_country', 30)->default('');
            $table->string('receiving_region', 30)->default('');
            $table->string('receiving_state', 30)->default('');
            $table->string('receiving_city', 30)->default('');
            $table->string('receiving_district', 30)->default('');
            $table->string('receiving_address', 1000)->default('');
            $table->string('receiving_postcode', 30)->default('');
            $table->string('receiving_cellphone', 30)->default('');
            $table->string('receiving_user_name', 100)->default('');
            $table->string('receiving_first_name', 100)->default('');
            $table->string('receiving_last_name', 100)->default('');
            $table->integer('logistics_company_id')->unsigned()->default(0);
            $table->string('logistics_number', 60)->default('');
            $table->tinyInteger('is_need_intercept')->default(0);
            $table->tinyInteger('state')->default(0)->comment('state: 0:default 1:created 2:frozen 3:cancelled 4:picking task generated 5:start picking 6:picking abnormal 7:picking completed 8:checking abnormal 9:checking completed 10:weighed 11:intercepted 12:handover');
            $table->integer('deleted_at')->nullable();
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);

            $table->index('warehouse_id');
            $table->index('outbound_number');
            $table->index('order_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outbounds');
    }
}
