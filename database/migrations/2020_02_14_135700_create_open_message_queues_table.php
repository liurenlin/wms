<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpenMessageQueuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('open_message_queues', function (Blueprint $table) {
            $table->increments('id');
            $table->string('message_id', 60)->default('');
            $table->tinyInteger('state')->default(0)->comment('state 0:wait 1:done 2:error');
            $table->string('from_app_id', 60)->default('');
            $table->string('message_type', 60)->default('');
            $table->text('data')->nullable();
            $table->string('attributes', 5000)->default('');
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('open_message_queues');
    }
}
