<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_versions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('version', 30)->default('');
            $table->string('download_url', 500)->default('');
            $table->string('message', 1000)->default('');
            $table->tinyInteger('platform')->default(0)->comment('platform 1:ios 2:android');
            $table->tinyInteger('is_force')->default(0)->comment('is_force_to_upgrade 0:false 1:true');
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_versions');
    }
}
