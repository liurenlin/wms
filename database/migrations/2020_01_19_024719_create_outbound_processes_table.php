<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOutboundProcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outbound_processes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('outbound_id')->default(0);
            $table->integer('state_before')->default(0);
            $table->integer('state_after')->default(0);
            $table->string('content', 2000)->default('');
            $table->integer('user_id')->default(0);
            $table->integer('deleted_at')->nullable();
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);

            $table->index('outbound_id');
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outbound_processes');
    }
}
