<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOutboundItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outbound_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('outbound_id')->unsigned()->default(0);
            $table->string('sku_code', 20)->default('');
            $table->integer('quantity')->default(0);
            $table->tinyInteger('is_frozen')->default(0);
            $table->integer('deleted_at')->nullable();
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);

            $table->index('outbound_id');
            $table->index('sku_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outbound_items');
    }
}
