<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogisticsCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logistics_companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name', 60)->default('');
            $table->string('company_code', 100)->default('');
            $table->string('company_type', 50)->nullable(false)->default('')
                ->comment('company type');
            $table->string('contact', 100)->default('');
            $table->string('sender', 100)->default('');
            $table->string('cellphone', 30)->default('');
            $table->string('telephone', 30)->default('');
            $table->string('fax', 30)->default('');
            $table->string('address', 200)->default('');
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);
            $table->integer('deleted_at')->nullable()->default(null);

            $table->index('company_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logistics_companies');
    }
}
