<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreReceivingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_receivings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('warehouse_id')->unsigned()->default(0);
            $table->string('pre_receiving_number', 60)->default('');
            $table->tinyInteger('state')->default(0)
                ->comment('state: 0:default 1:created 2:actual receiving created 3:finished');
            $table->tinyInteger('third_party_order_type')->default(0)->comment('type: 0:default 1:Purchase Order 2:Abnormal Order 3:RTS package');
            $table->string('third_party_order_number', 60)->default('');
            $table->integer('third_party_order_created_at')->default(0);
            $table->integer('owner_id')->unsigned()->default(0);
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);

            $table->index('warehouse_id');
            $table->index('pre_receiving_number');
            $table->index('third_party_order_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_receivings');
    }
}
