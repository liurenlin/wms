<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockDealWithLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_deal_with_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('warehouse_id')->unsigned()->default(0);
            $table->string('storage_location_code', 30)->default('');
            $table->string('sku_code', 20)->default('');
            $table->integer('inventory_before')->unsigned()->default(0);
            $table->integer('inventory_after')->unsigned()->default(0);
            $table->integer('user_id')->unsigned()->default(0);
            $table->string('remark', 1000)->default('');
            $table->string('abnormal_number', 60)->default('');
            $table->tinyInteger('deal_with_type')->default(0)->comment('deal_with_type: 0:default 1:inventory more 2:inventory less 3:picking less 4:check broken');

            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_deal_with_logs');
    }
}
