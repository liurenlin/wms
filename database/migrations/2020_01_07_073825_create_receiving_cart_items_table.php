<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReceivingCartItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receiving_cart_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('warehouse_id')->unsigned()->default(0);
            $table->integer('receiving_cart_id')->unsigned()->default(0);
            $table->string('sku_code', 20)->default('');
            $table->integer('quantity')->default(0);
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);

            $table->index('warehouse_id');
            $table->index('sku_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receiving_cart_items');
    }
}
