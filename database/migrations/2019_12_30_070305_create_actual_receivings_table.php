<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActualReceivingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actual_receivings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('warehouse_id')->unsigned()->default(0);
            $table->string('actual_receiving_number', 60)->default('');
            $table->integer('pre_receiving_id')->unsigned()->default(0);
            $table->tinyInteger('state')->default(0)
                ->comment('state: 0:default 1:created 2:completed');
            $table->integer('user_id')->unsigned()->default(0);
            $table->integer('finished_at')->nullable();
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);

            $table->index('warehouse_id');
            $table->index('actual_receiving_number');
            $table->index('pre_receiving_id');
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actual_receivings');
    }
}
