<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePickingTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('picking_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('warehouse_id')->unsigned()->default(0);
            $table->string('task_number', 60)->default('');
            $table->integer('user_id')->unsigned()->default(0)->comment('operator id');
            $table->tinyInteger('task_type')->default(0)->comment('task_type: 0:default 1:PDA 2:Paper');
            $table->integer('finished_at')->nullable();
            $table->tinyInteger('state')->default(0)->comment('state: 0:default 1:created 2:in progress 3:done');
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);

            $table->index('warehouse_id');
            $table->index('task_number');
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('picking_tasks');
    }
}
