<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAbnormalItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abnormal_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('abnormal_id')->unsigned()->default(0);
            $table->string('bin', 20)->default('');
            $table->string('sku_code', 20)->default('');
            $table->integer('quantity')->default(0);
            $table->integer('missing_quantity')->default(0);
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);

            $table->index('abnormal_id');
            $table->index('sku_code');
            $table->index('bin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abnormal_items');
    }
}
