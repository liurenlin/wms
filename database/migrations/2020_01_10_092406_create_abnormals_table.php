<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAbnormalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abnormals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('warehouse_id')->unsigned()->default(0);
            $table->string('abnormal_number', 60)->default('');
            $table->tinyInteger('abnormal_type')->default(0)
                ->comment('abnormal_type 1:Receiving Extra 2:Receiving Defective 3:Receiving Wrong 4:Putaway Extra 5:Putaway Missing 6:Putaway Wrong 7:Picking Missing 8:Checking Defective 9:Checking Failed 10:Order Intercepted');
            $table->string('relation_type', 30)->default('')
                ->comment('relation_type: pre_receiving,receiving,actual_receiving,putaway,picking,check,weigh,handover,inventory');
            $table->string('relation_number', 60)->default('');
            $table->string('abnormal_code', 60)->default('');
            $table->string('pre_receiving_number', 60)->default('');
            $table->string('outbound_number', 60)->default('');
            $table->string('basket_code', 60)->default('');
            $table->string('reason', 100)->default('');
            $table->tinyInteger('state')->default(0)->comment('state 0:default 1:created 2:in progress 3:confirmed 4:closed 5:completed');
            $table->integer('image')->unsigned()->default(0);
            $table->integer('report_user_id')->unsigned()->default(0);
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);

            $table->index('warehouse_id');
            $table->index('abnormal_number');
            $table->index('relation_number');
            $table->index('abnormal_code');
            $table->index('report_user_id');
            $table->index('pre_receiving_number');
            $table->index('outbound_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abnormals');
    }
}
