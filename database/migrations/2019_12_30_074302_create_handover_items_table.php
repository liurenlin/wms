<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHandoverItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('handover_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('handover_id')->unsigned()->default(0);
            $table->string('logistics_number', 60)->default('');
            $table->decimal('weight', 10, 2)->default(0);
            $table->integer('quantity')->default(0);
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);

            $table->index('handover_id');
            $table->index('logistics_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('handover_items');
    }
}
