<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActualReceivingItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actual_receiving_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('actual_receiving_id')->unsigned()->default(0);
            $table->string('sku_code', 20)->default('');
            $table->integer('receiving_quantity')->default(0);
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);

            $table->index('actual_receiving_id');
            $table->index('sku_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actual_receiving_items');
    }
}
