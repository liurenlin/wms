<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOutboundRequestLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outbound_request_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('trace_id', 100)->default('');
            $table->string('client', 255)->default('');
            $table->string('uri', 255)->default('');
            $table->string('method', 10)->default('');
            $table->string('headers', 2000)->default('');
            $table->string('input', 5000)->default('');
            $table->string('query_string', 2000)->default('');
            $table->text('response_body')->nullable();
            $table->tinyInteger('state')->default(0)->comment('state 1:success 2:fail 3:no response 4:abandoned');
            $table->tinyInteger('retry_time')->default(0);
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);

            $table->index('trace_id');
            $table->index('client');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outbound_request_logs');
    }
}
