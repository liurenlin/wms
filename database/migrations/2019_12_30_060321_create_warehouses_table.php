<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('warehouse_name', 100)->default('');
            $table->string('warehouse_code', 50)->default('')->comment('warehouse code');
            $table->string('country', 60)->default('');
            $table->string('state', 60)->default('');
            $table->string('city', 60)->default('');
            $table->string('district', 60)->default('');
            $table->string('address', 200)->default('');
            $table->string('cellphone', 30)->default('');
            $table->string('manager', 60)->default('');
            $table->integer('deleted_at')->nullable();
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);

            $table->index('warehouse_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouses');
    }
}
