<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCheckTaskItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('check_task_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('check_task_id')->unsigned()->default(0);
            $table->string('sku_code', 20)->default('');
            $table->integer('quantity')->default(0);
            $table->integer('check_quantity')->default(0);
            $table->tinyInteger('state')->default(0)
                ->comment('check state 0:defualt 1:normal 2:abnormal');
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);

            $table->index('check_task_id');
            $table->index('sku_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('check_task_items');
    }
}
