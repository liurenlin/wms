<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStorageLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('storage_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('warehouse_id')->unsigned()->default(0);
            $table->tinyInteger('storage_type')->default(0)
                ->comment('storage type: 1:receiving, 2:picking, 3:packing, 4:shipping, 5:abnormal, 6:defective');
            $table->string('storage_area_code', 10)->default('');
            $table->string('storage_location_code', 30)->default('');
            $table->tinyInteger('is_empty')->default(0)->comment('is empty 0:not empty 1:empty');
            $table->tinyInteger('is_suggest_lock')->default(0)->comment('is suggest lock 0:not lock 1:lock');
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);
            $table->integer('deleted_at')->nullable()->default(null);

            $table->index('warehouse_id');
            $table->index('storage_area_code');
            $table->index('storage_location_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('storage_locations');
    }
}
