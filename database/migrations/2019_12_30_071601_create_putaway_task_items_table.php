<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePutawayTaskItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('putaway_task_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('putaway_task_id')->default(0);
            $table->string('sku_code', 20)->default('');
            $table->string('suggest_bin', 20)->default('');
            $table->integer('receiving_quantity')->default(0);
            $table->integer('putaway_quantity')->default(0);
            $table->string('putaway_bin', 20)->default('');
            $table->integer('created_at')->default(0);
            $table->integer('updated_at')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('putaway_task_items');
    }
}
