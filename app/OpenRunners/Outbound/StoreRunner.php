<?php

namespace App\OpenRunners\Outbound;

use App\Models\OpenMessageQueue;
use App\Models\Outbound;
use App\OpenRunners\OpenRunner;
use App\OpenRunners\Runner;
use App\Repositories\OutboundRepository;
use App\Services\MessageService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StoreRunner extends Runner implements OpenRunner
{
    public $outboundRepository;

    public function __construct(OutboundRepository $outboundRepository)
    {
        $this->outboundRepository = $outboundRepository;
    }

    public function run(Request $request): JsonResponse
    {
        // 检查是否已经同步过
        $warehouseId = (int) $request->get('warehouse_id');
        $orderNumber = $request->get('order_number');
        $outbounds = $this->outboundRepository->findWhere([
            'order_number' => $orderNumber,
        ]);
        if ($outbounds->isNotEmpty()) {
            return success([
                'outbound_number' => $outbounds->first()->outbound_number,
            ]);
        }

        // 检查items
        $items = json_decode($request->get('items'), JSON_OBJECT_AS_ARRAY);
        if (!$this->outboundRepository->checkOutboundItems($items)) {
            return fail(__('open.outbound_items_wrong'));
        }

        DB::beginTransaction();
        try {
            // 生成记录
            $outbound = $this->outboundRepository->createOutbound(
                $warehouseId,
                $request->only([
                    'outbound_type',
                    'order_number',
                    'order_created_at',
                    'currency',
                    'total_amount',
                    'freight',
                    'receiving_country',
                    'receiving_region',
                    'receiving_state',
                    'receiving_city',
                    'receiving_district',
                    'receiving_address',
                    'receiving_postcode',
                    'receiving_cellphone',
                    'receiving_user_name',
                    'receiving_first_name',
                    'receiving_last_name',
                    'logistics_company_id',
                    'logistics_number',
                ])
            );
            // 生成 items
            $this->outboundRepository->createItems($outbound->id, $items);
            // 生成 process
            $this->outboundRepository->createOutboundProcess(
                $outbound->id,
                Outbound::STATE_CREATED,
                0,
                0,
                'Outbound order number ' . $outbound->outbound_number
            );
            // 冻结库存
            $this->outboundRepository->frozenItems($outbound->id);
            // 更新出库单状态
            $this->outboundRepository->updateState($outbound->id, Outbound::STATE_FROZEN);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Open Store Outbound Fail', [
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
            return fail();
        }
        $outboundItems = $this->outboundRepository->getOutboundItems($outbound->id);
        $list = $outboundItems->transform(function ($item) {
            return [
                'sku_code' => $item->sku_code,
            ];

        })->toArray();

        MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_STOCK_NOTIFY, ['warehouse_id' => $warehouseId, 'order_number' => $outbound->order_number, 'list' => $list]);

        MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_ORDER_STATUS_NOTIFY, ['order_number' => $outbound->order_number]);

        return success([
            'outbound_number' => $outbound->outbound_number,
        ]);
    }
}
