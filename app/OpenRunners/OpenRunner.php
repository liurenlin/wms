<?php

namespace App\OpenRunners;

use Illuminate\Http\Request;

interface OpenRunner
{

    public function run(Request $request);
}
