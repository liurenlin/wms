<?php

namespace App\OpenRunners\Stock;

use App\OpenRunners\OpenRunner;
use App\OpenRunners\Runner;
use App\Repositories\StockRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class IndexRunner extends Runner implements OpenRunner
{
    public $stockRepository;

    public function __construct(StockRepository $stockRepository)
    {
        $this->stockRepository = $stockRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $warehouseId = (int) $request->get('warehouse_id', 1);
        $skuCodes = $request->get('sku_codes');
        if (empty($skuCodes) || !$warehouseId) {
            return success(['list' => []]);
        }
        $skuCodes = explode(',', $skuCodes);
        $list = $this->stockRepository->getStocksBySkuCodes($warehouseId, $skuCodes);
        return success([
            'list' => $list,
        ]);
    }
}
