<?php

namespace App\OpenRunners\Product;

use App\OpenRunners\OpenRunner;
use App\OpenRunners\Runner;
use App\Repositories\ProductRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class BatchStoreRunner extends Runner implements OpenRunner
{
    public $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $products = json_decode($request->get('products'), JSON_OBJECT_AS_ARRAY);
        if (!$this->checkProducts($products)) {
            return fail(__('open.products_wrong'));
        }

        DB::beginTransaction();
        try {
            foreach ($products as $product) {
                $productTemp = $this->productRepository->createProduct($product['product_code'], $product['name'], $product['images'] ?? null);
                $this->productRepository->createProductSkus($productTemp->id, $product['skus']);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Batch Product Store Fail', [
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
            return fail();
        }
        return success(['count' => count($products)]);
    }

    private function checkProducts(array $products): bool
    {
        if (empty($products)) {
            return false;
        }
        foreach ($products as $product) {
            $validator = Validator::make($product, [
                'product_code' => 'required|string',
                'name' => 'required|string',
                'images' => 'nullable|array',
                'skus' => 'required|array',
            ]);
            if ($validator->fails()) {
                return false;
            }
            foreach ($product['skus'] as $sku) {
                $validator = Validator::make($sku, [
                    'sku_code' => 'required|string',
                    'name' => 'required|string',
                    'attribute' => 'required|string',
                    'weight' => 'required|numeric',
                    'height' => 'required|numeric',
                    'length' => 'required|numeric',
                    'width' => 'required|numeric',
                    'images' => 'nullable|array',
                ]);
                if ($validator->fails()) {
                    return false;
                }
            }
        }
        return true;
    }
}
