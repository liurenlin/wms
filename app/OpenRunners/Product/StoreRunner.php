<?php

namespace App\OpenRunners\Product;

use App\OpenRunners\OpenRunner;
use App\OpenRunners\Runner;
use App\Repositories\ProductRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class StoreRunner extends Runner implements OpenRunner
{
    public $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function run(Request $request): JsonResponse
    {
        // 检查是否已经同步过
        $productCode = $request->get('product_code');
        $productName = $request->get('name');
        $images = json_decode($request->get('images'), JSON_OBJECT_AS_ARRAY);
        $skus = json_decode($request->get('skus'), JSON_OBJECT_AS_ARRAY);

        // 校验SKU格式
        if (!$this->checkSkus($skus)) {
            return fail(__('open.skus_wrong'));
        }
        DB::beginTransaction();
        try {
            $product = $this->productRepository->createProduct($productCode, $productName, $images);
            $this->productRepository->createProductSkus($product->id, $skus);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Product Store Fail', [
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
            return fail();
        }
        return success(['id' => $product->id]);
    }

    private function checkSkus(array $skus): bool
    {
        if (empty($skus)) {
            return false;
        }
        foreach ($skus as $sku) {
            $validator = Validator::make($sku, [
                'sku_code' => 'required|string|unique:product_skus,sku_code',
                'name' => 'required|string',
                'attribute' => 'required|string',
                'weight' => 'required|numeric',
                'height' => 'required|numeric',
                'length' => 'required|numeric',
                'width' => 'required|numeric',
                'images' => 'required|array',
            ]);
            if ($validator->fails()) {
                return false;
            }
        }
        return true;
    }
}
