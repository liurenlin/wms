<?php

namespace App\OpenRunners\Product;

use App\OpenRunners\OpenRunner;
use App\OpenRunners\Runner;
use App\Repositories\ProductRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StoreSkuRunner extends Runner implements OpenRunner
{
    public $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function run(Request $request): JsonResponse
    {
        // 检查是否已经同步过
        $productCode = $request->get('product_code');
        $skuParams = $request->only([
            'sku_code',
            'name',
            'attribute',
            'weight',
            'height',
            'length',
            'width',
        ]);
        $images = json_decode($request->get('images'), JSON_OBJECT_AS_ARRAY);
        $skuParams = Arr::add($skuParams, 'images', $images);
        $product = $this->productRepository->getProductByCode($productCode);
        DB::beginTransaction();
        try {
            $productSku = $this->productRepository->createProductSku($product->id, $skuParams);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Sku Store Fail', [
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
            return fail();
        }
        return success(['id' => $productSku->id]);
    }
}
