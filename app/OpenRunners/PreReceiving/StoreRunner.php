<?php

namespace App\OpenRunners\PreReceiving;

use App\Models\OpenMessageQueue;
use App\OpenRunners\OpenRunner;
use App\OpenRunners\Runner;
use App\Repositories\PreReceivingRepository;
use App\Services\MessageService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StoreRunner extends Runner implements OpenRunner
{
    public $preReceivingRepository;

    public function __construct(PreReceivingRepository $preReceivingRepository)
    {
        $this->preReceivingRepository = $preReceivingRepository;
    }

    public function run(Request $request): JsonResponse
    {
        // 检查是否已经同步过
        $warehouseId = (int) $request->get('warehouse_id');
        $thirdPartyOrderType = (int) $request->get('third_party_order_type');
        $thirdPartyOrderNumber = $request->get('third_party_order_number');
        $thirdPartyOrderCreatedAt = $request->get('third_party_order_created_at');
        $preReceivings = $this->preReceivingRepository->findWhere([
            'third_party_order_type' => $thirdPartyOrderType,
            'third_party_order_number' => $thirdPartyOrderNumber,
        ]);
        if ($preReceivings->isNotEmpty()) {
            return success([
                'pre_receiving_number' => $preReceivings->first()->pre_receiving_number,
            ]);
        }

        // 检查items
        $items = json_decode($request->get('items'), JSON_OBJECT_AS_ARRAY);
        if (!$this->preReceivingRepository->checkPreReceivingItems($items)) {
            return fail(__('open.pre_receiving_items_wrong'));
        }

        DB::beginTransaction();
        try {
            // 生成记录
            $preReceiving = $this->preReceivingRepository->createPreReceiving(
                $warehouseId,
                $thirdPartyOrderType,
                $thirdPartyOrderNumber,
                $thirdPartyOrderCreatedAt
            );
            // 生成 items
            $this->preReceivingRepository->createItems($preReceiving->id, $items);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Open Store Pre-Receiving Fail', [
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
            return fail();
        }
        MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_PRE_RECEIVING_NOTIFY, ['pre_receiving_number' => $preReceiving->pre_receiving_number, 'third_party_order_number' => $thirdPartyOrderNumber]);
        return success([
            'pre_receiving_number' => $preReceiving->pre_receiving_number,
        ]);
    }
}
