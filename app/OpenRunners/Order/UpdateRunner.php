<?php

namespace App\OpenRunners\Order;

use App\Models\OpenMessageQueue;
use App\Models\Outbound;
use App\OpenRunners\OpenRunner;
use App\OpenRunners\Runner;
use App\Repositories\OutboundRepository;
use App\Services\MessageService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UpdateRunner extends Runner implements OpenRunner
{
    public $outboundRepository;

    public function __construct(OutboundRepository $outboundRepository)
    {
        $this->outboundRepository = $outboundRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $warehouseId = $request->get('warehouse_id');
        // 检查是否已经同步过
        $orderNumber = $request->get('order_number');
        $outbound = $this->outboundRepository->getOutboundByOrderNumber($warehouseId, $orderNumber);
        if (!$outbound) {
            return fail(__('open.order_not_exist'));
        }

        // 状态判断
        if (!in_array($outbound->state, [
                Outbound::STATE_CREATED,
                Outbound::STATE_FROZEN,
                Outbound::STATE_PICKING_ABNORMAL,
                Outbound::STATE_CHECK_ABNORMAL,
                Outbound::STATE_INTERCEPTED,
            ])) {
            return fail('the order is not allowed to be updated');
        }

        // 检查items
        $items = json_decode($request->get('items'), JSON_OBJECT_AS_ARRAY);
        if (!$this->outboundRepository->checkOutboundItems($items)) {
            return fail(__('open.outbound_items_wrong'));
        }

        DB::beginTransaction();
        try {
            $updateData = $request->only([
                'outbound_type',
                'order_number',
                'order_created_at',
                'currency',
                'total_amount',
                'freight',
                'receiving_country',
                'receiving_region',
                'receiving_state',
                'receiving_city',
                'receiving_district',
                'receiving_address',
                'receiving_postcode',
                'receiving_cellphone',
                'receiving_user_name',
                'receiving_first_name',
                'receiving_last_name',
                'logistics_company_id',
                'logistics_number',
            ]);
            $updateData['state'] = $outbound->state;
            if ($outbound->state == Outbound::STATE_FROZEN) {
                $updateData['state'] = Outbound::STATE_CREATED;
            }
            // 出库单备份
            $this->outboundRepository->storeHistory($outbound->id);
            // 更新
            $this->outboundRepository->updateOutbound($outbound->id, $updateData);
            // 更新 items
            $stockNotifyList = $this->outboundRepository->updateItems(
                $warehouseId,
                $outbound->id,
                $outbound->state,
                $items
            );
            if (in_array($outbound->state, [
                Outbound::STATE_CREATED,
                Outbound::STATE_FROZEN,
            ])) {
                // 重新冻结
                $this->outboundRepository->frozenItems($outbound->id);
                $this->outboundRepository->updateState($outbound->id, Outbound::STATE_FROZEN);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Open Order Update Fail', [
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
            return fail();
        }
        if (!empty($stockNotifyList)) {
            MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_STOCK_NOTIFY, ['warehouse_id' => $warehouseId, 'order_number' => $outbound->order_number, 'list' => $stockNotifyList]);
        }
        return success();
    }
}
