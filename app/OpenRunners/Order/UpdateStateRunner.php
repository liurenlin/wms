<?php

namespace App\OpenRunners\Order;

use App\Models\Outbound;
use App\OpenRunners\OpenRunner;
use App\OpenRunners\Runner;
use App\Repositories\OutboundRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UpdateStateRunner extends Runner implements OpenRunner
{
    public $outboundRepository;

    public function __construct(OutboundRepository $outboundRepository)
    {
        $this->outboundRepository = $outboundRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $warehouseId = (int) $request->get('warehouse_id');
        $state = (int) $request->get('state');
        // 检查是否已经同步过
        $orderNumber = $request->get('order_number');
        $outbound = $this->outboundRepository->getOutboundByOrderNumber($warehouseId, $orderNumber);
        if (!$outbound) {
            return fail('order not exist');
        }

        // 状态判断
        if (!in_array($outbound->state, [
            Outbound::STATE_CREATED,
            Outbound::STATE_FROZEN,
            Outbound::STATE_PICKING_ABNORMAL,
            Outbound::STATE_CHECK_ABNORMAL,
        ]) && $state != Outbound::STATE_INTERCEPTED) {
            return fail('the status of the order is wrong');
        }

        if ($state == Outbound::STATE_INTERCEPTED
            && !in_array($outbound->state, [
                Outbound::STATE_PICKING_TASK_CREATED,
                Outbound::STATE_PICKING_ING,
                Outbound::STATE_PICKING_ABNORMAL,
                Outbound::STATE_PICKING_COMPLETED,
                Outbound::STATE_CHECK_ABNORMAL,
                Outbound::STATE_CHECK_COMPLETED,
            ])
        ) {
            return fail('the status of the order is not allowed to be intercepted');
        }

        DB::beginTransaction();
        try {
            if ($state == Outbound::STATE_CANCELLED
                && in_array(
                    $outbound->state,
                    [
                        Outbound::STATE_CREATED,
                        Outbound::STATE_FROZEN,
                        Outbound::STATE_PICKING_ABNORMAL,
                        Outbound::STATE_CHECK_ABNORMAL,
                    ]
                )
            ) {
                $this->outboundRepository->updateState($outbound->id, $state);
                if (in_array($outbound->state, [Outbound::STATE_CREATED, Outbound::STATE_FROZEN])) {
                    // 释放冻结库存
                    $this->outboundRepository->releaseOutbound($outbound->id);
                }

            }
            if ($state == Outbound::STATE_INTERCEPTED
                && in_array($outbound->state, [
                    Outbound::STATE_PICKING_TASK_CREATED,
                    Outbound::STATE_PICKING_ING,
                    Outbound::STATE_PICKING_ABNORMAL,
                    Outbound::STATE_PICKING_COMPLETED,
                    Outbound::STATE_CHECK_ABNORMAL,
                    Outbound::STATE_CHECK_COMPLETED,
                ])
            ) {
                $this->outboundRepository->setIsNeedIntercept($outbound->id, Outbound::IS_NEED_INTERCEPT_TRUE);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Open Order UpdateState Fail', [
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
            return fail();
        }
        return success();
    }
}
