<?php

namespace App\OpenRunners\Order;

use App\Models\Outbound;
use App\OpenRunners\OpenRunner;
use App\OpenRunners\Runner;
use App\Repositories\OutboundRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ShowRunner extends Runner implements OpenRunner
{
    public $outboundRepository;

    public function __construct(OutboundRepository $outboundRepository)
    {
        $this->outboundRepository = $outboundRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $warehouseId = (int) $request->get('warehouse_id');
        // 检查是否已经同步过
        $orderNumber = (string) $request->get('order_number');
        $outbound = $this->outboundRepository->getOutboundByOrderNumber($warehouseId, $orderNumber);
        if (!$outbound) {
            return fail('order not exist');
        }
        return success([
            'state' => $outbound->state,
        ]);
    }
}
