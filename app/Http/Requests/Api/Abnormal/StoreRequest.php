<?php

namespace App\Http\Requests\Api\Abnormal;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'abnormal_code' => 'nullable|string|unique:abnormals,abnormal_code|exists:abnormal_codes,abnormal_code',
            'sku_code' => 'nullable|string|exists:product_skus,sku_code',
            'reason' => 'nullable|string',
            'relation_type' => 'required|string|in:pre_receiving,receiving,actual_receiving,putaway,picking,check,weigh,handover,inventory',
            'relation_number' => 'required|string',
            'bin' => 'nullable|string|exists:storage_locations,storage_location_code',
        ];
    }

    public function messages()
    {
        return [
            'abnormal_code.*' => 'Error! Wrong abnormal record #, print a new one. ',
        ];
    }
}
