<?php

namespace App\Http\Requests\Api\Putaway;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class StorePutawayLogRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'putaway_task_id' => 'required|integer|exists:putaway_tasks,id',
            'bin' => 'required|string|exists:storage_locations,storage_location_code',
            'receiving_cart_code' => 'required|string|exists:putaway_tasks,receiving_cart_code',
            'sku_code' => 'required|string|exists:putaway_task_items,sku_code',
            'quantity' => 'required|integer|min:1',
        ];
    }

    public function messages()
    {
        return [
            'bin.*' => 'Error! Bin is not recorded in the picking area.',
            'quantity.min' => 'Error! Qty should be more than 0',
        ];
    }
}
