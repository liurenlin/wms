<?php

namespace App\Http\Requests\Api\Putaway;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class StorePutawayTaskRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'receiving_cart_code' => 'required|string|exists:receiving_carts,cart_code',
        ];
    }
}
