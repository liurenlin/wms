<?php

namespace App\Http\Requests\Api\Receiving;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class StoreReceivingRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pre_receiving_number' => 'required|string|exists:pre_receivings,pre_receiving_number',
        ];
    }

    public function messages()
    {
        return [
            'pre_receiving_number.required' => 'Error! Fill in the blank',
            'pre_receiving_number.exists' => 'Error! Pre-receiving order doesn’t exist',
        ];
    }
}
