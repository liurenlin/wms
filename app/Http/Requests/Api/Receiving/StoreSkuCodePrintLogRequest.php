<?php

namespace App\Http\Requests\Api\Receiving;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class StoreSkuCodePrintLogRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pre_receiving_id' => 'required|integer|exists:pre_receivings,id',
            'sku_code' => 'required|string|exists:product_skus,sku_code',
        ];
    }
}
