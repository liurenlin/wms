<?php

namespace App\Http\Requests\Api\Receiving;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class StoreReceivingLogRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'actual_receiving_id' => 'required|integer|exists:actual_receivings,id',
            'pre_receiving_number' => 'required|string|exists:pre_receivings,pre_receiving_number',
            'sku_code' => 'required|string|exists:product_skus,sku_code',
            'quantity' => 'required|integer|min:1',
            'receiving_cart_code' => 'required|string|exists:receiving_carts,cart_code',
        ];
    }

    public function messages()
    {
        return [
            'pre_receiving_id.exists' => 'Error! Pre-receiving order doesn’t exist',
            'pre_receiving_number.exists' => 'Error! Pre-receiving order doesn’t exist',
        ];
    }
}
