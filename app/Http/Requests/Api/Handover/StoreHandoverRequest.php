<?php

namespace App\Http\Requests\Api\Handover;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class StoreHandoverRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'logistics_company_id' => 'required|integer|exists:logistics_companies,id',
            'logistics_numbers' => 'required|json',
            'signature' => 'required|url',
        ];
    }
}
