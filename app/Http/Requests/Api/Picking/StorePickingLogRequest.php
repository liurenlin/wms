<?php

namespace App\Http\Requests\Api\Picking;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class StorePickingLogRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'picking_task_item_id' => 'required|integer|exists:picking_task_items,id',
            'sku_code' => 'required|string|exists:picking_task_items,sku_code',
            'quantity' => 'required|integer',
            'bin' => 'required|string|exists:storage_locations,storage_location_code',
        ];
    }
}
