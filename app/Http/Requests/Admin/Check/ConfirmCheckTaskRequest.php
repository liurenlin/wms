<?php

namespace App\Http\Requests\Admin\Check;

use App\Http\Requests\ApiRequest;
use App\Models\CheckTask;
use Illuminate\Foundation\Http\FormRequest;

class ConfirmCheckTaskRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'state' => 'required|integer|in:' . CheckTask::STATE_NORMAL,
            'skus' => 'required|string|json',
        ];
    }
}
