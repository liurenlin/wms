<?php

namespace App\Http\Requests\Admin\Check;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class StoreCheckLogRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'check_task_id' => 'required|integer|exists:check_tasks,id',
            'sku_code' => 'required|string|exists:product_skus,sku_code',
            'quantity' => 'required|integer|min:1',
        ];
    }
}
