<?php

namespace App\Http\Requests\Admin\PickingTask;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class StoreAssignRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'picking_tasks' => 'nullable|json',
            'task_amount' => 'nullable|integer|min:1',
            'task_type' => 'required|integer|in:1,2',
            'user_id' => 'required|integer|exists:users,id',
        ];
    }
}
