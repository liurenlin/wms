<?php

namespace App\Http\Requests\Admin\PickingTask;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class UpdateTasksRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'picking_tasks' => 'required|json',
            'state' => 'required|integer|in:2,3',
        ];
    }
}
