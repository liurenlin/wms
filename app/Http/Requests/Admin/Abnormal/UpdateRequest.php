<?php

namespace App\Http\Requests\Admin\Abnormal;

use App\Http\Requests\ApiRequest;

class UpdateRequest extends ApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'state' => 'required|integer|in:0,1,2,3,4,5',
            'content' => 'required|string',
            'inbound_products' => 'nullable|in:0,1',
            'sku_code' => 'nullable|string|exists:product_skus,sku_code',
        ];
    }
}
