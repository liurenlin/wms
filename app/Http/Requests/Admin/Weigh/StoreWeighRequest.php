<?php

namespace App\Http\Requests\Admin\Weigh;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class StoreWeighRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'picking_basket_code' => 'required|string',
            'weight' => 'required|numeric|min:0.01',
        ];
    }
}
