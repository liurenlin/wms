<?php

namespace App\Http\Requests\Admin\BasicData\Product;

use Illuminate\Foundation\Http\FormRequest;

class ProductUpdateWeightAndVolumeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'weight' => ['numeric', 'min:0'],
            'width' => ['numeric', 'min:0'],
            'height' => ['numeric', 'min:0'],
            'length' => ['numeric', 'min:0'],
        ];
    }
}
