<?php

namespace App\Http\Requests\Admin\BasicData\StorageLocation;

use App\Consts\StorageTypeConst;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StorageLocationListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'page' => ['required', 'integer', 'min:1'],
            'per_page' => ['required', 'integer', 'min:5'],
            'type' => ['integer'],
            'code' => ['string', 'max:30'],
        ];
    }
}
