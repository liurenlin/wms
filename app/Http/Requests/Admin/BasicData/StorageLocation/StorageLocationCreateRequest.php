<?php

namespace App\Http\Requests\Admin\BasicData\StorageLocation;

use Illuminate\Foundation\Http\FormRequest;

class StorageLocationCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'warehouse_id' => ['required', 'integer', 'min:1'],
            'area_id' => ['required', 'integer', 'min:1'],
            'path_start' => ['required', 'integer', 'min:1', 'max:99'],
            'path_end' => ['required', 'integer', 'min:1', 'max:99', 'gte:path_start'],
            'shelves_quantity' => ['required', 'integer', 'min:1', 'max:99'],
            'layers_per_shelf' => ['required', 'integer', 'min:1', 'max:10'],
            'bins_per_layer' => ['required', 'integer', 'min:1', 'max:10'],
        ];
    }
}
