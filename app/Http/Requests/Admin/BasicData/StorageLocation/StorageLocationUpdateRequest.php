<?php

namespace App\Http\Requests\Admin\BasicData\StorageLocation;

use Illuminate\Foundation\Http\FormRequest;

class StorageLocationUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'warehouse_id' => ['integer', 'min:1'],
            'storage_type' => ['integer'],
            'storage_area_code' => ['string', 'max:10'],
            'storage_location_code' => ['string', 'max:30'],
        ];
    }
}
