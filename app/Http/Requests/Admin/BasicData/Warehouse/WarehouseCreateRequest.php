<?php

namespace App\Http\Requests\Admin\BasicData\Warehouse;

use Illuminate\Foundation\Http\FormRequest;

class WarehouseCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => ['required', 'string', 'max:100'],
            'manager' => ['required', 'string', 'max:60'],
            'country' => ['required', 'string', 'max:60'],
            'address' => ['required', 'string', 'max:200'],
            'cellphone' => ['required', 'string', 'max:30'],
        ];
    }
}
