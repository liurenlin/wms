<?php

namespace App\Http\Requests\Admin\BasicData\Warehouse;

use Illuminate\Foundation\Http\FormRequest;

class WarehouseUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => ['string', 'max:100'],
            'manager' => ['string', 'max:60'],
            'country' => ['string', 'max:60'],
            'address' => ['string', 'max:200'],
            'cellphone' => ['string', 'max:30'],
        ];
    }
}
