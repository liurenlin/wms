<?php

namespace App\Http\Requests\Admin\BasicData\Warehouse;

use App\Http\Requests\AdminRequest;

class WarehouseListRequest extends AdminRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'page' => ['required', 'integer', 'min:1'],
            'per_page' => ['required', 'integer', 'min:5'],
        ];
    }
}
