<?php

namespace App\Http\Requests\Admin\BasicData\LogisticsCompany;

use Illuminate\Foundation\Http\FormRequest;

class LogisticsCompanyListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page' => ['required', 'integer', 'min:1'],
            'per_page' => ['required', 'integer', 'min:5'],
        ];
    }
}
