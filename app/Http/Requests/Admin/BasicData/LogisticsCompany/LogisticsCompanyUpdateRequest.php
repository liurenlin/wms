<?php

namespace App\Http\Requests\Admin\BasicData\LogisticsCompany;

use Illuminate\Foundation\Http\FormRequest;

class LogisticsCompanyUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sender' => ['string', 'max:100'],
            'address' => ['string', 'max:200'],
            'sender_phone' => ['string', 'max:30'],
        ];
    }
}
