<?php

namespace App\Http\Requests\Admin\BasicData\PickingBasket;

use Illuminate\Foundation\Http\FormRequest;

class PickingBasketListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'page' => ['required', 'integer', 'min:1'],
            'per_page' => ['required', 'integer', 'min:5'],
            'code' => ['string', 'max:60'],
        ];
    }
}
