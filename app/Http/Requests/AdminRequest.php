<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Log;

class AdminRequest extends FormRequest
{
    protected function failedValidation(Validator $validator)
    {
        if (request()->ajax()) {
            throw new HttpResponseException(response()->json([
                'code' => 400,
                'message' => $validator->errors()->first(),
            ]));
        } else {
            parent::failedValidation($validator);
        }
    }
}
