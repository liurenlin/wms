<?php

namespace App\Http\Requests\Open\Order;

use App\Http\Requests\ApiRequest;
use App\Models\Outbound;
use Illuminate\Foundation\Http\FormRequest;

class UpdateStateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'warehouse_id' => 'required|integer|exists:warehouses,id',
            'state' => 'required|integer|in:' . Outbound::STATE_CANCELLED . ',' . Outbound::STATE_INTERCEPTED,
            'order_number' => 'required|string|exists:outbounds,order_number',
        ];
    }
}
