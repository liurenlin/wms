<?php

namespace App\Http\Requests\Open\Order;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class ShowRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'warehouse_id' => 'required|integer|exists:warehouses,id',
            'order_number' => 'required|string|exists:outbounds,order_number',
        ];
    }
}
