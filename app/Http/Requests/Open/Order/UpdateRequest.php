<?php

namespace App\Http\Requests\Open\Order;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'warehouse_id' => 'required|integer|exists:warehouses,id',
            'outbound_type' => 'required|integer|in:1,2',
            'order_number' => 'required|string',
            'order_created_at' => 'required|required|date_format:Y-m-d H:i:s',
            'currency' => 'required|string',
            'total_amount' => 'required|numeric',
            'freight' => 'required|numeric',
            'receiving_country' => 'required|string',
            'receiving_state' => 'required|string',
            'receiving_city' => 'required|string',
            'receiving_district' => 'nullable|string',
            'receiving_address' => 'required|string',
            'receiving_cellphone' => 'required|string',
            'receiving_user_name' => 'required|string',
            'items' => 'required|json',
        ];
    }
}
