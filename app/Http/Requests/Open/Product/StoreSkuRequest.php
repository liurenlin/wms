<?php

namespace App\Http\Requests\Open\Product;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class StoreSkuRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'product_code' => 'required|string|exists:products,product_code',
//            'sku_code' => 'required|string|unique:product_skus,sku_code',
            'name' => 'required|string',
            'attribute' => 'required|string',
            'weight' => 'required|numeric',
            'height' => 'required|numeric',
            'length' => 'required|numeric',
            'images' => 'required|json',
        ];
    }
}
