<?php

namespace App\Http\Requests\Open\Product;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'product_code' => 'required|string|unique:products,product_code',
            'name' => 'required|string',
            'images' => 'required|json',
            'skus' => 'required|json',
        ];
    }
}
