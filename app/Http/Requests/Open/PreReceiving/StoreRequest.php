<?php

namespace App\Http\Requests\Open\PreReceiving;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'warehouse_id' => 'required|integer|exists:warehouses,id',
            'third_party_order_type' => 'required|integer|in:1,2,3',
            'third_party_order_number' => 'required|string',
            'third_party_order_created_at' => 'required|date_format:Y-m-d H:i:s',
            'items' => 'required|json',
        ];
    }
}
