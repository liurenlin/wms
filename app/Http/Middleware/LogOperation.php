<?php

namespace App\Http\Middleware;

use App\Models\OperationLog;
use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class LogOperation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param JsonResponse $response
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->shouldLogOperation($request)) {
            $log = [
                'user_id' => Auth::user()->id,
                'route_name' => (string) $request->route()->getName(),
                'path' => substr($request->path(), 0, 255),
                'method' => $request->method(),
                'ip' => $request->getClientIp(),
                'user_agent' => $request->userAgent(),
                'headers' => json_encode($request->header()),
                'input' => json_encode($request->input()),
                'query_string' => json_encode($request->query->all()),
            ];

            try {
                $operationLog = OperationLog::create($log);
            } catch (\Exception $exception) {
                // pass
                Log::error($exception->getMessage());
            }
        }

        $response = $next($request);

        if ($this->shouldLogOperation($request)) {
            try {
                $operationLog->response_body = $response->getContent();
                $operationLog->save();
            } catch (\Exception $exception) {
                // pass
                Log::error($exception->getMessage());

            }
        }


        return $response;
    }

    /**
     * @param  Request  $request
     *
     * @return bool
     */
    protected function shouldLogOperation(Request $request)
    {
        return config('operation_log.enable')
            && !$this->inExceptArray($request)
            && $this->inAllowedMethods($request->method())
            && Auth::user();
    }

    /**
     * Whether requests using this method are allowed to be logged.
     *
     * @param  string  $method
     *
     * @return bool
     */
    protected function inAllowedMethods($method)
    {
        $allowedMethods = collect(config('operation_log.allowed_methods'))->filter();

        if ($allowedMethods->isEmpty()) {
            return true;
        }

        return $allowedMethods->map(function ($method) {
            return strtoupper($method);
        })->contains($method);
    }

    /**
     * Determine if the request has a URI that should pass through CSRF verification.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return bool
     */
    protected function inExceptArray($request)
    {
        foreach (config('operation_log.except') as $except) {
            if ($except !== '/') {
                $except = trim($except, '/');
            }

            $methods = [];

            if (Str::contains($except, ':')) {
                list($methods, $except) = explode(':', $except);
                $methods = explode(',', $methods);
            }

            $methods = array_map('strtoupper', $methods);

            if ($request->is($except) &&
                (empty($methods) || in_array($request->method(), $methods))) {
                return true;
            }
        }

        return false;
    }
}

