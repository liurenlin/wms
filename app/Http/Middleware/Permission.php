<?php

namespace App\Http\Middleware;

use App\Services\PermissionService;
use Closure;
use Illuminate\Support\Facades\Auth;

class Permission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (app()->runningUnitTests()) {
            return $next($request);
        }
        // 获取当前用户
        $userId = Auth::id();
        if (!PermissionService::isUserRequestAllowed($userId, $request)) {
            return response()->json([
                'code' => 403,
                'message' => __('auth.forbidden'),
            ]);
        }
        return $next($request);
    }
}
