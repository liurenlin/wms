<?php

namespace App\Http\Middleware;

use App\Services\Sign;
use Closure;

class OpenAuthPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$this->check($request->all())) {
            return fail('auth fail')->send();
        }
        return $next($request);
    }

    private function check(array $params): bool
    {
        if (
            !isset($params['app_id'])
            || !isset($params['sign'])
            || !isset($params['timestamp'])
            || !isset($params['nonce'])
        ) {
            return false;
        }

        if (
            empty($params['app_id'])
            || empty($params['sign'])
            || empty($params['timestamp'])
            || empty($params['nonce'])
        ) {
            return false;
        }

        // sign 校验
        $actualSign = $params['sign'];
        unset($params['sign']);
        $checkSign = Sign::generateSign($params);

        if ($actualSign != $checkSign) {
            return false;
        }
        return true;
    }
}
