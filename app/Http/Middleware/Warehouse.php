<?php

namespace App\Http\Middleware;

use App\Services\UserService;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class Warehouse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userId = Auth::id();
        if (!$request->hasHeader('warehouse-id')) {
            return fail(__('common.fail'));
        }
        if ($request->hasHeader('warehouse-id') && $userId) {
            $warehouseId = $request->header('warehouse-id');
            $res = UserService::setWarehouseId($userId, (int) $warehouseId);
            if (!$res) {
                Log::error('Update User Warehouse ID FAIL');
                throw new \Exception('Warehouse Id Wrong');
            }
        }
        return $next($request);
    }
}
