<?php

namespace App\Http\Controllers\Api;

use App\ApiRunners\File\UploadImageRunner;
use Illuminate\Http\Request;

class FileController extends BaseController
{
    public function uploadImage(Request $request, UploadImageRunner $uploadImageRunner)
    {
        if (!$request->file('content')) {
            return fail(__('common.file_empty'));
        }
        $request->merge(['content' => $request->file('content')]);
        return $uploadImageRunner->run($request);
    }
}
