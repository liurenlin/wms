<?php

namespace App\Http\Controllers\Api;

use App\ApiRunners\Handover\CheckLogisticsNumberRunner;
use App\ApiRunners\Handover\LogisticsCompaniesRunner;
use App\ApiRunners\Handover\StoreHandoverRunner;
use App\Http\Requests\Api\Handover\StoreHandoverRequest;
use Illuminate\Http\Request;

class HandoverController extends BaseController
{
    public function logisticsCompanies(Request $request, LogisticsCompaniesRunner $logisticsCompaniesRunner)
    {
        return $logisticsCompaniesRunner->run($request);
    }

    public function storeHandover(StoreHandoverRequest $request, StoreHandoverRunner $storeHandoverRunner)
    {
        // 校验格式
        $logisticsCompanies = json_decode($request->get('logistics_numbers'), JSON_OBJECT_AS_ARRAY);
        if (empty($logisticsCompanies)) {
            return fail(__('handover.params_wrong'));
        }
        foreach ($logisticsCompanies as $logisticsCompany) {
            if (!isset($logisticsCompany['logistics_number']) || empty($logisticsCompany['logistics_number'])) {
                return fail(__('handover.params_wrong'));
            }
        }

        return $storeHandoverRunner->run($request);
    }

    public function checkLogisticsNumber(Request $request, CheckLogisticsNumberRunner $checkLogisticsNumberRunner)
    {
        return $checkLogisticsNumberRunner->run($request);
    }
}
