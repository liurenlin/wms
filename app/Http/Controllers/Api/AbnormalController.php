<?php

namespace App\Http\Controllers\Api;

use App\ApiRunners\Abnormal\StoreRunner;
use App\ApiRunners\Abnormal\StoreAbnormalCodeRunner;
use App\Http\Requests\Api\Abnormal\StoreRequest;
use Illuminate\Http\Request;

class AbnormalController extends BaseController
{
    public function storeAbnormalCode(Request $request, StoreAbnormalCodeRunner $storeAbnormalCodeRunner)
    {
        return $storeAbnormalCodeRunner->run($request);
    }

    public function store(StoreRequest $request, StoreRunner $storeRunner)
    {
        return $storeRunner->run($request);
    }
}
