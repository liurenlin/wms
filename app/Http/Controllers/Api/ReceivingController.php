<?php

namespace App\Http\Controllers\Api;

use App\ApiRunners\Receiving\PreReceivingProductShowRunner;
use App\ApiRunners\Receiving\PreReceivingShowRunner;
use App\ApiRunners\Receiving\StoreReceivingLogRunner;
use App\ApiRunners\Receiving\StoreReceivingRunner;
use App\ApiRunners\Receiving\StoreSkuCodePrintLogRunner;
use App\ApiRunners\Receiving\UpdateActualReceivingRunner;
use App\Http\Requests\Api\Receiving\StoreReceivingLogRequest;
use App\Http\Requests\Api\Receiving\StoreReceivingRequest;
use App\Http\Requests\Api\Receiving\StoreSkuCodePrintLogRequest;
use App\Http\Requests\Api\Receiving\UpdateActualReceivingRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ReceivingController extends BaseController
{

    public function storeReceiving(StoreReceivingRequest $request, StoreReceivingRunner $storeReceivingRunner)
    {
        return $storeReceivingRunner->run($request);
    }

    public function preReceivingShow(Request $request, string $preReceivingNumber, PreReceivingShowRunner $preReceivingShowRunner)
    {
        $request->merge(['pre_receiving_number' => $preReceivingNumber]);
        return $preReceivingShowRunner->run($request);

    }

    public function preReceivingProductShow(Request $request, string $preReceivingNumber, string $productId, PreReceivingProductShowRunner $preReceivingProductShowRunner)
    {
        $request->merge(['pre_receiving_number' => $preReceivingNumber]);
        $request->merge(['product_id' => $productId]);
        return $preReceivingProductShowRunner->run($request);

    }

    public function storeReceivingLog(StoreReceivingLogRequest $request, StoreReceivingLogRunner $storeReceivingLogRunner)
    {
        return $storeReceivingLogRunner->run($request);
    }

    public function storeSkuCodePrintLog(StoreSkuCodePrintLogRequest $request, StoreSkuCodePrintLogRunner $skuCodePrintLogRunner)
    {
        return $skuCodePrintLogRunner->run($request);

    }

    public function updateActualReceiving(UpdateActualReceivingRequest $request, $actualReceivingId, UpdateActualReceivingRunner $updateActualReceivingRunner)
    {
        $request->merge(['actual_receiving_id' => $actualReceivingId]);
        return $updateActualReceivingRunner->run($request);
    }
}
