<?php

namespace App\Http\Controllers\Api;

use App\ApiRunners\Putaway\StorePutawayLogRunner;
use App\ApiRunners\Putaway\StorePutawayTaskRunner;
use App\ApiRunners\Putaway\UpdatePutawayTaskRunner;
use App\Http\Requests\Api\Putaway\StorePutawayLogRequest;
use App\Http\Requests\Api\Putaway\StorePutawayTaskRequest;
use App\Http\Requests\Api\Putaway\UpdatePutawayTaskRequest;
use Illuminate\Http\Request;

class PutawayController extends BaseController
{
    public function storePutawayTask(StorePutawayTaskRequest $request, StorePutawayTaskRunner $storePutawayTaskRunner)
    {
        return $storePutawayTaskRunner->run($request);
    }

    public function storePutawayLog(StorePutawayLogRequest $request, StorePutawayLogRunner $storePutawayLogRunner)
    {
        return $storePutawayLogRunner->run($request);
    }

    public function updatePutawayTask(UpdatePutawayTaskRequest $request, $putawayTaskId, UpdatePutawayTaskRunner $updatePutawayTaskRunner)
    {
        $request->offsetSet('putaway_task_id', $putawayTaskId);
        return $updatePutawayTaskRunner->run($request);
    }
}
