<?php

namespace App\Http\Controllers\Api;

use App\ApiRunners\Stock\IndexRunner;
use Illuminate\Http\Request;

class StockController extends BaseController
{
    public function index(Request $request, IndexRunner $indexRunner)
    {
        return $indexRunner->run($request);
    }
}
