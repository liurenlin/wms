<?php

namespace App\Http\Controllers\Api;

use App\ApiRunners\Picking\PickingBasketAvailableRunner;
use App\ApiRunners\Picking\PickingTaskListRunner;
use App\ApiRunners\Picking\PickingTaskShowRunner;
use App\ApiRunners\Picking\StorePickingLogRunner;
use App\ApiRunners\Picking\UpdatePickingTaskBasketsRunner;
use App\ApiRunners\Picking\StorePickingTaskRunner;
use App\Http\Requests\Api\Picking\StorePickingLogRequest;
use App\Http\Requests\Api\Picking\UpdatePickingTaskBasketsRequest;
use App\Http\Requests\Api\Picking\StorePickingTaskRequest;
use Illuminate\Http\Request;

class PickingController extends BaseController
{
    public function storePickingTask(StorePickingTaskRequest $request, StorePickingTaskRunner $storePickingTaskRunner)
    {
        return $storePickingTaskRunner->run($request);
    }

    public function storePickingLog(StorePickingLogRequest $request, StorePickingLogRunner $storePickingLogRunner)
    {
        return $storePickingLogRunner->run($request);
    }

    public function pickingTaskLit(Request $request, PickingTaskListRunner $pickingTaskListRunner)
    {
        return $pickingTaskListRunner->run($request);
    }

    public function pickingBasketAvailable(Request $request, PickingBasketAvailableRunner $pickingBasketAvailableRunner)
    {
        return $pickingBasketAvailableRunner->run($request);
    }

    public function updatePickingTaskBaskets(UpdatePickingTaskBasketsRequest $request, $pickingTaskId, UpdatePickingTaskBasketsRunner $updatePickingTaskBasketsRunner)
    {
        $request->merge(['picking_task_id' => $pickingTaskId]);
        return $updatePickingTaskBasketsRunner->run($request);
    }

    public function pickingTaskShow(Request $request, $pickingTaskId, PickingTaskShowRunner $pickingTaskShowRunner)
    {
        $request->merge(['picking_task_id' => $pickingTaskId]);
        return $pickingTaskShowRunner->run($request);
    }
}
