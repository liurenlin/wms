<?php

namespace App\Http\Controllers\Api;

use App\ApiRunners\Version\IndexRunner;
use Illuminate\Http\Request;

class VersionController extends BaseController
{
    public function index(Request $request, IndexRunner $indexRunner)
    {
        return $indexRunner->run($request);
    }
}
