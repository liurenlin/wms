<?php

namespace App\Http\Controllers\Api;

use App\ApiRunners\User\LoginRunner;
use App\ApiRunners\User\LogoutRunner;
use App\Http\Requests\Api\User\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends BaseController
{
    public function login(LoginRequest $request, LoginRunner $loginRunner)
    {
        return $loginRunner->run($request);
    }

    public function logout(Request $request, LogoutRunner $logoutRunner)
    {
        return $logoutRunner->run($request);
    }
}
