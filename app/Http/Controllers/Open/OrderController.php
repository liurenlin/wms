<?php

namespace App\Http\Controllers\Open;

use App\Http\Requests\Open\Order\UpdateRequest;
use App\Http\Requests\Open\Order\UpdateStateRequest;
use App\Http\Requests\Open\Order\ShowRequest;
use App\OpenRunners\Order\UpdateRunner;
use App\OpenRunners\Order\UpdateStateRunner;
use App\OpenRunners\Order\ShowRunner;
use Illuminate\Http\Request;

class OrderController extends BaseController
{
    public function show(ShowRequest $request, ShowRunner $showRunner)
    {
        return $showRunner->run($request);
    }

    public function updateState(UpdateStateRequest $request, UpdateStateRunner $updateStateRunner)
    {
        return $updateStateRunner->run($request);
    }

    public function update(UpdateRequest $request, UpdateRunner $updateRunner)
    {
        return $updateRunner->run($request);
    }
}
