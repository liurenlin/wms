<?php

namespace App\Http\Controllers\Open;

use App\Http\Requests\Open\PreReceiving\StoreRequest;
use App\Models\OpenMessageQueue;
use App\OpenRunners\PreReceiving\StoreRunner;
use App\Services\MessageService;
use Illuminate\Http\Request;

class PreReceivingController extends BaseController
{
    public function store(StoreRequest $request, StoreRunner $storeRunner)
    {
        $res = MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_STORE_PRE_RECEIVING, $request->all());
        if (!$res) {
            return fail();
        }
        return success();
    }

}
