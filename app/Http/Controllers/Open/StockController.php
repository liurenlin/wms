<?php

namespace App\Http\Controllers\Open;

use App\OpenRunners\Stock\IndexRunner;
use Illuminate\Http\Request;

class StockController extends BaseController
{
    public function index(Request $request, IndexRunner $showRunner)
    {
        return $showRunner->run($request);
    }
}
