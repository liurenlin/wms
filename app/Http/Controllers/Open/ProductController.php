<?php

namespace App\Http\Controllers\Open;

use App\Http\Requests\Open\Product\BatchStoreRequest;
use App\Http\Requests\Open\Product\BatchStoreSkuRequest;
use App\Http\Requests\Open\Product\StoreRequest;
use App\Http\Requests\Open\Product\StoreSkuRequest;
use App\Models\OpenMessageQueue;
use App\OpenRunners\Product\BatchStoreRunner;
use App\OpenRunners\Product\BatchStoreSkuRunner;
use App\OpenRunners\Product\StoreRunner;
use App\OpenRunners\Product\StoreSkuRunner;
use App\Services\MessageService;
use Illuminate\Http\Request;

class ProductController extends BaseController
{
    public function store(StoreRequest $request, StoreRunner $storeRunner)
    {
        $res = MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_STORE_PRODUCT, $request->all());
        if (!$res) {
            return fail();
        }
        return success();
    }

    public function batchStore(BatchStoreRequest $request, BatchStoreRunner $batchStoreRunner)
    {
        $res = MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_BATCH_STORE_PRODUCT, $request->all());
        if (!$res) {
            return fail();
        }
        return success();
    }

    public function storeSku(StoreSkuRequest $request, StoreSkuRunner $storeSkuRunner)
    {
        $res = MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_STORE_SKU, $request->all());
        if (!$res) {
            return fail();
        }
        return success();
    }

    public function batchStoreSku(BatchStoreSkuRequest $request, BatchStoreSkuRunner $batchStoreSkuRunner)
    {
        $res = MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_BATCH_STORE_SKU, $request->all());
        if (!$res) {
            return fail();
        }
        return success();
    }

}
