<?php

namespace App\Http\Controllers\Open;

use App\Http\Requests\Open\Outbound\StoreRequest;
use App\Models\OpenMessageQueue;
use App\OpenRunners\Outbound\StoreRunner;
use App\Services\MessageService;
use Illuminate\Http\Request;

class OutboundController extends BaseController
{
    public function store(StoreRequest $request, StoreRunner $storeRunner)
    {
        $res = MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_STORE_OUTBOUND, $request->all());
        if (!$res) {
            return fail();
        }
        return success();
    }

}
