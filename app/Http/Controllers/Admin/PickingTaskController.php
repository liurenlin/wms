<?php

namespace App\Http\Controllers\Admin;

use App\AdminRunners\PickingTask\IndexRunner;
use App\AdminRunners\PickingTask\ShowRunner;
use App\AdminRunners\PickingTask\StoreAssignRunner;
use App\AdminRunners\PickingTask\UpdateTasksRunner;
use App\ApiRunners\Picking\DetailsRunner;
use App\Http\Requests\Admin\PickingTask\StoreAssignRequest;
use App\Http\Requests\Admin\PickingTask\UpdateTasksRequest;
use Illuminate\Http\Request;

class PickingTaskController extends BaseController
{
    public function index(Request $request, IndexRunner $indexRunner)
    {
        if (
            !empty($request->get('created_at_from'))
            && !empty($request->get('created_at_to'))
            && $request->get('created_at_from') > $request->get('created_at_to')
        ) {
            return fail(__('outbound.search_time_is_wrong'));
        }

        if (
            !empty($request->get('finished_at_from'))
            && !empty($request->get('finished_at_to'))
            && $request->get('finished_at_from') > $request->get('finished_at_to')
        ) {
            return fail(__('outbound.search_time_is_wrong'));
        }
        return $indexRunner->run($request);
    }

    public function show(Request $request, $pickingTaskId, ShowRunner $showRunner)
    {
        $request->offsetSet('picking_task_id', $pickingTaskId);
        return $showRunner->run($request);
    }

    public function storeAssign(StoreAssignRequest $request, StoreAssignRunner $storeAssignRunner)
    {
        return $storeAssignRunner->run($request);
    }

    public function updateTasks(UpdateTasksRequest $request, UpdateTasksRunner $updateTasksRunner)
    {
        return $updateTasksRunner->run($request);
    }

    public function details(Request $request, DetailsRunner $detailsRunner)
    {
        return $detailsRunner->run($request);
    }
}
