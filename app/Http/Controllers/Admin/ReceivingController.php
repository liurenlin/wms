<?php

namespace App\Http\Controllers\Admin;

use App\AdminRunners\Receiving\ActualReceivingDetailRunner;
use App\AdminRunners\Receiving\ActualReceivingListRunner;
use App\AdminRunners\Receiving\PreReceivingDetailRunner;
use App\AdminRunners\Receiving\PreReceivingListRunner;
use App\AdminRunners\Receiving\PutAwayCartDetailRunner;
use App\AdminRunners\Receiving\PutAwayCartListRunner;
use App\AdminRunners\Receiving\PutAwayTaskDetailRunner;
use App\AdminRunners\Receiving\PutAwayTaskListRunner;
use Illuminate\Http\Request;

class ReceivingController extends BaseController
{
    public function preReceivingList(Request $request, PreReceivingListRunner $preReceivingListRunner)
    {
        if (
            !empty($request->get('created_at_from'))
            && !empty($request->get('created_at_to'))
            && $request->get('created_at_from') > $request->get('created_at_to')
        ) {
            return fail(__('outbound.search_time_is_wrong'));
        }
        return $preReceivingListRunner->run($request);
    }

    public function preReceivingDetail(
        Request $request,
        $preReceivingId,
        PreReceivingDetailRunner $preReceivingDetailRunner
    ) {
        $request->merge(['pre_receiving_id' => $preReceivingId]);
        return $preReceivingDetailRunner->run($request);
    }

    public function actualReceivingList(Request $request, ActualReceivingListRunner $actualReceivingListRunner)
    {
        if (
            !empty($request->get('created_at_from'))
            && !empty($request->get('created_at_to'))
            && $request->get('created_at_from') > $request->get('created_at_to')
        ) {
            return fail(__('outbound.search_time_is_wrong'));
        }
        if (
            !empty($request->get('finished_at_from'))
            && !empty($request->get('finished_at_to'))
            && $request->get('finished_at_from') > $request->get('finished_at_to')
        ) {
            return fail(__('outbound.search_time_is_wrong'));
        }
        return $actualReceivingListRunner->run($request);
    }

    public function actualReceivingDetail(
        Request $request,
        $actualReceivingId,
        ActualReceivingDetailRunner $actualReceivingDetailRunner
    ) {
        $request->merge(['actual_receiving_id' => $actualReceivingId]);
        return $actualReceivingDetailRunner->run($request);
    }

    public function putAwayTaskList(Request $request, PutAwayTaskListRunner $awayTaskListRunner)
    {
        if (
            !empty($request->get('created_at_from'))
            && !empty($request->get('created_at_to'))
            && $request->get('created_at_from') > $request->get('created_at_to')
        ) {
            return fail(__('outbound.search_time_is_wrong'));
        }
        if (
            !empty($request->get('finished_at_from'))
            && !empty($request->get('finished_at_to'))
            && $request->get('finished_at_from') > $request->get('finished_at_to')
        ) {
            return fail(__('outbound.search_time_is_wrong'));
        }
        return $awayTaskListRunner->run($request);
    }

    public function putAwayTaskDetail(
        Request $request,
        $putAwayTaskId,
        PutAwayTaskDetailRunner $putAwayTaskDetailRunner
    ) {
        $request->merge(['putaway_task_id' => $putAwayTaskId]);
        return $putAwayTaskDetailRunner->run($request);
    }

    public function putawayCartsList(Request $request, PutAwayCartListRunner $putAwayCartListRunner)
    {
        if (
            !empty($request->get('created_at_from'))
            && !empty($request->get('created_at_to'))
            && $request->get('created_at_from') > $request->get('created_at_to')
        ) {
            return fail(__('outbound.search_time_is_wrong'));
        }
        return $putAwayCartListRunner->run($request);
    }

    public function putawayCartDetail(Request $request, $cartId, PutAwayCartDetailRunner $putAwayCartDetailRunner)
    {
        $request->merge(['cart_id' => $cartId]);
        return $putAwayCartDetailRunner->run($request);
    }
}
