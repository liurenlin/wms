<?php

namespace App\Http\Controllers\Admin;

use App\AdminRunners\Weigh\CheckOutboundAvailableRunner;
use App\AdminRunners\Weigh\IndexRunner;
use App\AdminRunners\Weigh\StoreWeighRunner;
use App\Http\Requests\Admin\Weigh\StoreWeighRequest;
use Illuminate\Http\Request;

class WeighController extends BaseController
{
    public function index(Request $request, IndexRunner $indexRunner)
    {
        if (
            !empty($request->get('created_at_from'))
            && !empty($request->get('created_at_to'))
            && $request->get('created_at_from') > $request->get('created_at_to')
        ) {
            return fail(__('outbound.search_time_is_wrong'));
        }
        return $indexRunner->run($request);
    }

    public function checkOutboundAvailable(Request $request, CheckOutboundAvailableRunner $checkOutboundAvailableRunner)
    {
        return $checkOutboundAvailableRunner->run($request);
    }

    public function storeWeigh(StoreWeighRequest $request, StoreWeighRunner $storeWeighRunner)
    {
        return $storeWeighRunner->run($request);
    }
}
