<?php

namespace App\Http\Controllers\Admin\BasicData;

use App\AdminRunners\StorageArea\StorageAreaCreateRunner;
use App\AdminRunners\StorageArea\StorageAreaListRunner;
use App\AdminRunners\StorageArea\StorageAreaUpdateRunner;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BasicData\StorageArea\StorageAreaCreateRequest;
use App\Http\Requests\Admin\BasicData\StorageArea\StorageAreaListRequest;
use App\Http\Requests\Admin\BasicData\StorageArea\StorageAreaUpdateRequest;

class StorageAreaController extends Controller
{
    public function getList(StorageAreaListRequest $request, StorageAreaListRunner $runner)
    {
        return $runner->run($request);
    }

    public function create(StorageAreaCreateRequest $request, StorageAreaCreateRunner $runner)
    {
        return $runner->run($request);
    }

    public function update($id, StorageAreaUpdateRequest $request, StorageAreaUpdateRunner $runner)
    {
        $runner->setId($id);
        return $runner->run($request);
    }
}
