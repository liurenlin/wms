<?php

namespace App\Http\Controllers\Admin\BasicData;

use App\AdminRunners\PickingBasket\PickingBasketBatchRunner;
use App\AdminRunners\PickingBasket\PickingBasketListRunner;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BasicData\PickingBasket\PickingBasketBatchRequest;
use App\Http\Requests\Admin\BasicData\PickingBasket\PickingBasketListRequest;

class PickingBasketController extends Controller
{
    public function getList(PickingBasketListRequest $request, PickingBasketListRunner $runner)
    {
        return $runner->run($request);
    }

    public function batchAdd(PickingBasketBatchRequest $request, PickingBasketBatchRunner $runner)
    {
        return $runner->run($request);
    }
}
