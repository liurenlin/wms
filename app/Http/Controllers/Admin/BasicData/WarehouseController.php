<?php
/**
 * Created by qiqizhang.
 * User: qiqizhang
 * Date: 2020-01-15
 * Time: 21:31
 */

namespace App\Http\Controllers\Admin\BasicData;

use App\AdminRunners\Warehouse\WarehouseCreateRunner;
use App\AdminRunners\Warehouse\WarehouseListRunner;
use App\AdminRunners\Warehouse\WarehouseUpdateRunner;
use App\Http\Controllers\Admin\BaseController;
use App\Http\Requests\Admin\BasicData\Warehouse\WarehouseCreateRequest;
use App\Http\Requests\Admin\BasicData\Warehouse\WarehouseListRequest;
use App\Http\Requests\Admin\BasicData\Warehouse\WarehouseUpdateRequest;

/**
 * warehouse api
 * Class WarehouseController
 * @package App\Http\Controllers\Api\BasicData
 */
class WarehouseController extends BaseController
{
    public function getList(WarehouseListRequest $request, WarehouseListRunner $runner)
    {
        return $runner->run($request);
    }

    public function create(WarehouseCreateRequest $request, WarehouseCreateRunner $runner)
    {
        return $runner->run($request);
    }

    public function update($id, WarehouseUpdateRequest $request, WarehouseUpdateRunner $runner)
    {
        $runner->setId($id);
        return $runner->run($request);
    }
}
