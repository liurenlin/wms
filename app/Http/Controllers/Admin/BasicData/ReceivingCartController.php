<?php

namespace App\Http\Controllers\Admin\BasicData;

use App\AdminRunners\ReceivingCart\ReceivingCartBatchRunner;
use App\AdminRunners\ReceivingCart\ReceivingCartListRunner;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BasicData\ReceivingCart\ReceivingCartBatchRequest;
use App\Http\Requests\Admin\BasicData\ReceivingCart\ReceivingCartListRequest;

class ReceivingCartController extends Controller
{
    public function getList(ReceivingCartListRequest $request, ReceivingCartListRunner $runner)
    {
        return $runner->run($request);
    }

    public function batchAdd(ReceivingCartBatchRequest $request, ReceivingCartBatchRunner $runner)
    {
        return $runner->run($request);
    }
}
