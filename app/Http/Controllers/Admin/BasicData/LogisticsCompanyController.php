<?php

namespace App\Http\Controllers\Admin\BasicData;

use App\AdminRunners\LogisticsCompany\LogisticsCompanyListRunner;
use App\AdminRunners\LogisticsCompany\LogisticsCompanyUpdateRunner;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BasicData\LogisticsCompany\LogisticsCompanyListRequest;
use App\Http\Requests\Admin\BasicData\LogisticsCompany\LogisticsCompanyUpdateRequest;
use App\Models\LogisticsCompany;

class LogisticsCompanyController extends Controller
{
    public function getList(LogisticsCompanyListRequest $request, LogisticsCompanyListRunner $runner)
    {
        return $runner->run($request);
    }

    public function update($id, LogisticsCompanyUpdateRequest $request, LogisticsCompanyUpdateRunner $runner)
    {
        return $runner->setId($id)->run($request);
    }
}
