<?php

namespace App\Http\Controllers\Admin\BasicData;

use App\AdminRunners\Product\ProductListRunner;
use App\AdminRunners\Product\ProductUpdateWeightAndVolumeRunner;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BasicData\Product\ProductListRequest;
use App\Http\Requests\Admin\BasicData\Product\ProductUpdateWeightAndVolumeRequest;

class ProductController extends Controller
{
    public function getList(ProductListRequest $request, ProductListRunner $runner)
    {
        return $runner->run($request);
    }

    public function updateWeightAndVolume($id, ProductUpdateWeightAndVolumeRequest $request, ProductUpdateWeightAndVolumeRunner $runner)
    {
        return $runner->setId($id)->run($request);
    }
}
