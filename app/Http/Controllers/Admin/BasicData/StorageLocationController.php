<?php

namespace App\Http\Controllers\Admin\BasicData;

use App\AdminRunners\StorageLocation\StorageLocationCreateRunner;
use App\AdminRunners\StorageLocation\StorageLocationDeleteRunner;
use App\AdminRunners\StorageLocation\StorageLocationListRunner;
use App\AdminRunners\StorageLocation\StorageLocationUpdateRunner;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BasicData\StorageLocation\StorageLocationCreateRequest;
use App\Http\Requests\Admin\BasicData\StorageLocation\StorageLocationDeleteRequest;
use App\Http\Requests\Admin\BasicData\StorageLocation\StorageLocationListRequest;
use App\Http\Requests\Admin\BasicData\StorageLocation\StorageLocationUpdateRequest;

class StorageLocationController extends Controller
{
    public function getList(StorageLocationListRequest $request, StorageLocationListRunner $runner)
    {
        return $runner->run($request);
    }

    public function create(StorageLocationCreateRequest $request, StorageLocationCreateRunner $runner)
    {
        return $runner->run($request);
    }

    public function update($id, StorageLocationUpdateRequest $request, StorageLocationUpdateRunner $runner)
    {
        $runner->setId($id);
        return $runner->run($request);
    }

    public function delete($ids, StorageLocationDeleteRequest $request, StorageLocationDeleteRunner $runner)
    {
        $runner->setId($ids);
        return $runner->run($request);
    }
}
