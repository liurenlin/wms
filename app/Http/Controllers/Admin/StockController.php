<?php

namespace App\Http\Controllers\Admin;

use App\AdminRunners\InWareHouse\StockListRunner;
use App\AdminRunners\InWareHouse\StockManagementRunner;
use App\AdminRunners\Stock\DealWithLogListRunner;
use App\AdminRunners\Stock\SkuDetailRunner;
use App\AdminRunners\Stock\SkuStocksRunner;
use App\AdminRunners\Stock\StoreStockDealWithLogRunner;
use App\Http\Requests\Admin\Stock\StoreStockDealWithLogRequest;
use Illuminate\Http\Request;

class StockController extends BaseController
{
    public function stockList(Request $request, StockListRunner $stockListRunner)
    {
        return $stockListRunner->run($request);
    }

    public function GetProductSku(Request $request, SkuDetailRunner $skuDetailRunner){
        return $skuDetailRunner->run($request);
    }

    public function stockManagement(Request $request, StockManagementRunner $stockManagementRunner)
    {
        if (
            !empty($request->get('created_at_from'))
            && !empty($request->get('created_at_to'))
            && $request->get('created_at_from') > $request->get('created_at_to')
        ) {
            return fail(__('outbound.search_time_is_wrong'));
        }
        return $stockManagementRunner->run($request);
    }

    public function storeStockDealWithLog(StoreStockDealWithLogRequest $request, StoreStockDealWithLogRunner $storeDealWithLogRunner)
    {
        return $storeDealWithLogRunner->run($request);
    }

    public function skuStocks(Request $request, SkuStocksRunner $skuStocksRunner)
    {
        return $skuStocksRunner->run($request);

    }
}
