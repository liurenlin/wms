<?php

namespace App\Http\Controllers\Admin;

use App\AdminRunners\Role\DestroyRunner;
use App\AdminRunners\Role\RoleListRunner;
use App\AdminRunners\Role\StoreRunner;
use App\AdminRunners\Role\UpdateRunner;
use App\Http\Requests\Admin\Role\StoreRequest;
use App\Http\Requests\Admin\Role\UpdateRequest;
use Illuminate\Http\Request;

class RoleController extends BaseController
{
    public function roleList(Request $request, RoleListRunner $roleListRunner)
    {
        return $roleListRunner->run($request);
    }

    public function update(UpdateRequest $request, $roleId, UpdateRunner $updateRunner)
    {
        $request->offsetSet('role_id', $roleId);
        return $updateRunner->run($request);
    }

    public function store(StoreRequest $request, StoreRunner $storeRunner)
    {
        return $storeRunner->run($request);
    }

    public function destroy(Request $request, $roleId, DestroyRunner $destroyRunner)
    {
        $request->offsetSet('role_id', $roleId);
        return $destroyRunner->run($request);
    }
}
