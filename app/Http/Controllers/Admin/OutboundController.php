<?php

namespace App\Http\Controllers\Admin;

use App\AdminRunners\Outbound\IndexRunner;
use App\AdminRunners\Outbound\ShowRunner;
use Illuminate\Http\Request;

class OutboundController extends BaseController
{
    public function index(Request $request, IndexRunner $indexRunner)
    {
        if (
            !empty($request->get('created_at_from'))
            && !empty($request->get('created_at_to'))
            && $request->get('created_at_from') > $request->get('created_at_to')
        ) {
            return fail(__('outbound.search_time_is_wrong'));
        }
        return $indexRunner->run($request);
    }

    public function show(Request $request, $outboundId, ShowRunner $showRunner)
    {
        $request->offsetSet('outbound_id', $outboundId);
        return $showRunner->run($request);
    }
}
