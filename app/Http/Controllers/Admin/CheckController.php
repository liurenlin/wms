<?php

namespace App\Http\Controllers\Admin;

use App\AdminRunners\Check\ConfirmCheckTaskRunner;
use App\AdminRunners\Check\StoreCheckLogRunner;
use App\AdminRunners\Check\StoreCheckTaskRunner;
use App\Http\Requests\Admin\Check\ConfirmCheckTaskRequest;
use App\Http\Requests\Admin\Check\StoreCheckLogRequest;
use App\Http\Requests\Admin\Check\StoreCheckTaskRequest;
use Illuminate\Http\Request;

class CheckController extends BaseController
{
    public function storeCheckTask(StoreCheckTaskRequest $request, StoreCheckTaskRunner $storeCheckTaskRunner)
    {
        return $storeCheckTaskRunner->run($request);
    }

    public function storeCheckLog(StoreCheckLogRequest $request, StoreCheckLogRunner $storeCheckLogRunner)
    {
        return $storeCheckLogRunner->run($request);
    }

    public function confirmCheckTask(ConfirmCheckTaskRequest $request, $checkTaskId, ConfirmCheckTaskRunner $confirmCheckTaskRunner)
    {
        $request->merge(['check_task_id' => $checkTaskId]);
        return $confirmCheckTaskRunner->run($request);
    }
}
