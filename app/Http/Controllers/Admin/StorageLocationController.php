<?php

namespace App\Http\Controllers\Admin;

use App\AdminRunners\StorageLocation\ShowRunner;
use Illuminate\Http\Request;

class StorageLocationController extends BaseController
{
    public function show(Request $request, $storageLocationCode, ShowRunner $showRunner)
    {
        $request->offsetSet('storage_location_code', $storageLocationCode);
        return $showRunner->run($request);
    }
}
