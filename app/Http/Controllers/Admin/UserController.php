<?php

namespace App\Http\Controllers\Admin;

use App\AdminRunners\User\DeleteUserRoleRunner;
use App\AdminRunners\User\StoreUserRunner;
use App\AdminRunners\User\UpdateUserRunner;
use App\AdminRunners\User\UserListRunner;
use App\AdminRunners\User\LoginRunner;
use App\ApiRunners\User\LogoutRunner;
use App\Http\Requests\Admin\User\StoreUserRequest;
use App\Http\Requests\Admin\User\UpdateUserRequest;
use App\Http\Requests\Api\User\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class UserController extends BaseController
{
    public function userList(Request $request, UserListRunner $userListRunner)
    {
        return $userListRunner->run($request);
    }

    public function storeUser(StoreUserRequest $request, StoreUserRunner $storeUserRunner)
    {
        return $storeUserRunner->run($request);
    }

    public function updateUser(UpdateUserRequest $request, $userId, UpdateUserRunner $updateUserRunner)
    {
        $request->offsetSet('user_id', $userId);
        return $updateUserRunner->run($request);
    }

    public function deleteUserRole(Request $request, $userId, $roleId, DeleteUserRoleRunner $deleteUserRoleRunner)
    {
        $request->merge([
            'user_id' => $userId,
            'role_id' => $roleId,
        ]);
        return $deleteUserRoleRunner->run($request);
    }

    public function login(LoginRequest $request, LoginRunner $loginRunner)
    {
        return $loginRunner->run($request);
    }

    public function logout(Request $request, LogoutRunner $logoutRunner)
    {
        return $logoutRunner->run($request);
    }
}
