<?php

namespace App\Http\Controllers\Admin;

use App\AdminRunners\Abnormal\StoreSkuToAbnormalRunner;
use App\AdminRunners\InWareHouse\AbnormalShowRunner;
use App\AdminRunners\InWareHouse\AbnormalListRunner;
use App\AdminRunners\InWareHouse\AbnormalUpdateRunner;
use App\AdminRunners\Abnormal\StoreAbnormalCodeRunner;
use App\AdminRunners\Abnormal\StoreRunner;
use App\Http\Requests\Admin\Abnormal\StoreAbnormalSkuRequest;
use App\Http\Requests\Admin\Abnormal\UpdateRequest;
use App\Http\Requests\Admin\Abnormal\StoreRequest;
use Illuminate\Http\Request;

class AbnormalController extends BaseController
{
    public function index(Request $request, AbnormalListRunner $abnormalListRunner)
    {
        return $abnormalListRunner->run($request);
    }

    public function show(Request $request, $abnormalId, AbnormalShowRunner $abnormalDetailRunner)
    {
        $request->merge(['abnormal_id' => $abnormalId]);
        return $abnormalDetailRunner->run($request);
    }

    public function update(UpdateRequest $request, $abnormalId, AbnormalUpdateRunner $abnormalUpdateRunner)
    {
        $request->merge(['abnormal_id' => $abnormalId]);
        return $abnormalUpdateRunner->run($request);
    }

    public function storeAbnormalCode(Request $request, StoreAbnormalCodeRunner $storeRunner)
    {
        return $storeRunner->run($request);
    }

    public function store(StoreRequest $request, StoreRunner $storeRunner)
    {
        return $storeRunner->run($request);
    }
}
