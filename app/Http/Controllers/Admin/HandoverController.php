<?php

namespace App\Http\Controllers\Admin;

use App\AdminRunners\Handover\IndexRunner;
use App\AdminRunners\Handover\ShowRunner;
use Illuminate\Http\Request;

class HandoverController extends BaseController
{
    public function index(Request $request, IndexRunner $indexRunner)
    {
        if (
            !empty($request->get('created_at_from'))
            && !empty($request->get('created_at_to'))
            && $request->get('created_at_from') > $request->get('created_at_to')
        ) {
            return fail(__('outbound.search_time_is_wrong'));
        }
        return $indexRunner->run($request);
    }

    public function show(Request $request, $pickingTaskId, ShowRunner $showRunner)
    {
        $request->offsetSet('handover_id', $pickingTaskId);
        return $showRunner->run($request);
    }
}
