<?php

namespace App\Http\Controllers\Admin;

use App\AdminRunners\Permission\PermissionGroupListRunner;
use App\AdminRunners\Permission\RoleListRunner;
use App\Models\Permission;
use Illuminate\Http\Request;

class PermissionController extends BaseController
{
    public function index(Request $request)
    {
        return Permission::query()->get();
    }

    public function store(Request $request)
    {
//        $permission = Permission::query()
//            ->where('')
    }

    public function permissionGroupList(Request $request, PermissionGroupListRunner $permissionGroupListRunner)
    {
        return $permissionGroupListRunner->run($request);
    }
}
