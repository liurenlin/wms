<?php

namespace App\Http\Controllers\Admin;

use App\AdminRunners\PickingTaskBasket\IndexRunner;
use App\AdminRunners\PickingTaskBasket\ShowRunner;
use Illuminate\Http\Request;

class PickingTaskBasketController extends BaseController
{
    public function index(Request $request, IndexRunner $indexRunner)
    {
        if (
            !empty($request->get('created_at_from'))
            && !empty($request->get('created_at_to'))
            && $request->get('created_at_from') > $request->get('created_at_to')
        ) {
            return fail(__('outbound.search_time_is_wrong'));
        }
        return $indexRunner->run($request);
    }

    public function show(Request $request, $pickingTaskBasketId, ShowRunner $showRunner)
    {
        $request->offsetSet('picking_task_basket_id', $pickingTaskBasketId);
        return $showRunner->run($request);
    }
}
