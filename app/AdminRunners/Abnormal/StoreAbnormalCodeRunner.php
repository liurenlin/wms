<?php

namespace App\AdminRunners\Abnormal;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Repositories\AbnormalRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class StoreAbnormalCodeRunner extends Runner implements AdminRunner
{
    public $abnormalRepository;

    public function __construct(AbnormalRepository $abnormalRepository)
    {
        $this->abnormalRepository = $abnormalRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $code = $this->abnormalRepository->generateAbnormalCode();

        return success([
            'code' => $code,
        ]);
    }
}
