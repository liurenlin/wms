<?php

namespace App\AdminRunners\PickingBasket;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Exceptions\NotExistException;
use App\Models\Warehouse;
use App\Services\PickingBasketService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PickingBasketBatchRunner extends Runner implements AdminRunner
{

    public function run(Request $request): JsonResponse
    {
        $amount = $request->input('amount');
        $warehouseId = $request->input('warehouse_id');
        $warehouse = Warehouse::find($warehouseId);
        if (!$warehouse) {
            throw new NotExistException([
                'message' => 'warehouse not exist',
            ]);
        }
        $service = new PickingBasketService();
        DB::beginTransaction();
        try {
            $service->batchAddBaskets($warehouseId, $amount);
            DB::commit();
            return success(['amount' => $amount]);
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }
}
