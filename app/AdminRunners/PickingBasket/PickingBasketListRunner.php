<?php
namespace App\AdminRunners\PickingBasket;


use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Consts\CommonConst;
use App\Models\Basket;
use App\Models\ProductSku;
use App\Models\ReceivingCart;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PickingBasketListRunner extends Runner implements AdminRunner
{
    private $listFields = [
        'id' => 'id',
        'basket_code' => 'basket_code',
        'created_at' => 'created_at',
    ];

    public function run(Request $request): JsonResponse
    {
        $perPage = $request->input('per_page') ?? CommonConst::DEFAULT_PER_PAGE;
        $search = $request->input('basket_code');
        $search = '%' . $search. '%';
        $baskets = Basket::query()
            ->select($this->listFields)
            ->when($search, function (Builder $query) use ($search) {
                $query->where('basket_code', 'like', $search);
            })
            ->paginate($perPage);
        return success([
            'baskets' => $baskets->toArray()['data'],
            'page_bean' => [
                'total' => (int)$baskets->total(),
                'page' => (int)$baskets->currentPage(),
                'per_page' => (int)$baskets->perPage(),
            ],
        ]);
    }
}
