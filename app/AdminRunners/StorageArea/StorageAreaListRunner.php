<?php
namespace App\AdminRunners\StorageArea;


use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Consts\CommonConst;
use App\Models\StorageArea;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class StorageAreaListRunner extends Runner implements AdminRunner
{
    private $listFields = [
        'id' => 'id',
        'storage_type' => 'storage_type',
        'area_code' => 'area_code',
        'created_at' => 'created_at',
    ];

    public function run(Request $request): JsonResponse
    {
        $perPage = $request->input('per_page') ?? CommonConst::DEFAULT_PER_PAGE;
        $areas = StorageArea::select($this->listFields)
            ->orderBy('storage_type')
            ->paginate($perPage);
        return success([
            'areas' => $areas->toArray()['data'],
            'page_bean' => [
                'total' => (int)$areas->total(),
                'page' => (int)$areas->currentPage(),
                'per_page' => (int)$areas->perPage(),
            ],
        ]);
    }
}
