<?php
namespace App\AdminRunners\StorageArea;


use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Exceptions\NotExistException;
use App\Models\StorageArea;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StorageAreaUpdateRunner extends Runner implements AdminRunner
{
    protected $areaId;

    public function setId($areaId)
    {
        $this->areaId = $areaId;
    }

    public function run(Request $request): JsonResponse
    {
        $type = $request->input('type');
        $code = $request->input('code');

        DB::beginTransaction();
        try {
            $area = StorageArea::find($this->areaId);
            if (!$area) {
                throw new NotExistException([
                    'message' => 'storage area not exist'
                ]);
            }
            if ($type) {
                $area->storage_type = $type;
            }
            if ($code) {
                $area->area_code = $code;
            }
            $area->save();
            DB::commit();
            return success();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }
}