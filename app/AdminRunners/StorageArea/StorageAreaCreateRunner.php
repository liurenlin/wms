<?php
namespace App\AdminRunners\StorageArea;


use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Services\StorageAreaService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StorageAreaCreateRunner extends Runner implements AdminRunner
{
    public function run(Request $request): JsonResponse
    {
        $type = $request->input('type');
        DB::beginTransaction();
        try {
            $service = new StorageAreaService();
            $area = $service->createStorageArea($type);
            DB::commit();
            return success(['id' => $area->id]);
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }
}