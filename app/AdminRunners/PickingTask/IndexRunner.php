<?php

namespace App\AdminRunners\PickingTask;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Repositories\PickingTaskRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class IndexRunner extends Runner implements AdminRunner
{
    public $pickingTaskRepository;

    public function __construct(PickingTaskRepository $pickingTaskRepository)
    {
        $this->pickingTaskRepository= $pickingTaskRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $page = (int) $request->get('page', 1);
        $perPage = (int) $request->get('per_page', 20);
        $querys = $request->query->all();
        $pagePickingTasks = $this->pickingTaskRepository->getListByPage($querys, $page, $perPage);
        return success($pagePickingTasks);
    }
}
