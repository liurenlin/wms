<?php

namespace App\AdminRunners\PickingTask;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Repositories\PickingTaskRepository;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UpdateTasksRunner extends Runner implements AdminRunner
{
    public $pickingTaskRepository;

    public function __construct(PickingTaskRepository $pickingTaskRepository)
    {
        $this->pickingTaskRepository = $pickingTaskRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $userId = Auth::id();
        $warehouseId = $request->header('warehouse-id');
        $state = (int) $request->get('state');

        $pickingTasks = json_decode($request->get('picking_tasks'), JSON_OBJECT_AS_ARRAY);
        $taskIds = collect($pickingTasks)->pluck('id')->toArray();
        if (!empty($taskIds)) {
            if (!$this->pickingTaskRepository->checkPickingTaskIds($taskIds, $state)) {
                return fail(__('picking.task_not_exists'));
            }
        }

        $this->pickingTaskRepository->updateTasksState($warehouseId, $userId, $taskIds, $state);
        return success();
    }
}
