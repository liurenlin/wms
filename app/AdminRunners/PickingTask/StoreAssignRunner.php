<?php

namespace App\AdminRunners\PickingTask;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Models\Outbound;
use App\Repositories\PickingTaskRepository;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StoreAssignRunner extends Runner implements AdminRunner
{
    public $pickingTaskRepository;

    public function __construct(PickingTaskRepository $pickingTaskRepository)
    {
        $this->pickingTaskRepository = $pickingTaskRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $warehouseId = $request->header('warehouse-id');
        $assignUserId = (int) $request->get('user_id');

        $pickingTasks = json_decode($request->get('picking_tasks'), JSON_OBJECT_AS_ARRAY);
        $taskIds = collect($pickingTasks)->pluck('id')->toArray();
        if (!empty($taskIds)) {
            if (!$this->pickingTaskRepository->checkPickingTaskIds($taskIds)) {
                return fail(__('picking.task_not_exists'));
            }
        }
        $taskAmount = (int) $request->get('task_amount');
        $taskType = (int) $request->get('task_type');
        if ($taskAmount) {
            $outboundCount = Outbound::query()
                ->where('warehouse_id', $warehouseId)
                ->where('is_need_intercept', 0)
                ->where('state', Outbound::STATE_FROZEN)
                ->count();
            if (!$outboundCount) {
                return fail('No outbound orders need to be picked！');
            }
        }

        $amount = $this->pickingTaskRepository->assingPickingTask($warehouseId, $assignUserId, $taskType, $taskIds, $taskAmount);
        return success(['amount' => $amount]);
    }
}
