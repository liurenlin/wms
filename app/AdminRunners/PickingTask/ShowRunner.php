<?php

namespace App\AdminRunners\PickingTask;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Repositories\PickingTaskRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ShowRunner extends Runner implements AdminRunner
{
    public $pickingTaskRepository;

    public function __construct(PickingTaskRepository $pickingTaskRepository)
    {
        $this->pickingTaskRepository = $pickingTaskRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $pickingTaskId = (int) $request->get('picking_task_id');
        // 检查出库单是否存在
        if (!$this->pickingTaskRepository->existsPickingTask($pickingTaskId)) {
            return fail(__('picking.task_not_exists'));
        }

        $pickingTask = $this->pickingTaskRepository->getDetail($pickingTaskId);
        return success($pickingTask);
    }
}
