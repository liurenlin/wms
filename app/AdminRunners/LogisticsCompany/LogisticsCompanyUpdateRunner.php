<?php
namespace App\AdminRunners\LogisticsCompany;


use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Consts\CommonConst;
use App\Exceptions\NotExistException;
use App\Models\LogisticsCompany;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class LogisticsCompanyUpdateRunner extends Runner implements AdminRunner
{
    protected $companyId;

    public function setId($id)
    {
        $this->companyId = $id;
        return $this;
    }

    public function run(Request $request): JsonResponse
    {
        $company = LogisticsCompany::find($this->companyId);
        if (!$company) {
            throw new NotExistException([
                'message' => 'logistics company not exist',
            ]);
        }
        $sender = $request->input('sender');
        $address = $request->input('address');
        $telephone = $request->input('sender_phone');
        if ($sender) {
            $company->sender = $sender;
        }
        if ($address) {
            $company->address = $address;
        }
        if ($telephone) {
            $company->telephone = $telephone;
        }
        $company->save();
        return success();
    }
}