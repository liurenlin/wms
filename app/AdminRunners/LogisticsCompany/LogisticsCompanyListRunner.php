<?php
namespace App\AdminRunners\LogisticsCompany;


use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Consts\CommonConst;
use App\Models\LogisticsCompany;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class LogisticsCompanyListRunner extends Runner implements AdminRunner
{
    private $listFields = [
        'id' => 'id',
        'code' => 'company_code as code',
        'name' => 'company_name as name',
        'contact' => 'contact',
        'cellphone' => 'cellphone',
        'sender' => 'sender',
        'address' => 'address',
        'type' => 'company_type as type',
        'telephone' => 'telephone as sender_phone',
    ];

    public function run(Request $request): JsonResponse
    {
        $perPage = $request->input('per_page') ?? CommonConst::DEFAULT_PER_PAGE;
        $companies = LogisticsCompany::select($this->listFields)
            ->orderBy('updated_at', 'desc')
            ->paginate($perPage);
        return success([
            'logistics_companies' => $companies->toArray()['data'],
            'page_bean' => [
                'total' => (int)$companies->total(),
                'page' => (int)$companies->currentPage(),
                'per_page' => (int)$companies->perPage(),
            ],
        ]);
    }
}