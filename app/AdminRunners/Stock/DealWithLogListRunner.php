<?php

namespace App\AdminRunners\Stock;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Repositories\StockRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class DealWithLogListRunner extends Runner implements AdminRunner
{
    public $stockRepository;

    public function __construct(StockRepository $stockRepository)
    {
        $this->stockRepository = $stockRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $warehouseId = (int) $request->header('warehouse-id');
        $page = (int) $request->get('page', 1);
        $perPage = (int) $request->get('per_page', 20);
        $query = $request->query->all();
        $dealWithLogs = $this->stockRepository->getDealWithLogsByPage($warehouseId, $query, $page, $perPage);
        return success($dealWithLogs);
    }
}
