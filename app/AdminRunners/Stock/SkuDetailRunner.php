<?php

namespace App\AdminRunners\Stock;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Models\ProductSku;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SkuDetailRunner extends Runner implements AdminRunner
{
    public function run(Request $request): JsonResponse
    {
        $productSkuModel = new ProductSku();
        $params = $request->all();
        $skuDetail = $productSkuModel->getSkuDetail($params);
        return success($skuDetail);
    }
}
