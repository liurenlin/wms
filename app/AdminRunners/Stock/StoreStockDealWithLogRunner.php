<?php

namespace App\AdminRunners\Stock;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Consts\CommonConst;
use App\Models\OpenMessageQueue;
use App\Models\StockLog;
use App\Models\StorageArea;
use App\Repositories\OutboundRepository;
use App\Repositories\PickingTaskRepository;
use App\Repositories\PutawayTaskRepository;
use App\Repositories\StockRepository;
use App\Services\MessageService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StoreStockDealWithLogRunner extends Runner implements AdminRunner
{
    public $stockRepository;
    public $putawayTaskRepository;
    public $outboundRepository;
    public $pickingTaskRepository;

    public function __construct(
        StockRepository $stockRepository,
        PutawayTaskRepository $putawayTaskRepository,
        OutboundRepository $outboundRepository,
        PickingTaskRepository $pickingTaskRepository
    )
    {
        $this->stockRepository = $stockRepository;
        $this->putawayTaskRepository = $putawayTaskRepository;
        $this->outboundRepository = $outboundRepository;
        $this->pickingTaskRepository = $pickingTaskRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $userId = Auth::id();
        $warehouseId = (int) $request->header('warehouse-id');
        if (!$warehouseId) {
            return fail();
        }

        $storageLocationCode = $request->get('storage_location_code');
        $inventoryAfter = (int) $request->get('inventory_after');
        $abnormalNumber = (string) $request->get('abnormal_number', '');
        $remark = $request->get('remark');
        // 查询库位当前库存
        $stock = $this->stockRepository->getStockByBin($warehouseId, $storageLocationCode);

        // 无需调整
        if ($inventoryAfter == $stock->quantity) {
            return success();
        }

        // 检查是否可以允许调整
        if ($inventoryAfter < $stock->frozen_quantity) {
            // 检查是否有，未完成的拣货任务
            $sumQuantity = $this->pickingTaskRepository->getSumQuantityOfUnPickingSku($stock->sku_code, $stock->bin);
            if ($sumQuantity > $inventoryAfter) {
                return fail('Error! You can only adjust the stock to ' . $sumQuantity . ' or greater.');
            }
        }

        DB::beginTransaction();
        try {
            $stockDealWithLog = $this->stockRepository->createStockDealWithLog(
                $warehouseId,
                $userId,
                $storageLocationCode,
                $stock->sku_code,
                $stock->quantity,
                $inventoryAfter,
                $abnormalNumber,
                $remark
            );

            $updateQuantity = $inventoryAfter - $stock->quantity;

            if ($inventoryAfter > $stock->quantity) {
                $this->stockRepository->updateStock(
                    $warehouseId,
                    $userId,
                    StorageArea::STORAGE_TYPE_PICKING,
                    StockLog::RELATION_TYPE_STOCK_DEAL_WITH_LOG,
                    $stockDealWithLog->id,
                    $stock->sku_code,
                    $storageLocationCode,
                    $updateQuantity,
                    0,
                    $updateQuantity
                );
                // 更新锁库
                $this->outboundRepository->frozenOutboundItems($warehouseId, $stock->sku_code);
            } elseif ($inventoryAfter < $stock->quantity) {
                if ($inventoryAfter < $stock->frozen_quantity) {
                    $cancelQuantity = $stock->frozen_quantity - $inventoryAfter;
                    $this->stockRepository->unFrozenStock(
                        $warehouseId,
                        $stock->sku_code,
                        $storageLocationCode,
                        $cancelQuantity,
                        $userId,
                        StockLog::RELATION_TYPE_STOCK_DEAL_WITH_LOG,
                        $stockDealWithLog->id
                    );
                }
                $this->stockRepository->updateStock(
                    $warehouseId,
                    $userId,
                    StorageArea::STORAGE_TYPE_PICKING,
                    StockLog::RELATION_TYPE_STOCK_DEAL_WITH_LOG,
                    $stockDealWithLog->id,
                    $stock->sku_code,
                    $storageLocationCode,
                    $updateQuantity,
                    0,
                    $updateQuantity
                );
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('StoreStockDealWithLogRunner Run Fail', [
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
            return fail();
        }
        $list[] = [
            'sku_code' => $stock->sku_code,
        ];
        MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_STOCK_NOTIFY, ['warehouse_id' => $warehouseId, 'list' => $list]);
        return success();
    }
}
