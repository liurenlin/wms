<?php
namespace App\AdminRunners\Warehouse;


use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Consts\CommonConst;
use App\Models\Warehouse;
use App\Services\WarehouseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class WarehouseCreateRunner extends Runner implements AdminRunner
{
    private $fields = [
        'name' => 'warehouse_name',
        'country' => 'country',
        'address' => 'address',
        'manager' => 'manager',
        'cellphone' => 'cellphone',
    ];

    public function run(Request $request): JsonResponse
    {
        $service = new WarehouseService();
        $warehouse = [];
        foreach ($this->fields as $field => $name) {
            $warehouse[$name] = $request->input($field) ?? '';
        }
        $warehouse['warehouse_code'] = $service->generateWarehouseCode($warehouse['country']);
        $warehouse = Warehouse::create($warehouse);
        return success([
            'id' => $warehouse->id,
        ]);
    }

}