<?php
namespace App\AdminRunners\Warehouse;


use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Exceptions\NotExistException;
use App\Models\Warehouse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;

class WarehouseUpdateRunner extends Runner implements AdminRunner
{
    private $fields = [
        'name' => 'warehouse_name',
        'country' => 'country',
        'address' => 'address',
        'manager' => 'manager',
        'cellphone' => 'cellphone',
    ];

    private $warehouseId;

    public function setId($warehouseId)
    {
        $this->warehouseId = $warehouseId;
    }

    public function run(Request $request): JsonResponse
    {
        DB::beginTransaction();
        try {
            $warehouse = Warehouse::find($this->warehouseId);
            if (!$warehouse) {
                throw new NotExistException([
                    'message' => 'warehouse not exist',
                ]);
            }
            foreach ($this->fields as $field => $name) {
                if ($request->input($field)) {
                    $warehouse->$name = $request->input($field);
                }
            }
            $warehouse->save();
            DB::commit();
            return success();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

}