<?php
namespace App\AdminRunners\Warehouse;


use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Consts\CommonConst;
use App\Models\Warehouse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class WarehouseListRunner extends Runner implements AdminRunner
{
    private $listFields = [
        'id' => 'id',
        'name' => 'warehouse_name as name',
        'country' => 'country',
        'code' => 'warehouse_code as code',
        'address' => 'address',
        'manager' => 'manager',
        'cellphone' => 'cellphone',
    ];

    public function run(Request $request): JsonResponse
    {
        $perPage = $request->input('per_page') ?? CommonConst::DEFAULT_PER_PAGE;
        $warehouses = Warehouse::select($this->listFields)
            ->orderBy('updated_at', 'desc')
            ->paginate($perPage);
        return success([
            'warehouses' => $warehouses->toArray()['data'],
            'page_bean' => [
                'total' => (int)$warehouses->total(),
                'page' => (int)$warehouses->currentPage(),
                'per_page' => (int)$warehouses->perPage(),
            ],
        ]);
    }

}