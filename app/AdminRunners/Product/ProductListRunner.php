<?php
namespace App\AdminRunners\Product;


use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Consts\CommonConst;
use App\Models\ProductSku;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ProductListRunner extends Runner implements AdminRunner
{
    private $listFields = [
        'id' => 'id',
        'spu_code' => 'product_code',
        'sku_code' => 'sku_code',
        'variant' => 'sku_attribute',
        'sku_name' => 'sku_name',
        'weight' => 'weight',
        'width' => 'width',
        'height' => 'height',
        'length' => 'length',
    ];

    public function run(Request $request): JsonResponse
    {
        $perPage = $request->input('per_page') ?? CommonConst::DEFAULT_PER_PAGE;
        $spuSearch = $request->input('spu_code');
        $skuSearch = $request->input('sku_code');
        $products = ProductSku::with(['product', 'images'])
            ->select(['product_skus.*', 'products.product_code'])
            ->leftJoin('products', 'product_id', '=', 'products.id')
            ->when($skuSearch, function (Builder $query) use ($skuSearch) {
                $query->where('sku_code', $skuSearch);
            })
            ->when($spuSearch, function (Builder $query) use ($spuSearch) {
                $query->where('product_code', $spuSearch);
            })
            ->orderBy('product_skus.updated_at', 'desc')
            ->paginate($perPage);
        $productData = [];
        foreach ($products as $product) {
            $data = [];
            foreach ($this->listFields as $key=>$name) {
                $data[$key] = $product->$name;
            }
            $images = [];
            if ($product->images) {
                foreach ($product->images as $image) {
                    if ($image->url) {
                        $images[] = $image->url;
                    }
                }
            }
            $data['image_url'] = $images;
            $productData[] = $data;
        }
        return success([
            'skus' => $productData,
            'page_bean' => [
                'total' => (int)$products->total(),
                'page' => (int)$products->currentPage(),
                'per_page' => (int)$products->perPage(),
            ],
        ]);
    }
}