<?php
namespace App\AdminRunners\Product;


use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Exceptions\NotExistException;
use App\Models\OpenMessageQueue;
use App\Models\ProductSku;
use App\Services\MessageService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProductUpdateWeightAndVolumeRunner extends Runner implements AdminRunner
{
    protected $skuId;

    protected $fields = [
        'weight', 'width', 'height', 'length',
    ];

    public function setId($id)
    {
        $this->skuId = $id;
        return $this;
    }

    public function run(Request $request): JsonResponse
    {
        $sku = ProductSku::find($this->skuId);
        if (!$sku) {
            throw new NotExistException([
                'message' => 'sku not exist',
            ]);
        }
        foreach ($this->fields as $field) {
            if ($fieldValue = $request->input($field)) {
                $sku->$field = $fieldValue;
            }
        }
        $sku->save();
        MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_SYNC_PRODUCT_SKU, ['sku_code' => $sku->sku_code]);
        return success();
    }
}
