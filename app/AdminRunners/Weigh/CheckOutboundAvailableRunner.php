<?php

namespace App\AdminRunners\Weigh;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Models\Abnormal;
use App\Models\OpenMessageQueue;
use App\Models\Outbound;
use App\Models\StockLog;
use App\Models\StorageArea;
use App\Repositories\AbnormalRepository;
use App\Repositories\CheckTaskRepository;
use App\Repositories\OutboundRepository;
use App\Repositories\StockRepository;
use App\Repositories\WeighRepository;
use App\Services\Clients\OmsClient;
use App\Services\MessageService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class CheckOutboundAvailableRunner extends Runner implements AdminRunner
{
    public $weighRepository;
    public $checkTaskRepository;
    public $outboundRepository;
    public $abnormalRepository;
    public $stockRepository;

    public function __construct(
        WeighRepository $weighRepository,
        CheckTaskRepository $checkTaskRepository,
        OutboundRepository $outboundRepository,
        AbnormalRepository $abnormalRepository,
        StockRepository $stockRepository
    )
    {
        $this->weighRepository = $weighRepository;
        $this->checkTaskRepository = $checkTaskRepository;
        $this->outboundRepository = $outboundRepository;
        $this->abnormalRepository = $abnormalRepository;
        $this->stockRepository = $stockRepository;

    }

    public function run(Request $request): JsonResponse
    {
        $userId = Auth::id();
        $warehouseId = $request->header('warehouse-id');
        $pickingBasketCode = $request->get('picking_basket_code', '');
        if (empty($pickingBasketCode)) {
            return fail();
        }

        // 检查 code 类型
        if (Str::startsWith($pickingBasketCode, 'BAS-')) {
            // 检查参数
            $pickingTaskBasket = $this->weighRepository->getPickingTaskBasket($pickingBasketCode);
            if (!$pickingTaskBasket) {
                return fail(__('check.basket_wrong'));
            }
            // 检查出库单
            $outbound = $this->outboundRepository->getOutbound($warehouseId, $pickingTaskBasket->outbound_number);
        } else {
            // 检查出库单
            $outbound = $this->outboundRepository->getOutbound($warehouseId, $pickingBasketCode);
        }
        if (!$outbound) {
            return fail(__('check.outbound_wrong'));
        }
        if ($outbound->state != Outbound::STATE_CHECK_COMPLETED) {
            return fail(__('check.outbound_not_completed', ['state' => $this->outboundRepository->getOutboundStateTip($outbound->state)]));
        }
        $intercepted = false;
        if ($outbound->is_need_intercept == Outbound::IS_NEED_INTERCEPT_TRUE) {
            $intercepted = true;
        }
        if (!$intercepted) {
            // 检查，调用第三方API
            $omsClient = new OmsClient();
            $res = $omsClient->checkOrderShipped($outbound->order_number);
            if (!$res) {
                $intercepted = true;
            }
        }

        if ($intercepted) {
            $interceptedDone = false;
            DB::beginTransaction();
            try {
                // 创建异常
                $abnormal = $this->abnormalRepository->createAbnormal(
                    $warehouseId,
                    $userId,
                    Abnormal::RELATION_TYPE_WEIGH,
                    $outbound->outbound_number,
                    '',
                    '',
                    $outbound->outbound_number,
                    Abnormal::REASON_INTERCEPTED,
                    Abnormal::STATE_CREATED
                );
                $outboundItems = $this->outboundRepository->getOutboundItems($outbound->id);
                // 添加异常单 item
                $skus = collect($outboundItems)->transform(function ($item) {
                    return [
                        'sku_code' => $item['sku_code'],
                        'quantity' => $item['quantity'],
                    ];
                })->toArray();
                $this->abnormalRepository->createAbnormalItems($abnormal->id, $skus);
                foreach ($skus as $sku) {
                    $this->stockRepository->updateStock(
                        $warehouseId,
                        $userId,
                        StorageArea::STORAGE_TYPE_PACKAGE,
                        StockLog::RELATION_TYPE_ABNORMAL,
                        $abnormal->id,
                        $sku['sku_code'],
                        '',
                        -$sku['quantity']
                    );
                    $this->stockRepository->updateStock(
                        $warehouseId,
                        $userId,
                        StorageArea::STORAGE_TYPE_ABNORMAL,
                        StockLog::RELATION_TYPE_ABNORMAL,
                        $abnormal->id,
                        $sku['sku_code'],
                        '',
                        $sku['quantity']
                    );
                }

                $this->outboundRepository->updateState(
                    $outbound->id,
                    Outbound::STATE_INTERCEPTED,
                    $userId,
                    'Abnormal order number ' . $abnormal->abnormal_number
                );
                DB::commit();
                $interceptedDone = true;
            } catch (\Exception $e) {
                DB::rollBack();
                Log::error('CheckOutboundAvailable Fail', [
                    'exception' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ]);
            }
            if ($interceptedDone) {
                MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_ORDER_CANCEL_RESULT_NOTIFY, ['order_number' => $outbound->order_number, 'state' => OmsClient::CANCEL_STATE_SUCCESS]);
            } else {
                MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_ORDER_CANCEL_RESULT_NOTIFY, ['order_number' => $outbound->order_number, 'state' => OmsClient::CANCEL_STATE_FAIL]);
            }
            MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_ORDER_STATUS_NOTIFY, ['order_number' => $outbound->order_number]);
            return fail('The order has been intercepted');
        }
        MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_ORDER_STATUS_NOTIFY, ['order_number' => $outbound->order_number]);
        return success();

    }
}
