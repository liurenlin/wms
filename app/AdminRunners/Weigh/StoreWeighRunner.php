<?php

namespace App\AdminRunners\Weigh;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Models\LogisticsCompany;
use App\Models\OpenMessageQueue;
use App\Models\Outbound;
use App\Models\StockLog;
use App\Models\StorageArea;
use App\Repositories\CheckTaskRepository;
use App\Repositories\OutboundRepository;
use App\Repositories\StockRepository;
use App\Repositories\WeighRepository;
use App\Services\MessageService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class StoreWeighRunner extends Runner implements AdminRunner
{
    public $weighRepository;
    public $checkTaskRepository;
    public $outboundRepository;
    public $stockRepository;

    public function __construct(
        WeighRepository $weighRepository,
        CheckTaskRepository $checkTaskRepository,
        OutboundRepository $outboundRepository,
        StockRepository $stockRepository
    )
    {
        $this->weighRepository = $weighRepository;
        $this->checkTaskRepository = $checkTaskRepository;
        $this->outboundRepository = $outboundRepository;
        $this->stockRepository = $stockRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $userId = Auth::id();
        $warehouseId = $request->header('warehouse-id');
        $pickingBasketCode = $request->get('picking_basket_code');
        $weight = (float) $request->get('weight');

        if (Str::startsWith($pickingBasketCode, 'BAS-')) {
            // 检查参数
            $pickingTaskBasket = $this->weighRepository->getPickingTaskBasket($pickingBasketCode);
            if (!$pickingTaskBasket) {
                return fail(__('check.basket_wrong'));
            }
            // 检查出库单
            $outbound = $this->outboundRepository->getOutbound($warehouseId, $pickingTaskBasket->outbound_number);
        } else {
            // 检查出库单
            $outbound = $this->outboundRepository->getOutbound($warehouseId, $pickingBasketCode);
            $pickingTaskBasket = $this->checkTaskRepository->getPickingTaskBasketByOutboundNumber($pickingBasketCode);
        }
        if (!$outbound) {
            return fail(__('check.outbound_wrong'));
        }
        if ($outbound->state != Outbound::STATE_CHECK_COMPLETED) {
            return fail(__('check.outbound_not_completed', ['state' => $this->outboundRepository->getOutboundStateTip($outbound->state)]));
        }

        DB::beginTransaction();
        try {

            // 查找物流商
            $logisticsCompany = $this->weighRepository->getBestLogisticsCompany(
                [
                    'receiving_country' => $outbound->receiving_country,
                    'receiving_state' => $outbound->receiving_state,
                    'receiving_city' => $outbound->receiving_city,
                    'receiving_district' => $outbound->receiving_district,
                    'receiving_address' => $outbound->receiving_address,
                ],
                $weight
            );

            // 记录称重记录
            $weigh = $this->weighRepository->storeWeigh(
                $warehouseId,
                $userId,
                $outbound->outbound_number,
                $pickingTaskBasket->picking_task_id,
                $pickingTaskBasket->basket_code,
                $weight,
                $outbound->logistics_company_id,
                $outbound->logistics_number
            );

            // 更新 outbound 状态
            $this->outboundRepository->updateState(
                $outbound->id,
                Outbound::STATE_WEIGHED,
                $userId,
                'Package weight: ' . $weight . 'kg'
            );

            // 更新 basket 占用状态
            $this->weighRepository->releaseBasket($warehouseId, $pickingTaskBasket->basket_code);

            // 更新出库去库存
            $outboundItems = $this->outboundRepository->getOutboundItems($outbound->id);
            foreach ($outboundItems as $outboundItem) {
                $this->stockRepository->updateStock(
                    $warehouseId,
                    $userId,
                    StorageArea::STORAGE_TYPE_PACKAGE,
                    StockLog::RELATION_TYPE_WEIGH,
                    $weigh->id,
                    $outboundItem->sku_code,
                    '',
                    -$outboundItem->quantity
                );
                $this->stockRepository->updateStock(
                    $warehouseId,
                    $userId,
                    StorageArea::STORAGE_TYPE_OUTBOUND,
                    StockLog::RELATION_TYPE_WEIGH,
                    $weigh->id,
                    $outboundItem->sku_code,
                    '',
                    $outboundItem->quantity
                );
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('StoreWeigh Fail', [
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
            return fail();
        }

        try {
            MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_ORDER_OUT, ['outbound_number' => $outbound->outbound_number]);
        } catch (\Exception $e) {
            Log::error('OmsOrderOutEvent Fail', [
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
        }
        MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_ORDER_STATUS_NOTIFY, ['order_number' => $outbound->order_number]);

        return success([
            'outbound_number' => $outbound->outbound_number,
            'logistics_company' => $logisticsCompany->company_name,
            'logistics_number' => $outbound->logistics_number,
            'sender' => LogisticsCompany::query()->where('id', $outbound->logistics_company_id)->value('sender') ?? '',
            'receiving_country' => $outbound->receiving_country,
            'receiving_region' => $outbound->receiving_region,
            'receiving_state' => $outbound->receiving_state,
            'receiving_city' => $outbound->receiving_city,
            'receiving_district' => $outbound->receiving_district,
            'receiving_address' => $outbound->receiving_address,
            'receiving_postcode' => $outbound->receiving_postcode,
            'receiving_cellphone' => $outbound->receiving_cellphone,
            'receiving_user_name' => $outbound->receiving_user_name,
            'receiving_first_name' => $outbound->receiving_first_name,
            'receiving_last_name' => $outbound->receiving_last_name,
            'cod_amount' => $outbound->total_amount,
        ]);
    }
}
