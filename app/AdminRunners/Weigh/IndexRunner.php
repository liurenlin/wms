<?php

namespace App\AdminRunners\Weigh;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Repositories\WeighRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class IndexRunner extends Runner implements AdminRunner
{
    public $weighRepository;

    public function __construct(WeighRepository $weighRepository)
    {
        $this->weighRepository = $weighRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $page = (int) $request->get('page', 1);
        $perPage = (int) $request->get('per_page', 20);
        $querys = $request->query->all();
        $pageWeighs = $this->weighRepository->getListByPage($querys, $page, $perPage);
        return success($pageWeighs);
    }
}
