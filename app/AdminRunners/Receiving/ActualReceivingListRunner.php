<?php

namespace App\AdminRunners\Receiving;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Models\ActualReceiving;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ActualReceivingListRunner extends Runner implements AdminRunner
{
    public function run(Request $request): JsonResponse
    {
        $actualReceivingModel = new ActualReceiving();
        $params = $request->all();
        $actualReceivingList = $actualReceivingModel->getActualReceivingList($params);
        return success($actualReceivingList);
    }
}
