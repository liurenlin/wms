<?php

namespace App\AdminRunners\Receiving;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Models\ActualReceiving;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ActualReceivingDetailRunner extends Runner implements AdminRunner
{
    public function run(Request $request): JsonResponse
    {
        $actualReceivingId = $request->get('actual_receiving_id');
        if (is_null($actualReceivingId) || !is_numeric($actualReceivingId)) {
            return fail(__('check.check_log_params_wrong'));
        }
        $actualReceivingModel = new ActualReceiving();
        $params = $request->all();
        $actualReceivingDetail = $actualReceivingModel->getActualReceivingDetail($params);
        return success($actualReceivingDetail);
    }
}
