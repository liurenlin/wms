<?php

namespace App\AdminRunners\Receiving;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Models\PreReceiving;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PreReceivingListRunner extends Runner implements AdminRunner
{
    public function run(Request $request): JsonResponse
    {
        $preReceivingModel = new PreReceiving();
        $params = $request->all();
        $preReceivingList = $preReceivingModel->getPreReceivingList($params);
        return success($preReceivingList);
    }
}
