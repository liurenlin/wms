<?php

namespace App\AdminRunners\Receiving;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Models\PutawayTask;
use App\Models\ReceivingCart;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PutAwayCartDetailRunner extends Runner implements AdminRunner
{
    public function run(Request $request): JsonResponse
    {
        $cartId = $request->get('cart_id');
        if (is_null($cartId) || !is_numeric($cartId)) {
            return fail(__('check.check_log_params_wrong'));
        }
        $receivingCartModel = new ReceivingCart();
        $params = $request->all();
        $receivingCartDetail = $receivingCartModel->getPutAwayCartDetail($params);
        return success($receivingCartDetail);
    }
}
