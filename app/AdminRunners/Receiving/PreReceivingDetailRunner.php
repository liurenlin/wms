<?php

namespace App\AdminRunners\Receiving;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Models\PreReceiving;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PreReceivingDetailRunner extends Runner implements AdminRunner
{
    public function run(Request $request): JsonResponse
    {
        $preReceivingId = $request->get('pre_receiving_id');
        if (is_null($preReceivingId) || !is_numeric($preReceivingId)) {
            return fail(__('check.check_log_params_wrong'));
        }
        $preReceivingModel = new PreReceiving();
        $params = $request->all();
        $preReceivingDetail = $preReceivingModel->getPreReceivingDetail($params);
        return success($preReceivingDetail);
    }
}
