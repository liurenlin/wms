<?php

namespace App\AdminRunners\Receiving;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Models\PutawayTask;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PutAwayTaskListRunner extends Runner implements AdminRunner
{
    public function run(Request $request): JsonResponse
    {
        $putAwayTaskModel = new PutawayTask();
        $params = $request->all();
        $putAwayTaskList = $putAwayTaskModel->getPutAwayTaskList($params);
        return success($putAwayTaskList);
    }
}
