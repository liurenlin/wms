<?php

namespace App\AdminRunners\Receiving;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Models\PutawayTask;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PutAwayTaskDetailRunner extends Runner implements AdminRunner
{
    public function run(Request $request): JsonResponse
    {
        $putAwayTaskId = $request->get('putaway_task_id');
        if (is_null($putAwayTaskId) || !is_numeric($putAwayTaskId)) {
            return fail(__('check.check_log_params_wrong'));
        }
        $putAwayTaskModel = new PutawayTask();
        $params = $request->all();
        $putAwayTaskDetail = $putAwayTaskModel->getPutAwayTaskDetail($params);
        return success($putAwayTaskDetail);
    }
}
