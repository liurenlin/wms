<?php

namespace App\AdminRunners\Receiving;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Models\PutawayTask;
use App\Models\ReceivingCart;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PutAwayCartListRunner extends Runner implements AdminRunner
{
    public function run(Request $request): JsonResponse
    {
        $receivingCartModel = new ReceivingCart();
        $params = $request->all();
        $receivingCartList = $receivingCartModel->getPutAwayCartList($params);
        return success($receivingCartList);
    }
}
