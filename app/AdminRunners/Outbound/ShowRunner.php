<?php

namespace App\AdminRunners\Outbound;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Repositories\OutboundRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ShowRunner extends Runner implements AdminRunner
{
    public $outboundRepository;

    public function __construct(OutboundRepository $outboundRepository)
    {
        $this->outboundRepository = $outboundRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $ouboundId = (int) $request->get('outbound_id');
        // 检查出库单是否存在
        if (!$this->outboundRepository->existsOutbound($ouboundId)) {
            return fail(__('outbound.not_exists'));
        }

        $outbound = $this->outboundRepository->getDetail($ouboundId);
        return success($outbound);
    }
}
