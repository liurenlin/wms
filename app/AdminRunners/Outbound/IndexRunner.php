<?php

namespace App\AdminRunners\Outbound;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Repositories\OutboundRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class IndexRunner extends Runner implements AdminRunner
{
    public $outboundRepository;

    public function __construct(OutboundRepository $outboundRepository)
    {
        $this->outboundRepository = $outboundRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $page = (int) $request->get('page', 1);
        $perPage = (int) $request->get('per_page', 20);
        $querys = $request->query->all();
        $pageOutbounds = $this->outboundRepository->getListByPage($querys, $page, $perPage);
        return success($pageOutbounds);
    }
}
