<?php

namespace App\AdminRunners;

use Illuminate\Http\Request;

interface AdminRunner
{

    public function run(Request $request);
}
