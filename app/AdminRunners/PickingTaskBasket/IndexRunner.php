<?php

namespace App\AdminRunners\PickingTaskBasket;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Repositories\PickingTaskBasketRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class IndexRunner extends Runner implements AdminRunner
{
    public $pickingTaskBasketRepository;

    public function __construct(PickingTaskBasketRepository $pickingTaskBasketRepository)
    {
        $this->pickingTaskBasketRepository= $pickingTaskBasketRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $page = (int) $request->get('page', 1);
        $perPage = (int) $request->get('per_page', 20);
        $querys = $request->query->all();
        $pagePickingTaskBaskets = $this->pickingTaskBasketRepository->getListByPage($querys, $page, $perPage);
        return success($pagePickingTaskBaskets);
    }
}
