<?php

namespace App\AdminRunners\PickingTaskBasket;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Repositories\PickingTaskBasketRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ShowRunner extends Runner implements AdminRunner
{
    public $pickingTaskBasketRepository;

    public function __construct(PickingTaskBasketRepository $pickingTaskBasketRepository)
    {
        $this->pickingTaskBasketRepository = $pickingTaskBasketRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $pickingTaskBasketId = (int) $request->get('picking_task_basket_id');
        $pickingTaskBasket = $this->pickingTaskBasketRepository->getDetail($pickingTaskBasketId);
        return success($pickingTaskBasket);
    }
}
