<?php

namespace App\AdminRunners\User;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class StoreUserRunner extends Runner implements AdminRunner
{
    public $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $name = $request->get('name');
        $password = $request->get('password');
        $email = $request->get('email');
        if (empty($email)) {
            // 自动生成邮箱
            $email = $this->generateEmail();
        }
        $user = $this->userRepository->createUser($name, $password, $email);
        return success(['id' => $user->id]);
    }

    private function generateEmail()
    {
        $email = Str::random(12) . '@abukito.com';
        $user = User::query()->where('email', $email)->first();
        if ($user) {
            return self::generateEmail();
        }
        return $email;
    }
}
