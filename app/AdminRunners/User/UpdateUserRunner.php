<?php

namespace App\AdminRunners\User;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Repositories\UserRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class UpdateUserRunner extends Runner implements AdminRunner
{
    public $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $userId = (int) $request->input('user_id');
        $warehouseId = $request->header('warehouse-id');
        $roles = json_decode($request->get('roles'), JSON_OBJECT_AS_ARRAY);
        if (!$this->userRepository->checkRoles($roles)) {
            return fail();
        }
        $res = $this->userRepository->updateUserRoles($warehouseId, $userId, $roles);
        if (!$res) {
            return fail();
        }
        return success();
    }
}
