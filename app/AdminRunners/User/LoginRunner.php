<?php

namespace App\AdminRunners\User;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Repositories\PermissionRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class LoginRunner extends Runner implements AdminRunner
{
    public $userRepository;
    public $permissionRepository;

    public function __construct(UserRepository $userRepository, PermissionRepository $permissionRepository)
    {
        $this->userRepository = $userRepository;
        $this->permissionRepository = $permissionRepository;
    }

    public function run(Request $request): JsonResponse
    {
        // 校验登录
        $credentials = $request->only(['name', 'password']);
        if ($token = auth('api')->attempt($credentials)) {
            $user = auth('api')->user();
            $menus = $this->permissionRepository->getUserMenus($user->id);
            return success([
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'token' => $token,
                'menus' => $menus,
                'permission_groups' => collect($menus)->pluck('subs')->collapse()->toArray(),
                'warehouses' => $this->userRepository->getUserWarehouses($user->id)
            ]);
        } else {
            return fail(__('auth.failed'));
        }
    }
}
