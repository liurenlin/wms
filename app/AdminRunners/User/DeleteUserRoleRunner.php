<?php

namespace App\AdminRunners\User;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Repositories\UserRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class DeleteUserRoleRunner extends Runner implements AdminRunner
{
    public $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $userId = (int) $request->get('user_id');
        $roleId = (int) $request->get('role_id');
        if (!$this->userRepository->checkRoles([['id' => $roleId]])) {
            return fail();
        }
        $res = $this->userRepository->deleteUserRole($userId, $roleId);
        if (!$res) {
            return fail();
        }
        return success();
    }
}
