<?php

namespace App\AdminRunners\User;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Repositories\UserRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserListRunner extends Runner implements AdminRunner
{
    public $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $users = $this->userRepository->getUserList();
        return success(['users' => $users]);
    }
}
