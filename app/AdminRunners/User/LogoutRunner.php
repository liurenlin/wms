<?php

namespace App\AdminRunners\User;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class LogoutRunner extends Runner implements AdminRunner
{
    public function run(Request $request): JsonResponse
    {
        // 校验登录
        auth()->logout();
        return success();
    }
}
