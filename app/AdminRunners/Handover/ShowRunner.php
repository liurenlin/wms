<?php

namespace App\AdminRunners\Handover;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Repositories\HandoverRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ShowRunner extends Runner implements AdminRunner
{
    public $handoverRepository;

    public function __construct(HandoverRepository $handoverRepository)
    {
        $this->handoverRepository = $handoverRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $handoverId = (int) $request->get('handover_id');
        // 检查出库单是否存在
        if (!$this->handoverRepository->existsHandover($handoverId)) {
            return fail(__('check.task_not_exists'));
        }

        $pickingTask = $this->handoverRepository->getDetail($handoverId);
        return success($pickingTask);
    }
}
