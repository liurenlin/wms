<?php

namespace App\AdminRunners\Handover;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Repositories\HandoverRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class IndexRunner extends Runner implements AdminRunner
{
    public $handoverRepository;

    public function __construct(HandoverRepository $handoverRepository)
    {
        $this->handoverRepository = $handoverRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $page = (int) $request->get('page', 1);
        $perPage = (int) $request->get('per_page', 20);
        $querys = $request->query->all();
        $pageHandovers = $this->handoverRepository->getListByPage($querys, $page, $perPage);
        return success($pageHandovers);
    }
}
