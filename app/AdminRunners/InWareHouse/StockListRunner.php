<?php

namespace App\AdminRunners\InWareHouse;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Models\Stock;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class StockListRunner extends Runner implements AdminRunner
{
    public function run(Request $request): JsonResponse
    {
        $stockModel = new Stock();
        $params = $request->all();
        $AllStocks = $stockModel->getStockList($params);
        return success($AllStocks);
    }
}
