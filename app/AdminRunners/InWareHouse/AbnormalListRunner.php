<?php

namespace App\AdminRunners\InWareHouse;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Models\Abnormal;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AbnormalListRunner extends Runner implements AdminRunner
{
    public function run(Request $request): JsonResponse
    {
        $abnormalModel = new Abnormal();
        $params = $request->all();
        $abnormalLists = $abnormalModel->getAbnormalList($params);
        return success($abnormalLists);
    }
}
