<?php

namespace App\AdminRunners\InWareHouse;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Models\StockDealWithLog;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class StockManagementRunner extends Runner implements AdminRunner
{
    public function run(Request $request): JsonResponse
    {
        $stockModel = new StockDealWithLog();
        $params = $request->all();
        $StockManagement = $stockModel->getStockManagement($params);
        return success($StockManagement);
    }
}
