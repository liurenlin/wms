<?php

namespace App\AdminRunners\InWareHouse;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Models\Abnormal;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AbnormalShowRunner extends Runner implements AdminRunner
{
    public function run(Request $request): JsonResponse
    {
        $abnormalId = $request->get('abnormal_id');
        if (is_null($abnormalId) || !is_numeric($abnormalId)) {
            return fail(__('check.check_log_params_wrong'));
        }
        $abnormalModel = new Abnormal();
        $abnormalDetail = $abnormalModel->getAbnormalDetail($abnormalId);
        return success($abnormalDetail);
    }
}
