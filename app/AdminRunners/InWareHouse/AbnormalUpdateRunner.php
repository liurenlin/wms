<?php

namespace App\AdminRunners\InWareHouse;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Models\Abnormal;
use App\Models\AbnormalItem;
use App\Models\OpenMessageQueue;
use App\Models\Outbound;
use App\Models\PreReceiving;
use App\Models\StockLog;
use App\Models\StorageArea;
use App\Repositories\AbnormalRepository;
use App\Repositories\CheckTaskRepository;
use App\Repositories\OutboundRepository;
use App\Repositories\PickingTaskRepository;
use App\Repositories\PreReceivingRepository;
use App\Repositories\PutawayTaskRepository;
use App\Repositories\StockRepository;
use App\Repositories\WeighRepository;
use App\Services\MessageService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AbnormalUpdateRunner extends Runner implements AdminRunner
{

    public $abnormalRepository;
    public $preReceivingRepository;
    public $stockRepository;
    public $pickingTaskRepository;
    public $checkTaskRepository;
    public $outboundRepository;
    public $weighRepository;

    public function __construct(
        AbnormalRepository $abnormalRepository,
        PreReceivingRepository $preReceivingRepository,
        StockRepository $stockRepository,
        PickingTaskRepository $pickingTaskRepository,
        CheckTaskRepository $checkTaskRepository,
        OutboundRepository $outboundRepository,
        WeighRepository $weighRepository
    )
    {
        $this->abnormalRepository = $abnormalRepository;
        $this->preReceivingRepository = $preReceivingRepository;
        $this->stockRepository = $stockRepository;
        $this->pickingTaskRepository = $pickingTaskRepository;
        $this->checkTaskRepository = $checkTaskRepository;
        $this->outboundRepository = $outboundRepository;
        $this->weighRepository = $weighRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $userId = Auth::id();
        $abnormalId = (int) $request->get('abnormal_id');
        $stateAfter = (int) $request->get('state');
        $abnormal = Abnormal::query()->find($abnormalId);
        if (!$abnormal) {
            return fail('abnormal not exist');
        }
        if (in_array($abnormal->state, [
            Abnormal::STATE_CLOSED,
            Abnormal::STATE_COMPLETED,
        ])) {
            return fail('the abnormal status not allowed to changed');
        }
        if ($abnormal->state > $stateAfter) {
            return fail('the status is wrong');
        }

        $abnormalItems = AbnormalItem::query()->where('abnormal_id', $abnormalId)->get();

        // 检查 sku
        if ($request->get('inbound_products')
            && !$request->get('sku_code')
            && $abnormalItems->isEmpty()
        ) {
            return fail('Error! SKU is not recorded in the system');
        }

        DB::beginTransaction();
        try {
            // update state
            $this->abnormalRepository->updateState($abnormalId, $stateAfter);

            // process
            $this->abnormalRepository->createAbnormalProcess(
                $abnormalId,
                $userId,
                $abnormal->state,
                $stateAfter,
                $request->get('content')
            );
            $outbound = null;
            switch ($abnormal->abnormal_type) {
                case Abnormal::TYPE_RECEIVING_EXTRA:
                case Abnormal::TYPE_PUTAWAY_MISSING:
                    DB::commit();
                    return success();
                    break;
                case Abnormal::TYPE_RECEIVING_DEFECTIVE:
                case Abnormal::TYPE_RECEIVING_WRONG:
                case Abnormal::TYPE_PUTAWAY_EXTRA:
                case Abnormal::TYPE_PUTAWAY_WRONG:
                case Abnormal::TYPE_ORDER_INTERCEPTED:
                    // 处理 sku
                    $preReceiving = $this->preReceivingRepository->createPreReceiving(
                        $abnormal->warehouse_id,
                        PreReceiving::TYPE_ABNORMAL_ORDER,
                        $abnormal->abnormal_number,
                        date('Y-m-d H:i:s')
                    );
                    $items = [];
                    if ($request->get('sku_code')) {
                        AbnormalItem::create([
                            'abnormal_id' => $abnormalId,
                            'sku_code' => $request->get('sku_code'),
                            'quantity' => 1,
                        ]);
                        $items[] = [
                            'sku_code' => $request->get('sku_code'),
                            'quantity' => 1,
                        ];
                    } else {
                        foreach ($abnormalItems as $abnormalItem) {
                            $items[] = [
                                'sku_code' => $abnormalItem->sku_code,
                                'quantity' => $abnormalItem->quantity,
                            ];
                        }
                    }
                    // 生成 items
                    $this->preReceivingRepository->createItems($preReceiving->id, $items);
                    // 异常区库存减少
                    foreach ($items as $item) {
                        $this->stockRepository->updateStock(
                            $abnormal->warehouse_id,
                            $userId,
                            StorageArea::STORAGE_TYPE_ABNORMAL,
                            StockLog::RELATION_TYPE_ABNORMAL,
                            $abnormalId,
                            $item['sku_code'],
                            '',
                            -$item['quantity']
                        );
                    }
                    break;
                case Abnormal::TYPE_PICKING_MISSING:
                    $pickingTask = $this->pickingTaskRepository->getPickingTaskByNumber($abnormal->relation_number);
                    $pickingTaskBasket = $this->pickingTaskRepository->getPickingTaskBasket($pickingTask->id, $abnormal->basket_code);
                    $outbound = $this->outboundRepository->getOutbound($abnormal->warehouse_id, $pickingTaskBasket->outbound_number);
                case Abnormal::TYPE_CHECKING_DEFECTIVE:
                case Abnormal::TYPE_CHECKING_FAILED:
                    // 检查订单状态
                    if (!$outbound) {
                        $checkTask = $this->checkTaskRepository->getCheckTaskByNumber($abnormal->relation_number);
                        $outbound = $this->outboundRepository->getOutbound($abnormal->warehouse_id, $checkTask->outbound_number);
                    }
                    if ($outbound->state == Outbound::STATE_CANCELLED) {
                        // 处理 sku
                        $preReceiving = $this->preReceivingRepository->createPreReceiving(
                            $abnormal->warehouse_id,
                            PreReceiving::TYPE_ABNORMAL_ORDER,
                            $abnormal->abnormal_number,
                            date('Y-m-d H:i:s')
                        );
                        $items = [];
                        foreach ($abnormalItems as $abnormalItem) {
                            $items[] = [
                                'sku_code' => $abnormalItem->sku_code,
                                'quantity' => $abnormalItem->quantity,
                            ];
                        }
                        // 生成 items
                        $this->preReceivingRepository->createItems($preReceiving->id, $items);
                        // 异常区库存减少
                        foreach ($items as $item) {
                            $this->stockRepository->updateStock(
                                $abnormal->warehouse_id,
                                $userId,
                                StorageArea::STORAGE_TYPE_ABNORMAL,
                                StockLog::RELATION_TYPE_ABNORMAL,
                                $abnormalId,
                                $item['sku_code'],
                                '',
                                -$item['quantity']
                            );
                        }
                        if ($abnormal->basket_code) {
                            $basketCode = $abnormal->basket_code;
                        } else {
                            $pickingTaskBasket = $this->pickingTaskRepository->getPickingTaskBasketByOutboundNumber($outbound->outbound_number);
                            $basketCode = $pickingTaskBasket->basket_code;
                        }
                        $this->weighRepository->releaseBasket($outbound->warehouse_id, $basketCode);
                    } else {
                        $items = [];
                        foreach ($abnormalItems as $abnormalItem) {
                            $items[] = [
                                'sku_code' => $abnormalItem->sku_code,
                                'quantity' => $abnormalItem->quantity,
                            ];
                        }
                        // 异常区库存减少
                        foreach ($items as $item) {
                            $this->stockRepository->updateStock(
                                $abnormal->warehouse_id,
                                $userId,
                                StorageArea::STORAGE_TYPE_ABNORMAL,
                                StockLog::RELATION_TYPE_ABNORMAL,
                                $abnormalId,
                                $item['sku_code'],
                                '',
                                -$item['quantity']
                            );
                        }
                        // 恢复到拣货完成
                        $this->outboundRepository->updateState($outbound->id, Outbound::STATE_PICKING_COMPLETED);
                        $outboundItems = $this->outboundRepository->getOutboundItems($outbound->id);
                        $items = [];
                        foreach ($outboundItems as $outboundItem) {
                            $items[] = [
                                'sku_code' => $outboundItem->sku_code,
                                'quantity' => $outboundItem->quantity,
                            ];
                        }
                        // 异常区库存减少
                        foreach ($items as $item) {
                            $this->stockRepository->updateStock(
                                $abnormal->warehouse_id,
                                $userId,
                                StorageArea::STORAGE_TYPE_PACKAGE,
                                StockLog::RELATION_TYPE_ABNORMAL,
                                $abnormalId,
                                $item['sku_code'],
                                '',
                                $item['quantity']
                            );
                        }
                        $this->pickingTaskRepository->updatePickingTaskItem($outbound->outbound_number, $items);
                    }
                    break;
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('AbnormalUpdateRunner run Fail', [
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
            return fail();
        }
        if ($outbound) {
            MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_ORDER_STATUS_NOTIFY, ['order_number' => $outbound->order_number]);
        }
        return success();
    }
}
