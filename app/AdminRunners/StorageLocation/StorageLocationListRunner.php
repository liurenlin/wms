<?php
namespace App\AdminRunners\StorageLocation;


use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Consts\CommonConst;
use App\Models\StorageLocation;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class StorageLocationListRunner extends Runner implements AdminRunner
{
    private $listFields = [
        'id' => 'id',
        'storage_type' => 'storage_type as type',
        'storage_location_code' => 'storage_location_code as code',
        'created_at' => 'created_at',
    ];

    public function run(Request $request): JsonResponse
    {
        $perPage = $request->input('per_page') ?? CommonConst::DEFAULT_PER_PAGE;
        $type = $request->input('type');
        $code = $request->input('code');
        $locationQuery = StorageLocation::query()
            ->select($this->listFields);
        if ($type) {
            $locationQuery->where('storage_type', $type);
        }
        if ($code) {
            $locationQuery->where('storage_location_code', 'like', '%' . $code . '%');
        }
        $locations = $locationQuery
            ->orderBy('storage_location_code')
            ->paginate($perPage);
        return success([
            'storage_locations' => $locations->items(),
            'page_bean' => [
                'total' => (int)$locations->total(),
                'page' => (int)$locations->currentPage(),
                'per_page' => (int)$locations->perPage(),
            ],
        ]);
    }

}
