<?php

namespace App\AdminRunners\StorageLocation;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Repositories\StorageLocationRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ShowRunner extends Runner implements AdminRunner
{
    public $storageLocationRepository;

    public function __construct(StorageLocationRepository $storageLocationRepository)
    {
        $this->storageLocationRepository = $storageLocationRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $warehouseId = (int) $request->header('warehouse-id');
        $storageLocationCode = $request->get('storage_location_code');
        if (empty($storageLocationCode)) {
            return fail();
        }

        // 检查库位是否存在
        if ($this->storageLocationRepository->findWhere([
            'warehouse_id' => $warehouseId,
            'storage_location_code' => $storageLocationCode
        ])->isEmpty()) {
            return fail();
        }

        $stock = $this->storageLocationRepository->getStock($warehouseId, $storageLocationCode);
        if (empty($stock)) {
            return fail();
        }
        return success($stock);
    }
}
