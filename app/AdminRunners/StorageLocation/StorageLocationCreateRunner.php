<?php
namespace App\AdminRunners\StorageLocation;


use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Exceptions\NotExistException;
use App\Models\StorageArea;
use App\Models\Warehouse;
use App\Services\StorageLocationService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StorageLocationCreateRunner extends Runner implements AdminRunner
{
    public function run(Request $request): JsonResponse
    {
        $warehouseId = $request->input('warehouse_id');
        $areaId = $request->input('area_id');
        $pathStart = $request->input('path_start');
        $pathEnd = $request->input('path_end');
        $shelfQty = $request->input('shelves_quantity');
        $layerQty = $request->input('layers_per_shelf');
        $binQty = $request->input('bins_per_layer');

        $warehouse = Warehouse::find($warehouseId);
        if (!$warehouse) {
            throw new NotExistException([
                'message' => 'warehouse not exist',
            ]);
        }
        $area = StorageArea::find($areaId);
        if (!$area) {
            throw new NotExistException([
                'message' => 'storage area not exist',
            ]);
        }

        DB::beginTransaction();
        try {
            $service = new StorageLocationService();
            $service->batchAddLocations($warehouse, $area, $pathStart, $pathEnd, $shelfQty, $layerQty, $binQty);
            DB::commit();
            return success();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

}
