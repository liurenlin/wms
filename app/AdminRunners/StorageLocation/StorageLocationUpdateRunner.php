<?php
namespace App\AdminRunners\StorageLocation;


use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Exceptions\NotExistException;
use App\Models\StorageLocation;
use App\Models\Warehouse;
use App\Services\WarehouseService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StorageLocationUpdateRunner extends Runner implements AdminRunner
{
    private $fields = [
        'warehouse_id' => 'warehouse_id',
        'storage_type' => 'storage_type',
        'storage_area_code' => 'storage_area_code',
        'storage_location_code' => 'storage_location_code',
    ];

    private $locationId;

    public function setId($locationId)
    {
        $this->locationId = $locationId;
    }

    public function run(Request $request): JsonResponse
    {
        DB::beginTransaction();
        try {
            $location = StorageLocation::find($this->locationId);
            if (!$location) {
                throw new NotExistException([
                    'message' => 'storage location not exist',
                ]);
            }
            foreach ($this->fields as $field => $name) {
                if ($request->input($field)) {
                    $location->$name = $request->input($field);
                }
            }
            $location->save();
            DB::commit();
            return success();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

}