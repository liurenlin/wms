<?php
namespace App\AdminRunners\StorageLocation;


use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Exceptions\NotExistException;
use App\Exceptions\ParameterException;
use App\Models\Stock;
use App\Models\StorageLocation;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class StorageLocationDeleteRunner extends Runner implements AdminRunner
{
    protected $ids;

    public function setId($ids)
    {
        $this->ids = $ids;
    }

    public function run(Request $request) : JsonResponse
    {
        $paramIds = explode(',', trim($this->ids));
        foreach ($paramIds as $paramId) {
            $location = StorageLocation::find($paramId);
            if (!$location) {
                throw new NotExistException([
                    'message' => 'storage location not exist',
                ]);
            }
            // stock
            $stocks = Stock::getStockByBinCode($location->storage_location_code);
            if (count($stocks) > 0) {
                foreach ($stocks as $stock) {
                    if ($stock->quantity > 0) {
                        throw new ParameterException([
                            'message' => 'storage location not empty',
                        ]);
                    }
                }
            }
        }
        StorageLocation::query()
            ->whereIn('id', $paramIds)
            ->delete();
        return success();
    }

}
