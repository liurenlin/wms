<?php

namespace App\AdminRunners\Role;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Repositories\RoleRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class StoreRunner extends Runner implements AdminRunner
{
    public $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $res = $this->roleRepository->createRole($request->get('role_name'));
        if (!$res) {
            return fail();
        }
        return success();
    }
}
