<?php

namespace App\AdminRunners\Role;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Repositories\RoleRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class RoleListRunner extends Runner implements AdminRunner
{
    public $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $roles = $this->roleRepository->getRoleList();
        return success(['roles' => $roles]);
    }
}
