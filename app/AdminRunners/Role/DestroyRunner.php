<?php

namespace App\AdminRunners\Role;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Repositories\RoleRepository;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DestroyRunner extends Runner implements AdminRunner
{
    public $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $roleId = (int) $request->input('role_id');
        if ($roleId == 1) {
            return fail('Admin role cannot be deleted');
        }
        DB::beginTransaction();
        try {
            $this->roleRepository->deleteRole($roleId);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Delete Role Fail', [
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
            return fail($e->getMessage());
        }
        return success();
    }
}
