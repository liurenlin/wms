<?php

namespace App\AdminRunners\Role;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Repositories\RoleRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UpdateRunner extends Runner implements AdminRunner
{
    public $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $roleId = (int) $request->input('role_id');
        // 检查 permission_groups
        $permissionGroups = json_decode($request->get('permission_groups'), JSON_OBJECT_AS_ARRAY);
        if (empty($permissionGroups)) {
            return fail();
        }
        if (!$this->roleRepository->checkPermissionGroups($permissionGroups)) {
            return fail();
        }

        $res = $this->roleRepository->updateRolePermissionGroups($roleId, $permissionGroups);
        if (!$res) {
            return fail();
        }
        return success();
    }
}
