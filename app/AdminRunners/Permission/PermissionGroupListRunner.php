<?php

namespace App\AdminRunners\Permission;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Repositories\PermissionRepository;
use App\Repositories\PickingTaskRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PermissionGroupListRunner extends Runner implements AdminRunner
{
    public $permissionRepository;

    public function __construct(PermissionRepository $permissionRepository)
    {
        $this->permissionRepository = $permissionRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $permissionGroups = $this->permissionRepository->getGroupPermissions();
        return success(['permission_groups' => $permissionGroups]);

    }
}
