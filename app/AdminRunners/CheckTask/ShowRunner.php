<?php

namespace App\AdminRunners\CheckTask;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Repositories\CheckTaskRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ShowRunner extends Runner implements AdminRunner
{
    public $checkTaskRepository;

    public function __construct(CheckTaskRepository $checkTaskRepository)
    {
        $this->checkTaskRepository = $checkTaskRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $checkTaskId = (int) $request->get('check_task_id');
        // 检查出库单是否存在
        if (!$this->checkTaskRepository->existsCheckTask($checkTaskId)) {
            return fail(__('check.task_not_exists'));
        }

        $pickingTask = $this->checkTaskRepository->getDetail($checkTaskId);
        return success($pickingTask);
    }
}
