<?php

namespace App\AdminRunners\CheckTask;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Repositories\CheckTaskRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class IndexRunner extends Runner implements AdminRunner
{
    public $checkTaskRepository;

    public function __construct(CheckTaskRepository $checkTaskRepository)
    {
        $this->checkTaskRepository = $checkTaskRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $page = (int) $request->get('page', 1);
        $perPage = (int) $request->get('per_page', 20);
        $querys = $request->query->all();
        $pageCheckTasks = $this->checkTaskRepository->getListByPage($querys, $page, $perPage);
        return success($pageCheckTasks);
    }
}
