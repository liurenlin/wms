<?php

namespace App\AdminRunners\Check;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Models\OpenMessageQueue;
use App\Repositories\CheckTaskRepository;
use App\Services\MessageService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ConfirmCheckTaskRunner extends Runner implements AdminRunner
{
    public $checkTaskRepository;

    public function __construct(CheckTaskRepository $checkTaskRepository)
    {
        $this->checkTaskRepository = $checkTaskRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $userId = Auth::id();
        $warehouseId = $request->header('warehouse-id');
        $checkTaskId = $request->input('check_task_id');
        $state = (int) $request->get('state');
        $skus = json_decode($request->get('skus'), JSON_OBJECT_AS_ARRAY);

        if (empty($skus)) {
            return fail('Err! No skus');
        }

        $res = $this->checkTaskRepository->confirmTask($warehouseId, $userId, $checkTaskId, $state, $skus);
        if (!$res) {
            return fail(__('check.wrong_sku'));
        }
        return success();
    }
}
