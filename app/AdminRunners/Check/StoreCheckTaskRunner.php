<?php

namespace App\AdminRunners\Check;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Models\Outbound;
use App\Repositories\CheckTaskRepository;
use App\Repositories\OutboundRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class StoreCheckTaskRunner extends Runner implements AdminRunner
{
    public $checkTaskRepository;
    public $outboundRepository;

    public function __construct(CheckTaskRepository $checkTaskRepository, OutboundRepository $outboundRepository)
    {
        $this->checkTaskRepository = $checkTaskRepository;
        $this->outboundRepository = $outboundRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $userId = Auth::id();
        $warehouseId = $request->header('warehouse-id');
        $basketCode = $request->get('basket_code');

        // 检查 code 类型
        if (Str::startsWith($basketCode, 'BAS-')) {
            // 检查参数
            $pickingTaskBasket = $this->checkTaskRepository->getPickingTaskBasket($warehouseId, $basketCode);
            if (!$pickingTaskBasket) {
                return fail('Error! Empty picking basket! ');
            }
            // 检查出库单
            $outbound = $this->outboundRepository->getOutbound($warehouseId, $pickingTaskBasket->outbound_number);

        } else {
            // 检查出库单
            $outbound = $this->outboundRepository->getOutbound($warehouseId, $basketCode);
            // 获取该出库单拣货篮子
            $pickingTaskBasket = $this->checkTaskRepository->getPickingTaskBasketByOutboundNumber($basketCode);
            if (!$pickingTaskBasket) {
                return fail(__('check.empty_basket'));
            }
            $basketCode = $pickingTaskBasket->basket_code;
        }

        if (!$outbound) {
            return fail(__('check.outbound_wrong'));
        }

        if (in_array($outbound->state, [Outbound::STATE_PICKING_ABNORMAL, Outbound::STATE_CHECK_ABNORMAL])) {
            return fail(__('check.picking_has_abnormal'));
        }

        if ($outbound->state != Outbound::STATE_PICKING_COMPLETED) {
            return fail(__('check.outbound_not_completed', ['state' => $this->outboundRepository->getOutboundStateTip($outbound->state)]));
        }

        // 判断是否已经存在检查任务
        $checkTask =  $this->checkTaskRepository->getCheckTaskByOutboundNumber($outbound->outbound_number);
        if ($checkTask) {
            return success([
                'id' => $checkTask->id,
                'task_number' => $checkTask->task_number,
                'picking_task' => [
                    'id' => $checkTask->picking_task_id,
                ],
            ]);
        }

        // 领取任务
        DB::beginTransaction();
        try {
            // 领取任务
            $checkTask = $this->checkTaskRepository->createCheckTask($warehouseId, $outbound->outbound_number, $basketCode, $userId);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('StoreCheckTask Fail', [
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
            return fail();
        }

        return success([
            'id' => $checkTask->id,
            'task_number' => $checkTask->task_number,
            'picking_task' => [
                'id' => $checkTask->picking_task_id,
            ],
        ]);
    }
}
