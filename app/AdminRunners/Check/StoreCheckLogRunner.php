<?php

namespace App\AdminRunners\Check;

use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Models\CheckTask;
use App\Repositories\CheckTaskRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StoreCheckLogRunner extends Runner implements AdminRunner
{
    public $checkTaskRepository;
    public $outboundRepository;

    public function __construct(CheckTaskRepository $checkTaskRepository) {
        $this->checkTaskRepository = $checkTaskRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $userId = Auth::id();
        $warehouseId = $request->header('warehouse-id');
        $checkTaskId = $request->get('check_task_id');
        $skuCode = $request->get('sku_code');
        $quantity = $request->get('quantity');
        $checkTask = CheckTask::query()->find($checkTaskId);

        $checkTaskItem = $this->checkTaskRepository->getCheckTaskItemBySkuCode($checkTaskId, $skuCode);
        if (!$checkTaskItem || $checkTaskItem->quantity < $quantity) {
            return fail(__('check.wrong_sku'));
        }
        // 正常件，只进行日志记录
        DB::beginTransaction();
        try {
            $this->checkTaskRepository->createCheckLog(
                $warehouseId,
                $userId,
                $checkTaskId,
                $checkTask->basket_code,
                $skuCode,
                $quantity
            );
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('StoreCheckLog Fail', [
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
            return fail();
        }
        return success();
    }
}
