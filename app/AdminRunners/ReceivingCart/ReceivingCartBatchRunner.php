<?php
namespace App\AdminRunners\ReceivingCart;


use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Exceptions\NotExistException;
use App\Models\Warehouse;
use App\Services\ReceivingCartService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReceivingCartBatchRunner extends Runner implements AdminRunner
{

    public function run(Request $request): JsonResponse
    {
        $amount = $request->input('amount');
        $warehouseId = $request->input('warehouse_id');
        $warehouse = Warehouse::find($warehouseId);
        if (!$warehouse) {
            throw new NotExistException([
                'message' => 'warehouse not exist',
            ]);
        }
        $service = new ReceivingCartService();
        DB::beginTransaction();
        try {
            $service->batchAddCarts($warehouseId, $amount);
            DB::commit();
            return success(['amount' => $amount]);
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }
}