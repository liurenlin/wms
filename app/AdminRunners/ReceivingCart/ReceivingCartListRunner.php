<?php
namespace App\AdminRunners\ReceivingCart;


use App\AdminRunners\AdminRunner;
use App\AdminRunners\Runner;
use App\Consts\CommonConst;
use App\Models\ReceivingCart;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ReceivingCartListRunner extends Runner implements AdminRunner
{
    private $listFields = [
        'id' => 'id',
        'cart_code' => 'cart_code as code',
        'state' => 'state',
        'created_at' => 'created_at',
    ];

    public function run(Request $request): JsonResponse
    {
        $perPage = $request->input('per_page') ?? CommonConst::DEFAULT_PER_PAGE;
        $search = $request->input('code');
        $search = '%' . $search. '%';
        $carts = ReceivingCart::query()
            ->select($this->listFields)
            ->when($search, function (Builder $query) use ($search) {
                $query->where('cart_code', 'like', $search);
            })
            ->paginate($perPage);
        return success([
            'carts' => $carts->items(),
            'page_bean' => [
                'total' => (int)$carts->total(),
                'page' => (int)$carts->currentPage(),
                'per_page' => (int)$carts->perPage(),
            ],
        ]);
    }
}
