<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InterceptLog extends Model
{
    protected $dateFormat = 'U';

    protected $guarded = [];
}
