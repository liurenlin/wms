<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Weigh extends Model
{
    protected $dateFormat = 'U';

    protected $guarded = [];
}
