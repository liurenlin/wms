<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $dateFormat = 'U';

    protected $guarded = [];

    protected $hidden = ['pivot'];

    public function permissionGroups()
    {
        return $this->belongsToMany(PermissionGroup::class, 'role_permission_groups', 'role_id', 'permission_group_id');
    }
}
