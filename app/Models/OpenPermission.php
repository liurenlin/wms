<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OpenPermission extends Model
{
    protected $dateFormat = 'U';

    protected $guarded = [];
}
