<?php

namespace App\Models;

use App\Consts\CommonConst;
use App\Services\ReceivingService;
use App\User;
use Illuminate\Database\Eloquent\Model;

class ActualReceiving extends Model
{
    const STATE_ING = 1;
    const STATE_COMPLETED = 2;

    protected $dateFormat = 'U';

    protected $guarded = [];

    protected $pageName = 'actualReceivingList';

    protected $selectColumns = [
        'id',
        'warehouse_id',
        'actual_receiving_number',
        'pre_receiving_id',
        'state',
        'created_at',
        'updated_at'
    ];

    public function preReceiving()
    {
        return $this->belongsTo(PreReceiving::class, 'pre_receiving_id', 'id');
    }

    public function receivingLogs()
    {
        return $this->hasMany(ReceivingLog::class, 'pre_receiving_id', 'pre_receiving_id');
    }

    public function items()
    {
        return $this->hasMany(ActualReceivingItem::class, 'actual_receiving_id', 'id');
    }

    public function abnormals()
    {
        return $this->hasMany(Abnormal::class, 'warehouse_id', 'warehouse_id');
    }

    public function operator()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public static function getActualReceiving($preReceiving)
    {
        return ActualReceiving::query()
            ->where('warehouse_id', $preReceiving->warehouse_id)
            ->where('pre_receiving_id', $preReceiving->id)
            ->get();
    }

    public function getActualReceivingList($params): array
    {
        $perPage = isset($params['per_page']) ? $params['per_page'] : CommonConst::DEFAULT_PER_PAGE;
        $preReceivingNumber = isset($params['pre_receiving_number']) ? $params['pre_receiving_number'] : '';
        $createdAtFrom = isset($params['created_at_from']) ? $params['created_at_from'] : '';
        $createdAtTo = isset($params['created_at_to']) ? $params['created_at_to'] : '';
        $finishAtFrom = isset($params['finished_at_from']) ? $params['finished_at_from'] : '';
        $finishAtTo = isset($params['finished_at_to']) ? $params['finished_at_to'] : '';
        $thirdPartyOrderNumber = isset($params['third_party_order_number']) ? $params['third_party_order_number'] : '';
        $operatorId = isset($params['operator_id']) ? $params['operator_id'] : 0;
        $ActualLists = self::query()->leftjoin('pre_receivings as pr', 'actual_receivings.pre_receiving_id', '=',
            'pr.id')
            ->select('actual_receivings.*','pr.third_party_order_number','pr.third_party_order_number','pr.id as pid')
            ->where(function ($query) use (
                $preReceivingNumber,
                $createdAtFrom,
                $createdAtTo,
                $thirdPartyOrderNumber,
                $operatorId,
                $finishAtTo,
                $finishAtFrom
            ) {
                if ($operatorId) {
                    $query->where('actual_receivings.user_id', $operatorId);
                }
                if ($thirdPartyOrderNumber) {
                    $query->where('pr.third_party_order_number', $thirdPartyOrderNumber);
                }
                if ($preReceivingNumber) {
                    $query->where('pr.pre_receiving_number', $preReceivingNumber);
                }
                if ($createdAtFrom) {
                    $query->where('actual_receivings.created_at', '>=', strtotime($createdAtFrom));
                }
                if ($createdAtTo) {
                    $query->where('actual_receivings.created_at', '<=', strtotime($createdAtTo));
                }
                if ($finishAtFrom) {
                    $query->where('actual_receivings.finished_at', '>=', strtotime($finishAtFrom));
                }
                if ($finishAtTo) {
                    $query->where('actual_receivings.finished_at', '<=', strtotime($finishAtTo));
                }
            })
            ->orderBy('actual_receivings.updated_at', 'desc')
            ->paginate($perPage);
        $pageInfo = [
            'total' => (int)$ActualLists->total(),
            'page' => (int)$ActualLists->currentPage(),
            'per_page' => (int)$ActualLists->perPage(),
        ];
        $ActualLists = $this->transformData($ActualLists);
        $list = [
            'actual_receivings' => $ActualLists,
            'page_bean' => $pageInfo
        ];
        return $list;
    }

    private function transformData($actualReceivingList): array
    {
        $actualList = [];
        foreach ($actualReceivingList as $list) {
            $actualList[] = [
                'id' => $list->id,
                'pre_receiving_number' => isset($list->preReceiving) ? $list->preReceiving->pre_receiving_number : '',
                'acutal_receiving_number' => $list->actual_receiving_number,
                'third_party_order_number' => isset($list->preReceiving) ? $list->preReceiving->third_party_order_number : '',
                'owner' => isset($list->preReceiving) ? $list->preReceiving->owner_id : '',
                'operator' => isset($list->operator) ? $list->operator->name : '',
                'created_at' => date($list->created_at),
                'finished_at' => $list->finished_at ? date('Y-m-s h:i:s',$list->finished_at) : '',
            ];
        }
        return $actualList;
    }

    public function getActualReceivingDetail($params)
    {
        $actualReceivingId = $params['actual_receiving_id'];
        $actualReceiving = self::query()->find($actualReceivingId);
        $info = array();
        $receivingService = new ReceivingService();
        $abnormals = $receivingService->formatActualReceivingAbnormal($actualReceiving);
        if ($actualReceiving) {
            $info = [
                'id' => $actualReceiving->id,
                'actual_receiving_number' => $actualReceiving->actual_receiving_number,
                'pre_receiving_number' => isset($actualReceiving->preReceiving) ? $actualReceiving->preReceiving->pre_receiving_number : '',
                'finished_at' => $actualReceiving->finished_at ? date('Y-m-d H:i:s',$actualReceiving->finished_at) : '',
                'created_at' => date($actualReceiving->created_at),
                'third_party_order_number' => isset($actualReceiving->preReceiving) ? $actualReceiving->preReceiving->third_party_order_number : '',
                'third_party_order_type' => isset($actualReceiving->preReceiving) ? $actualReceiving->preReceiving->third_party_order_type : '',
                'operator' => isset($actualReceiving->operator) ? $actualReceiving->operator->name : '',
                'owner' => isset($actualReceiving->preReceiving) ? $actualReceiving->preReceiving->owner_id : '',
                'items' => $receivingService->formatActualReceivingItems($actualReceiving),
                'receiving_logs' => $receivingService->formatActualReceivingLogs($actualReceiving),
                'abnormals' => $abnormals,
            ];
        }
        return $info;
    }
}
