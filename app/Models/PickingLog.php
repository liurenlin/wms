<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PickingLog extends Model
{
    protected $dateFormat = 'U';

    protected $guarded = [];
}
