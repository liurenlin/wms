<?php

namespace App\Models;

use App\Consts\CommonConst;
use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $dateFormat = 'U';

    protected $guarded = [];

    protected $pageName = 'inWareHouseList';

    public function storageLocation()
    {
        return $this->belongsTo(StorageLocation::class, 'warehouse_id', 'warehouse_id');
    }

    public function sku()
    {
        return $this->belongsTo(ProductSku::class, 'sku_code', 'sku_code');
    }

    public function abnormal()
    {
        return $this->hasMany(Abnormal::class, 'warehouse_id', 'warehouse_id')
            ->select('abnormal_number', 'reason');
    }
    public function getStockList($params): array
    {
        $perPage = isset($params['per_page']) ? $params['per_page'] : CommonConst::DEFAULT_PER_PAGE;
        $skuCode = isset($params['sku_code']) ? $params['sku_code'] : '';
        $lists = self::query()
            ->where(function ($query) use ($skuCode) {
                if ($skuCode) {
                    $query->where('sku_code', $skuCode);
                }
            })
            ->orderBy('updated_at', 'desc')
            ->paginate($perPage);
        $stockList = $lists;
        $stockList = $this->transformData($stockList);
        $pageInfo = [
            'total' => (int)$lists->total(),
            'page' => (int)$lists->currentPage(),
            'per_page' => (int)$lists->perPage(),
        ];
        $lists = [
            'stocks' => $stockList,
            'page_bean' => $pageInfo
        ];
        return $lists;
    }

    private function transformData($stocks): array
    {
        $stockList = [];
        foreach ($stocks as $stock) {
            $stockList[] = [
                'id'=>$stock->id,
                'sku_code'=>$stock->sku_code,
                'quantity'=>$stock->quantity,
                'receiving_area'=>isset($stock->warehouse)?$stock->warehouse->warehouse_name:'',
                'frozen_stock'=>$stock->frozen_quantity,
                'unfrozen_stock'=>$stock->unfrozen_quantity,
                'packing_area'=>$stock->bin,
            ];
        }
        return $stockList;
    }

    public static function getStockByBinCode($storageLocationCode)
    {
        return self::query()
            ->where('bin', $storageLocationCode)
            ->get();
    }
}
