<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PreReceivingItem extends Model
{
    protected $dateFormat = 'U';

    protected $guarded = [];

    public function sku()
    {
        return $this->belongsTo(ProductSku::class, 'sku_code', 'sku_code');
    }

}
