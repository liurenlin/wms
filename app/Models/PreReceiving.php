<?php

namespace App\Models;

use App\Consts\CommonConst;
use App\Services\ReceivingService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class PreReceiving extends Model
{
    const STATE_DEFAULT = 0;
    const STATE_CREATED = 1;
    const STATE_ACTUAL_RECEIVING_CREATED = 2;
    const STATE_FINISHED = 3;

    const TYPE_PURCHASE_ORDER = 1;
    const TYPE_ABNORMAL_ORDER = 2;
    const TYPE_RTS_PACKAGE = 3;

    protected $dateFormat = 'U';

    protected $pageName = 'preReceivingList';

    protected $guarded = [];

    public function items()
    {
        return $this->hasMany(PreReceivingItem::class);
    }

    public function actualReceivings()
    {
        return $this->hasMany(ActualReceiving::class);
    }

    public function getPreReceivingList($params): array
    {
        $perPage = isset($params['per_page']) ? $params['per_page'] : CommonConst::DEFAULT_PER_PAGE;
        $preReceivingNumber = isset($params['pre_receiving_number']) ? $params['pre_receiving_number'] : '';
        $createdAtFrom = isset($params['created_at_from']) ? $params['created_at_from'] : '';
        $createdAtTo = isset($params['created_at_to']) ? $params['created_at_to'] : '';
        $thirdPartyOrderNumber = isset($params['third_party_order_number']) ? $params['third_party_order_number'] : '';
        $type = isset($params['type']) ? $params['type'] : 0;
        $lists = self::query()->where(function ($query) use (
            $preReceivingNumber,
            $createdAtFrom,
            $createdAtTo,
            $thirdPartyOrderNumber,
            $type
        ) {
            if ($type) {
                $query->where('third_party_order_type', $type);
            }
            if ($thirdPartyOrderNumber) {
                $query->where('third_party_order_number', $thirdPartyOrderNumber);
            }
            if ($preReceivingNumber) {
                $query->where('pre_receiving_number', $preReceivingNumber);
            }
            if ($createdAtFrom) {
                $query->where('created_at', '>=', strtotime($createdAtFrom));
            }
            if ($createdAtTo) {
                $query->where('created_at', '<=', strtotime($createdAtTo));
            }
        })
            ->orderBy('updated_at', 'desc')
            ->paginate($perPage);
        $preReceivingList = $lists;
        $preReceivingList = $this->transformData($preReceivingList);
        $pageInfo = [
            'total' => (int)$lists->total(),
            'page' => (int)$lists->currentPage(),
            'per_page' => (int)$lists->perPage(),
        ];
        $lists = [
            'pre_receivings' => $preReceivingList,
            'page_bean' => $pageInfo
        ];
        return $lists;
    }

    private function transformData($preReceivingList): array
    {
        $stockList = [];
        foreach ($preReceivingList as $preReceiving) {
            $stockList[] = [
                'pre_receiving_number' => $preReceiving->pre_receiving_number,
                'created_at' => date($preReceiving->created_at),
                'third_party_order_number' => $preReceiving->third_party_order_number,
                'type' => $preReceiving->third_party_order_type,
                'owner' => 'Bukito',
                'id' => $preReceiving->id
            ];
        }
        return $stockList;
    }

    public function getPreReceivingDetail($params)
    {
        $preReceivingId = $params['pre_receiving_id'];
        $preReceiving = self::query()->find($preReceivingId);
        $info = array();
        $receivingService = new ReceivingService();
        if (!is_null($preReceiving)) {
            $info = [
                'id' => $preReceiving->id,
                'pre_receiving_number' => $preReceiving->pre_receiving_number,
                'created_at' => date($preReceiving->created_at),
                'owner' => '',
                'operator' => '',
                'type' => $preReceiving->third_party_order_type,
                'third_party_order_number' => $preReceiving->third_party_order_number,
                'items' => $receivingService->formatPreReceivingItems($preReceiving),
                'actual_receivings' => $receivingService->formatActualReceiving(ActualReceiving::getActualReceiving($preReceiving)),
            ];
        }
        return $info;
    }
}
