<?php

namespace App\Models;

use App\Consts\CommonConst;
use App\User;
use Illuminate\Database\Eloquent\Model;

class StockDealWithLog extends Model
{
    protected $dateFormat = 'U';

    protected $guarded = [];

    public function abnormal()
    {
        return $this->hasOne(Abnormal::class, 'abnormal_number', 'abnormal_number');
    }

    public function operator()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function getStockManagement($params): array
    {
        $perPage = isset($params['per_page']) ? $params['per_page'] : CommonConst::DEFAULT_PER_PAGE;
        $locationCode = isset($params['storage_location_code']) ? $params['storage_location_code'] : '';
        $skuCode = isset($params['sku_code']) ? $params['sku_code'] : '';
        $createdAtFrom = isset($params['created_at_from']) ? $params['created_at_from'] : '';
        $createdAtTo = isset($params['created_at_to']) ? $params['created_at_to'] : '';
        $operatorId = isset($params['user_id']) ? $params['user_id'] : '';
        $abnormalNumber = isset($params['abnormal_number']) ? $params['abnormal_number'] : '';
        $lists = self::query()
            ->where(function ($query) use ($locationCode, $skuCode, $createdAtFrom, $createdAtTo, $operatorId,$abnormalNumber) {
                if ($operatorId) {
                    $query->where('user_id', $operatorId);
                }
                if ($locationCode) {
                    $query->where('storage_location_code', $locationCode);
                }
                if ($skuCode) {
                    $query->where('sku_code', $skuCode);
                }
                if ($createdAtFrom) {
                    $query->where('created_at', '>=', strtotime($createdAtFrom));
                }
                if ($createdAtTo) {
                    $query->where('created_at', '<=', strtotime($createdAtTo));
                }
                if ($abnormalNumber) {
                    $query->where('abnormal_number', $abnormalNumber);
                }
            })
            ->orderBy('updated_at', 'desc')
            ->paginate($perPage);
        $stockList = $lists;
        $stockList = $this->transformStockManagementData($stockList);
        $pageInfo = [
            'total' => (int)$lists->total(),
            'page' => (int)$lists->currentPage(),
            'per_page' => (int)$lists->perPage(),
        ];
        $lists = [
            'list' => $stockList,
            'page_bean' => $pageInfo
        ];
        return $lists;
    }

    private function transformStockManagementData($stockManagements): array
    {
        $List = [];
        foreach ($stockManagements as $stockManagement) {
            $abnormal = $stockManagement->abnormal;
            $List[] = [
                'storage_location_code' => $stockManagement->storage_location_code,
                'created_at' => $stockManagement->created_at->format('Y-m-d H:i:s'),
                'sku_code' => $stockManagement->sku_code,
                'inventory_before' => $stockManagement->inventory_before,
                'inventory_after' => $stockManagement->inventory_after,
                'fixed_quantity' => $stockManagement->inventory_after - $stockManagement->inventory_before,
                'operator' => isset($stockManagement->operator) ? $stockManagement->operator->name : '',
                'remark' => isset($abnormal) ? $abnormal->reason : '',
                'abnormal_number' => isset($abnormal) ? $abnormal->abnormal_number : '',
            ];
        }
        return $List;
    }
}
