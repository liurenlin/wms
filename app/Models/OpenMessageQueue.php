<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OpenMessageQueue extends Model
{
    const STATE_DONE = 2;
    const STATE_ERROR = 3;

    const MESSAGE_TYPE_STORE_OUTBOUND = 'store_outbound';
    const MESSAGE_TYPE_STORE_PRE_RECEIVING = 'store_pre_receiving';
    const MESSAGE_TYPE_BATCH_STORE_PRODUCT = 'batch_store_product';
    const MESSAGE_TYPE_BATCH_STORE_SKU = 'batch_store_sku';
    const MESSAGE_TYPE_IN_WARE = 'in_ware';
    const MESSAGE_TYPE_ORDER_OUT = 'order_out';
    const MESSAGE_TYPE_SHELF_ON = 'shelf_on';
    const MESSAGE_TYPE_ORDER_STATUS_NOTIFY = 'order_status_notify';
    const MESSAGE_TYPE_ORDER_CANCEL_RESULT_NOTIFY = 'order_cancel_result_notify';
    const MESSAGE_TYPE_STOCK_NOTIFY = 'stock_notify';
    const MESSAGE_TYPE_PRE_RECEIVING_NOTIFY = 'pre_receiving_notify';
    const MESSAGE_TYPE_SYNC_PRODUCT_SKU = 'sync_product_sku';

    protected $dateFormat = 'U';

    protected $guarded = [];
}
