<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Basket extends Model
{
    use SoftDeletes;

    const STATE_FREE = 0;
    const STATE_BUSY = 1;

    protected $dateFormat = 'U';

    protected $guarded = [];

    public function getCreatedAtAttribute($value)
    {
        return Carbon::createFromTimestamp($value)->format('Y-m-d H:i:s');
    }
}
