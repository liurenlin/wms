<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReceivingCartItem extends Model
{
    protected $dateFormat = 'U';

    protected $guarded = [];
}
