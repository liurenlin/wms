<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HandoverItem extends Model
{
    protected $dateFormat = 'U';

    protected $guarded = [];
}
