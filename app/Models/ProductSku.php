<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductSku extends Model
{
    use SoftDeletes;

    protected $dateFormat = 'U';

    protected $guarded = [];

    public function images()
    {
        return $this->belongsToMany(Resource::class, ProductSkuImage::class, 'product_sku_id', 'resource_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function getSkuDetail($params){
        $skuCode = isset($params['sku_code']) ? $params['sku_code'] : '';
        $data = [];
        $detail = self::query()->where('sku_code',$skuCode)->select('sku_code')->first();
        if ($detail){
            $data = [
                'sku'=>$detail
            ];
        }
        return $data;
    }
}
