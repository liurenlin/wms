<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PickingTaskItem extends Model
{
    const STATE_NORMAL = 1;

    protected $dateFormat = 'U';

    protected $guarded = [];
}
