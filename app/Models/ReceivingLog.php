<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ReceivingLog extends Model
{
    protected $dateFormat = 'U';

    protected $guarded = [];

    public function operator()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
