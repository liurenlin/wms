<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Outbound extends Model
{
    use SoftDeletes;

    const IS_NEED_INTERCEPT_FALSE = 0;
    const IS_NEED_INTERCEPT_TRUE = 1;

    const STATE_DEFAULT = 0;
    const STATE_CREATED = 1;
    const STATE_FROZEN = 2;
    const STATE_CANCELLED = 3;
    const STATE_PICKING_TASK_CREATED = 4;
    const STATE_PICKING_ING = 5;
    const STATE_PICKING_ABNORMAL = 6;
    const STATE_PICKING_COMPLETED = 7;
    const STATE_CHECK_ABNORMAL = 8;
    const STATE_CHECK_COMPLETED = 9;
    const STATE_WEIGHED = 10;
    const STATE_INTERCEPTED = 11;
    const STATE_HANDOVER = 12;

    const TYPE_ORDER = 1;

    protected $dateFormat = 'U';

    protected $dates = [
        'order_created_at',
        'created_at',
        'updated_at',
    ];

    protected $guarded = [];

    public function items()
    {
        return $this->hasMany(OutboundItem::class);
    }
}
