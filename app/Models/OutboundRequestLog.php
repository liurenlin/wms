<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OutboundRequestLog extends Model
{
    const STATE_CREATED = 0;
    const STATE_SUCCESS = 1;
    const STATE_FAIL = 2;
    const STATE_EXCEPTION = 3;
    const STATE_ABANDONED = 4;

    protected $dateFormat = 'U';

    protected $guarded = [];
}
