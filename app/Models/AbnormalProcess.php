<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AbnormalProcess extends Model
{
    protected $dateFormat = 'U';

    protected $guarded = [];
}
