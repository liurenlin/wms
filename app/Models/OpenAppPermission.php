<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OpenAppPermission extends Model
{
    protected $dateFormat = 'U';

    protected $guarded = [];
}
