<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OpenApp extends Model
{
    protected $dateFormat = 'U';

    protected $guarded = [];
}
