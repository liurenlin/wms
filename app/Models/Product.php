<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $dateFormat = 'U';

    protected $guarded = [];

    public function images()
    {
        return $this->belongsToMany(Resource::class, ProductImage::class, 'product_id', 'resource_id');
    }
}
