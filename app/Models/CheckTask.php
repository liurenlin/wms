<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CheckTask extends Model
{
    const STATE_CREATED = 1;
    const STATE_NORMAL = 2;
    const STATE_ABNORMAL = 3;

    protected $dateFormat = 'U';

    protected $guarded = [];
}
