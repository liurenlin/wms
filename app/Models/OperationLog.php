<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OperationLog extends Model
{
    protected $dateFormat = 'U';

    protected $guarded = [];
}
