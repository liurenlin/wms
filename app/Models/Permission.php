<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $dateFormat = 'U';

    protected $guarded = [];
}
