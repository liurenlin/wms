<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Warehouse extends BaseModel
{
    use SoftDeletes;

    protected $dateFormat = 'U';

    protected $guarded = [];
}
