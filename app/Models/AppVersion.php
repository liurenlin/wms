<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppVersion extends Model
{
    const PLATFORM_IOS = 1;
    const PLATFORM_ANDROID = 2;

    protected $dateFormat = 'U';

    protected $guarded = [];
}
