<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SkuCodePrintLog extends Model
{
    protected $dateFormat = 'U';

    protected $guarded = [];
}
