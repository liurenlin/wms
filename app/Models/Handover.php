<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Handover extends Model
{
    protected $dateFormat = 'U';

    protected $guarded = [];
}
