<?php

namespace App\Models;

use App\Consts\CommonConst;
use App\Services\PutawayService;
use App\Services\ReceivingService;
use App\User;
use Illuminate\Database\Eloquent\Model;

class PutawayTask extends Model
{
    const STATE_DEFAULT = 0;
    const STATE_CREATEED = 1;
    const STATE_FINISHED = 2;

    protected $dateFormat = 'U';

    protected $guarded = [];

    protected $pageName = 'putAwayTaskList';

    public function operator()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');

    }

    public function items()
    {
        return $this->hasMany(PutawayTaskItem::class);
    }

    public function getPutAwayTaskList($params): array
    {
        $page = isset($params['page']) ? $params['page'] : CommonConst::DEFAULT_PAGE;
        $perPage = isset($params['per_page']) ? $params['per_page'] : CommonConst::DEFAULT_PER_PAGE;
        $taskNumber = isset($params['task_number']) ? $params['task_number'] : '';
        $createdAtFrom = isset($params['created_at_from']) ? $params['created_at_from'] : '';
        $createdAtTo = isset($params['created_at_to']) ? $params['created_at_to'] : '';
        $finishAtFrom = isset($params['finished_at_from']) ? $params['finished_at_from'] : '';
        $finishAtTo = isset($params['finished_at_to']) ? $params['finished_at_to'] : '';
        $receivingCartCode = isset($params['receiving_cart_code']) ? $params['receiving_cart_code'] : '';
        $operatorId = isset($params['operator_id']) ? $params['operator_id'] : '';
        $lists = self::query()
            ->where(function ($query) use (
                $taskNumber,
                $createdAtFrom,
                $createdAtTo,
                $finishAtFrom,
                $finishAtTo,
                $receivingCartCode,
                $operatorId
            ) {
                if ($operatorId) {
                    $query->where('user_id', $operatorId);
                }
                if ($receivingCartCode) {
                    $query->where('receiving_cart_code', $receivingCartCode);
                }
                if ($taskNumber) {
                    $query->where('task_number', $taskNumber);
                }
                if ($createdAtFrom) {
                    $query->where('created_at', '>=', strtotime($createdAtFrom));
                }
                if ($createdAtTo) {
                    $query->where('created_at', '<=', strtotime($createdAtTo));
                }
                if ($finishAtFrom) {
                    $query->where('finished_at', '>=', strtotime($finishAtFrom));
                }
                if ($finishAtTo) {
                    $query->where('finished_at', '<=', strtotime($finishAtTo));
                }
            })
            ->orderBy('updated_at', 'desc')
            ->paginate($perPage);
        $putAwayTaskList = $lists;
        $putAwayTaskList = $this->transformData($putAwayTaskList);
        $pageInfo = [
            'total' => (int)$lists->total(),
            'page' => (int)$lists->currentPage(),
            'per_page' => (int)$lists->perPage(),
        ];
        $lists = [
            'putaway_tasks' => $putAwayTaskList,
            'page_bean' => $pageInfo
        ];
        return $lists;
    }

    public function getPutAwayTaskDetail($params)
    {
        $putawayTaskId = $params['putaway_task_id'];
        $putawayTask = self::query()->find($putawayTaskId);
        $info = array();
        $putawayService = new PutawayService();
        if (!is_null($putawayTask)) {
            $info = [
                'id' => $putawayTask->id,
                'task_number' => $putawayTask->task_number,
                'receiving_cart_code' => $putawayTask->receiving_cart_code,
                'created_at' => date($putawayTask->created_at),
                'finished_at' => $putawayTask->finished_at ? date('Y-m-s h:i:s', $putawayTask->finished_at) : '',
                'operator' => isset($putawayTask->operator) ? $putawayTask->operator->name : '',
                'task_items' => $putawayService->formatPutAwayTaskItems($putawayTask),
                'sku_items' => $putawayService->formatPutAwayTaskSkuItems($putawayTask),
                'abnormal_items' => $putawayService->formatPutAwayTaskAbnormals($putawayTask),
            ];
        }
        return $info;
    }

    private function transformData($putAwayTaskList): array
    {
        $list = [];
        foreach ($putAwayTaskList as $taskList) {
            $list[] = [
                'id' => $taskList->id,
                'task_number' => $taskList->task_number,
                'receiving_cart_code' => $taskList->receiving_cart_code,
                'created_at' => date($taskList->created_at),
                'finished_at' => $taskList->finished_at ? date('Y-m-s h:i:s',$taskList->finished_at) : '',
                'operator' => isset($taskList->operator) ? $taskList->operator->name : ''
            ];
        }
        return $list;
    }
}
