<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSkuImage extends Model
{
    protected $dateFormat = 'U';

    protected $guarded = [];

    public function resource()
    {
        return $this->belongsTo(Resource::class);
    }
}
