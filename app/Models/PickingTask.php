<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PickingTask extends Model
{
    const STATE_DEFAULT = 0;
    const STATE_CREATED = 1;
    const STATE_IN_PROGRESS = 2;
    const STATE_DONE = 3;

    const TASK_TYPE_DEFAULT = 0;
    const TASK_TYPE_PDA = 1;
    const TASK_TYPE_PAPER = 2;

    protected $dateFormat = 'U';

    protected $dates = [
        'finished_at',
        'created_at',
        'updated_at',
    ];

    protected $guarded = [];
}
