<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AbnormalCode extends Model
{
    protected $dateFormat = 'U';

    protected $guarded = [];
}
