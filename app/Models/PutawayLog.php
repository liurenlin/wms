<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PutawayLog extends Model
{
    protected $dateFormat = 'U';

    protected $guarded = [];
}
