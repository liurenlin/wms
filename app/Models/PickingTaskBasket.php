<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PickingTaskBasket extends Model
{
    protected $dateFormat = 'U';

    protected $guarded = [];
}
