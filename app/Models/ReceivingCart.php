<?php

namespace App\Models;

use App\Consts\CommonConst;
use App\Services\PutawayService;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReceivingCart extends Model
{
    use SoftDeletes;

    const STATE_FREE = 0;
    const STATE_BUSY = 1;

    protected $dateFormat = 'U';

    protected $guarded = [];

    protected $fillable = [
        'warehouse_id',
        'cart_code',
        'state'
    ];

    public function getCreatedAtAttribute($value)
    {
        return Carbon::createFromTimestamp($value)->format('Y-m-d H:i:s');
    }

    public function operator()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');

    }

    public function putawayTask()
    {
        return $this->hasOne(PutawayTask::class, 'receiving_cart_code', 'cart_code')->orderByDesc('putaway_tasks.id');

    }

    public function items()
    {
        return $this->hasMany(ReceivingCartItem::class);
    }

    public function getPutAwayCartList($params): array
    {
        $perPage = isset($params['per_page']) ? $params['per_page'] : CommonConst::DEFAULT_PER_PAGE;
        $cartCode = isset($params['cart_code']) ? $params['cart_code'] : '';
        $createdAtFrom = isset($params['created_at_from']) ? $params['created_at_from'] : '';
        $createdAtTo = isset($params['created_at_to']) ? $params['created_at_to'] : '';
        $operatorId = isset($params['operator_id']) ? $params['operator_id'] : 0;
        $taskNumber = isset($params['task_number']) ? $params['task_number'] : '';
        $subPutawayTasks = PutawayTask::query()
            ->where('state', PutawayTask::STATE_CREATEED)
            ->select([
                'warehouse_id',
                'task_number',
                'state',
                'receiving_cart_code',
                'user_id',
                'created_at',
                'updated_at',
                'finished_at',
            ]);
        $lists = ReceivingCart::query()
            ->leftJoinSub($subPutawayTasks, 'sub_putaway_tasks', function ($join) {
                $join->on('receiving_carts.warehouse_id', '=', 'sub_putaway_tasks.warehouse_id');
                $join->on('receiving_carts.cart_code', '=', 'sub_putaway_tasks.receiving_cart_code');
            })
            ->where(function ($query) use (
                $cartCode,
                $createdAtFrom,
                $createdAtTo,
                $operatorId,
                $taskNumber
            ) {
                if ($cartCode) {
                    $query->where('receiving_carts.cart_code', $cartCode);
                }
                if ($operatorId) {
                    $query->where('sub_putaway_tasks.user_id', $operatorId);
                }
                if ($taskNumber) {
                    $query->where('sub_putaway_tasks.task_number', $taskNumber);
                }
                if ($createdAtFrom) {
                    $query->where('sub_putaway_tasks.created_at', '>=', strtotime($createdAtFrom));
                }
                if ($createdAtTo) {
                    $query->where('sub_putaway_tasks.created_at', '<=', strtotime($createdAtTo));
                }
            })
            ->orderByDesc('sub_putaway_tasks.updated_at')
            ->orderBy('receiving_carts.cart_code')
            ->paginate($perPage);
        $putAwayCartList = $lists;
        $putAwayCartList = $this->transformData($putAwayCartList);
        $pageInfo = [
            'total' => (int)$lists->total(),
            'page' => (int)$lists->currentPage(),
            'per_page' => (int)$lists->perPage(),
        ];
        $lists = [
            'receiving_carts' => $putAwayCartList,
            'page_bean' => $pageInfo
        ];
        return $lists;
    }

    public function getPutAwayCartDetail($params)
    {
        $CartId = $params['cart_id'];
        $putawayCart = self::query()->find($CartId);
        // 进行中的任务
        $putAwayTask = isset($putawayCart->putawayTask) ? $putawayCart->putawayTask()->where('putaway_tasks.state', PutawayTask::STATE_CREATEED)->first() : null;
        $operator = isset($putAwayTask) ? $putAwayTask->operator : null;
        $info = array();
        $putawayService = new PutawayService();
        if (!is_null($putawayCart)) {
            $info = [
                'id' => $putawayCart->id,
                'code' => $putawayCart->cart_code,
                'putaway_task' => $putAwayTask ? [
                    'id' =>$putAwayTask->id,
                    'task_number' => $putAwayTask->task_number,
                ] : [],
                'created_at' => date($putawayCart->created_at),
                'operator' => isset($operator) ? $operator->name : '',
                'items' => $putawayService->formatPutAwayCartItems($putawayCart),
            ];
        }
        return $info;
    }

    private function transformData($putAwayCartList): array
    {
        $list = [];
        foreach ($putAwayCartList as $cartList) {
            $putAwayTask = PutawayTask::query()->where('task_number', $cartList->task_number)->first();
            $operator = isset($putAwayTask) ? $putAwayTask->operator : null;
            $list[] = [
                'id' => $cartList->id,
                'cart_code' => $cartList->cart_code,
                'created_at' => $putAwayTask ? date($putAwayTask->created_at) : '',
                'operator' => isset($operator) ? $operator->name : '',
                'putaway_task' => ['task_number' => $cartList->task_number]
            ];
        }
        return $list;
    }
}
