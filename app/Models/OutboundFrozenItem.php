<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OutboundFrozenItem extends Model
{
    use SoftDeletes;

    protected $dateFormat = 'U';

    protected $guarded = [];
}
