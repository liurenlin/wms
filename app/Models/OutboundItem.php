<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OutboundItem extends Model
{
    use SoftDeletes;

    const IS_FROZEN_FALSE = 0;
    const IS_FROZEN_TRUE = 1;

    protected $dateFormat = 'U';

    protected $guarded = [];

    public function frozenItems()
    {
        return $this->hasMany(OutboundFrozenItem::class);
    }
}
