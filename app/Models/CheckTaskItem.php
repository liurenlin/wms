<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CheckTaskItem extends Model
{
    const STATE_DEFAULT = 0;
    const STATE_NORMAL = 1;
    const STATE_ABNORMAL = 2;
    protected $dateFormat = 'U';

    protected $guarded = [];
}
