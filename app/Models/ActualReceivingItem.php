<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActualReceivingItem extends Model
{
    const TYPE_RECEIVING = 'receiving';

    protected $dateFormat = 'U';

    protected $guarded = [];

    public static function getReceivingQuantity($preReceiving, $skuCode): int
    {
        $quantity = 0;
        $actualReceivings = $preReceiving->actualReceivings;
        if (is_null($actualReceivings)){
            return $quantity;
        }
        foreach ($actualReceivings as $actualReceiving){
            $num = self::query()
                ->where('actual_receiving_id', $actualReceiving->id)
                ->where('sku_code', $skuCode)
                ->sum('receiving_quantity');
            $quantity+=$num;
        }
        return $quantity;
    }
}
