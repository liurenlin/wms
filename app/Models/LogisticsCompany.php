<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LogisticsCompany extends Model
{
    use SoftDeletes;

    protected $dateFormat = 'U';

    protected $guarded = [];
}
