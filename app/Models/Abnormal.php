<?php

namespace App\Models;

use App\Consts\CommonConst;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Abnormal extends Model
{
    const TYPE_RECEIVING_EXTRA = 1;
    const TYPE_RECEIVING_DEFECTIVE = 2;
    const TYPE_RECEIVING_WRONG = 3;
    const TYPE_PUTAWAY_EXTRA = 4;
    const TYPE_PUTAWAY_MISSING = 5;
    const TYPE_PUTAWAY_WRONG = 6;
    const TYPE_PICKING_MISSING = 7;
    const TYPE_CHECKING_DEFECTIVE = 8;
    const TYPE_CHECKING_FAILED = 9;
    const TYPE_ORDER_INTERCEPTED = 10;

    const RELATION_TYPE_PRE_RECEIVING = 'pre_receiving';
    const RELATION_TYPE_RECEIVING = 'receiving';
    const RELATION_TYPE_ACTUAL_RECEIVING = 'actual_receiving';
    const RELATION_TYPE_PUTAWAY = 'putaway';
    const RELATION_TYPE_PICKING = 'picking';
    const RELATION_TYPE_CHECK = 'check';
    const RELATION_TYPE_WEIGH = 'weigh';
    const RELATION_TYPE_HANDOVER = 'handover';
    const RELATION_TYPE_INVENTORY = 'inventory';

    const REASON_NOT_FOUND = 'Not found';
    const REASON_BROKEN = 'Broken';
    const REASON_STAINS = 'Stains';
    const REASON_COLOR_SHADING = 'Color shading';
    const REASON_EXTRA = 'Extra';
    const REASON_MISSING = 'Missing';
    const REASON_WRONG = 'Wrong';
    const REASON_DEFECTIVE = 'Defective';
    const REASON_FAILED = 'Failed';
    const REASON_INTERCEPTED = 'Intercepted';

    const STATE_CREATED = 1;
    const STATE_IN_PROGRESS = 2;
    const STATE_CONFIRMED = 3;
    const STATE_CLOSED = 4;
    const STATE_COMPLETED = 5;

    protected $dateFormat = 'U';

    protected $guarded = [];

    protected $pageName = 'abnormalList';

    public function operator()
    {
        return $this->belongsTo(User::class, 'report_user_id', 'id');
    }

    public static function getAbnormalTypeTip(int $abnormalType): string
    {
        $tip = 'Unknown';
        switch ($abnormalType) {
            case self::TYPE_RECEIVING_EXTRA:
                $tip = 'Receiving Extra';
                break;
            case self::TYPE_RECEIVING_DEFECTIVE:
                $tip = 'Receiving Defective';
                break;
            case self::TYPE_RECEIVING_WRONG:
                $tip = 'Receiving Wrong';
                break;
            case self::TYPE_PUTAWAY_EXTRA:
                $tip = 'Putaway Extra';
                break;
            case self::TYPE_PUTAWAY_MISSING:
                $tip = 'Putaway Missing';
                break;
            case self::TYPE_PUTAWAY_WRONG:
                $tip = 'Putaway Wrong';
                break;
            case self::TYPE_PICKING_MISSING:
                $tip = 'Picking Missing';
                break;
            case self::TYPE_CHECKING_DEFECTIVE:
                $tip = 'Checking Defective';
                break;
            case self::TYPE_CHECKING_FAILED:
                $tip = 'Checking Failed';
                break;
            case self::TYPE_ORDER_INTERCEPTED:
                $tip = 'Order Intercepted';
                break;
        }
        return $tip;
    }

    public function getAbnormalList($params): array
    {
        $perPage = isset($params['per_page']) ? $params['per_page'] : CommonConst::DEFAULT_PER_PAGE;
        $abnormalNumber = isset($params['abnormal_number']) ? $params['abnormal_number'] : '';
        $abnormalCode = isset($params['abnormal_code']) ? $params['abnormal_code'] : '';
        $state = isset($params['state']) ? $params['state'] : 0;
        $abnormalType = isset($params['abnormal_type']) ? $params['abnormal_type'] : 0;
        $createdAtFrom = isset($params['created_at_from']) ? $params['created_at_from'] : '';
        $createdAtTo = isset($params['created_at_to']) ? $params['created_at_to'] : '';
        $abnormalsQuery = self::query();
        if ($state) {
            $abnormalsQuery->where('state', $state);
        }
        if (!empty($abnormalNumber)) {
            $abnormalsQuery->where('abnormal_number', 'like', '%'.$abnormalNumber.'%');
        }
        if (!empty($abnormalCode)) {
            $abnormalsQuery->where('abnormal_code', 'like', '%'.$abnormalCode.'%');
        }
        if ($abnormalType) {
            $abnormalsQuery->where('abnormal_type', $abnormalType);
        }
        if ($createdAtFrom) {
            $abnormalsQuery->where('created_at', '>=', Carbon::parse($createdAtFrom)->timestamp);
        }
        if ($createdAtTo) {
            $abnormalsQuery->where('created_at', '<=', Carbon::parse($createdAtTo)->timestamp);
        }
        $abnormalList = $abnormalsQuery->orderBy('updated_at', 'desc')
            ->paginate($perPage);
        $abnormals = $this->transformData($abnormalList);
        $pageInfo = [
            'total' => (int) $abnormalList->total(),
            'page' => (int) $abnormalList->currentPage(),
            'per_page' => (int) $abnormalList->perPage(),
        ];
        $lists = [
            'abnormals' => $abnormals,
            'page_bean' => $pageInfo
        ];
        return $lists;
    }

    private function transformData($abnormals)
    {
        $List = [];
        foreach ($abnormals as $abnormal) {
            $List[] = [
                'id' => $abnormal->id,
                'abnormal_number' => $abnormal->abnormal_number,
                'abnormal_code' => $abnormal->abnormal_code,
                'relation_type' => $abnormal->relation_type,
                'relation_number' => $abnormal->relation_number,
                'basket_code'=>$abnormal->basket_code,
                'state' => $abnormal->state,
                'reason' => $abnormal->reason,
                'abnormal_type' => $abnormal->abnormal_type,
                'type' => $abnormal->abnormal_type,
                'created_at' => date($abnormal->created_at),
                'operator' => isset($abnormal->operator) ? $abnormal->operator->name : ''
            ];
        }
        return $List;
    }

    public function getAbnormalDetail(int $abnormalId)
    {
        $abnormal = self::query()->find($abnormalId);
        $info = array();
        if ($abnormal) {
            $processes = AbnormalProcess::query()
                ->where('abnormal_id', $abnormalId)
                ->get()->transform(function ($item) {
                    return [
                        'state' => $item->state_after,
                        'created_at' => $item->created_at->format('Y-m-d H:i:s'),
                        'content' => $item->content,
                        'operator' => User::query()->where('id', $item->user_id)->value('name') ?? '',
                    ];

                })
                ->toArray();
            $skus = AbnormalItem::query()
                ->where('abnormal_id', $abnormalId)
                ->get([
                    'sku_code',
                    'quantity'
                ])
                ->toArray();
            $info = [
                'id' => $abnormal->id,
                'abnormal_number' => $abnormal->abnormal_number,
                'abnormal_code' => $abnormal->abnormal_code,
                'abnormal_type' => $abnormal->abnormal_type,
                'basket_code' => $abnormal->basket_code,
                'state' => $abnormal->state,
                'relation_number' => $abnormal->relation_number,
                'pre_receiving_number' => $abnormal->pre_receiving_number,
                'outbound_number' => $abnormal->outbound_number,
                'created_at' => date($abnormal->created_at),
                'operator' => isset($abnormal->operator) ? $abnormal->operator->name : '',
                'processes' => $processes,
                'skus' => $skus,
            ];
        }
        return $info;
    }
}
