<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockLog extends Model
{

    const RELATION_TYPE_RECEIVING_LOG = 'receiving_log';
    const RELATION_TYPE_PUTAWAY_LOG = 'putaway_log';
    const RELATION_TYPE_PICKING_LOG = 'picking_log';
    const RELATION_TYPE_HANDOVER_ITEM = 'handover_item';
    const RELATION_TYPE_ABNORMAL = 'abnormal';
    const RELATION_TYPE_ABNORMAL_ITEM = 'abnormal_item';
    const RELATION_TYPE_STOCK_DEAL_WITH_LOG= 'stock_deal_with_log';
    const RELATION_TYPE_OUTBOUND= 'outbound';
    const RELATION_TYPE_OUTBOUND_ITEM= 'outbound_item';

    const RELATION_TYPE_WEIGH = 'weigh';

    const RELATION_TYPE_PRE_RECEIVING = 'pre_receiving';
    const RELATION_TYPE_RECEIVING = 'receiving';
    const RELATION_TYPE_PUTAWAY = 'putaway';
    const RELATION_TYPE_PICKING = 'picking';
    const RELATION_TYPE_CHECK = 'check';

    const RELATION_TYPE_HANDOVER = 'handover';
    const RELATION_TYPE_INVENTORY = 'inventory';

    protected $dateFormat = 'U';

    protected $guarded = [];
}
