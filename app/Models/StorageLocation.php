<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StorageLocation extends Model
{
    use SoftDeletes;

    const IS_EMPTY_FALSE = 0;
    const IS_EMPTY_TRUE = 1;

    const IS_SUGGEST_LOCK_FALSE = 0;
    const IS_SUGGEST_LOCK_TRUE = 1;
    
    protected $dateFormat = 'U';

    protected $guarded = [];

    protected $fillable = [
        'warehouse_id', 'storage_type', 'storage_area_code', 'storage_location_code', 'is_empty', 'is_suggest_lock',
    ];

    public function getCreatedAtAttribute($value)
    {
        return Carbon::createFromTimestamp($value)->format('Y-m-d H:i:s');
    }

    public function setDeletedAtAttribute($value)
    {
        return Carbon::createFromTimestamp($value)->getTimestamp();
    }
}
