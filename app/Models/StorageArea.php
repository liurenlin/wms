<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StorageArea extends Model
{
    use SoftDeletes;

    const STORAGE_TYPE_RECEIVING = 1;
    const STORAGE_TYPE_PICKING = 2;
    const STORAGE_TYPE_PACKAGE = 3;
    const STORAGE_TYPE_OUTBOUND = 4;
    const STORAGE_TYPE_ABNORMAL = 5;
    const STORAGE_TYPE_DEFECTIVE = 6;

    protected $dateFormat = 'U';

    protected $guarded = [];

    protected $fillable = [
        'storage_type', 'area_code'
    ];
}
