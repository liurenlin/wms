<?php

namespace App\Repositories;

use App\Models\ProductImage;
use App\Models\ProductSku;
use App\Models\ProductSkuImage;
use App\Models\Resource;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ProductRepository;
use App\Models\Product;
use App\Validators\ProductValidator;

/**
 * Class ProductRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ProductRepositoryEloquent extends BaseRepository implements ProductRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Product::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function createProduct(string $productCode, string $productName, $images = null): ?Product
    {
        $product = Product::query()->where('product_code', $productCode)->first();
        if (!$product) {
            $product = Product::create([
                'product_code' => $productCode,
                'product_name' => $productName,
            ]);
        } else {
            if ($product->product_name != $productName) {
                $product->product_name = $productName;
                $product->save();
            }
        }
        if (!empty($images)) {
            ProductImage::query()
                ->where('product_id', $product->id)
                ->delete();
            foreach ($images as $image) {
                $resource = Resource::query()->create([
                    'url' => $image['url'],
                ]);
                ProductImage::query()->create([
                    'product_id' => $product->id,
                    'resource_id' => $resource->id,
                ]);
            }
        } elseif ($images == []) {
            ProductImage::query()
                ->where('product_id', $product->id)
                ->delete();
        }
        return $product;
    }

    public function createProductSku(int $productId, array $sku): ?ProductSku
    {
        $productSku = ProductSku::query()->create([
            'product_id' => $productId,
            'sku_code' => $sku['sku_code'],
            'sku_name' => $sku['name'],
            'sku_attribute' => $sku['attribute'],
            'weight' => $sku['weight'],
            'height' => $sku['height'],
            'length' => $sku['length'],
            'width' => $sku['width'],
        ]);
        foreach ($sku['images'] as $image) {
            $resource = Resource::query()->create([
                'url' => $image['url'],
            ]);
            ProductSkuImage::query()->create([
                'product_sku_id' => $productSku->id,
                'resource_id' => $resource->id,
            ]);
        }
        return $productSku;
    }

    public function createProductSkus(int $productId, array $skus): bool
    {
        foreach ($skus as $sku) {
            $productSku = ProductSku::query()
                ->where('product_id', $productId)
                ->where('sku_code', $sku['sku_code'])
                ->first();
            if (!$productSku) {
                $productSku = ProductSku::query()->create([
                    'product_id' => $productId,
                    'sku_code' => $sku['sku_code'],
                    'sku_name' => $sku['name'],
                    'sku_attribute' => $sku['attribute'],
                    'weight' => $sku['weight'],
                    'height' => $sku['height'],
                    'length' => $sku['length'],
                    'width' => $sku['width'],
                ]);
            } else {
                $productSku->sku_name = $sku['name'];
                $productSku->sku_attribute = $sku['attribute'];
                $productSku->weight = $sku['weight'];
                $productSku->height = $sku['height'];
                $productSku->length = $sku['length'];
                $productSku->width = $sku['width'];
                $productSku->save();
            }
            if (isset($sku['images'])) {
                ProductSkuImage::query()->where('product_sku_id', $productSku->id)->delete();
                foreach ($sku['images'] as $image) {
                    $resource = Resource::query()->create([
                        'url' => $image['url'],
                    ]);
                    ProductSkuImage::query()->create([
                        'product_sku_id' => $productSku->id,
                        'resource_id' => $resource->id,
                    ]);
                }
            }
        }
        return true;
    }

    public function getProductByCode(string $productCode): ?Product
    {
        return Product::query()
            ->where('product_code', $productCode)
            ->first();
    }
}
