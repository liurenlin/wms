<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OutboundRepository.
 *
 * @package namespace App\Repositories;
 */
interface OutboundRepository extends RepositoryInterface
{
    //
}
