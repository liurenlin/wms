<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface WeighRepository.
 *
 * @package namespace App\Repositories;
 */
interface WeighRepository extends RepositoryInterface
{
    //
}
