<?php

namespace App\Repositories;

use App\Consts\NumberPrefixConst;
use App\Models\ActualReceiving;
use App\Models\PreReceivingItem;
use App\Models\Product;
use App\Models\ProductSku;
use App\Models\ProductSkuImage;
use App\Models\ReceivingLog;
use App\Models\SkuCodePrintLog;
use Carbon\Carbon;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PreReceivingRepository;
use App\Models\PreReceiving;
use App\Validators\PreReceivingValidator;

/**
 * Class PreReceivingRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class PreReceivingRepositoryEloquent extends BaseRepository implements PreReceivingRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PreReceiving::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getPreReceivingByNumber(string $preReceivingNumber): array
    {
        $preReceiving = $this->with([
            'items.sku.product.images',
        ])
            ->findWhere(['pre_receiving_number' => $preReceivingNumber])
            ->transform(function ($preReceiving) {
                $preReceivingId = $preReceiving->id;
                $data = ['id' => $preReceivingId];
                $historyProductIds = [];
                $data['products'] = $preReceiving->items()
                    ->get()
                    ->transform(function ($item) use (&$historyProductIds) {
                        $product = $item->sku->product;
                        if (in_array($product->id, $historyProductIds)) {
                            return null;
                        }
                        $productImages = $product->images->transform(function ($item) {
                            return [
                                'url' => $item->url,
                            ];
                        })->values()->toArray();
                        array_push($historyProductIds, $product->id);
                        return [
                            'id' => $product->id,
                            'code' => $product->product_code,
                            'name' => $product->product_name,
                            'images' => $productImages,
                        ];
                    })
                    ->filter()
                    ->filter(function ($item) use ($preReceivingId) {
                        $skuCodes = ProductSku::query()
                            ->where('product_id', $item['id'])
                            ->pluck('sku_code')
                            ->toArray();

                        $preReceivingItems = PreReceivingItem::query()
                            ->where('pre_receiving_id', $preReceivingId)
                            ->whereIn('sku_code', $skuCodes)
                            ->get();
                        if ($preReceivingItems->isEmpty()) {
                            return false;
                        }
                        foreach ($preReceivingItems as $preReceivingItem) {
                            $sumActualReceivingItemQuantity = ActualReceiving::query()
                                ->leftJoin('actual_receiving_items', 'actual_receiving_items.actual_receiving_id', '=', 'actual_receivings.id')
                                ->where('actual_receiving_items.sku_code', $preReceivingItem->sku_code)
                                ->where('actual_receivings.pre_receiving_id', $preReceivingId)
                                ->sum('receiving_quantity');
                            if ($sumActualReceivingItemQuantity < $preReceivingItem->quantity) {
                                return true;
                            }
                        }
                        return false;

                    })
                    ->values()
                    ->toArray();
                return $data;
            });
        return $preReceiving->first();
    }

    public function getPreReceivingItem(string $preReceivingNumber, string $skuCode): ?PreReceivingItem
    {
        return PreReceivingItem::query()
            ->join('pre_receivings', 'pre_receiving_items.pre_receiving_id', '=', 'pre_receivings.id')
            ->where('pre_receivings.pre_receiving_number', $preReceivingNumber)
            ->where('pre_receiving_items.sku_code', $skuCode)
            ->first('pre_receiving_items.*');
    }

    public function hasSameSkuInReceivingCart(int $warehouseId, int $actualReceivingId, string $skuCode, string $receivingCartCode): bool
    {
        $actualReceiving = ActualReceiving::query()->find($actualReceivingId);
        $receivingLogExist = ReceivingLog::query()
            ->leftJoin('receiving_cart_items', 'receiving_cart_items.id', '=', 'receiving_logs.receiving_cart_item_id')
            ->where('receiving_cart_items.sku_code', $skuCode)
            ->where('receiving_logs.warehouse_id', $warehouseId)
            ->where('receiving_logs.receiving_cart_code', $receivingCartCode)
            ->where('receiving_logs.pre_receiving_id', '<>', $actualReceiving->pre_receiving_id)
            ->exists();
        if ($receivingLogExist) {
            return true;
        }
        return false;
    }

    public function getPreReceivingProductByNumberAndProductId(string $preReceivingNumber, int $productId)
    {
        $product = Product::query()->with('images')->find($productId, [
            'id',
            'product_code as code',
            'product_name as name',
        ]);
        $product->images = $product->images->transform(function ($item) {
           return [
               'url' => $item->url,
           ];
        });

        $productImages = $product->images;

        $productSkus = $this->from('pre_receivings as pr')
            ->leftJoin('pre_receiving_items as pri', 'pr.id', '=', 'pri.pre_receiving_id')
            ->leftJoin('product_skus as ps', 'pri.sku_code', '=', 'ps.sku_code')
            ->where('pr.pre_receiving_number', $preReceivingNumber)
            ->where('ps.product_id', $productId)
            ->get([
                'ps.*',
                'pri.quantity',
                'pr.id as pre_receiving_id',
                'pr.warehouse_id',
            ])
            ->transform(function ($item) use ($productImages) {
                $productSkuImages = ProductSkuImage::query()
                    ->with('resource')
                    ->where('product_sku_id', $item->id)
                    ->get()
                    ->transform(function ($item) {
                        if (!$item->resource) return null;
                        return [
                            'url' => $item->resource->url,
                        ];
                    })
                    ->filter();
                if ($productSkuImages->isEmpty()) {
                    $productSkuImages = $productImages;
                }
                // 已完成实际入库单收货情况
                $recivingSum = ActualReceiving::query()
                    ->leftJoin('actual_receiving_items', 'actual_receivings.id', '=', 'actual_receiving_items.actual_receiving_id')
                    ->where('actual_receiving_items.sku_code', $item->sku_code)
                    ->where('actual_receivings.pre_receiving_id', $item->pre_receiving_id)
                    ->sum('receiving_quantity');
                if ($item->quantity <= $recivingSum) {
                    return null;
                }
                return [
                    'id' => $item->id,
                    'code' => $item->sku_code,
                    'name' => $item->sku_name,
                    'attribute' => $item->sku_attribute,
                    'images' => $productSkuImages,
                    'quantity' => $item->quantity - $recivingSum,
                    'print_quantity' => $this->getSkuPrintQuantity($item->warehouse_id, $item->sku_code, $item->pre_receiving_id),
                ];
            })
            ->filter()
            ->values();
        $product->skus = $productSkus;
        return $product;
    }

    private function getSkuPrintQuantity(int $warehouseId, string $skuCode, int $preReceivingId): int
    {
        return (int) SkuCodePrintLog::query()
            ->where('warehouse_id', $warehouseId)
            ->where('pre_receiving_id', $preReceivingId)
            ->where('sku_code', $skuCode)
            ->sum('quantity');
    }

    public function checkPreReceivingItems(array $items): bool
    {
        if (empty($items)) {
            return false;
        }
        foreach ($items as $item) {
            if (!ProductSku::query()->where('sku_code', $item['sku_code'])->first()) {
                return false;
            }
        }
        return true;
    }

    public function createPreReceiving(int $warehouseId, int $thirdPartyOrderType, string $thirdPartyOrderNumber, string $thirdPartyOrderCreatedAt): ?PreReceiving
    {
        return PreReceiving::create([
            'warehouse_id' => $warehouseId,
            'pre_receiving_number' => $this->generatePreReceivingNumber(),
            'third_party_order_type' => $thirdPartyOrderType,
            'third_party_order_number' => $thirdPartyOrderNumber,
            'third_party_order_created_at' => Carbon::parse($thirdPartyOrderCreatedAt)->timestamp,
            'state' => PreReceiving::STATE_CREATED,
        ]);
    }

    private function generatePreReceivingNumber(): string
    {
        $preReceivingCount = PreReceiving::query()
            ->whereBetween('created_at', [
                Carbon::today()->startOfDay()->timestamp,
                Carbon::today()->endOfDay()->timestamp,
            ])
            ->count();
        $preReceivingCount = $preReceivingCount + 1;
        $number = NumberPrefixConst::PRE_RECEIVING . date('Ymd') . sprintf('%05s', $preReceivingCount);
        $exist = PreReceiving::query()
            ->where('pre_receiving_number', $number)
            ->count();
        if ($exist) {
            return self::generatePreReceivingNumber();
        }
        return $number;
    }

    /**
     * @param  int  $preReceivingId
     * @param  array  $items
     * [
     *  [
     *    "sku_code" => "SKU001",
     *    "quantity" => 1
     *  ]
     * ]
     * @return bool
     */
    public function createItems(int $preReceivingId, array $items): bool
    {
        $time = time();
        $preReceivingItems = collect($items)->transform(function ($item) use ($preReceivingId, $time) {
            return [
                'pre_receiving_id' => $preReceivingId,
                'sku_code' => $item['sku_code'],
                'quantity' => $item['quantity'],
                'created_at' => $time,
                'updated_at' => $time,
            ];
        })->toArray();
        PreReceivingItem::query()->insert($preReceivingItems);
        return true;
    }

    public function isActualReceivingCompleted(int $actualReceivingId): bool
    {
        return ActualReceiving::query()
            ->where('id', $actualReceivingId)
            ->where('state', ActualReceiving::STATE_COMPLETED)
            ->exists();
    }

}
