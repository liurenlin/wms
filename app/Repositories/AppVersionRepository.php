<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AppVersionRepository.
 *
 * @package namespace App\Repositories;
 */
interface AppVersionRepository extends RepositoryInterface
{
    //
}
