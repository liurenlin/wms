<?php

namespace App\Repositories;

use App\Consts\CommonConst;
use App\Models\Role;
use App\Models\UserRole;
use App\Models\Warehouse;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\UserRepository;
use App\Validators\UserValidator;

/**
 * Class UserRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getUserWarehouses(int $userId): array
    {
        return UserRole::query()
            ->where('user_id', $userId)
            ->get()->transform(function ($item) {
                return [
                    'id' => $item->warehouse_id,
                    'name' => Warehouse::query()->find($item->warehouse_id)->warehouse_name,
                ];
            })->toArray();
    }

    public function getUserList(): array
    {
        return User::query()
            ->with('roles')
            ->get()
            ->transform(function ($user) {
                return [
                    'id' => $user->id,
                    'name' => $user->name,
                    'roles' => $user->roles()->get([
                        'roles.id',
                        'roles.role_name as name',
                    ])->toArray(),
                ];
            })->toArray();
    }

    public function createUser(string $name, string $password, string $email = ''): User
    {
        return User::create([
            'name' => $name,
            'password' => Hash::make($password),
            'email' => (string) $email,
        ]);
    }

    /**
     * @param  array  $roles
     * [
     *      [
     *          "id": 1,
     *      ]
     * ]
     * @return bool
     */
    public function checkRoles(array $roles): bool
    {
        $count = count($roles);
        $rolesCount = Role::query()
            ->whereIn('id', collect($roles)->pluck('id')->toArray())
            ->count();
        if ($count != $rolesCount) {
            return false;
        }
        return true;
    }

    /**
     * @param  int  $warehouseId
     * @param  int  $userId
     * @param  array  $roles
     * [
     *      [
     *          "id": 1,
     *      ]
     * ]
     * @return bool
     */
    public function updateUserRoles(int $warehouseId, int $userId, array $roles): bool
    {
        DB::beginTransaction();
        try {
            // 删除
            UserRole::query()
                ->where('warehouse_id', $warehouseId)
                ->where('user_id', $userId)
                ->delete();
            // 添加
            $time = time();
            $rolePermissionGroups = collect($roles)->transform(function ($item) use ($warehouseId, $userId, $time) {
                return [
                    'warehouse_id' => $warehouseId,
                    'user_id' => $userId,
                    'role_id' => $item['id'],
                    'created_at' => $time,
                    'updated_at' => $time,
                ];
            })->toArray();
            UserRole::query()->insert($rolePermissionGroups);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('updateUserRoles Fail', [
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
            return false;
        }
        return true;
    }

    public function deleteUserRole(int $userId, int $roleId): bool
    {
        UserRole::query()
            ->where('user_id', $userId)
            ->where('role_id', $roleId)
            ->delete();
        return true;
    }
}
