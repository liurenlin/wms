<?php

namespace App\Repositories;

use App\Consts\NumberPrefixConst;
use App\Models\Abnormal;
use App\Models\CheckLog;
use App\Models\CheckTaskItem;
use App\Models\OpenMessageQueue;
use App\Models\Outbound;
use App\Models\OutboundItem;
use App\Models\PickingTaskBasket;
use App\Services\MessageService;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\CheckTaskRepository;
use App\Models\CheckTask;
use App\Validators\CheckTaskValidator;

/**
 * Class CheckTaskRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class CheckTaskRepositoryEloquent extends BaseRepository implements CheckTaskRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CheckTask::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getPickingTaskBasketByOutboundNumber(string $outboundNumber): ?PickingTaskBasket
    {
        return PickingTaskBasket::query()
            ->where('outbound_number', $outboundNumber)
            ->orderByDesc('created_at')
            ->first();
    }

    public function getPickingTaskBasket(int $warehouseId, string $basketCode): ?PickingTaskBasket
    {
        return $pickingTaskBasket = PickingTaskBasket::query()
            ->join('picking_tasks', 'picking_task_baskets.picking_task_id', '=', 'picking_tasks.id')
            ->where('picking_task_baskets.basket_code', $basketCode)
            ->where('picking_tasks.warehouse_id', $warehouseId)
            ->orderByDesc('picking_task_baskets.created_at')
            ->first('picking_task_baskets.*');
    }

    public function getCheckTaskByOutboundNumber(string $outboundNumber): ?CheckTask
    {
        return CheckTask::query()
            ->where('outbound_number', $outboundNumber)
            ->where('state', CheckTask::STATE_CREATED)
            ->first();
    }

    public function getCheckTaskByNumber(string $taskNumber): ?CheckTask
    {
        return CheckTask::query()
            ->where('task_number', $taskNumber)
            ->first();
    }

    public function updateState(int $checkTaskId, int $state): bool
    {
        return (bool) CheckTask::query()
            ->where('id', $checkTaskId)
            ->update([
                'state' => $state,
            ]);
    }

    public function createCheckTask(int $warehouseId, string $outboundNumber, string $basketCode, int $userId): CheckTask
    {
        $pickingTaskBasket = PickingTaskBasket::query()
            ->where('outbound_number', $outboundNumber)
            ->where('basket_code', $basketCode)
            ->first();
        $outbound = Outbound::query()
            ->where('outbound_number', $pickingTaskBasket->outbound_number)
            ->first();
        $outboundItems = OutboundItem::query()
            ->where('outbound_id', $outbound->id)
            ->get();
        // 生成任务
        $checkTask = CheckTask::create([
            'task_number' => $this->generateTaskNumber(),
            'warehouse_id' => $warehouseId,
            'picking_task_id' => $pickingTaskBasket->picking_task_id,
            'basket_code' => $basketCode,
            'outbound_number' => $outbound->outbound_number,
            'user_id' => $userId,
            'state' => CheckTask::STATE_CREATED
        ]);
        // 任务 items
        foreach ($outboundItems as $outboundItem) {
            CheckTaskItem::create([
                'check_task_id' => $checkTask->id,
                'sku_code' => $outboundItem->sku_code,
                'quantity' => $outboundItem->quantity,
            ]);
        }
        MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_ORDER_STATUS_NOTIFY, ['order_number' => $outbound->order_number]);
        return $checkTask;

    }

    public function getCheckTaskItemBySkuCode(int $checkTaskId, string $skuCode): ?CheckTaskItem
    {
        return CheckTaskItem::query()
            ->where('check_task_id', $checkTaskId)
            ->where('sku_code', $skuCode)
            ->first();
    }

    public function getCheckTaskItems(int $checkTaskId): array
    {
        return CheckTaskItem::query()
            ->where('check_task_id', $checkTaskId)
            ->get()
            ->toArray();
    }


    public function createCheckLog(
        int $warehouseId,
        int $userId,
        int $checkTaskId,
        string $basketCode,
        string $skuCode,
        int $quantity
    ): bool
    {
        CheckLog::create([
            'warehouse_id' => $warehouseId,
            'check_task_id' => $checkTaskId,
            'basket_code' => $basketCode,
            'sku_code' => $skuCode,
            'quantity' => $quantity,
            'user_id' => $userId,
        ]);

        return true;
    }

    private function generateTaskNumber(): string
    {
        $checkTaskCount = CheckTask::query()
            ->whereBetween('created_at', [
                Carbon::today()->startOfDay()->timestamp,
                Carbon::today()->endOfDay()->timestamp,
            ])
            ->count();
        $checkTaskCount = $checkTaskCount + 1;
        $number = NumberPrefixConst::CHECK . date('Ymd') . sprintf('%05s', $checkTaskCount);
        $exist = CheckTask::query()
            ->where('task_number', $number)
            ->count();
        if ($exist) {
            return self::generateTaskNumber();
        }
        return $number;
    }

    public function confirmTask(
        int $warehouseId,
        int $userId,
        int $checkTaskId,
        int $state = 0,
        array $skus
    ): bool
    {
        // 检查
        $checkTask = $this->findWhere([
            'id' => $checkTaskId,
            'warehouse_id' => $warehouseId,
            'user_id' => $userId,
            'state' => CheckTask::STATE_CREATED,
        ])->first();
        if (!$checkTask) {
            return false;
        }

        // 出库单
        $outbound = Outbound::query()
            ->where('outbound_number', $checkTask->outbound_number)
            ->first();

        foreach ($skus as $sku) {
            $checkTaskItem = CheckTaskItem::query()
                ->where('check_task_id', $checkTaskId)
                ->where('sku_code', $sku['code'])
                ->first();
            if (!$checkTaskItem || $checkTaskItem->quantity != $sku['quantity']) {
                return false;
            }
        }

        // 更新 check task item
        CheckTaskItem::query()
            ->where('check_task_id', $checkTaskId)
            ->update([
                'state' => CheckTaskItem::STATE_NORMAL,
                'check_quantity' => DB::raw('quantity'),
            ]);

        // 检查通过更新任务状态
        $checkTask->state = CheckTask::STATE_NORMAL;
        $checkTask->save();

        app(OutboundRepository::class)->updateState(
            $outbound->id,
            Outbound::STATE_CHECK_COMPLETED,
            $userId
        );
        MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_ORDER_STATUS_NOTIFY, ['order_number' => $outbound->order_number]);
        return true;
    }


    public function getListByPage(array $querys, int $page = 1, int $perPage = 1): array
    {
        $checkTaskQuery = CheckTask::query();

        if (!empty($querys['outbound_number'])) {
            $checkTaskQuery->where('outbound_number', $querys['outbound_number']);
        }

        if (!empty($querys['platform_order_number'])) {
            $outbound = Outbound::query()
                ->where('order_number', $querys['platform_order_number'])
                ->first();
            // 出库单
            $checkTaskQuery->where('outbound_number', $outbound ? $outbound->outbound_number : '');
        }

        if (!empty($querys['created_at_from'])) {
            $checkTaskQuery->where('created_at', '>=', Carbon::parse($querys['created_at_from'])->timestamp);
        }

        if (!empty($querys['created_at_to'])) {
            $checkTaskQuery->where('created_at', '<=', Carbon::parse($querys['created_at_to'])->timestamp);
        }

        if (!empty($querys['user_id'])) {
            $checkTaskQuery->where('user_id', $querys['user_id']);
        }

        $checkTaskQuery->orderByDesc('created_at');

        $total = $checkTaskQuery->count();
        $checkTasks = $checkTaskQuery->forPage($page, $perPage)
            ->get([
                'id',
                'picking_task_id',
                'basket_code',
                'created_at',
                'user_id',
                'state',
            ])
            ->transform(function ($item) {
                $outboundNumber = PickingTaskBasket::query()
                    ->where('picking_task_id', $item->picking_task_id)
                    ->where('basket_code', $item->basket_code)
                    ->value('outbound_number');
                $outbound = Outbound::query()
                    ->where('outbound_number', $outboundNumber)
                    ->first();
                return [
                    'id' => $item->id,
                    'outbound_number' => $outbound->outbound_number,
                    'platform_order_number' => $outbound->order_number,
                    'created_at' => $item->created_at->format('Y-m-d H:i:s'),
                    'operator' => User::query()->where('id', $item->user_id)->value('name') ?? '',
                    'state' => $item->state,
                ];
            })
            ->toArray();

        return [
            'check_tasks' => $checkTasks,
            'page_bean' => [
                'total' => $total,
                'page' => $page,
                'per_page' => $perPage,
            ],
        ];
    }

    public function existsCheckTask(int $checkTaskId): bool
    {
        return (bool) CheckTask::query()->where('id', $checkTaskId)->count();
    }

    public function getDetail(int $checkTaskId): array
    {
        $checkTask = CheckTask::query()->where('id', $checkTaskId)->first();
        if (!$checkTask) {
            return [];
        }
        // 补充 items
        $checkTaskItems = CheckTaskItem::query()
            ->where('check_task_id', $checkTask->id)
            ->get([
                'sku_code',
                'state',
            ])
            ->transform(function ($item) use ($checkTask) {
                return [
                    'sku_code' => $item->sku_code,
                    'basket_code' => $checkTask->basket_code,
                    'state' => $item->state,
                ];
            });

        $outboundNumber = PickingTaskBasket::query()
            ->where('picking_task_id', $checkTask->picking_task_id)
            ->where('basket_code', $checkTask->basket_code)
            ->value('outbound_number');
        $outbound = Outbound::query()
            ->where('outbound_number', $outboundNumber)
            ->first();

        // 补充关联异常
        $abnormals = Abnormal::query()
            ->where('relation_type', Abnormal::RELATION_TYPE_CHECK)
            ->where('relation_number', $outboundNumber)
            ->get()
            ->transform(function ($item) {
                return [
                    'id' => $item->id,
                    'abnormal_number' => $item->abnormal_number,
                    'state' => $item->state,
                    'operator' => User::query()->where('id', $item->report_user_id)->value('name') ?? '',
                ];
            })
            ->toArray();

        return [
            'id' => $checkTask->id,
            'outbound_number' => $outbound->outbound_number,
            'platform_order_number' => $outbound->order_number,
            'created_at' => $checkTask->created_at->format('Y-m-d H:i:s'),
            'operator' => User::query()->where('id', $checkTask->user_id)->value('name') ?? '',
            'state' => $checkTask->state,
            'items' => $checkTaskItems,
            'abnormals' => $abnormals,
        ];
    }
}
