<?php

namespace App\Repositories;

use App\Models\Basket;
use App\Models\LogisticsCompany;
use App\Models\Outbound;
use App\Models\PickingTaskBasket;
use App\User;
use Carbon\Carbon;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\WeighRepository;
use App\Models\Weigh;
use App\Validators\WeighValidator;

/**
 * Class WeighRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class WeighRepositoryEloquent extends BaseRepository implements WeighRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Weigh::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getAvailableOutbound(int $warehouseId, string $outboundNumber): ?Outbound
    {
        return Outbound::query()
            ->where('warehouse_id', $warehouseId)
            ->where('outbound_number', $outboundNumber)
            ->where('is_need_intercept', Outbound::IS_NEED_INTERCEPT_FALSE)
            ->where('state', Outbound::STATE_CHECK_COMPLETED)
            ->first();
    }


    public function getPickingTaskBasket(string $pickingBasketCode): ?PickingTaskBasket
    {
        return PickingTaskBasket::query()
            ->where('basket_code', $pickingBasketCode)
            ->orderByDesc('created_at')
            ->first();
    }

    public function getOutboundByNumber(string $outboundNumber): Outbound
    {
        return Outbound::query()
            ->where('outbound_number', $outboundNumber)
            ->first();
    }

    public function updateOutbound(int $outboundId, $params): bool
    {
        return (bool) Outbound::query()
            ->where('id', $outboundId)
            ->update($params);
    }

    public function storeWeigh(
        int $warehouseId,
        int $userId,
        string $outboundNumber,
        int $pickingTaskId,
        string $pickingBasketCode,
        float $weight,
        int $logisticsCompanyId,
        string $logisticsNumber
    ): Weigh
    {
        return Weigh::create([
            'warehouse_id' => $warehouseId,
            'picking_task_id' => $pickingTaskId,
            'basket_code' => $pickingBasketCode,
            'outbound_number' => $outboundNumber,
            'weight' => $weight,
            'logistics_company_id' => $logisticsCompanyId,
            'logistics_number' => $logisticsNumber,
            'user_id' => $userId,
        ]);
    }

    /**
     * @param  array  $receivingInfo
     * [
     *      "receiving_country" => "xx",
     *      "receiving_state" => "xx",
     *      "receiving_city" => "xx",
     *      "receiving_district" => "xx",
     *      "receiving_address" => "xx"
     * ]
     * @param  float  $weight
     * @return LogisticsCompany
     */
    public function getBestLogisticsCompany(array $receivingInfo, float $weight): LogisticsCompany
    {
        if (empty($receivingInfo) || !$weight) {
            return '';
        }
        return LogisticsCompany::query()->first();
    }

    private function getLogisticsNumberPrefix(): string
    {
        return 'BKTO';
    }

    public function getListByPage(array $querys, int $page = 1, int $perPage = 1): array
    {
        $weighQuery = Weigh::query();

        if (!empty($querys['outbound_number'])) {
            $weighQuery->where('outbound_number', $querys['outbound_number']);
        }

        if (!empty($querys['platform_order_number'])) {
            $outbound = Outbound::query()
                ->where('order_number', $querys['platform_order_number'])
                ->first();
            $weighQuery->where('outbound_number', $outbound->outbound_number);
        }

        if (!empty($querys['created_at_from'])) {
            $weighQuery->where('created_at', '>=', Carbon::parse($querys['created_at_from'])->timestamp);
        }

        if (!empty($querys['created_at_to'])) {
            $weighQuery->where('created_at', '<=', Carbon::parse($querys['created_at_to'])->timestamp);
        }

        if (!empty($querys['user_id'])) {
            $weighQuery->where('user_id', $querys['user_id']);
        }

        $weighQuery->orderByDesc('created_at');

        $total = $weighQuery->count();
        $weighs = $weighQuery->forPage($page, $perPage)
            ->get([
                'id',
                'outbound_number',
                'created_at',
                'weight',
                'user_id',
            ])
            ->transform(function ($item) {
                $outbound = Outbound::query()
                    ->where('outbound_number', $item->outbound_number)
                    ->first();
                return [
                    'id' => $item->id,
                    'outbound_number' => $outbound->outbound_number,
                    'platform_order_number' => $outbound->order_number,
                    'created_at' => $item->created_at->format('Y-m-d H:i:s'),
                    'weight' => (float) $item->weight,
                    'operator' => User::query()->where('id', $item->user_id)->value('name') ?? '',
                ];
            })
            ->toArray();

        return [
            'weighs' => $weighs,
            'page_bean' => [
                'total' => $total,
                'page' => $page,
                'per_page' => $perPage,
            ],
        ];
    }

    public function getWeighByLogistcsNumber(string $logisticsNumber): ?Weigh
    {
        return Weigh::query()
            ->where('logistics_number', $logisticsNumber)
            ->first();
    }

    public function releaseBasket(int $warehouseId, string $basketCode): bool
    {
        return (bool) Basket::query()
            ->where('warehouse_id', $warehouseId)
            ->where('basket_code', $basketCode)
            ->update([
                'state' => Basket::STATE_FREE,
            ]);
    }

}
