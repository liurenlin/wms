<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\OpenMessageQueueRepository;
use App\Models\OpenMessageQueue;
use App\Validators\OpenMessageQueueValidator;

/**
 * Class OpenMessageQueueRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class OpenMessageQueueRepositoryEloquent extends BaseRepository implements OpenMessageQueueRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return OpenMessageQueue::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function createOpenMessageQueue(string $messageType, array $data = [], $attributes = []): ?OpenMessageQueue
    {
        if (empty($data) && empty($attributes)) {
            return false;
        }
        $openMessageQueue = OpenMessageQueue::create([
            'message_type' => $messageType,
            'from_app_id' => $data['app_id'] ?? '',
            'data' => json_encode([
                'message_type' => $messageType,
                'payload' => $data,
            ]),
            'attributes' => json_encode($attributes),
        ]);
        return $openMessageQueue;
    }

}
