<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OpenMessageQueueRepository.
 *
 * @package namespace App\Repositories;
 */
interface OpenMessageQueueRepository extends RepositoryInterface
{
    //
}
