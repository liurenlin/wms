<?php

namespace App\Repositories;

use App\Models\Stock;
use App\Models\StorageArea;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\StorageLocationRepository;
use App\Models\StorageLocation;
use App\Validators\StorageLocationValidator;

/**
 * Class StorageLocationRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class StorageLocationRepositoryEloquent extends BaseRepository implements StorageLocationRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return StorageLocation::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getStock(int $warehouseId, string $storageLocationCode): array
    {
        $stock = Stock::query()
            ->where('warehouse_id', $warehouseId)
            ->where('storage_type', StorageArea::STORAGE_TYPE_PICKING)
            ->where('bin', $storageLocationCode)
            ->first();
        if (!$stock) {
            return [];
        }
        return [
            'sku_code' => $stock->sku_code,
            'quantity' => $stock->quantity,
        ];
    }

    public function getStockStorageLocation(int $warehouseId, string $storageLocationCode): ?StorageLocation
    {
        return StorageLocation::query()
            ->where('warehouse_id', $warehouseId)
            ->where('storage_type', StorageArea::STORAGE_TYPE_PICKING)
            ->where('storage_location_code', $storageLocationCode)
            ->first();
    }
}
