<?php

namespace App\Repositories;

use App\Models\Outbound;
use App\Models\OutboundFrozenItem;
use App\Models\OutboundItem;
use App\Models\ProductSku;
use App\Models\StockDealWithLog;
use App\Models\StockLog;
use App\Models\StorageArea;
use App\Models\StorageLocation;
use App\User;
use Illuminate\Support\Facades\Log;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\StockRepository;
use App\Models\Stock;
use App\Validators\StockValidator;

/**
 * Class StockRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class StockRepositoryEloquent extends BaseRepository implements StockRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Stock::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getStocksByBin(int $warehouseId, string $bin): array
    {
        return Stock::query()
            ->where('warehouse_id', $warehouseId)
            ->where('storage_type', StorageArea::STORAGE_TYPE_PICKING)
            ->where('bin', $bin)
            ->get([
                'bin',
                'sku_code',
                'quantity',
            ])
            ->toArray();

    }

    public function getStockByBin(int $warehouseId, string $bin): ?Stock
    {
        return Stock::query()
            ->where('warehouse_id', $warehouseId)
            ->where('storage_type', StorageArea::STORAGE_TYPE_PICKING)
            ->where('bin', $bin)
            ->first([
                'bin',
                'sku_code',
                'quantity',
                'frozen_quantity',
            ]);

    }

    public function getStocksBySkuCode(int $warehouseId, string $skuCode): array
    {
        return Stock::query()
            ->where('warehouse_id', $warehouseId)
            ->where('storage_type', StorageArea::STORAGE_TYPE_PICKING)
            ->where('sku_code', $skuCode)
            ->get([
                'bin',
                'sku_code',
                'quantity',
            ])
            ->toArray();

    }

    public function getStocksBySkuCodes(int $warehouseId, array $skuCodes): array
    {
        $list = [];
        foreach ($skuCodes as $skuCode) {
            $stocks = Stock::query()
                ->where('warehouse_id', $warehouseId)
                ->where('storage_type', StorageArea::STORAGE_TYPE_PICKING)
                ->where('sku_code', $skuCode)
                ->get([
                    'quantity',
                    'frozen_quantity',
                ]);
            $sumQuantity = 0;
            $sumFrozenQuantity = 0;
            foreach ($stocks as $stock) {
                $sumQuantity = $sumQuantity + $stock->quantity;
                $sumFrozenQuantity = $sumFrozenQuantity + $stock->frozen_quantity;
            }
            $list[] = [
                'sku_code' => $skuCode,
                'quantity' => $sumQuantity,
                'frozen_quantity' => $sumFrozenQuantity,
            ];

        }

        return $list;
    }

    public function getStocksBySkus(int $warehouseId, array $skus): array
    {
        $list = [];
        foreach ($skus as $sku) {
            $stocks = Stock::query()
                ->where('warehouse_id', $warehouseId)
                ->where('storage_type', StorageArea::STORAGE_TYPE_PICKING)
                ->where('sku_code', $sku['sku_code'])
                ->get([
                    'quantity',
                    'frozen_quantity',
                ]);
            $sumQuantity = 0;
            $sumFrozenQuantity = 0;
            foreach ($stocks as $stock) {
                $sumQuantity = $sumQuantity + $stock->quantity;
                $sumFrozenQuantity = $sumFrozenQuantity + $stock->frozen_quantity;
            }
            $list[] = [
                'sku_code' => $sku['sku_code'],
                'quantity' => $sumQuantity,
                'frozen_quantity' => $sumFrozenQuantity,
            ];

        }

        return $list;
    }

    public function getStocksByBinAndSkuCode(int $warehouseId, string $bin, string $skuCode): array
    {
        return Stock::query()
            ->where('warehouse_id', $warehouseId)
            ->where('storage_type', StorageArea::STORAGE_TYPE_PICKING)
            ->where('bin', $bin)
            ->where('sku_code', $skuCode)
            ->get([
                'bin',
                'sku_code',
                'quantity',
            ])
            ->toArray();
    }

    public function getDealWithLogsByPage(int $warehouseId, array $query, int $page, int $perPage): array
    {
        $stockDealWithLogQuery = StockDealWithLog::query()
            ->where('warehouse_id', $warehouseId);

        if (!empty($query['storage_location_code'])) {
            $stockDealWithLogQuery->where('storage_location_code', $query['storage_location_code']);
        }

        if (!empty($query['sku_code'])) {
            $stockDealWithLogQuery->where('sku_code', $query['sku_code']);
        }

        if (!empty($query['created_at_from'])) {
            $stockDealWithLogQuery->where('created_at', '>=', Carbon::parse($query['created_at_from'])->timestamp);
        }

        if (!empty($query['created_at_to'])) {
            $stockDealWithLogQuery->where('created_at', '<=', Carbon::parse($query['created_at_to'])->timestamp);
        }

        if (!empty($query['user_id'])) {
            $stockDealWithLogQuery->where('user_id', $query['user_id']);
        }

        if (!empty($query['abnormal_number'])) {
            $stockDealWithLogQuery->where('abnormal_number', $query['abnormal_number']);
        }

        $stockDealWithLogQuery->orderByDesc('created_at');

        $total = $stockDealWithLogQuery->count();
        $stockDealWithLogs = $stockDealWithLogQuery->forPage($page, $perPage)
            ->get([
                'storage_location_code',
                'sku_code',
                'created_at',
                'inventory_before',
                'inventory_after',
                'user_id',
                'remark',
                'abnormal_number',
            ])
            ->transform(function ($item) {
                return [
                    'storage_location_code' => $item->storage_location_code,
                    'sku_code' => $item->sku_code,
                    'created_at' => $item->created_at->format('Y-m-d H:i:s'),
                    'inventory_before' => $item->inventory_before,
                    'inventory_after' => $item->inventory_after,
                    'fixed_quantity' => $item->inventory_after - $item->inventory_before,
                    'operator' => User::query()->where('id', $item->user_id)->value('name') ?? '',
                    'remark' => $item->remark,
                    'abnormal_number' => $item->abnormal_number,
                ];
            })
            ->toArray();

        return [
            'list' => $stockDealWithLogs,
            'page_bean' => [
                'total' => $total,
                'page' => $page,
                'per_page' => $perPage,
            ],
        ];
    }

    public function getSkuStocksByPage(int $warehouseId, array $query, int $page, int $perPage): array
    {
        $stockQuery = Stock::query()
            ->where('warehouse_id', $warehouseId)
            ->whereIn('storage_type', [
                StorageArea::STORAGE_TYPE_RECEIVING,
                StorageArea::STORAGE_TYPE_PICKING,
                StorageArea::STORAGE_TYPE_PACKAGE,
            ]);

        if (!empty($query['sku_code'])) {
            $stockQuery->where('sku_code', $query['sku_code']);
        }

        if (!empty($query['bin'])) {
            $stockQuery->where('bin', $query['bin']);
        }

        if (!empty($query['storage_type'])) {
            $stockQuery->where('storage_type', $query['storage_type']);
        }

        $stockQuery->orderBy('sku_code');

        $total = $stockQuery->count();
        $stocks = $stockQuery->forPage($page, $perPage)
            ->get([
                'sku_code',
                'bin',
                'storage_type',
                'quantity',
                'frozen_quantity',
            ])
            ->transform(function ($item) {
                return [
                    'sku_code' => $item->sku_code,
                    'sku_name' => ProductSku::query()->where('sku_code', $item->sku_code)->value('sku_name') ?? '',
                    'bin' => $item->bin,
                    'storage_type' => $item->storage_type,
                    'quantity' => $item->quantity,
                    'frozen_quantity' => $item->frozen_quantity,
                ];
            })
            ->toArray();

        return [
            'list' => $stocks,
            'page_bean' => [
                'total' => $total,
                'page' => $page,
                'per_page' => $perPage,
            ],
        ];
    }

    public function createStockDealWithLog(
        int $warehouseId,
        int $userId,
        string $storageLocationCode,
        string $skuCode,
        int $inventoryBefore,
        int $inventoryAfter,
        string $abnormalNumber = '',
        string $remark
    ): ?StockDealWithLog
    {
        return StockDealWithLog::create([
            'warehouse_id' => $warehouseId,
            'storage_location_code' => $storageLocationCode,
            'sku_code' => $skuCode,
            'inventory_before' => $inventoryBefore,
            'inventory_after' => $inventoryAfter,
            'abnormal_number' => $abnormalNumber,
            'remark' => $remark,
            'user_id' => $userId,
        ]);
    }

    public function unFrozenStock(
        int $warehouseId,
        string $skuCode,
        string $storageLocationCode,
        int $cancelQuantity,
        int $userId = 0,
        string $relationType,
        int $relationId
    ): bool
    {
        // 最晚为未冻结订单
        $unFrozenOutboundItem = Outbound::query()
            ->leftJoin('outbound_items', 'outbounds.id', '=', 'outbound_items.outbound_id')
            ->leftJoin('outbound_frozen_items', 'outbound_items.id', '=', 'outbound_frozen_items.outbound_item_id')
            ->whereIn('outbounds.state', [Outbound::STATE_CREATED, Outbound::STATE_FROZEN])
            ->where('outbound_frozen_items.sku_code', $skuCode)
            ->where('outbound_frozen_items.frozen_bin', $storageLocationCode)
            ->whereNull('outbound_items.deleted_at')
            ->whereNull('outbounds.deleted_at')
            ->whereNull('outbound_frozen_items.deleted_at')
            ->orderByDesc('outbounds.created_at')
            ->first([
                'outbounds.warehouse_id',
                'outbound_items.id as outbound_item_id',
                'outbound_items.quantity',
                'outbounds.id as outbound_id',
                'outbounds.state as outbound_state',
            ]);
        if ($unFrozenOutboundItem) {
            if ($unFrozenOutboundItem->outbound_state == Outbound::STATE_FROZEN) {
                app(OutboundRepository::class)->updateState($unFrozenOutboundItem->outbound_id, Outbound::STATE_CREATED, $userId);
            }
            $sumFrozenQuantity = OutboundFrozenItem::query()
                ->where('outbound_item_id', $unFrozenOutboundItem->outbound_item_id)
                ->sum('frozen_quantity');
            if ($sumFrozenQuantity == 0) {
                return true;
            }
            $outboundFrozenItems = OutboundFrozenItem::query()
                ->where('outbound_item_id', $unFrozenOutboundItem->outbound_item_id)
                ->get();
            foreach ($outboundFrozenItems as $outboundFrozenItem) {
                $this->updateStock(
                    $warehouseId,
                    $userId,
                    StorageArea::STORAGE_TYPE_PICKING,
                    $relationType,
                    $relationId,
                    $skuCode,
                    $outboundFrozenItem->frozen_bin,
                    0,
                    -$outboundFrozenItem->frozen_quantity,
                    $outboundFrozenItem->frozen_quantity
                );
            }
            OutboundFrozenItem::query()
                ->where('outbound_item_id', $unFrozenOutboundItem->outbound_item_id)
                ->delete();
            OutboundItem::query()
                ->where('id', $unFrozenOutboundItem->outbound_item_id)
                ->update([
                    'is_frozen' => OutboundItem::IS_FROZEN_FALSE,
                ]);
            if ($sumFrozenQuantity < $cancelQuantity) {
                return self::unFrozenStock(
                    $warehouseId,
                    $skuCode,
                    $storageLocationCode,
                    $cancelQuantity - $sumFrozenQuantity,
                    $userId,
                    $relationType,
                    $relationId
                );
            }
            return true;
        }
        return false;
    }

    public function updateStock(
        int $warehouseId,
        int $userId,
        int $storageType,
        string $relationType,
        int $relationId,
        string $skuCode,
        string $bin = '',
        int $quantity = 0,
        $frozenQuantity = 0,
        $unFrozenQuantity = 0
    ): bool {
        if ($quantity == 0 && $frozenQuantity == 0 && $unFrozenQuantity == 0) {
            return true;
        }
        if (!in_array($storageType, [
            StorageArea::STORAGE_TYPE_RECEIVING,
            StorageArea::STORAGE_TYPE_PICKING,
            StorageArea::STORAGE_TYPE_PACKAGE,
            StorageArea::STORAGE_TYPE_OUTBOUND,
            StorageArea::STORAGE_TYPE_ABNORMAL,
            StorageArea::STORAGE_TYPE_DEFECTIVE,
        ])) {
            return false;
        }
        // 查找库位，如果不存在则创建
        if (empty($bin)) {
            $stock = Stock::query()
                ->where('warehouse_id', $warehouseId)
                ->where('storage_type', $storageType)
                ->where('sku_code', $skuCode)
                ->first();
            if (!$stock) {
                $stock = Stock::create([
                    'warehouse_id' => $warehouseId,
                    'storage_type' => $storageType,
                    'sku_code' => $skuCode,
                ]);
            }
        } else {
            $stock = Stock::query()
                ->where('warehouse_id', $warehouseId)
                ->where('storage_type', $storageType)
                ->where('bin', $bin)
                ->first();
            if (!$stock) {
                $stock = Stock::create([
                    'warehouse_id' => $warehouseId,
                    'storage_type' => $storageType,
                    'bin' => $bin,
                    'sku_code' => $skuCode,
                ]);
            } else {
                if ($stock->sku_code != $skuCode) {
                    if ($stock->quantity == 0) {
                        $stock->sku_code = $skuCode;
                        $stock->save();
                    } else {
                        $stock = Stock::create([
                            'warehouse_id' => $warehouseId,
                            'storage_type' => $storageType,
                            'bin' => $bin,
                            'sku_code' => $skuCode,
                        ]);
                    }
                }
            }
        }

        // 增加库存变更日志
        StockLog::create([
            'warehouse_id' => $warehouseId,
            'relation_type' => $relationType,
            'relation_id' => $relationId,
            'storage_type' => $storageType,
            'bin' => $bin,
            'sku_code' => $skuCode,
            'quantity' => $quantity,
            'frozen_quantity' => $frozenQuantity,
            'unfrozen_quantity' => $unFrozenQuantity,
            'user_id' => $userId,
        ]);

        // 更新库存
        $stock->quantity += $quantity;
        if ($stock->quantity < 0) {
            Log::error('stock update error, quantity < 0');
            $stock->quantity = 0;
        }
        $stock->frozen_quantity += $frozenQuantity;
        if ($stock->frozen_quantity < 0) {
            Log::error('stock update error, frozen_quantity < 0');
            $stock->frozen_quantity = 0;
        }
        $stock->unfrozen_quantity += $unFrozenQuantity;
        if ($stock->unfrozen_quantity < 0) {
            Log::error('stock update error, unfrozen_quantity < 0');
            $stock->unfrozen_quantity = 0;
        }
        if ($stock->quantity == 0) {
            if ($storageType == StorageArea::STORAGE_TYPE_PICKING) {
                StorageLocation::query()
                    ->where('storage_type', StorageArea::STORAGE_TYPE_PICKING)
                    ->where('storage_location_code', $stock->bin)
                    ->update([
                        'is_empty' => StorageLocation::IS_EMPTY_TRUE,
                    ]);
            }
        }
        return $stock->save();
    }
}
