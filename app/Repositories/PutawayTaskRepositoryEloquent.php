<?php

namespace App\Repositories;

use App\Consts\NumberPrefixConst;
use App\Models\Abnormal;
use App\Models\AbnormalItem;
use App\Models\ProductImage;
use App\Models\ProductSku;
use App\Models\ProductSkuImage;
use App\Models\PutawayLog;
use App\Models\PutawayTaskItem;
use App\Models\ReceivingCart;
use App\Models\ReceivingCartItem;
use App\Models\Stock;
use App\Models\StorageArea;
use App\Models\StorageLocation;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PutawayTaskRepository;
use App\Models\PutawayTask;
use App\Validators\PutawayTaskValidator;

/**
 * Class PutawayTaskRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class PutawayTaskRepositoryEloquent extends BaseRepository implements PutawayTaskRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PutawayTask::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getPutawayTaskByReceivingCartCode(int $warehouseId, string $receivingCartCode): ?PutawayTask
    {
        return PutawayTask::query()
            ->where('warehouse_id', $warehouseId)
            ->where('state', PutawayTask::STATE_CREATEED)
            ->where('receiving_cart_code', $receivingCartCode)
            ->first();
    }

    public function createPutawayTask(int $warehouseId, int $userId, string $receivingCartCode): ?PutawayTask
    {
        $receivingCart = app(ReceivingLogRepository::class)->getReceivingCart($receivingCartCode);
        // 车中的所有商品
        $receivingCartItems = ReceivingCartItem::query()
            ->where('warehouse_id', $warehouseId)
            ->where('receiving_cart_id', $receivingCart->id)
            ->groupBy('sku_code')
            ->get([
                'sku_code',
                DB::raw('sum(quantity) as quantity'),
            ])->toArray();

        // 生成推荐库位
        $suggestBins = $this->suggestBins($warehouseId, $receivingCartItems);
        if (empty($suggestBins)) {
            return null;
        }

        // 创建任务
        $putawayTask = $this->create([
            'warehouse_id' => $warehouseId,
            'task_number' => $this->generatePutawayTaskNumber(),
            'state' => PutawayTask::STATE_CREATEED,
            'user_id' => $userId,
            'receiving_cart_code' => $receivingCartCode,
        ]);


        $suggestBins = collect($suggestBins)->pluck('bin', 'sku_code')->all();
        $receivingCartItems = collect($receivingCartItems)->keyBy('sku_code')->all();

        // 遍历车中的商品，进行上架推荐
        foreach ($suggestBins as $skuCode => $suggestBin) {
            // 查找推荐货架
            PutawayTaskItem::create([
                'putaway_task_id' => $putawayTask->id,
                'sku_code' => $skuCode,
                'suggest_bin' => $suggestBin,
                'receiving_quantity' => $receivingCartItems[$skuCode]['quantity'],
            ]);
        };
        return $putawayTask;
    }

    private function generatePutawayTaskNumber(): string
    {
        $putawayTaskCount = PutawayTask::query()
            ->whereBetween('created_at', [
                Carbon::today()->startOfDay()->timestamp,
                Carbon::today()->endOfDay()->timestamp,
            ])
            ->count();
        $putawayTaskCount = $putawayTaskCount + 1;
        $number = NumberPrefixConst::PUTAWAY . date('Ymd') . sprintf('%05s', $putawayTaskCount);
        $exist = PutawayTask::query()
            ->where('task_number', $number)
            ->count();
        if ($exist) {
            return self::generatePutawayTaskNumber();
        }
        return $number;
    }

    public function getPutawayTaskItems(int $putawayTaskId, string $putawayTaskNumber): array
    {
        // 获取全部关联异常
        $abnormalItems = [];
        $abnormal = Abnormal::query()
            ->where('relation_type', Abnormal::RELATION_TYPE_PUTAWAY)
            ->where('relation_number', $putawayTaskNumber)
            ->first();
        if ($abnormal) {
            $abnormalItems = AbnormalItem::query()
                ->where('abnormal_id', $abnormal->id)
                ->get([
                    'sku_code',
                    'quantity',
                ])->toArray();
        }
        return PutawayTaskItem::query()
            ->where('putaway_task_id', $putawayTaskId)
            ->whereColumn('receiving_quantity', '<>', 'putaway_quantity')
            ->get([
                'sku_code as code',
                'suggest_bin',
                'receiving_quantity as quantity',
                'putaway_quantity',
            ])
            ->transform(function ($item) use ($abnormalItems) {
                $missingQuantity = 0;
                $abnormalItem = collect($abnormalItems)->where('sku_code', $item->code)->first();
                if ($abnormalItem) {
                    $missingQuantity = $abnormalItem['quantity'];
                }
                return [
                    'code' => $item->code,
                    'suggest_bin' => $item->suggest_bin,
                    'quantity' => $item->quantity,
                    'putaway_quantity' => $item->putaway_quantity,
                    'missing_quantity' => $missingQuantity,
                    'images' => $this->getSkuImages($item->code),
                ];
            })
            ->toArray();
    }

    private function getSkuImages(string $skuCode): array
    {
        $productSku = ProductSku::query()->where('sku_code', $skuCode)->first();
        $images = ProductSkuImage::query()
            ->leftJoin('resources', 'product_sku_images.resource_id', '=', 'resources.id')
            ->where('product_sku_images.product_sku_id', $productSku->id)
            ->get([
                'resources.url',
            ])
            ->toArray();
        if (empty($images)) {

            $images = ProductImage::query()
                ->leftJoin('resources', 'product_images.resource_id', '=', 'resources.id')
                ->where('product_images.product_id', $productSku->product_id)
                ->get([
                    'resources.url',
                ])
                ->toArray();
        }
        return $images;
    }

    public function createPutawayLog(
        int $warehouseId,
        int $userId,
        int $putawayTaskId,
        string $skuCode,
        int $quantity,
        string $bin
    ): PutawayLog {
        return PutawayLog::create([
            'warehouse_id' => $warehouseId,
            'putaway_task_id' => $putawayTaskId,
            'sku_code' => $skuCode,
            'bin' => $bin,
            'quantity' => $quantity,
            'user_id' => $userId,
        ]);
    }

    public function updatePutawayTaskItem(int $putawayTaskId, string $skuCode, string $bin, int $quantity)
    {
        return (bool) PutawayTaskItem::query()
            ->where('putaway_task_id', $putawayTaskId)
            ->where('sku_code', $skuCode)
            ->update([
                'putaway_bin' => $bin,
                'putaway_quantity' => DB::raw('putaway_quantity + ' . $quantity),
            ]);
    }

    public function decreaseReceivingCartItem(
        int $warehouseId,
        string $receivingCartCode,
        string $skuCode,
        int $quantity
    ): bool {
        $receivingCart = ReceivingCart::query()
            ->where('warehouse_id', $warehouseId)
            ->where('cart_code', $receivingCartCode)
            ->first();
        if (!$receivingCart) {
            return false;
        }
        $receivingCartItems = ReceivingCartItem::query()
            ->where('warehouse_id', $warehouseId)
            ->where('receiving_cart_id', $receivingCart->id)
            ->where('sku_code', $skuCode)
            ->get();
        if ($receivingCartItems->isEmpty()) {
            return false;
        }
        foreach ($receivingCartItems as $receivingCartItem) {
            if ($quantity == 0) {
                break;
            }
            if ($quantity < $receivingCartItem->quantity) {
                ReceivingCartItem::query()
                    ->where('id', $receivingCartItem->id)
                    ->decrement('quantity', $quantity);
                break;
            }
            if ($quantity >= $receivingCartItem->quantity) {
                ReceivingCartItem::query()
                    ->where('id', $receivingCartItem->id)
                    ->delete();
                $quantity -= $receivingCartItem->quantity;
                if ($quantity == 0) {
                    break;
                }
            }
        }
        return true;
    }

    public function checkReceivingCartState(int $warehouseId, string $receivingCartCode): bool
    {
        // 检查item
        $receivingCart = ReceivingCart::query()
            ->where('warehouse_id', $warehouseId)
            ->where('cart_code', $receivingCartCode)
            ->first();
        $receivingCartItemCount = ReceivingCartItem::query()
            ->where('receiving_cart_id', $receivingCart->id)
            ->count();
        if (!$receivingCartItemCount) {
            $receivingCart->update([
                'state' => ReceivingCart::STATE_FREE,
            ]);
        }
        return true;
    }

    public function getPutawayTask(int $putawayTaskId): ?PutawayTask
    {
        return PutawayTask::query()->find($putawayTaskId);
    }

    public function getPutawayTaskByNumber(string $taskNumber): ?PutawayTask
    {
        return PutawayTask::query()->where('task_number', $taskNumber)->first();
    }

    public function isPutawayTaskCompleted(int $putawayTaskId): bool
    {
        $putawayTask = PutawayTask::query()->find($putawayTaskId);
        // 检查购物车是否为空
        $receivingCartItemCount = ReceivingCart::query()
            ->leftJoin('receiving_cart_items', 'receiving_carts.id', '=', 'receiving_cart_items.receiving_cart_id')
            ->where('receiving_carts.cart_code', $putawayTask->receiving_cart_code)
            ->whereNotNull('receiving_cart_items.id')
            ->count();
        if (!$receivingCartItemCount) {
            return true;
        }
        return false;
    }

    public function getPutawayTaskItemBySkuCode(int $putawayTaskId, string $skuCode): ?PutawayTaskItem
    {
        return PutawayTaskItem::query()
            ->where('putaway_task_id', $putawayTaskId)
            ->where('sku_code', $skuCode)
            ->first();
    }

    /**
     * TODO 完善推荐算法
     * @param  int  $warehouseId
     * @param  array  $skus
     *
     * [
     *      [
     *          "sku_code" => "SKU0001",
     *      ],
     *      ...
     * ]
     * @param  string|null  $lastBin
     * @return array
     *
     * [
     *      [
     *          "sku_code" => "SKU0001",
     *          "bin" => "BIN0001",
     *      ],
     *      ...
     * ]
     */
    public function suggestBins(int $warehouseId, array $skus): array
    {
        $outSkus = [];
        $existSkuStocks = Stock::query()
            ->leftJoin('storage_locations', 'stocks.bin', '=', 'storage_locations.storage_location_code')
            ->where('stocks.warehouse_id', $warehouseId)
            ->where('stocks.storage_type', StorageArea::STORAGE_TYPE_PICKING)
            ->whereIn('stocks.sku_code', collect($skus)->pluck('sku_code')->toArray())
            ->where('stocks.quantity', '>=', 0)
            ->whereNotNull('storage_locations.id')
            ->orderBy('stocks.bin')
            ->get([
                'stocks.sku_code',
                'stocks.bin',
            ]);
        $existSkuStocks->each(function ($item) use (&$outSkus) {
            array_push($outSkus, [
                'sku_code' => $item->sku_code,
                'bin' => $item->bin,
            ]);
        });
        // 未分配的 sku
        $notInStockSkus = collect($skus)->whereNotIn('sku_code',
            $existSkuStocks->pluck('sku_code'))->values()->toArray();
        // 临时锁住的库位
        $tempBin = [];
        // 查找相同 SPU 靠前空库位
        foreach ($notInStockSkus as $notInStockSku) {
            $productSku = ProductSku::query()->where('sku_code', $notInStockSku['sku_code'])->first();
            $brotherSkus = ProductSku::query()
                ->where('product_id', $productSku->product_id)
                ->where('sku_code', '<>', $notInStockSku['sku_code'])
                ->pluck('sku_code')
                ->toArray();
            // 如果存在，查找最靠前不为空的库位
            $existBrotherSkuStock = Stock::query()
                ->where('warehouse_id', $warehouseId)
                ->where('storage_type', StorageArea::STORAGE_TYPE_PICKING)
                ->whereIn('sku_code', $brotherSkus)
                ->where('quantity', '>', 0)
                ->orderBy('bin')
                ->first([
                    'sku_code',
                    'bin',
                ]);
            if ($existBrotherSkuStock) {
                // 查找附近空库位
                // 向前查找
                $emptyStorageLocation = StorageLocation::query()
                    ->where('warehouse_id', $warehouseId)
                    ->where('storage_type', StorageArea::STORAGE_TYPE_PICKING)
                    ->where('is_empty', StorageLocation::IS_EMPTY_TRUE)
                    ->where('is_suggest_lock', StorageLocation::IS_SUGGEST_LOCK_FALSE)
                    ->where('storage_location_code', '<', $existBrotherSkuStock->bin)
                    ->whereNotIn('storage_location_code', $tempBin)
                    ->orderByDesc('storage_location_code')
                    ->first();
                if (!$emptyStorageLocation) {
                    // 先后查找
                    $emptyStorageLocation = StorageLocation::query()
                        ->where('warehouse_id', $warehouseId)
                        ->where('storage_type', StorageArea::STORAGE_TYPE_PICKING)
                        ->where('is_empty', StorageLocation::IS_EMPTY_TRUE)
                        ->where('is_suggest_lock', StorageLocation::IS_SUGGEST_LOCK_FALSE)
                        ->where('storage_location_code', '>', $existBrotherSkuStock->bin)
                        ->whereNotIn('storage_location_code', $tempBin)
                        ->orderBy('storage_location_code')
                        ->first();
                }
                if ($emptyStorageLocation) {
                    // 临时锁住
                    array_push($tempBin, $emptyStorageLocation->storage_location_code);
                    array_push($outSkus, [
                        'sku_code' => $notInStockSku['sku_code'],
                        'bin' => $emptyStorageLocation->storage_location_code,
                    ]);
                }
            }
        }

        // 未找到 SKU & SPU 库位，自动查找靠前未分配空库位
        $unSuggestSkus = collect($skus)->whereNotIn('sku_code', Arr::pluck($outSkus, 'sku_code'))->values();
        if ($unSuggestSkus->isNotEmpty()) {
            foreach ($unSuggestSkus as $unSuggestSku) {
                $emptyStorageLocation = StorageLocation::query()
                    ->where('warehouse_id', $warehouseId)
                    ->where('storage_type', StorageArea::STORAGE_TYPE_PICKING)
                    ->where('is_empty', StorageLocation::IS_EMPTY_TRUE)
                    ->where('is_suggest_lock', StorageLocation::IS_SUGGEST_LOCK_FALSE)
                    ->whereNotIn('storage_location_code', $tempBin)
                    ->orderBy('storage_location_code')
                    ->first();
                if (!$emptyStorageLocation) {
                    return [];
                }
                // 临时锁住
                array_push($tempBin, $emptyStorageLocation->storage_location_code);
                array_push($outSkus, [
                    'sku_code' => $unSuggestSku['sku_code'],
                    'bin' => $emptyStorageLocation->storage_location_code,
                ]);
            }
        }

        // 依据 bin 排序 & 锁住推荐库位
        StorageLocation::query()
            ->where('warehouse_id', $warehouseId)
            ->where('storage_type', StorageArea::STORAGE_TYPE_PICKING)
            ->where('is_suggest_lock', StorageLocation::IS_SUGGEST_LOCK_FALSE)
            ->whereIn('storage_location_code', Arr::pluck($outSkus, 'bin'))
            ->update(['is_suggest_lock' => StorageLocation::IS_SUGGEST_LOCK_TRUE]);

        return collect($outSkus)->sortBy('bin')->all();

    }

    public function unLockAndUnEmptyStorageLocationSuggest(int $warehouseId, string $bin): bool
    {
        return StorageLocation::query()
            ->where('warehouse_id', $warehouseId)
            ->where('storage_location_code', $bin)
            ->update([
                'is_suggest_lock' => StorageLocation::IS_SUGGEST_LOCK_FALSE,
                'is_empty' => StorageLocation::IS_EMPTY_FALSE,
            ]);
    }

    public function isReceivingCartEmpty(int $warehouseId, string $receivingCartCode): bool
    {
        $receivingCart = ReceivingCart::query()
            ->where('warehouse_id', $warehouseId)
            ->where('cart_code', $receivingCartCode)
            ->first();
        if (!$receivingCart) {
            return true;
        }
        $count = ReceivingCartItem::query()
            ->where('warehouse_id', $warehouseId)
            ->where('receiving_cart_id', $receivingCart->id)
            ->count();
        if (!$count) {
            return true;
        }
        return false;
    }

    public function updatePutawayTaskState(int $putawayTaskId, int $state): bool
    {
        return (bool) PutawayTask::query()
            ->where('id', $putawayTaskId)
            ->update([
                'state' => $state,
            ]);

    }

    public function releaseReceivingCart(string $receivingCartCode): bool
    {
        return (bool) ReceivingCart::query()
            ->where('cart_code', $receivingCartCode)
            ->update([
                'state' => ReceivingCart::STATE_FREE,
            ]);

    }

}
