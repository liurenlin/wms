<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ResourceRepository;
use App\Models\Resource;
use App\Validators\ResourceValidator;

/**
 * Class ResourceRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ResourceRepositoryEloquent extends BaseRepository implements ResourceRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Resource::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function createResource(string $url, string $resourceName = '', string $extroInfo = ''): ?Resource
    {
        return Resource::create([
            'resource_name' => $resourceName,
            'url' => $url,
            'extro_info' => $extroInfo,
        ]);
    }

}
