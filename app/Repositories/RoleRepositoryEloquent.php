<?php

namespace App\Repositories;

use App\Models\PermissionGroup;
use App\Models\RolePermission;
use App\Models\RolePermissionGroup;
use App\Models\UserRole;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\RoleRepository;
use App\Models\Role;
use App\Validators\RoleValidator;

/**
 * Class RoleRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class RoleRepositoryEloquent extends BaseRepository implements RoleRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Role::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getRoleList(): array
    {
        return Role::query()
            ->with('permissionGroups')
            ->get()
            ->transform(function ($role) {
                return [
                    'id' => $role->id,
                    'name' => $role->role_name,
                    'permission_groups' => $role->permissionGroups()->get([
                        'permission_groups.id',
                        'permission_groups.group_name as name',
                    ])->toArray(),
                ];
            })->toArray();
    }

    /**
     * @param  array  $permissionGroups
     * [
     *      [
     *          "id": 1,
     *      ]
     * ]
     * @return bool
     */
    public function checkPermissionGroups(array $permissionGroups): bool
    {
        $count = count($permissionGroups);
        $permissionGroupCount = PermissionGroup::query()
            ->whereIn('id', collect($permissionGroups)->pluck('id')->toArray())
            ->count();
        if ($count != $permissionGroupCount) {
            return false;
        }
        return true;

    }

    /**
     * @param  int  $roleId
     * @param  array  $permissionGroups
     * [
     *      [
     *          "id": 1,
     *      ]
     * ]
     * @return bool
     */
    public function updateRolePermissionGroups(int $roleId, array $permissionGroups): bool
    {
        DB::beginTransaction();
        try {
            // 删除
            RolePermissionGroup::query()
                ->where('role_id', $roleId)
                ->delete();
            // 添加
            $time = time();
            $rolePermissionGroups = collect($permissionGroups)->transform(function ($item) use ($roleId, $time) {
                return [
                    'role_id' => $roleId,
                    'permission_group_id' => $item['id'],
                    'created_at' => $time,
                    'updated_at' => $time,
                ];
            })->toArray();
            RolePermissionGroup::query()->insert($rolePermissionGroups);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('updateRolePermissionGroups Fail', [
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
            return false;
        }
        return true;
    }

    public function createRole(string $roleName): bool
    {
        if (Role::query()->where('role_name', $roleName)->exists()) {
            return false;
        }
        Role::create([
            'role_name' => $roleName,
        ]);
        return true;
    }

    public function deleteRole(int $roleId): bool
    {
        // 删除角
        Role::query()
            ->where('id', $roleId)
            ->delete();
        // 删除角色用户关联
        UserRole::query()
            ->where('role_id', $roleId)
            ->delete();
        // 删除角色权限关联
        RolePermissionGroup::query()
            ->where('role_id', $roleId)
            ->delete();
        RolePermission::query()
            ->where('role_id', $roleId)
            ->delete();
        return true;
    }
}
