<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PreReceivingRepository.
 *
 * @package namespace App\Repositories;
 */
interface PreReceivingRepository extends RepositoryInterface
{
    //
}
