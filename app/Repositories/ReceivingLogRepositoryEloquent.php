<?php

namespace App\Repositories;

use App\Consts\NumberPrefixConst;
use App\Models\ActualReceiving;
use App\Models\ActualReceivingItem;
use App\Models\PreReceiving;
use App\Models\PreReceivingItem;
use App\Models\PutawayTask;
use App\Models\ReceivingCart;
use App\Models\ReceivingCartItem;
use App\Models\ReceivingLog;
use App\Models\SkuCodePrintLog;
use Carbon\Carbon;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Validators\ReceivingLogValidator;

/**
 * Class ReceivingLogRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ReceivingLogRepositoryEloquent extends BaseRepository implements ReceivingLogRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ReceivingLog::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getActualReceivingId(string $preReceivingNumber): int
    {
        $preReceiving = PreReceiving::query()
            ->where('pre_receiving_number', $preReceivingNumber)
            ->first();
        if (!$preReceiving) {
            return 0;
        }
        $actualReceiving = ActualReceiving::query()
            ->where('pre_receiving_id', $preReceiving->id)
            ->where('state', ActualReceiving::STATE_ING)
            ->first();
        if (!$actualReceiving) {
            return 0;
        }
        return $actualReceiving->id;
    }

    public function getActualReceivingByPreReceivingNumber(string $preReceivingNumber): ?ActualReceiving
    {
        $preReceiving = PreReceiving::query()
            ->where('pre_receiving_number', $preReceivingNumber)
            ->first();
        if (!$preReceiving) {
            return null;
        }
        return ActualReceiving::query()
            ->where('pre_receiving_id', $preReceiving->id)
            ->where('state', ActualReceiving::STATE_ING)
            ->first();
    }

    public function getActualReceivingById(int $actualReceivingId): ?ActualReceiving
    {
        return ActualReceiving::query()->find($actualReceivingId);
    }

    public function getActualReceivingByNumber(string $actualReceivingNumber): ?ActualReceiving
    {
        return ActualReceiving::query()->where('actual_receiving_number', $actualReceivingNumber)->first();
    }

    public function createActualReceiving(int $userId, string $preReceivingNumber): ?ActualReceiving
    {
        $preReceiving = PreReceiving::query()->where('pre_receiving_number', $preReceivingNumber)->first();
        if (!$preReceiving) {
            return null;
        }
        return ActualReceiving::create([
            'warehouse_id' => $preReceiving->warehouse_id,
            'pre_receiving_id' => $preReceiving->id,
            'actual_receiving_number' => self::generateActualReceivingNumber(),
            'state' => ActualReceiving::STATE_ING,
            'user_id' => $userId,
        ]);
    }

    private function generateActualReceivingNumber(): string
    {
        $actualReceivingCount = ActualReceiving::query()
            ->whereBetween('created_at', [
                Carbon::today()->startOfDay()->timestamp,
                Carbon::today()->endOfDay()->timestamp,
            ])
            ->count();
        $actualReceivingCount = $actualReceivingCount + 1;
        $number = NumberPrefixConst::ACTUAL_RECEIVING . date('Ymd') . sprintf('%05s', $actualReceivingCount);
        $exist = ActualReceiving::query()
            ->where('actual_receiving_number', $number)
            ->count();
        if ($exist) {
            self::generateActualReceivingNumber();
        }
        return $number;
    }

    public function createReceivingLog(int $userId, int $warehouseId, array $params, int $receivingCartItemId): ReceivingLog
    {
        return ReceivingLog::create([
            'warehouse_id' => $warehouseId,
            'pre_receiving_id' => $params['pre_receiving_id'],
            'actual_receiving_id' => $params['actual_receiving_id'],
            'receiving_cart_code' => $params['receiving_cart_code'],
            'sku_code' => $params['sku_code'],
            'quantity' => $params['quantity'],
            'user_id' => $userId,
            'receiving_cart_item_id' => $receivingCartItemId,
        ]);
    }

    public function createOrUpdateActualReceivingItem(array $params): bool
    {
        // 查找对应的实际入库单
        $actualReceiving = ActualReceiving::query()
            ->where('id', $params['actual_receiving_id'])
            ->first();
        if (!$actualReceiving) {
            throw new \Exception('Actual Receiving Not Found');
        }

        $actualReceivingItem = ActualReceivingItem::query()
            ->where('actual_receiving_id', $actualReceiving->id)
            ->where('sku_code', $params['sku_code'])
            ->first();
        if ($actualReceivingItem) {
            $actualReceivingItem->receiving_quantity = $actualReceivingItem->receiving_quantity + $params['quantity'];
            return $actualReceivingItem->save();

        }
        return (bool) ActualReceivingItem::create([
            'actual_receiving_id' => $actualReceiving->id,
            'sku_code' => $params['sku_code'],
            'receiving_quantity' => $params['quantity'],
        ]);
    }

    public function createSkuCodePrintLog(int $userId, int $warehouseId, int $preReceivingId, string $skuCode, int $quantity): bool
    {
        return (bool) SkuCodePrintLog::create([
            'user_id' => $userId,
            'warehouse_id' => $warehouseId,
            'pre_receiving_id' => $preReceivingId,
            'sku_code' => $skuCode,
            'quantity' => $quantity,
        ]);
    }

    public function isPreReceivingFinished(string $preReceivingNumber): bool
    {
        return (bool) PreReceiving::query()
            ->where('pre_receiving_number', $preReceivingNumber)
            ->where('state', PreReceiving::STATE_FINISHED)
            ->count();
    }

    public function isReceivingCartFree(string $receivingCartCode): bool
    {
        return (bool) ReceivingCart::query()
            ->where('cart_code', $receivingCartCode)
            ->where('state', ReceivingCart::STATE_FREE)
            ->count();
    }

    public function getReceivingCart(string $receivingCartCode): ReceivingCart
    {
        return ReceivingCart::query()->where('cart_code', $receivingCartCode)->first();
    }

    public function getLatestPutawayTask(string $receivingCartCode): ?PutawayTask
    {
        return PutawayTask::query()
            ->where('receiving_cart_code', $receivingCartCode)
            ->where('state', PutawayTask::STATE_CREATEED)
            ->first();
    }

    public function createReceivingCartItem(int $warehouseId, string $receivingCartCode, string $skuCode, int $quantity): ReceivingCartItem
    {
        // 查找收货车
        $receivingCart =  $this->getReceivingCart($receivingCartCode);
        return ReceivingCartItem::create([
            'warehouse_id' => $warehouseId,
            'receiving_cart_id' => $receivingCart->id,
            'sku_code' => $skuCode,
            'quantity' => $quantity,
        ]);
    }

    public function updateReceivingCartState(string $receivingCartCode, int $state): bool
    {
        return (bool) ReceivingCart::query()
            ->where('cart_code', $receivingCartCode)
            ->update(['state' => $state]);
    }

    public function updateActualReceivingState(int $actualReceivingId, int $state): bool
    {
        $updateData = [
            'state' => $state,
        ];
        if ($state == ActualReceiving::STATE_COMPLETED) {
            $updateData['finished_at'] = time();
        }
        return (bool) ActualReceiving::query()
            ->where('id', $actualReceivingId)
            ->update($updateData);
    }

    public function completedPreReceiving(int $preReceivingId): bool
    {
        $prePreceivingItems = PreReceivingItem::query()
            ->where('pre_receiving_id', $preReceivingId)
            ->get();

        $actualReceivingIds = ActualReceiving::query()
            ->where('pre_receiving_id', $preReceivingId)
            ->pluck('id')
            ->toArray();

        foreach ($prePreceivingItems as $preceivingItem) {
            $actualReceivingItemQuantitySum = ActualReceivingItem::query()
                ->whereIn('actual_receiving_id', $actualReceivingIds)
                ->where('sku_code', $preceivingItem->sku_code)
                ->sum('receiving_quantity');
            if ($preceivingItem->quantity > $actualReceivingItemQuantitySum) {
                return false;
            }
        }
        // 完成
        PreReceiving::query()
            ->where('id', $preReceivingId)
            ->update([
                'state' => PreReceiving::STATE_FINISHED,
            ]);
        return true;
    }

    public function getPreReceivingItem(string $preReceivingNumber, string $skuCode): PreReceivingItem
    {
        $preReceiving = PreReceiving::query()
            ->where('pre_receiving_number', $preReceivingNumber)
            ->first();
        return PreReceivingItem::query()
            ->where('pre_receiving_id', $preReceiving->id)
            ->where('sku_code', $skuCode)
            ->first();
    }

    public function getLatestLog(int $warehouseId, string $receivingCartCode, string $skuCode): ?ReceivingLog
    {
        return ReceivingLog::query()
            ->where('warehouse_id', $warehouseId)
            ->where('receiving_cart_code', $receivingCartCode)
            ->where('sku_code', $skuCode)
            ->first();
    }

    public function getPreReceivingById(int $preReceivingId): ?PreReceiving
    {
        return PreReceiving::query()->find($preReceivingId);
    }

    public function getSkuTotalReceivingQuantity(int $preReceivingId, string $skuCode): int
    {
        $actualReceivingIds = ActualReceiving::query()
            ->where('pre_receiving_id', $preReceivingId)
            ->pluck('id')
            ->toArray();
        return (int) ActualReceivingItem::query()
            ->whereIn('actual_receiving_id', $actualReceivingIds)
            ->where('sku_code', $skuCode)
            ->sum('receiving_quantity');
    }

}
