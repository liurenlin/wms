<?php

namespace App\Repositories;

use App\Consts\CommonConst;
use App\Consts\NumberPrefixConst;
use App\Models\Abnormal;
use App\Models\AbnormalItem;
use App\Models\Basket;
use App\Models\OpenMessageQueue;
use App\Models\Outbound;
use App\Models\OutboundItem;
use App\Models\OutboundProcess;
use App\Models\PickingLog;
use App\Models\PickingTask;
use App\Models\PickingTaskBasket;
use App\Models\PickingTaskItem;
use App\Models\ProductSku;
use App\Models\StockLog;
use App\Models\StorageArea;
use App\Services\MessageService;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PickingTaskRepository;
use App\Validators\PickingValidator;

/**
 * Class PickingRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class PickingTaskRepositoryEloquent extends BaseRepository implements PickingTaskRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PickingTask::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @param  array  $baskets
     * [
     *      [
     *          "code": "BASKET0001",
     *      ],
     *      ...
     * ]
     * @return array
     */
    public function checkBaskets(int $warehouseId, array $baskets): array
    {
        $errorBaskets = [];
        foreach ($baskets as $basket) {
            $exsit = Basket::query()
                ->where('warehouse_id', $warehouseId)
                ->where('basket_code', $basket['code'])
                ->where('state', Basket::STATE_FREE)
                ->count();
            if (!$exsit) {
                array_push($errorBaskets, $basket['code']);
            }
        }
        return $errorBaskets;
    }

    /**
     * @param  array  $baskets
     * [
     *      [
     *          "code": "BASKET0001",
     *      ],
     *      ...
     * ]
     * @return array
     */
    public function checkBasketsExist(int $warehouseId, array $baskets): array
    {
        $errorBaskets = [];
        foreach ($baskets as $basket) {
            $exsit = Basket::query()
                ->where('warehouse_id', $warehouseId)
                ->where('basket_code', $basket['code'])
                ->exists();
            if (!$exsit) {
                array_push($errorBaskets, $basket['code']);
            }
        }
        return $errorBaskets;
    }

    /**
     * @param  int  $warehouseId
     * @param  int  $userId
     * @param  array  $baskets
     * [
     *      [
     *          "code": "BASKET0001",
     *      ],
     *      ...
     * ]
     * @param  array  $suggestPickingTaskItems
     * [
     *      [
     *          "outbound_id" => 1,
     *          "items" => [
     *              [
     *                  "sku_code" => "SKU0001",
     *                  "quantity" => 1,
     *                  "bin" => "BIN0001",
     *              ],
     *              ...
     *          ]
     *      ],
     *      ...
     * ]
     * @return array
     * @throws \Exception
     */
    public function createPickingTask(int $warehouseId, int $userId, array $baskets, array $suggestPickingTaskItems): array
    {
        // 创建任务记录
        $pickingTask = PickingTask::create([
            'warehouse_id' => $warehouseId,
            'user_id' => $userId,
            'task_number' => $this->generatePickingNumber(),
            'task_type' => PickingTask::TASK_TYPE_PDA,
            'state' => PickingTask::STATE_CREATED,
        ]);
        $taskItems = [];
        $baskets = collect($baskets);
        foreach ($suggestPickingTaskItems as $suggestPickingTaskItem) {
            $tempBasket = $baskets->shift();
            $pickingTaskBasket = PickingTaskBasket::create([
                'picking_task_id' => $pickingTask->id,
                'outbound_number' => $suggestPickingTaskItem['outbound_number'],
                'basket_code' => $tempBasket['code'],
            ]);
            // 篮子占用
            Basket::query()
                ->where('basket_code', $tempBasket['code'])
                ->update(['state' => Basket::STATE_BUSY]);
            // 出库单状态更新
            Outbound::query()
                ->where('id', $suggestPickingTaskItem['outbound_id'])
                ->update(['state' => Outbound::STATE_PICKING_ING]);
            // 补充拣货任务 item
            foreach ($suggestPickingTaskItem['items'] as $item) {
                $pickingTaskItem = PickingTaskItem::create([
                    'picking_task_basket_id' => $pickingTaskBasket->id,
                    'sku_code' => $item['sku_code'],
                    'suggest_bin' => $item['bin'],
                    'quantity' => $item['quantity'],
                ]);
                array_push($taskItems, [
                    'suggest_bin' => $item['bin'],
                    'sku' => [
                        'code' => $item['sku_code'],
                    ],
                    'basket' => [
                        'code' => $tempBasket['code'],
                    ],
                    'quantity' => $item['quantity'],
                    'id' => $pickingTaskItem->id,
                ]);
            }
            // 补充 outbound_processes
            OutboundProcess::create([
                'outbound_id' => $suggestPickingTaskItem['outbound_id'],
                'state_before' => Outbound::STATE_FROZEN,
                'state_after' => Outbound::STATE_PICKING_ING,
                'user_id' => $userId,
                'content' => 'Picking task ' . $pickingTask->picking_number,
            ]);
        }
        return [
            'id' => $pickingTask->id,
            'task_number' => $pickingTask->task_number,
            'task_items' => $taskItems,
        ];

    }

    private function generatePickingNumber(): string
    {
        $pickingTaskCount = PickingTask::query()
            ->whereBetween('created_at', [
                Carbon::today()->startOfDay()->timestamp,
                Carbon::today()->endOfDay()->timestamp,
            ])
            ->count();
        $pickingTaskCount = $pickingTaskCount + 1;
        $number = NumberPrefixConst::PICKING . date('Ymd') . sprintf('%05s', $pickingTaskCount);
        $exist = PickingTask::query()
            ->where('task_number', $number)
            ->count();
        if ($exist) {
            return self::generatePickingNumber();
        }
        return $number;
    }

    /**
     * @param  int  $warehouseId
     * @param  int  $itemCount
     * @return array
     * [
     *      [
     *          "outbound_id" => 1,
     *          "items" => [
     *              [
     *                  "sku_code" => "SKU0001",
     *                  "quantity" => 1,
     *                  "bin" => "BIN0001",
     *              ],
     *              ...
     *          ]
     *      ],
     *      ...
     * ]
     */
    public function suggestPickingTaskItems(int $warehouseId, int $itemCount): array
    {
        $outbounds = Outbound::query()
            ->where('warehouse_id', $warehouseId)
            ->where('is_need_intercept', 0)
            ->where('state', Outbound::STATE_FROZEN)
            ->orderBy('created_at')
            ->limit($itemCount)
            ->get();
        // 检查数量 超过 80 分批
        $totalPickingItemCount = 0;
        $pickingItems = [];
        foreach ($outbounds as $outbound) {
            $tempItems = [];
            // items
            foreach ($outbound->items as $item) {
                foreach ($item->frozenItems as $frozenItem) {
                    array_push($tempItems, [
                        'sku_code' => $frozenItem->sku_code,
                        'quantity' => $frozenItem->frozen_quantity,
                        'bin' => $frozenItem->frozen_bin,
                    ]);
                }
                $totalPickingItemCount += $item->quantity;
            }
            array_push($pickingItems, [
                'outbound_id' => $outbound->id,
                'outbound_number' => $outbound->outbound_number,
                'items' => $tempItems,
            ]);

            if ($totalPickingItemCount >= CommonConst::PICKING_TASK_SKU_MAX_QUANTITY) {
                break;
            }
        }
        return $pickingItems;
    }

    public function checkPickingLogParams(
        int $warehouseId,
        int $userId,
        int $pickingTaskItemId,
        string $skuCode,
        int $quantity
    ): bool
    {
        $pickingTaskItem = PickingTaskItem::query()
            ->where('id', $pickingTaskItemId)
            ->where('sku_code', $skuCode)
            ->first();
        if (!$pickingTaskItem) {
            return false;
        }
        if ($pickingTaskItem->picking_quantity + $quantity > $pickingTaskItem->quantity) {
            return false;
        }

        $pickingTaskBasket = PickingTaskBasket::query()->find($pickingTaskItem->picking_task_basket_id);
        if (!$pickingTaskBasket) {
            return false;
        }

        $pickingTask = PickingTask::query()
            ->where('id', $pickingTaskBasket->picking_task_id)
            ->where('warehouse_id', $warehouseId)
            ->where('user_id', $userId)
            ->whereIn('state', [PickingTask::STATE_CREATED, PickingTask::STATE_IN_PROGRESS])
            ->where('task_type', PickingTask::TASK_TYPE_PDA)
            ->first();
        if (!$pickingTask) {
            return false;
        }
        return true;
    }

    public function createPickingLog(
        int $warehouseId,
        int $userId,
        int $pickingTaskItemId,
        string $skuCode,
        int $quantity,
        string $bin
    ): PickingLog
    {
        $pickingLog = PickingLog::create([
            'warehouse_id' => $warehouseId,
            'picking_task_item_id' => $pickingTaskItemId,
            'sku_code' => $skuCode,
            'bin' => $bin,
            'quantity' => $quantity,
            'user_id' => $userId,
        ]);
        return $pickingLog;
    }

    public function getUserUnFinishedPickingTasks(int $warehouseId, int $userId): array
    {
        return PickingTask::query()
            ->where('warehouse_id', $warehouseId)
            ->where('user_id', $userId)
            ->whereIn('state', [PickingTask::STATE_CREATED, PickingTask::STATE_IN_PROGRESS])
            ->orderBy('created_at')
            ->limit(20)
            ->get()->transform(function ($item) {
                $outboundNumbers = PickingTaskBasket::query()
                    ->where('picking_task_id', $item->id)
                    ->pluck('outbound_number')
                    ->toArray();
                $outbounds = Outbound::query()
                    ->whereIn('outbound_number', $outboundNumbers)
                    ->get()->transform(function ($outbound) {
                        return [
                            'id' => $outbound->id,
                            'sku_quantity' => (int) OutboundItem::query()
                                ->where('outbound_id', $outbound->id)
                                ->sum('quantity'),
                        ];
                    })->toArray();
                return [
                    'id' => $item->id,
                    'task_number' => $item->task_number,
                    'has_bind_basket' => (bool) PickingTaskBasket::query()
                        ->where('picking_task_id', $item->id)
                        ->where('basket_code', '<>', '')
                        ->count(),
                    'created_at' => $item->created_at->format('Y-m-d H:i:s'),
                    'outbounds' => $outbounds,
                ];
            })->toArray();
    }

    public function getPickingTaskBaskets(int $pickingTaskId): array
    {
        return PickingTaskBasket::query()
            ->where('picking_task_id', $pickingTaskId)
            ->get()
            ->toArray();
    }

    /**
     * @param  int  $pickingTaskId
     * @param  array  $baskets
     * [
     *      [
     *          "code": "BASKET0001",
     *          "outbound_id": 1
     *      ]
     * ]
     * @return array
     */
    public function boundBaskets(array $baskets): array
    {
        $taskItems = [];
        foreach ($baskets as $basket) {
            $pickingTaskBasket = PickingTaskBasket::query()
                ->where('basket_code', '')
                ->where('outbound_number', Outbound::query()->find($basket['outbound_id'])->outbound_number)
                ->first();
            $pickingTaskBasket->basket_code = $basket['code'];
            $pickingTaskBasket->save();
            // 篮子占用
            Basket::query()
                ->where('basket_code', $basket['code'])
                ->update(['state' => Basket::STATE_BUSY]);
            $pickingTaskItems = PickingTaskItem::query()
                ->where('picking_task_basket_id', $pickingTaskBasket->id)
                ->get([
                    'id',
                    'sku_code',
                    'quantity',
                    'suggest_bin',
                ])
                ->toArray();
            foreach ($pickingTaskItems as $pickingTaskItem) {
                array_push($taskItems, [
                    'suggest_bin' => $pickingTaskItem['suggest_bin'],
                    'sku' => [
                        'code' => $pickingTaskItem['sku_code'],
                    ],
                    'basket' => [
                        'code' => $basket['code'],
                    ],
                    'quantity' => $pickingTaskItem['quantity'],
                    'id' => $pickingTaskItem['id'],
                ]);
            }
        }
        return $taskItems;
    }

    public function getPickingTaskItems(int $pickingTaskId, string $pickingTaskNumber): array
    {
        $taskItems = [];
        $pickingTaskBaskets = PickingTaskBasket::query()
            ->where('picking_task_id', $pickingTaskId)
            ->get();
        foreach ($pickingTaskBaskets as $pickingTaskBasket) {
            $pickingTaskItems = PickingTaskItem::query()
                ->where('picking_task_basket_id', $pickingTaskBasket->id)
                ->get([
                    'id',
                    'sku_code',
                    'quantity',
                    'picking_quantity',
                    'suggest_bin',
                ]);
            // 获取全部关联异常
            $abnormalItems = [];
            $abnormal = Abnormal::query()
                ->where('relation_type', Abnormal::RELATION_TYPE_PICKING)
                ->where('relation_number', $pickingTaskNumber)
                ->where('basket_code', $pickingTaskBasket->basket_code)
                ->first();
            if ($abnormal) {
                $abnormalItems = AbnormalItem::query()
                    ->where('abnormal_id', $abnormal->id)
                    ->get([
                        'sku_code',
                        'missing_quantity',
                    ])->toArray();
            }
            foreach ($pickingTaskItems as $pickingTaskItem) {
                $missingQuantity = 0;
                $abnormalItem = collect($abnormalItems)->where('sku_code', $pickingTaskItem->sku_code)->first();
                if ($abnormalItem) {
                    $missingQuantity = $abnormalItem['missing_quantity'];
                }
                array_push($taskItems, [
                    'suggest_bin' => $pickingTaskItem->suggest_bin,
                    'sku' => [
                        'code' => $pickingTaskItem->sku_code,
                    ],
                    'basket' => [
                        'code' => $pickingTaskBasket->basket_code,
                    ],
                    'quantity' => $pickingTaskItem->quantity,
                    'picking_quantity' => $pickingTaskItem->picking_quantity,
                    'missing_quantity' => $missingQuantity,
                    'id' => $pickingTaskItem->id,
                ]);
            }
        }
        return $taskItems;

    }

    public function getUserPickingTask(int $pickingTaskId, int $warehouseId, int $userId): ?PickingTask
    {
        return PickingTask::query()
            ->where('id', $pickingTaskId)
            ->where('warehouse_id', $warehouseId)
            ->where('user_id', $userId)
            ->first();
    }

    public function getListByPage(array $querys, int $page = 1, int $perPage = 1): array
    {
        $pickingTaskQuery = PickingTask::query();
        if (!empty($querys['task_number'])) {
            $pickingTaskQuery->where('task_number', $querys['task_number']);
        }

        if (!empty($querys['created_at_from'])) {
            $pickingTaskQuery->where('created_at', '>=', Carbon::parse($querys['created_at_from'])->timestamp);
        }

        if (!empty($querys['created_at_to'])) {
            $pickingTaskQuery->where('created_at', '<=', Carbon::parse($querys['created_at_to'])->timestamp);
        }

        if (!empty($querys['finished_at_from'])) {
            $pickingTaskQuery->where('finished_at', '>=', Carbon::parse($querys['finished_at_from'])->timestamp);
        }

        if (!empty($querys['finished_at_to'])) {
            $pickingTaskQuery->where('finished_at', '<=', Carbon::parse($querys['finished_at_to'])->timestamp);
        }

        if (!empty($querys['user_id'])) {
            $pickingTaskQuery->where('user_id', $querys['user_id']);
        }

        if (!empty($querys['state'])) {
            $pickingTaskQuery->whereIn('state', explode(',', $querys['state']));
        }

        if (!empty($querys['task_type'])) {
            $pickingTaskQuery->where('task_type', $querys['task_type']);
        }

        $pickingTaskQuery->orderByDesc('created_at');

        $total = $pickingTaskQuery->count();
        $pickingTasks = $pickingTaskQuery->forPage($page, $perPage)
            ->get([
                'id',
                'task_number',
                'created_at',
                'finished_at',
                'user_id',
                'state',
                'task_type',
            ])
            ->transform(function ($item) {
                return [
                    'id' => $item->id,
                    'task_number' => $item->task_number,
                    'created_at' => $item->created_at->format('Y-m-d H:i:s'),
                    'finished_at' => is_null($item->finished_at) || !$item->finished_at ? '' : $item->finished_at->format('Y-m-d H:i:s'),
                    'operator' => User::query()->where('id', $item->user_id)->value('name') ?? '',
                    'state' => $item->state,
                    'task_type' => $item->task_type,
                ];
            })
            ->toArray();

        return [
            'picking_tasks' => $pickingTasks,
            'page_bean' => [
                'total' => $total,
                'page' => $page,
                'per_page' => $perPage,
            ],
        ];
    }

    public function existsPickingTask(int $pickingTaskId): bool
    {
        return (bool) PickingTask::query()->where('id', $pickingTaskId)->count();
    }

    public function getDetail(int $pickingTaskId): array
    {
        $pickingTask = PickingTask::query()->where('id', $pickingTaskId)->first();
        if (!$pickingTask) {
            return [];
        }
        // 补充 items
        $pickingTaskBaskets = PickingTaskBasket::query()
            ->where('picking_task_id', $pickingTask->id)
            ->get([
                'outbound_number',
                'basket_code',
            ])
            ->transform(function ($item) {
                return [
                    'outbound' => Outbound::query()
                        ->where('outbound_number', $item->outbound_number)
                        ->first(['id', 'outbound_number'])
                        ->toArray(),
                    'picking_basket_code' => $item->basket_code,
                ];
            })->toArray();

        // 补充关联异常
        $abnormals = Abnormal::query()
            ->where('relation_type', Abnormal::RELATION_TYPE_PICKING)
            ->where('relation_number', $pickingTask->task_number)
            ->get()
            ->transform(function ($item) {
                return [
                    'id' => $item->id,
                    'abnormal_number' => $item->abnormal_number,
                    'created_at' => $item->created_at->format('Y-m-d H:i:s'),
                    'state' => $item->state,
                    'type' => $item->reason,
                    'operator' => User::query()->where('id', $item->report_user_id)->value('name') ?? '',
                ];
            })
            ->toArray();
        return [
            'id' => $pickingTask->id,
            'task_number' => $pickingTask->task_number,
            'state' => $pickingTask->state,
            'task_type' => $pickingTask->task_type,
            'created_at' => $pickingTask->created_at->format('Y-m-d H:i:s'),
            'finished_at' => is_null($pickingTask->finished_at) || !$pickingTask->finished_at ? '' : $pickingTask->finished_at->format('Y-m-d H:i:s'),
            'operator' => User::query()->where('id', $pickingTask->user_id)->value('name') ?? '',
            'items' => $pickingTaskBaskets,
            'abnormals' => $abnormals,
        ];
    }

    public function getPickingTaskByNumber(string $taskNumber): ?PickingTask
    {
        return PickingTask::query()
            ->where('task_number', $taskNumber)
            ->first();
    }

    public function getPickingTask(int $pickingTaskId): array
    {
        $pickingTask = PickingTask::query()->where('id', $pickingTaskId)->first();
        if (!$pickingTask) {
            return [];
        }
        $pickingTaskBaskets = PickingTaskBasket::query()
            ->where('picking_task_id', $pickingTask->id)
            ->get()
            ->transform(function ($item) {
                return [
                    'outbound_number' => $item->outbound_number,
                    'basket_code' => $item->basket_code,
                    'pcs' => PickingTaskItem::query()
                        ->where('picking_task_basket_id', $item->id)
                        ->sum('quantity'),

                ];
            })
            ->toArray();
        // 补充 items
        $items = PickingTaskBasket::query()
            ->leftJoin('picking_task_items', 'picking_task_baskets.id', '=', 'picking_task_items.picking_task_basket_id')
            ->where('picking_task_baskets.picking_task_id', $pickingTaskId)
            ->get()
            ->transform(function ($item) {
                return [
                    'sku_code' => $item->sku_code,
                    'sku_name' => ProductSku::query()->where('sku_code', $item->sku_code)->value('sku_name') ?? '',
                    'quantity' => $item->quantity,
                    'basket_code' => $item->basket_code,
                    'bin' => $item->suggest_bin,
                ];
            })->toArray();

        return [
            'task_number' => $pickingTask->task_number,
            'operator' => User::query()->where('id', $pickingTask->user_id)->value('name') ?? '',
            'created_at' => $pickingTask->created_at->format('Y-m-d H:i:s'),
            'item_quantity' => 0,
            'sku_quantity' => 0,
            'baskets' => $pickingTaskBaskets,
            'items' => $items,
        ];

    }

    public function assingPickingTask(int $warehouseId, int $userId, int $taskType, array $taskIds = [], int $amount = 0): int
    {
        if (!empty($taskIds)) {
            return PickingTask::query()
                ->whereIn('id', $taskIds)
                ->whereIn('state', [
                    PickingTask::STATE_DEFAULT,
                    PickingTask::STATE_CREATED,
                ])
                ->update([
                    'user_id' => $userId,
                    'task_type' => $taskType,
                ]);
        }

        if ($amount) {
            $successAmount = 0;
            // 创建任务
            for ($i = 0; $i < $amount; $i++) {
                // 根据数量，智能推荐任务
                $pickingTaskItems = $this->suggestPickingTaskItems($warehouseId, CommonConst::PICKING_TASK_QUANTITY_PER_TIME); // 默认每次四个篮子
                if (empty($pickingTaskItems)) {
                    continue;
                }

                // 创建任务记录
                $pickingTask = PickingTask::create([
                    'warehouse_id' => $warehouseId,
                    'user_id' => $userId,
                    'task_number' => $this->generatePickingNumber(),
                    'task_type' => $taskType,
                    'state' => PickingTask::STATE_CREATED,
                ]);
                foreach ($pickingTaskItems as $pickingTaskItem) {
                    $pickingTaskBasket = PickingTaskBasket::create([
                        'picking_task_id' => $pickingTask->id,
                        'outbound_number' => $pickingTaskItem['outbound_number'],
                    ]);
                    // 出库单状态更新
                    app(OutboundRepository::class)->updateState($pickingTaskItem['outbound_id'], Outbound::STATE_PICKING_ING, $userId);
                    // 补充拣货任务 item
                    foreach ($pickingTaskItem['items'] as $item) {
                        PickingTaskItem::create([
                            'picking_task_basket_id' => $pickingTaskBasket->id,
                            'sku_code' => $item['sku_code'],
                            'suggest_bin' => $item['bin'],
                            'quantity' => $item['quantity'],
                        ]);
                    }
                    MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_ORDER_STATUS_NOTIFY, ['order_number' => $pickingTaskItem['outbound_number']]);
                }
                $successAmount++;
            }
            return $successAmount;
        }
        return 0;
    }

    public function checkPickingTaskIds(array $pickingTaskIds, int $state = 0): bool
    {
        $filterStates = [
            PickingTask::STATE_DEFAULT,
            PickingTask::STATE_CREATED,
        ];
        $filterTypes = [
            PickingTask::TASK_TYPE_DEFAULT,
            PickingTask::TASK_TYPE_PDA,
            PickingTask::TASK_TYPE_PAPER,
        ];
        if ($state == PickingTask::STATE_IN_PROGRESS) {
            $filterStates = [
                PickingTask::STATE_CREATED,
            ];
            $filterTypes = [
                PickingTask::TASK_TYPE_PAPER,

            ];
        }
        if ($state == PickingTask::STATE_DONE) {
            $filterStates = [
                PickingTask::STATE_IN_PROGRESS,
            ];
            $filterTypes = [
                PickingTask::TASK_TYPE_PAPER,

            ];
        }
        $count = count($pickingTaskIds);
        $taskCount = PickingTask::query()
            ->whereIn('id', $pickingTaskIds)
            ->whereIn('state', $filterStates)
            ->whereIn('task_type', $filterTypes)
            ->count();
        if ($count != $taskCount) {
            return false;
        }
        return true;
    }

    public function updateTaskState(int $warehouseId, int $userId, int $pickingTaskId, $state): bool
    {
        $updateData = [
            'state' => $state,
        ];
        if ($state == PickingTask::STATE_DONE) {
            $updateData['finished_at'] = time();
        }
        $pickingTask = PickingTask::query()->find($pickingTaskId);
        PickingTask::query()
            ->where('warehouse_id', $warehouseId)
            ->where('id', $pickingTaskId)
            ->update($updateData);
        if ($state == PickingTask::STATE_DONE) {
            // 获取拣货任务关联 出库单
            $pickingTaskBaskets = PickingTaskBasket::query()
                ->where('picking_task_id', $pickingTaskId)
                ->get();
            foreach ($pickingTaskBaskets as $pickingTaskBasket) {
                $outbound = Outbound::query()->where('outbound_number', $pickingTaskBasket->outbound_number)->first();
                if (!$outbound) {
                    continue;
                }
                // 检查是否有异常
                $abnormal = Abnormal::query()
                    ->where('relation_type', Abnormal::RELATION_TYPE_PICKING)
                    ->where('relation_number', $pickingTask->task_number)
                    ->where('basket_code', $pickingTaskBasket->basket_code)
                    ->whereNotIn('state', [
                        Abnormal::STATE_CLOSED,
                        Abnormal::STATE_COMPLETED,
                    ])
                    ->first();
                if ($abnormal) {
                    app(OutboundRepository::class)->updateState($outbound->id, Outbound::STATE_PICKING_ABNORMAL, $userId);
                    // 对拣出的货转移到异常区，生成异常 items
                    $pickingTaskItems = PickingTaskItem::query()
                        ->where('picking_task_basket_id', $pickingTaskBasket->id)
                        ->get();
                    // 补充异常 item
                    $skus = [];
                    foreach ($pickingTaskItems as $pickingTaskItem) {
                        $skus[] = [
                            'bin' => $pickingTaskItem->suggest_bin,
                            'sku_code' => $pickingTaskItem->sku_code,
                            'quantity' => $pickingTaskItem->picking_quantity,
                        ];
                    }
                    app(AbnormalRepository::class)->createPickingMissingAbnormalItems($abnormal->id, $skus);
                    foreach ($pickingTaskItems as $pickingTaskItem) {
                        app(StockRepository::class)->updateStock(
                            $warehouseId,
                            $userId,
                            StorageArea::STORAGE_TYPE_PACKAGE,
                            StockLog::RELATION_TYPE_ABNORMAL,
                            $abnormal->id,
                            $pickingTaskItem->sku_code,
                            '',
                            -$pickingTaskItem->picking_quantity
                        );
                        app(StockRepository::class)->updateStock(
                            $warehouseId,
                            $userId,
                            StorageArea::STORAGE_TYPE_ABNORMAL,
                            StockLog::RELATION_TYPE_ABNORMAL,
                            $abnormal->id,
                            $pickingTaskItem->sku_code,
                            '',
                            $pickingTaskItem->picking_quantity
                        );
                    }
                } else {
                    app(OutboundRepository::class)->updateState($outbound->id, Outbound::STATE_PICKING_COMPLETED, $userId);
                }

                MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_ORDER_STATUS_NOTIFY, ['order_number' => $outbound->order_number]);
            }
        }
        return true;
    }

    public function updateTasksState(int $warehouseId, int $userId, array $taskIds, $state): bool
    {
        $updateData = [
            'state' => $state,
        ];
        if ($state == PickingTask::STATE_DONE) {
            $updateData['finished_at'] = time();
        }
        PickingTask::query()
            ->where('warehouse_id', $warehouseId)
            ->whereIn('id', $taskIds)
            ->update($updateData);
        if ($state == PickingTask::STATE_DONE) {
            foreach ($taskIds as $taskId) {
                // 获取拣货任务关联 出库单
                $pickingTaskBaskets = PickingTaskBasket::query()
                    ->where('picking_task_id', $taskId)
                    ->get();
                foreach ($pickingTaskBaskets as $pickingTaskBasket) {
                    $outbound = Outbound::query()->where('outbound_number', $pickingTaskBasket->outbound_number)->first();
                    if (!$outbound) {
                        continue;
                    }
                    app(OutboundRepository::class)->updateState($outbound->id, Outbound::STATE_PICKING_COMPLETED, $userId);
                    MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_ORDER_STATUS_NOTIFY, ['order_number' => $outbound->order_number]);
                    $pickingTaskItems = PickingTaskItem::query()
                        ->where('picking_task_basket_id', $pickingTaskBasket->id)
                        ->get();
                    $stockNotify = [];
                    foreach ($pickingTaskItems as $pickingTaskItem) {
                        app(StockRepository::class)->updateStock(
                            $warehouseId,
                            $userId,
                            StorageArea::STORAGE_TYPE_PICKING,
                            StockLog::RELATION_TYPE_PICKING,
                            $taskId,
                            $pickingTaskItem->sku_code,
                            $pickingTaskItem->suggest_bin,
                            -$pickingTaskItem->quantity,
                            -$pickingTaskItem->quantity
                        );
                        app(StockRepository::class)->updateStock(
                            $warehouseId,
                            $userId,
                            StorageArea::STORAGE_TYPE_PACKAGE,
                            StockLog::RELATION_TYPE_PICKING,
                            $taskId,
                            $pickingTaskItem->sku_code,
                            '',
                            $pickingTaskItem->quantity
                        );
                        $stockNotify[] = [
                            'sku_code' => $pickingTaskItem->sku_code,

                        ];
                    }
                    MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_STOCK_NOTIFY, ['warehouse_id' => $warehouseId, 'list' => $stockNotify]);
                }
            }
        }
        return true;
    }

    public function getPickingTaskByPickingTaskItemId(int $pickingTaskItemId): ?PickingTask
    {
        $pickingTaskItem = PickingTaskItem::query()
            ->find($pickingTaskItemId);
        if (!$pickingTaskItem) {
            return null;
        }
        $pickingTaskBasket = PickingTaskBasket::query()
            ->find($pickingTaskItem->picking_task_basket_id);
        if (!$pickingTaskBasket) {
            return null;
        }
        return PickingTask::query()->find($pickingTaskBasket->picking_task_id);
    }

    public function getPickingTaskBasketByPickingTaskItemId(int $pickingTaskItemId): ?PickingTaskBasket
    {
        $pickingTaskItem = PickingTaskItem::query()
            ->find($pickingTaskItemId);
        if (!$pickingTaskItem) {
            return null;
        }
        return $pickingTaskBasket = PickingTaskBasket::query()
            ->find($pickingTaskItem->picking_task_basket_id);
    }

    public function incrementPickingTaskItemPickingQuantity(int $pickingTaskItemId, int $quantity): bool
    {
        return (bool) PickingTaskItem::query()
            ->where('id', $pickingTaskItemId)
            ->increment('picking_quantity', $quantity);
    }

    public function getSumQuantityOfUnPickingSku(string $skuCode, string $bin): int
    {
        return PickingTaskItem::query()
            ->leftJoin('picking_task_baskets', 'picking_task_baskets.id', '=', 'picking_task_items.picking_task_basket_id')
            ->leftJoin('picking_tasks', 'picking_tasks.id', '=', 'picking_task_baskets.picking_task_id')
            ->where('picking_task_items.sku_code', $skuCode)
            ->where('picking_task_items.suggest_bin', $bin)
            ->whereIn('picking_tasks.state', [
                PickingTask::STATE_DEFAULT,
                PickingTask::STATE_CREATED,
                PickingTask::STATE_IN_PROGRESS,
            ])
            ->sum(DB::raw('picking_task_items.quantity - picking_task_items.picking_quantity'));
    }

    public function getPickingTaskBasket(int $pickingTaskId, string $basketCode): ?PickingTaskBasket
    {
        return PickingTaskBasket::query()
            ->where('picking_task_id', $pickingTaskId)
            ->where('basket_code', $basketCode)
            ->first();
    }

    public function getPickingTaskBasketByOutboundNumber(string $outboundNumber): ?PickingTaskBasket
    {
        return PickingTaskBasket::query()
            ->where('outbound_number', $outboundNumber)
            ->orderByDesc('created_at')
            ->first();
    }

    public function updatePickingTaskItem(string $outboundNumber, array $items): bool
    {
        $pickingTaskBasket = $this->getPickingTaskBasketByOutboundNumber($outboundNumber);

        if (!$pickingTaskBasket) {
            return false;
        }
        foreach ($items as $item) {
            $pickingTaskItem = PickingTaskItem::query()
                ->where('picking_task_basket_id', $pickingTaskBasket->id)
                ->where('sku_code', $item['sku_code'])
                ->first();
            if (!$pickingTaskItem) {
                PickingTaskItem::create([
                    'picking_task_basket_id' => $pickingTaskBasket->id,
                    'sku_code' => $item['sku_code'],
                    'quantity' => $item['quantity'],
                    'picking_quantity' => $item['quantity'],
                    'state' => PickingTaskItem::STATE_NORMAL,
                ]);
            } else {
                $pickingTaskItem->picking_quantity = $item['quantity'];
                $pickingTaskItem->state = PickingTaskItem::STATE_NORMAL;
                $pickingTaskItem->save();
            }
        }
        return true;
    }
}
