<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CheckTaskRepository.
 *
 * @package namespace App\Repositories;
 */
interface CheckTaskRepository extends RepositoryInterface
{
    //
}
