<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PutawayTaskRepository.
 *
 * @package namespace App\Repositories;
 */
interface PutawayTaskRepository extends RepositoryInterface
{
    //
}
