<?php

namespace App\Repositories;

use App\Consts\NumberPrefixConst;
use App\Models\AbnormalCode;
use App\Models\AbnormalItem;
use App\Models\AbnormalProcess;
use App\Models\StorageArea;
use App\Models\StorageLocation;
use App\Services\CacheService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\AbnormalRepository;
use App\Models\Abnormal;
use App\Validators\AbnormalValidator;

/**
 * Class AbnormalRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class AbnormalRepositoryEloquent extends BaseRepository implements AbnormalRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Abnormal::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function createAbnormal(
        int $warehouseId,
        int $userId,
        string $relationType,
        string $relationNumber,
        string $abnormalCode = '',
        string $preReceivingNumber = '',
        string $outboundNumber = '',
        string $reason = '',
        int $state = 0,
        int $image = 0,
        string $basketCode = ''
    ): Abnormal
    {
        // 创建异常
        $abnormal = Abnormal::create([
            'warehouse_id' => $warehouseId,
            'abnormal_type' => $this->getAbnormalType($relationType, $reason),
            'abnormal_number' => $this->generateAbnormalNumber(),
            'relation_type' => $relationType,
            'relation_number' => $relationNumber,
            'abnormal_code' => !empty($abnormalCode) ? $abnormalCode : $this->generateAbnormalCode(),
            'pre_receiving_number' => $preReceivingNumber,
            'outbound_number' => $outboundNumber,
            'reason' => $reason,
            'state' => $state,
            'image' => $image,
            'report_user_id' => $userId,
            'basket_code' => $basketCode,
        ]);
        AbnormalProcess::create([
            'abnormal_id' => $abnormal->id,
            'user_id' => $userId,
            'state_after' => Abnormal::STATE_CREATED,
            'content' => $reason
        ]);
        return $abnormal;

    }

    private function getAbnormalType(string $abnormalRelationType, string $reason): int
    {
        if ($abnormalRelationType == Abnormal::RELATION_TYPE_ACTUAL_RECEIVING) {
            switch ($reason) {
                case Abnormal::REASON_EXTRA:
                    return Abnormal::TYPE_RECEIVING_EXTRA;
                    break;
                case Abnormal::REASON_BROKEN:
                case Abnormal::REASON_STAINS:
                case Abnormal::REASON_COLOR_SHADING:
                    return Abnormal::TYPE_RECEIVING_DEFECTIVE;
                    break;
                case Abnormal::REASON_NOT_FOUND:
                    return Abnormal::TYPE_RECEIVING_WRONG;
                    break;
            }
        }
        if ($abnormalRelationType == Abnormal::RELATION_TYPE_PUTAWAY) {
            switch ($reason) {
                case Abnormal::REASON_EXTRA:
                    return Abnormal::TYPE_PUTAWAY_EXTRA;
                    break;
                case Abnormal::REASON_MISSING:
                    return Abnormal::TYPE_PUTAWAY_MISSING;
                    break;
                case Abnormal::REASON_WRONG:
                    return Abnormal::TYPE_PUTAWAY_WRONG;
                    break;
            }
        }
        if ($abnormalRelationType == Abnormal::RELATION_TYPE_PICKING) {
            return Abnormal::TYPE_PICKING_MISSING;
        }
        if ($abnormalRelationType == Abnormal::RELATION_TYPE_CHECK) {
            switch ($reason) {
                case Abnormal::REASON_DEFECTIVE:
                    return Abnormal::TYPE_CHECKING_DEFECTIVE;
                    break;
                case Abnormal::REASON_FAILED:
                    return Abnormal::TYPE_CHECKING_FAILED;
                    break;
            }
        }
        if ($abnormalRelationType == Abnormal::RELATION_TYPE_WEIGH) {
            return Abnormal::TYPE_ORDER_INTERCEPTED;
        }
        return 0;
    }

    private function generateAbnormalNumber(): string
    {
        $abnormalCount = Abnormal::query()
            ->whereBetween('created_at', [
                Carbon::today()->startOfDay()->timestamp,
                Carbon::today()->endOfDay()->timestamp,
            ])
            ->count();
        $abnormalCount = $abnormalCount + 1;
        $number = NumberPrefixConst::ABNORMAL . date('Ymd') . sprintf('%05s', $abnormalCount);
        $exist = Abnormal::query()
            ->where('abnormal_number', $number)
            ->count();
        if ($exist) {
            return self::generateAbnormalNumber();
        }
        return $number;
    }


    public function generateAbnormalCode(): string
    {
        $abnormalCodeCount = Cache::get(CacheService::getAbnormalCodeCountKey(date('Y-m-d')));
        if (!$abnormalCodeCount) {
            $abnormalCodeCount = AbnormalCode::query()
                ->whereBetween('created_at', [
                    Carbon::today()->startOfDay()->timestamp,
                    Carbon::today()->endOfDay()->timestamp,
                ])
                ->count();
            Cache::put(CacheService::getAbnormalCodeCountKey(date('Y-m-d')), $abnormalCodeCount, 24 * 60 * 60);
        }
        $abnormalCodeCount = $abnormalCodeCount + 1;
        $abnormalCode =  NumberPrefixConst::ABNORMAL_CODE . date('Ymd') . sprintf('%04s', $abnormalCodeCount);
        AbnormalCode::create([
            'abnormal_code' => $abnormalCode,
        ]);
        Cache::increment(CacheService::getAbnormalCodeCountKey(date('Y-m-d')), 1);
        return $abnormalCode;
    }

    public function createAbnormalProcess(
        int $abnormalId,
        int $userId,
        int $stateBefore,
        int $stateAfter,
        string $content = ''
    ): ?AbnormalProcess
    {
        return AbnormalProcess::create([
            'abnormal_id' => $abnormalId,
            'user_id' => $userId,
            'state_before' => $stateBefore,
            'state_after' => $stateAfter,
            'content' => $content
        ]);
    }

    public function updateState(int $abnormalId, int $state): bool
    {
        return (bool) Abnormal::query()
            ->where('id', $abnormalId)
            ->update([
                'state' => $state,
            ]);
    }

    public function createAbnormalItems(int $abnormalId, array $skus): bool
    {
        $time = time();
        $insertArr = [];
        foreach ($skus as $sku) {
            $insertArr[] = [
                'abnormal_id' => $abnormalId,
                'sku_code' => $sku['sku_code'],
                'quantity' => $sku['quantity'],
                'created_at' => $time,
                'updated_at' => $time,
            ];
        }
        AbnormalItem::query()->insert($insertArr);
        return true;
    }

    public function createPickingMissingAbnormalItems(int $abnormalId, array $skus): bool
    {
        foreach ($skus as $sku) {
            $abnormalItem = AbnormalItem::query()
                ->where('abnormal_id', $abnormalId)
                ->where('bin', $sku['bin'])
                ->where('sku_code', $sku['sku_code'])
                ->first();
            if (!$abnormalItem) {
                AbnormalItem::create([
                    'abnormal_id' => $abnormalId,
                    'bin' => $sku['bin'],
                    'sku_code' => $sku['sku_code'],
                    'quantity' => isset($sku['quantity']) ? $sku['quantity'] : 0,
                    'missing_quantity' => isset($sku['missing_quantity']) ? $sku['missing_quantity'] : 0,
                ]);
            } else {
                $needSave = false;
                if (isset($sku['quantity']) && $sku['quantity'] != $abnormalItem->quantity) {
                    $abnormalItem->quantity = $sku['quantity'];
                    $needSave = true;
                }
                if (isset($sku['missing_quantity']) && $sku['missing_quantity'] != $abnormalItem->missing_quantity) {
                    $abnormalItem->missing_quantity = $sku['missing_quantity'];
                    $needSave = true;
                }
                if ($needSave) {
                    $abnormalItem->save();
                }
            }
        }
        return true;
    }

    public function checkAbnormalBin(int $warehouseId, string $bin): bool
    {
        if (!$bin) {
            return false;
        }
        return StorageLocation::query()
            ->where('warehouse_id', $warehouseId)
            ->where('storage_type', StorageArea::STORAGE_TYPE_ABNORMAL)
            ->where('storage_location_code', $bin)
            ->exists();
    }
}
