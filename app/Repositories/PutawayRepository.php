<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PutawayRepository.
 *
 * @package namespace App\Repositories;
 */
interface PutawayRepository extends RepositoryInterface
{
    //
}
