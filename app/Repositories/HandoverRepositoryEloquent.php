<?php

namespace App\Repositories;

use App\Models\Abnormal;
use App\Models\HandoverItem;
use App\Models\LogisticsCompany;
use App\Models\Outbound;
use App\Models\Weigh;
use App\User;
use Carbon\Carbon;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\HandoverRepository;
use App\Models\Handover;
use App\Validators\HandoverValidator;

/**
 * Class HandoverRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class HandoverRepositoryEloquent extends BaseRepository implements HandoverRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Handover::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getListByPage(array $querys, int $page = 1, int $perPage = 1): array
    {
        $handoverQuery = Handover::query();
        if (!empty($querys['handover_number'])) {
            $handoverQuery->where('handover_number', $querys['handover_number']);
        }

        if (!empty($querys['created_at_from'])) {
            $handoverQuery->where('created_at', '>=', Carbon::parse($querys['created_at_from'])->timestamp);
        }

        if (!empty($querys['created_at_to'])) {
            $handoverQuery->where('created_at', '<=', Carbon::parse($querys['created_at_to'])->timestamp);
        }

        if (!empty($querys['user_id'])) {
            $handoverQuery->where('user_id', $querys['user_id']);
        }

        $handoverQuery->orderByDesc('created_at');

        $total = $handoverQuery->count();
        $handovers = $handoverQuery->forPage($page, $perPage)
            ->get([
                'id',
                'handover_number',
                'logistics_company_id',
                'created_at',
                'user_id',
            ])
            ->transform(function ($item) {
                $logisticsCompany = LogisticsCompany::query()
                    ->where('id', $item->logistics_company_id)
                    ->first();
                return [
                    'id' => $item->id,
                    'handover_number' => $item->handover_number,
                    'tpl' => $logisticsCompany->company_name,
                    'tpl_contact' => $logisticsCompany->contact,
                    'created_at' => $item->created_at->format('Y-m-d H:i:s'),
                    'operator' => User::query()->where('id', $item->user_id)->value('name') ?? '',
                ];
            })
            ->toArray();

        return [
            'handovers' => $handovers,
            'page_bean' => [
                'total' => $total,
                'page' => $page,
                'per_page' => $perPage,
            ],
        ];
    }

    public function existsHandover(int $handoverId): bool
    {
        return (bool) Handover::query()->where('id', $handoverId)->count();
    }

    public function getDetail(int $handoverId): array
    {
        $handover = Handover::query()->where('id', $handoverId)->first();
        if (!$handover) {
            return [];
        }
        // 补充 items
        $handoverItems = HandoverItem::query()
            ->where('handover_id', $handover->id)
            ->get()
            ->transform(function ($item) {
                $weigh = Weigh::query()
                    ->where('logistics_number', $item->logistics_number)
                    ->first();
                $outbound = Outbound::query()
                    ->where('outbound_number', $weigh->outbound_number)
                    ->first();
                return [
                    'logistics_number' => $item->logistics_number,
                    'platform_order_number' => $outbound->order_number,
                    'weight' => (float) $item->weight,
                    'quantity' => $item->quantity,
                ];
            });

        // 补充关联异常
        $logisticsNumbers = HandoverItem::query()
            ->where('handover_id', $handover->id)
            ->pluck('logistics_number')
            ->toArray();
        $abnormals = Abnormal::query()
            ->where('relation_type', Abnormal::RELATION_TYPE_HANDOVER)
            ->whereIn('relation_number', $logisticsNumbers)
            ->get()
            ->transform(function ($item) {
                return [
                    'id' => $item->id,
                    'abnormal_number' => $item->abnormal_number,
                    'state' => $item->state,
                    'type' => $item->reason,
                    'operator' => User::query()->where('id', $item->report_user_id)->value('name') ?? '',
                ];
            })
            ->toArray();


        $logisticsCompany = LogisticsCompany::query()
            ->where('id', $handover->logistics_company_id)
            ->first();
        return [
            'id' => $handover->id,
            'handover_number' => $handover->handover_number,
            'tpl' => $logisticsCompany->company_name,
            'tpl_contact' => $logisticsCompany->contact,
            'created_at' => $handover->created_at->format('Y-m-d H:i:s'),
            'operator' => User::query()->where('id', $handover->user_id)->value('name') ?? '',
            'signature_url' => (string) $handover->signature,
            'items' => $handoverItems,
            'abnormals' => $abnormals,
        ];
    }

}
