<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\AppVersionRepository;
use App\Models\AppVersion;
use App\Validators\AppVersionValidator;

/**
 * Class AppVersionRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class AppVersionRepositoryEloquent extends BaseRepository implements AppVersionRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AppVersion::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getVersions(int $platform, string $version): array
    {
        return AppVersion::query()
            ->where('platform', $platform)
            ->where('version', '>', $version)
            ->get()
            ->transform(function ($item) {
                return [
                    'version' => $item->version,
                    'message' => $item->message,
                    'download_url' => $item->download_url,
                    'platform' => $item->platform == 1 ? 'ios' : 'android',
                    'force' => (bool) $item->is_force,
                ];
            })
            ->toArray();

    }

}
