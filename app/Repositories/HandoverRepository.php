<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface HandoverRepository.
 *
 * @package namespace App\Repositories;
 */
interface HandoverRepository extends RepositoryInterface
{
    //
}
