<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PickingTaskBasketRepository.
 *
 * @package namespace App\Repositories;
 */
interface PickingTaskBasketRepository extends RepositoryInterface
{
    //
}
