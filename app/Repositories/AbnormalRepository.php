<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AbnormalRepository.
 *
 * @package namespace App\Repositories;
 */
interface AbnormalRepository extends RepositoryInterface
{
    //
}
