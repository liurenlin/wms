<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PickingRepository.
 *
 * @package namespace App\Repositories;
 */
interface PickingTaskRepository extends RepositoryInterface
{
    //
}
