<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PutawayRepository;
use App\Validators\PutawayValidator;

/**
 * Class PutawayRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class PutawayRepositoryEloquent extends BaseRepository implements PutawayRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Putaway::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
