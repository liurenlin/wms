<?php

namespace App\Repositories;

use App\Models\Outbound;
use App\Models\PickingTask;
use App\Models\PickingTaskBasket;
use App\Models\PickingTaskItem;
use Carbon\Carbon;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PickingTaskBasketRepository;
use App\Validators\PickingTaskBasketValidator;

/**
 * Class PickingTaskBasketRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class PickingTaskBasketRepositoryEloquent extends BaseRepository implements PickingTaskBasketRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PickingTaskBasket::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getListByPage(array $querys, int $page = 1, int $perPage = 1): array
    {
        $pickingTaskBasketsQuery = PickingTaskBasket::query()
            ->join('picking_tasks', 'picking_task_baskets.picking_task_id', '=', 'picking_tasks.id')
            ->join('outbounds', 'picking_task_baskets.outbound_number', '=', 'outbounds.outbound_number')
            ->whereIn('outbounds.state', [
                Outbound::STATE_PICKING_ING,
                Outbound::STATE_PICKING_ABNORMAL,
                Outbound::STATE_PICKING_COMPLETED,
                Outbound::STATE_CHECK_ABNORMAL,
                Outbound::STATE_CHECK_COMPLETED,
            ]);

        if (!empty($querys['basket_code'])) {
            $pickingTaskBasketsQuery->where('picking_task_baskets.basket_code', $querys['basket_code']);
        }

        if (!empty($querys['picking_task_number'])) {
            $pickingTaskBasketsQuery->where('picking_tasks.task_number', $querys['picking_task_number']);
        }

        if (!empty($querys['outbound_number'])) {
            $pickingTaskBasketsQuery->where('outbounds.outbound_number', $querys['outbound_number']);
        }

        if (!empty($querys['created_at_from'])) {
            $pickingTaskBasketsQuery->where('picking_task_baskets.created_at', '>=', Carbon::parse($querys['created_at_from'])->timestamp);
        }

        if (!empty($querys['created_at_to'])) {
            $pickingTaskBasketsQuery->where('picking_task_baskets.created_at', '<=', Carbon::parse($querys['created_at_to'])->timestamp);
        }

        $pickingTaskBasketsQuery->orderByDesc('picking_task_baskets.created_at');

        $total = $pickingTaskBasketsQuery->count();
        $pickingTaskBaskets = $pickingTaskBasketsQuery->forPage($page, $perPage)
            ->get([
                'picking_task_baskets.id',
                'picking_task_baskets.basket_code',
                'picking_tasks.task_number',
                'picking_task_baskets.outbound_number',
                'picking_task_baskets.created_at',
            ])
            ->transform(function ($item) {
                return [
                    'id' => $item->id,
                    'basket_code' => $item->basket_code,
                    'picking_task_number' => $item->task_number,
                    'outbound_number' => $item->outbound_number,
                    'created_at' => $item->created_at->format('Y-m-d H:i:s'),
                ];
            })
            ->toArray();

        return [
            'list' => $pickingTaskBaskets,
            'page_bean' => [
                'total' => $total,
                'page' => $page,
                'per_page' => $perPage,
            ],
        ];
    }

    public function getDetail(int $pickingTaskBasketId): array
    {
        $pickingTaskBasket = PickingTaskBasket::query()->find($pickingTaskBasketId);
        $pickingTask = PickingTask::query()->find($pickingTaskBasket->picking_task_id);

        $skus = PickingTaskItem::query()
            ->where('picking_task_basket_id', $pickingTaskBasket->id)
            ->where('picking_quantity', '>', 0)
            ->get([
                'sku_code',
                'picking_quantity as quantity'
            ])->toArray();


        return [
            'basket_code' => $pickingTaskBasket->basket_code,
            'picking_task_number' => $pickingTask->task_number,
            'outbound_number' => $pickingTaskBasket->outbound_number,
            'created_at' => $pickingTaskBasket->created_at->format('Y-m-d H:i:s'),
            'skus' => $skus,
        ];
    }

}
