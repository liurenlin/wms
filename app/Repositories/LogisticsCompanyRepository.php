<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LogisticsCompanyRepository.
 *
 * @package namespace App\Repositories;
 */
interface LogisticsCompanyRepository extends RepositoryInterface
{
    //
}
