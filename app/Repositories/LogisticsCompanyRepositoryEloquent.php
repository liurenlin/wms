<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\LogisticsCompanyRepository;
use App\Models\LogisticsCompany;
use App\Validators\LogisticsCompanyValidator;

/**
 * Class LogisticsCompanyRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class LogisticsCompanyRepositoryEloquent extends BaseRepository implements LogisticsCompanyRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return LogisticsCompany::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
