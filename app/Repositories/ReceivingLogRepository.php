<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ReceivingLogRepository.
 *
 * @package namespace App\Repositories;
 */
interface ReceivingLogRepository extends RepositoryInterface
{
    //
}
