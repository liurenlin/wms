<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface StorageLocationRepository.
 *
 * @package namespace App\Repositories;
 */
interface StorageLocationRepository extends RepositoryInterface
{
    //
}
