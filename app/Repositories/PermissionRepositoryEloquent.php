<?php

namespace App\Repositories;

use App\Models\PermissionGroup;
use App\Models\RolePermissionGroup;
use App\Models\UserRole;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PermissionRepository;
use App\Models\Permission;
use App\Validators\PermissionValidator;

/**
 * Class PermissionRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class PermissionRepositoryEloquent extends BaseRepository implements PermissionRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Permission::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getGroupPermissions(): array
    {
        return PermissionGroup::query()
            ->where('parent_id', 0)
            ->get()
            ->transform(function ($item) {
                return [
                    'id' => $item->id,
                    'name' => $item->group_name,
                    'permission_groups' => PermissionGroup::query()
                        ->where('parent_id', $item->id)
                        ->get([
                            'id',
                            'group_name as name',
                        ])->toArray(),
                ];
            })->toArray();
    }

    public function getUserMenus(int $userId): array
    {
        // 查找用户角色
        $userRoles = UserRole::query()
            ->where('user_id', $userId)
            ->get();

        $userRoleIds = $userRoles->pluck('role_id')->toArray();
        // 查找角色对应 permission group
        $permissionGroupIds = RolePermissionGroup::query()
            ->whereIn('role_id', $userRoleIds)
            ->pluck('permission_group_id')
            ->toArray();

        $permissionGroups = PermissionGroup::query()
            ->whereIn('id', $permissionGroupIds)
            ->get([
                'id',
                'group_name as name',
                'parent_id',
            ])
            ->toArray();

        $permissionGroups = collect($permissionGroups)->groupBy('parent_id')->toArray();
        $menus = [];
        foreach ($permissionGroups as $key => $groups) {
            foreach ($groups as &$group) {
                unset($group['parent_id']);
            }
            array_push($menus, [
                'id' => $key,
                'name' => PermissionGroup::query()
                    ->where('id', $key)
                    ->value('group_name'),
                'subs' => $groups,
            ]);
        }
        return $menus;
    }
}
