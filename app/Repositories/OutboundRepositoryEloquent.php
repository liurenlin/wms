<?php

namespace App\Repositories;

use App\Consts\NumberPrefixConst;
use App\Models\Abnormal;
use App\Models\OpenMessageQueue;
use App\Models\OutboundFrozenItem;
use App\Models\OutboundHistory;
use App\Models\OutboundItem;
use App\Models\OutboundProcess;
use App\Models\PickingTask;
use App\Models\PickingTaskBasket;
use App\Models\ProductSku;
use App\Models\Stock;
use App\Models\StockLog;
use App\Models\StorageArea;
use App\Services\MessageService;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\OutboundRepository;
use App\Models\Outbound;
use App\Validators\OutboundValidator;

/**
 * Class OutboundRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class OutboundRepositoryEloquent extends BaseRepository implements OutboundRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Outbound::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getListByPage(array $querys, int $page = 1, int $perPage = 1): array
    {
        $outboundQuery = Outbound::query();
        if (!empty($querys['outbound_number'])) {
            $outboundQuery->where('outbound_number', $querys['outbound_number']);
        }

        if (!empty($querys['outbound_number'])) {
            $outboundQuery->where('outbound_number', $querys['outbound_number']);
        }

        if (!empty($querys['created_at_from'])) {
            $outboundQuery->where('created_at', '>=', Carbon::parse($querys['created_at_from'])->timestamp);
        }

        if (!empty($querys['created_at_to'])) {
            $outboundQuery->where('created_at', '<=', Carbon::parse($querys['created_at_to'])->timestamp);
        }

        if (!empty($querys['platform_order_number'])) {
            $outboundQuery->where('order_number', $querys['platform_order_number']);
        }

        if (!empty($querys['platform_order_created_at_from'])) {
            $outboundQuery->where('order_created_at', '>=', Carbon::parse($querys['platform_order_created_at_from'])->timestamp);
        }

        if (!empty($querys['platform_order_created_at_to'])) {
            $outboundQuery->where('order_created_at', '<=', Carbon::parse($querys['platform_order_created_at_to'])->timestamp);
        }

        if (!empty($querys['state'])) {
            $outboundQuery->where('state', $querys['state']);
        }

        $outboundQuery->orderByDesc('created_at');

        $total = $outboundQuery->count();
        $outbounds = $outboundQuery->forPage($page, $perPage)
            ->get([
                'id',
                'outbound_number',
                'created_at',
                'order_number',
                'order_created_at',
                'state',
            ])
            ->transform(function ($item) {
                return [
                    'id' => $item->id,
                    'outbound_number' => $item->outbound_number,
                    'created_at' => $item->created_at->timestamp ? $item->created_at->format('Y-m-d H:i:s') : '',
                    'state' => $item->state,
                    'platform_order' => [
                        'order_number' => $item->order_number,
                        'created_at' => $item->order_created_at->timestamp ? $item->order_created_at->toDateTimeString() : '',
                    ],
                    'quantity' => (int) OutboundItem::query()->where('outbound_id', $item->id)->sum('quantity'),
                ];
            })
            ->toArray();

        return [
            'outbounds' => $outbounds,
            'page_bean' => [
                'total' => $total,
                'page' => $page,
                'per_page' => $perPage,
            ],
        ];
    }

    public function existsOutbound(int $outboundId): bool
    {
        return (bool) Outbound::query()->where('id', $outboundId)->count();
    }

    public function getDetail(int $outboundId): array
    {
        $outbound = Outbound::query()->where('id', $outboundId)->first();
        if (!$outbound) {
            return [];
        }

        // 补充拣货任务
        $pickingTask = PickingTask::query()
            ->leftJoin('picking_task_baskets', 'picking_tasks.id', '=', 'picking_task_baskets.picking_task_id')
            ->where('picking_task_baskets.outbound_number', $outbound->outbound_number)
            ->first([
                'picking_tasks.id',
                'picking_tasks.task_number',
                'picking_task_baskets.id as picking_task_basket_id',
            ]);

        // 补充 sku
        $skus = OutboundItem::query()
            ->where('outbound_id', $outbound->id)
            ->get([
                'sku_code as code',
                'quantity',
            ])->toArray();

        // 补充进度
        $processes = OutboundProcess::query()
            ->where('outbound_id', $outbound->id)
            ->get()
            ->transform(function ($item) {
                return [
                    'state' => $item->state_after,
                    'created_at' => $item->created_at->format('Y-m-d H:i:s'),
                    'operator' => User::query()->where('id', $item->user_id)->value('name') ?? '',
                    'content' => $item->content,
                ];
            })->toArray();


        // 补充关联异常
        $abnormals = Abnormal::query()
            ->where(function ($query) use ($pickingTask, $outbound) {
                if ($pickingTask) {
                    $pickingTaskBasket = PickingTaskBasket::query()
                        ->where('picking_task_id', $pickingTask->id)
                        ->where('outbound_number', $outbound->outbound_number)
                        ->first();
                    $query->orWhere(function ($query) use ($pickingTask, $pickingTaskBasket) {
                        $query->where('relation_type', Abnormal::RELATION_TYPE_PICKING);
                        $query->where('relation_number', $pickingTask->task_number);
                        $query->where('basket_code', $pickingTaskBasket->basket_code);
                    });
                }
                $query->orWhere(function ($query) use ($outbound) {
                    $query->whereIn('relation_type', [
                        Abnormal::RELATION_TYPE_CHECK,
                        Abnormal::RELATION_TYPE_WEIGH,
                    ]);
                    $query->where('relation_number', $outbound->outbound_number);
                });
                $query->orWhere(function ($query) use ($outbound) {
                    $query->where('relation_type', Abnormal::RELATION_TYPE_HANDOVER);
                    $query->where('relation_number', $outbound->outbound_number);
                });
            })
            ->get()
            ->transform(function ($item) {
                return [
                    'id' => $item->id,
                    'abnormal_number' => $item->abnormal_number,
                    'state' => $item->state,
                    'type' => Abnormal::getAbnormalTypeTip($item->abnormal_type),
                    'created_at' => $item->created_at->format('Y-m-d H:i:s'),
                    'operator' => User::query()->where('id', $item->report_user_id)->value('name') ?? '',
                ];
            })
            ->toArray();

        $outbound = [
            'id' => $outbound->id,
            'outbound_number' => $outbound->outbound_number,
            'outbound_type' => $outbound->outbound_type,
            'created_at' => $outbound->created_at->format('Y-m-d H:i:s'),
            'owner' => '',
            'platform' => 'Dollarhoo',
            'platform_order' => [
                'order_number' => $outbound->order_number,
                'created_at' => $outbound->order_created_at->format('Y-m-d H:i:s'),
            ],
            'receiving_country' => $outbound->receiving_country,
            'receiving_region' => $outbound->receiving_region,
            'receiving_state' => $outbound->receiving_state,
            'receiving_city' => $outbound->receiving_city,
            'receiving_district' => $outbound->receiving_district,
            'receiving_address' => $outbound->receiving_address,
            'receiving_cellphone' => $outbound->receiving_cellphone,
            'receiving_user_name' => $outbound->receiving_user_name,

            'skus' => $skus,
            'processes' => $processes,
            'abnormals' => $abnormals,
        ];
        if ($pickingTask) {
            $outbound['picking_task'] = $pickingTask;
        }
        return $outbound;
    }

    public function checkOutboundItems(array $items): bool
    {
        if (empty($items)) {
            return false;
        }
        foreach ($items as $item) {
            if (!ProductSku::query()->where('sku_code', $item['sku_code'])->first()) {
                return false;
            }
        }
        return true;
    }

    public function createOutbound(int $warehouseId, array $params): ?Outbound
    {

        $params['order_created_at'] = Carbon::parse($params['order_created_at'])->timestamp;
        return Outbound::create(array_merge($params, [
            'warehouse_id' => $warehouseId,
            'outbound_number' => $this->generateOutboundNumber(),
            'state' => Outbound::STATE_CREATED,
        ]));
    }

    public function createOutboundProcess(
        int $outboundId,
        int $stateAfter,
        int $stateBefore = 0,
        int $userId = 0,
        string $content = ''
    ): OutboundProcess
    {
        return OutboundProcess::create([
            'outbound_id' => $outboundId,
            'state_before' => $stateBefore,
            'state_after' => $stateAfter,
            'user_id' => $userId,
            'content' => $content,
        ]);
    }

    private function generateOutboundNumber(): string
    {
        $outboundCount = Outbound::query()
            ->whereBetween('created_at', [
                Carbon::today()->startOfDay()->timestamp,
                Carbon::today()->endOfDay()->timestamp,
            ])
            ->count();
        $outboundCount = $outboundCount + 1;
        $number = NumberPrefixConst::OUTBOUND . date('Ymd') . sprintf('%05s', $outboundCount);
        $exist = Outbound::query()
            ->where('outbound_number', $number)
            ->count();
        if ($exist) {
            return self::generateOutboundNumber();
        }
        return $number;
    }

    /**
     * @param  int  $outboundId
     * @param  array  $items
     * [
     *  [
     *    "sku_code" => "SKU001",
     *    "quantity" => 1
     *  ]
     * ]
     * @return bool
     */
    public function createItems(int $outboundId, array $items): bool
    {
        $time = time();
        $itemArr = [];
        foreach ($items as $item) {
            $itemArr[] = [
                'outbound_id' => $outboundId,
                'sku_code' => $item['sku_code'],
                'quantity' => $item['quantity'],
                'created_at' => $time,
                'updated_at' => $time,
            ];
        }
        if (empty($itemArr)) {
            return true;
        }
        OutboundItem::query()->insert($itemArr);
        return true;
    }

    public function frozenItems(int $outboundId): bool
    {
        $outboundItems = OutboundItem::query()
            ->where('outbound_id', $outboundId)
            ->where('is_frozen', OutboundItem::IS_FROZEN_FALSE)
            ->get();
        foreach ($outboundItems as $outboundItem) {
            // 能否冻结
            $sumStockQuantity = Stock::query()
                ->where('storage_type', StorageArea::STORAGE_TYPE_PICKING)
                ->where('sku_code', $outboundItem->sku_code)
                ->sum('unfrozen_quantity');
            if ($sumStockQuantity == 0) {
                continue;
            }
            if ($sumStockQuantity < $outboundItem->quantity) {
                // 不够冻结一个 item
                continue;
            }
            $stocks = Stock::query()
                ->where('storage_type', StorageArea::STORAGE_TYPE_PICKING)
                ->where('sku_code', $outboundItem->sku_code)
                ->where('unfrozen_quantity', '>', 0)
                ->orderBy('quantity')
                ->get();
            if ($stocks->isEmpty()) {
                continue;
            }
            $needFrozenQuantity = $outboundItem->quantity;
            foreach ($stocks as $stock) {
                // 冻结库存
                if ($needFrozenQuantity <= $stock->unfrozen_quantity) {
                    $frozenQuantity = $needFrozenQuantity;
                } else {
                    $frozenQuantity = $stock->unfrozen_quantity;
                }
                app(StockRepository::class)->updateStock(
                    $stock->warehouse_id,
                    0,
                    StorageArea::STORAGE_TYPE_PICKING,
                    StockLog::RELATION_TYPE_OUTBOUND_ITEM,
                    $outboundItem->id,
                    $stock->sku_code,
                    $stock->bin,
                    0,
                    $frozenQuantity,
                    -$frozenQuantity
                );
                // 生成冻结记录
                $outboundFrozenItem = OutboundFrozenItem::query()
                    ->where('outbound_item_id', $outboundItem->id)
                    ->where('sku_code', $outboundItem->sku_code)
                    ->where('frozen_bin', $stock->bin)
                    ->first();
                if (!$outboundFrozenItem) {
                    OutboundFrozenItem::create([
                        'outbound_id' => $outboundId,
                        'outbound_item_id' => $outboundItem->id,
                        'sku_code' => $outboundItem->sku_code,
                        'frozen_bin' => $stock->bin,
                        'frozen_quantity' => $frozenQuantity,
                    ]);
                } else {
                    $outboundFrozenItem->frozen_bin = $stock->bin;
                    $outboundFrozenItem->frozen_quantity = $frozenQuantity;
                    $outboundFrozenItem->save();
                }

                $needFrozenQuantity -= $frozenQuantity;
                if ($needFrozenQuantity == 0) {
                    OutboundItem::query()
                        ->where('id', $outboundItem->id)
                        ->update([
                            'is_frozen' => OutboundItem::IS_FROZEN_TRUE,
                        ]);
                }
            }
        }
        return true;
    }

    /**
     * @param  int  $warehouseId
     * @param  int  $outboundId
     * @param  int  $outboundState
     * @param  array  $items
     * [
     *  [
     *    "sku_code" => "SKU001",
     *    "quantity" => 1
     *  ]
     * ]
     * @return array
     */
    public function updateItems(int $warehouseId, int $outboundId, int $outboundState, array $items): array
    {
        $stockNotifyList = [];
        if (in_array($outboundState, [
            Outbound::STATE_CREATED,
            Outbound::STATE_FROZEN,
        ])) {
            $outboundFrozenItems = OutboundFrozenItem::query()
                ->where('outbound_id', $outboundId)
                ->get();
            if ($outboundFrozenItems->isNotEmpty()) {
                foreach ($outboundFrozenItems as $outboundFrozenItem) {
                    app(StockRepository::class)->updateStock(
                        $warehouseId,
                        0,
                        StorageArea::STORAGE_TYPE_PICKING,
                        StockLog::RELATION_TYPE_OUTBOUND,
                        $outboundId,
                        $outboundFrozenItem->sku_code,
                        $outboundFrozenItem->frozen_bin,
                        0,
                        -$outboundFrozenItem->frozen_quantity,
                        $outboundFrozenItem->frozen_quantity
                    );
                    $stockNotifyList[] = [
                        'sku_code' => $outboundFrozenItem->sku_code,
                    ];
                }
                OutboundFrozenItem::query()
                    ->where('outbound_id', $outboundId)
                    ->delete();
            }
        }
        OutboundItem::query()
            ->where('outbound_id', $outboundId)
            ->delete();
        // 重新生成
        $this->createItems($outboundId, $items);
        return $stockNotifyList;
    }

    public function updateState(int $outboundId, int $state, int $userId = 0, string $content = ''): bool
    {
        if (!in_array($state, [
            Outbound::STATE_CREATED,
            Outbound::STATE_FROZEN,
            Outbound::STATE_CANCELLED,
            Outbound::STATE_PICKING_TASK_CREATED,
            Outbound::STATE_PICKING_ING,
            Outbound::STATE_PICKING_ABNORMAL,
            Outbound::STATE_PICKING_COMPLETED,
            Outbound::STATE_CHECK_ABNORMAL,
            Outbound::STATE_CHECK_COMPLETED,
            Outbound::STATE_WEIGHED,
            Outbound::STATE_INTERCEPTED,
            Outbound::STATE_HANDOVER,
        ])) {
            return false;
        }

        $outbound = Outbound::query()->find($outboundId);
        if (!$outbound) {
            return false;
        }
        if ($outbound->state == $state) {
            return true;
        }
        $stateBefore = $outbound->state;
        switch ($state) {
            case Outbound::STATE_FROZEN:
                // 检查是否全部冻结
                $outboundItems = OutboundItem::query()
                    ->where('outbound_id', $outboundId)
                    ->get();
                foreach ($outboundItems as $outboundItem) {
                    $sumFrozenQuantity = OutboundFrozenItem::query()
                        ->where('outbound_id', $outboundItem->outbound_id)
                        ->where('outbound_item_id', $outboundItem->id)
                        ->sum('frozen_quantity');
                    if ($sumFrozenQuantity < $outboundItem->quantity) {
                        return false;
                    }
                }
                break;
        }
        Outbound::query()
            ->where('id', $outboundId)
            ->update([
                'state' => $state,
            ]);

        // 生成 process
        $this->createOutboundProcess(
            $outboundId,
            $state,
            $stateBefore,
            $userId,
            !empty($content) ? $content : 'Swifted automatically'
        );

        return true;

    }

    public function setIsNeedIntercept(int $outboundId, int $isNeedIntercept): bool
    {
        return (bool) Outbound::query()
            ->where('id', $outboundId)
            ->update([
                'is_need_intercept' => $isNeedIntercept,
            ]);
    }

    public function getOutbound(int $warehouseId, string $outboundNumber): ?Outbound
    {
        return Outbound::query()
            ->where('warehouse_id', $warehouseId)
            ->where('outbound_number', $outboundNumber)
            ->orderByDesc('created_at')
            ->first();
    }

    public function getOutboundStateTip(int $state): string
    {

        switch ($state) {
            case Outbound::STATE_CREATED:
                $tip = 'created';
                break;
            case Outbound::STATE_FROZEN:
                $tip = 'frozen';
                break;
            case Outbound::STATE_CANCELLED:
                $tip = 'cancelled';
                break;
            case Outbound::STATE_PICKING_TASK_CREATED:
                $tip = 'picking task generated';
                break;
            case Outbound::STATE_PICKING_ING:
                $tip = 'picking abnormal';
                break;
            case Outbound::STATE_PICKING_ABNORMAL:
                $tip = 'start picking';
                break;
            case Outbound::STATE_PICKING_COMPLETED:
                $tip = 'picking completed';
                break;
            case Outbound::STATE_CHECK_ABNORMAL:
                $tip = 'checking abnormal';
                break;
            case Outbound::STATE_CHECK_COMPLETED:
                $tip = 'checking completed';
                break;
            case Outbound::STATE_WEIGHED:
                $tip = 'weighed';
                break;
            case Outbound::STATE_INTERCEPTED:
                $tip = 'intercepted';
                break;
            case Outbound::STATE_HANDOVER:
                $tip = 'handover';
                break;
            default:
                $tip = 'Unknown';
                break;
        }
        return $tip;
    }

    public function frozenOutboundItems(int $warehouseId, string $skuCode)
    {
        // stock
        // 当前 SKU 未冻结库存
        $sumUnfrozenQuantity = Stock::query()
            ->where('warehouse_id', $warehouseId)
            ->where('storage_type', StorageArea::STORAGE_TYPE_PICKING)
            ->where('sku_code', $skuCode)
            ->sum('unfrozen_quantity');
        if (!$sumUnfrozenQuantity) {
            return true;
        }
        $outboundItems = OutboundItem::query()
            ->leftJoin('outbounds', 'outbounds.id', '=', 'outbound_items.outbound_id')
            ->where('outbound_items.sku_code', $skuCode)
            ->where('outbound_items.is_frozen', OutboundItem::IS_FROZEN_FALSE)
            ->where('outbounds.state', Outbound::STATE_CREATED)
            ->whereNull('outbound_items.deleted_at')
            ->whereNull('outbounds.deleted_at')
            ->get([
                'outbound_items.*'
            ]);
        if ($outboundItems->isEmpty()) {
            return true;
        }
        foreach ($outboundItems as $outboundItem) {
            // 能否冻结
            $sumStockQuantity = Stock::query()
                ->where('storage_type', StorageArea::STORAGE_TYPE_PICKING)
                ->where('sku_code', $outboundItem->sku_code)
                ->sum('unfrozen_quantity');
            if ($sumStockQuantity == 0) {
                break;
            }
            if ($sumStockQuantity < $outboundItem->quantity) {
                // 不够冻结一个 item
                continue;
            }
            $stocks = Stock::query()
                ->where('storage_type', StorageArea::STORAGE_TYPE_PICKING)
                ->where('sku_code', $outboundItem->sku_code)
                ->where('unfrozen_quantity', '>', 0)
                ->orderBy('quantity')
                ->get();
            if ($stocks->isEmpty()) {
                continue;
            }
            $needFrozenQuantity = $outboundItem->quantity;
            foreach ($stocks as $stock) {
                // 冻结库存
                if ($needFrozenQuantity <= $stock->unfrozen_quantity) {
                    $frozenQuantity = $needFrozenQuantity;
                } else {
                    $frozenQuantity = $stock->unfrozen_quantity;
                }
                app(StockRepository::class)->updateStock(
                    $stock->warehouse_id,
                    0,
                    StorageArea::STORAGE_TYPE_PICKING,
                    StockLog::RELATION_TYPE_OUTBOUND_ITEM,
                    $outboundItem->id,
                    $stock->sku_code,
                    $stock->bin,
                    0,
                    $frozenQuantity,
                    -$frozenQuantity
                );
                // 生成冻结记录
                $outboundFrozenItem = OutboundFrozenItem::query()
                    ->where('outbound_item_id', $outboundItem->id)
                    ->where('sku_code', $outboundItem->sku_code)
                    ->where('frozen_bin', $stock->bin)
                    ->first();
                if (!$outboundFrozenItem) {
                    OutboundFrozenItem::create([
                        'outbound_id' => $outboundItem->outbound_id,
                        'outbound_item_id' => $outboundItem->id,
                        'sku_code' => $outboundItem->sku_code,
                        'frozen_bin' => $stock->bin,
                        'frozen_quantity' => $frozenQuantity,
                    ]);
                } else {
                    $outboundFrozenItem->frozen_bin = $stock->bin;
                    $outboundFrozenItem->frozen_quantity = $frozenQuantity;
                    $outboundFrozenItem->save();
                }

                $needFrozenQuantity -= $frozenQuantity;
                if ($needFrozenQuantity == 0) {
                    OutboundItem::query()
                        ->where('id', $outboundItem->id)
                        ->update([
                            'is_frozen' => OutboundItem::IS_FROZEN_TRUE,
                        ]);
                    // update outbound
                    $unfrozenOutboundItemExist = OutboundItem::query()
                        ->where('outbound_id', $outboundItem->outbound_id)
                        ->where('is_frozen', OutboundItem::IS_FROZEN_FALSE)
                        ->exists();
                    if (!$unfrozenOutboundItemExist) {
                        $this->updateState($outboundItem->outbound_id, Outbound::STATE_FROZEN);
                        try {
                            $outbound = Outbound::query()->find($outboundItem->outbound_id);
                            MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_ORDER_STATUS_NOTIFY, ['order_number' => $outbound->order_number]);
                        } catch (\Exception $e) {
                            Log::error('releaseOutbound inQueue Order Status Notify Fail', [
                                'exception' => $e->getMessage(),
                                'trace' => $e->getTrace(),
                            ]);
                        }
                    }
                }
            }
        }
        return true;
    }

    public function getOutboundByOrderNumber(int $warehouseId, string $orderNumber): ?Outbound
    {
        return Outbound::query()
            ->where('warehouse_id', $warehouseId)
            ->where('order_number', $orderNumber)
            ->latest('id')
            ->first();
    }

    public function getOutboundItems(int $outboundId): ?Collection
    {
        return OutboundItem::query()
            ->where('outbound_id', $outboundId)
            ->get();
    }

    public function releaseOutbound(int $outboundId): bool
    {
        $outboundItems = OutboundItem::query()
            ->where('outbound_id', $outboundId)
            ->where('is_frozen', OutboundItem::IS_FROZEN_TRUE)
            ->get();
        if ($outboundItems->isEmpty()) {
            return true;
        }
        $outbound = Outbound::query()->find($outboundId);
        foreach ($outboundItems as $outboundItem) {
            $outboundFrozenItems = OutboundFrozenItem::query()
                ->where('outbound_item_id', $outboundItem->id)
                ->get();
            if ($outboundFrozenItems->isEmpty()) {
                OutboundItem::query()
                    ->where('id', $outboundItem->id)
                    ->update([
                        'is_frozen' => OutboundItem::IS_FROZEN_FALSE,
                    ]);
                continue;
            }
            foreach ($outboundFrozenItems as $outboundFrozenItem) {
                app(StockRepository::class)->updateStock(
                    $outbound->warehouse_id,
                    0,
                    StorageArea::STORAGE_TYPE_PICKING,
                    StockLog::RELATION_TYPE_OUTBOUND,
                    $outboundId,
                    $outboundFrozenItem->sku_code,
                    $outboundFrozenItem->frozen_bin,
                    0,
                    -$outboundFrozenItem->frozen_quantity,
                    $outboundFrozenItem->frozen_quantity
                );
            }

            OutboundItem::query()
                ->where('id', $outboundItem->id)
                ->update([
                    'is_frozen' => OutboundItem::IS_FROZEN_FALSE,
                ]);

            $this->frozenOutboundItems($outbound->warehouse_id, $outboundItem->sku_code);
        }
        return true;
    }

    public function storeHistory(int $outboundId): bool
    {
        $outbound = Outbound::query()->find($outboundId)->toArray();
        $outboundItems = OutboundItem::query()->where('outbound_id', $outboundId)->get()->toArray();
        $outboundFrozenItems = OutboundFrozenItem::query('outbound_id', $outboundId)->get()->toArray();

        return (bool) OutboundHistory::create([
            'outbound_id' => $outboundId,
            'info' => json_encode([
                'outbound' => $outbound,
                'outbound_items' => $outboundItems,
                'outbound_frozen_items' => $outboundFrozenItems,
            ]),
        ]);
    }

    public function updateOutbound(int $outboundId, array $params): bool
    {
        $params['order_created_at'] = Carbon::parse($params['order_created_at'])->timestamp;
        return Outbound::query()
            ->where('id', $outboundId)
            ->update($params);
    }

}
