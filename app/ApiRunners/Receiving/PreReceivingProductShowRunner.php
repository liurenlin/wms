<?php

namespace App\ApiRunners\Receiving;

use App\ApiRunners\ApiRunner;
use App\ApiRunners\Runner;
use App\Repositories\PreReceivingRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PreReceivingProductShowRunner extends Runner implements ApiRunner
{
    public $preReceivingRepository;

    public function __construct(PreReceivingRepository $preReceivingRepository)
    {
        $this->preReceivingRepository = $preReceivingRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $preReceiving = $this->preReceivingRepository->getPreReceivingProductByNumberAndProductId($request->get('pre_receiving_number'), (int) $request->get('product_id'));

        return success($preReceiving->toArray());
    }
}
