<?php

namespace App\ApiRunners\Receiving;

use App\ApiRunners\ApiRunner;
use App\ApiRunners\Runner;
use App\Repositories\ReceivingLogRepository;
use App\Services\CacheService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class StoreReceivingRunner extends Runner implements ApiRunner
{
    public $receivingLogRepository;

    public function __construct(ReceivingLogRepository $receivingLogRepository)
    {
        $this->receivingLogRepository = $receivingLogRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $preReceivingNumber = $request->get('pre_receiving_number');
        // 检查预录入库单状态
        if ($this->receivingLogRepository->isPreReceivingFinished($preReceivingNumber)) {
            return fail(__('receiving.receiving_finished'));
        }
        // 排队检查
        $lock = Cache::lock(CacheService::getReceivingTaskConfirmKey($preReceivingNumber), 2);
        if (!$lock->get()) {
            return fail(__('receiving.receiving_busy'));
        }
        // 检查是否已经被入库
        $actualReceiving = $this->receivingLogRepository->getActualReceivingByPreReceivingNumber($preReceivingNumber);
        if ($actualReceiving) {
            $lock->release();
            return success([
                'actual_receiving_id' => $actualReceiving->id,
                'actual_receiving_number' => $actualReceiving->actual_receiving_number,
            ]);
        }
        $userId = Auth::id();
        // 生成实际入库单
        $actualReceiving = $this->receivingLogRepository->createActualReceiving($userId, $preReceivingNumber);

        if (!$actualReceiving) {
            $lock->release();
            return fail();
        }
        $lock->release();
        return success([
            'actual_receiving_id' => $actualReceiving->id,
            'actual_receiving_number' => $actualReceiving->actual_receiving_number,
        ]);
    }
}
