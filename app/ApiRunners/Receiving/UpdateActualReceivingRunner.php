<?php

namespace App\ApiRunners\Receiving;

use App\ApiRunners\ApiRunner;
use App\ApiRunners\Runner;
use App\Models\ActualReceiving;
use App\Models\OpenMessageQueue;
use App\Repositories\ReceivingLogRepository;
use App\Services\MessageService;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class UpdateActualReceivingRunner extends Runner implements ApiRunner
{
    public $receivingLogRepository;

    public function __construct(ReceivingLogRepository $receivingLogRepository)
    {
        $this->receivingLogRepository = $receivingLogRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $actualReceivingId = (int) $request->input('actual_receiving_id');
        $state = (int) $request->get('state');

        $actualReceiving = $this->receivingLogRepository->getActualReceivingById($actualReceivingId);
        if (!$actualReceiving) {
            return fail(__('receiving.actual_receiving_not_exist'));
        }
        // 检查状态
        if ($actualReceiving->state == ActualReceiving::STATE_COMPLETED) {
            return success();
        }
        // 检查归属
        $userId = Auth::id();
        if ($actualReceiving->user_id != $userId) {
            $actualReceivingUser = User::query()->find($actualReceiving->user_id);
            return fail("Error! You're not the starter, ask " . $actualReceivingUser->name . " to finish receiving.");
        }

        // 更新状态
        $res = $this->receivingLogRepository->updateActualReceivingState($actualReceivingId, $state);
        if (!$res) {
            return fail();
        }

        if ($state == ActualReceiving::STATE_COMPLETED) {
            $this->receivingLogRepository->completedPreReceiving($actualReceiving->pre_receiving_id);
            try {
                // 通知 oms
                MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_IN_WARE, ['actual_receiving_id' => $actualReceiving->id]);
            } catch (\Exception $e) {
                Log::error('OmsInWareEvent Fail', [
                    'exception' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ]);
            }
        }
        return success();
    }
}
