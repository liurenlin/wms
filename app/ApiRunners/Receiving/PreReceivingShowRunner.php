<?php

namespace App\ApiRunners\Receiving;

use App\ApiRunners\ApiRunner;
use App\ApiRunners\Runner;
use App\Repositories\PreReceivingRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PreReceivingShowRunner extends Runner implements ApiRunner
{
    public $preReceivingRepository;

    public function __construct(PreReceivingRepository $preReceivingRepository)
    {
        $this->preReceivingRepository = $preReceivingRepository;
    }

    public function run(Request $request): JsonResponse
    {
        if (empty($request->get('pre_receiving_number'))) {
            return fail(__('receiving.pre_receiving_number_wrong'));
        }
        $preReceiving = $this->preReceivingRepository->getPrereceivingByNumber($request->get('pre_receiving_number'));

        return success($preReceiving);
    }
}
