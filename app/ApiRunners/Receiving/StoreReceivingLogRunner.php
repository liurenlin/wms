<?php

namespace App\ApiRunners\Receiving;

use App\ApiRunners\ApiRunner;
use App\ApiRunners\Runner;
use App\Models\Abnormal;
use App\Models\StockLog;
use App\Models\StorageArea;
use App\Repositories\AbnormalRepository;
use App\Repositories\PreReceivingRepository;
use App\Repositories\ReceivingLogRepository;
use App\Repositories\StockRepository;
use App\Services\UserService;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StoreReceivingLogRunner extends Runner implements ApiRunner
{
    public $receivingLogRepository;
    public $abnormalRepository;
    public $preReceivingRepository;
    public $stockRepository;

    public function __construct(
        ReceivingLogRepository $receivingLogRepository,
        AbnormalRepository $abnormalRepository,
        PreReceivingRepository $preReceivingRepository,
        StockRepository $stockRepository
    ) {
        $this->receivingLogRepository = $receivingLogRepository;
        $this->abnormalRepository = $abnormalRepository;
        $this->preReceivingRepository = $preReceivingRepository;
        $this->stockRepository = $stockRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $userId = Auth::id();
        $warehouseId = UserService::getWarehouseId($userId);
        $preReceivingNumber = $request->get('pre_receiving_number');
        $receivingCartCode = $request->get('receiving_cart_code');
        $actualReceivingId = (int) $request->get('actual_receiving_id');
        $skuCode = $request->get('sku_code');
        // 检查非同一个预录入库单的sku
        if ($this->preReceivingRepository->hasSameSkuInReceivingCart($warehouseId, $actualReceivingId, $skuCode, $receivingCartCode)) {
            return fail(__('receiving.receiving_duplicated_sku_in_cart'));
        }

        $preReceivingItem = $this->preReceivingRepository->getPreReceivingItem($preReceivingNumber, $skuCode);
        if (!$preReceivingItem) {
            return fail(__('receiving.sku_not_in_pre_receiving'));
        }

        // 检查收货车是否可用
        if (!$this->receivingLogRepository->isReceivingCartFree($receivingCartCode)) {
            $lastPutawayTask = $this->receivingLogRepository->getLatestPutawayTask($receivingCartCode);
            $userName = $lastPutawayTask ? (User::query()->where('id', $lastPutawayTask->user_id)->value('name') ?? '') : 'Some one';
            return fail('Error! ' . $userName . ' is doing the putaway task, please use another cart.”');
        }

        // 检查当前实际入库单是否完成
        if ($this->preReceivingRepository->isActualReceivingCompleted($actualReceivingId)) {
            return fail(__('receiving.actual_receiving_finished'));
        }

        $params = $request->all();
        $params['pre_receiving_id'] = $preReceivingItem->pre_receiving_id;

        DB::beginTransaction();
        try {
            // sku 加车
            $recevingCartItem = $this->receivingLogRepository->createReceivingCartItem($warehouseId, $params['receiving_cart_code'], $params['sku_code'], $params['quantity']);
            // 生成收货日志
            $receivingLog = $this->receivingLogRepository->createReceivingLog($userId, $warehouseId, $params, $recevingCartItem->id);
            // 生成实际入库单 items
            $this->receivingLogRepository->createOrUpdateActualReceivingItem($params);
            // 增加收货暂存区库存
            $this->stockRepository->updateStock(
                $warehouseId,
                $userId,
                StorageArea::STORAGE_TYPE_RECEIVING,
                StockLog::RELATION_TYPE_RECEIVING_LOG,
                $receivingLog->id,
                $skuCode,
                '',
                (int) $params['quantity']
            );
            // 判断是否存在多件异常
            $totalReceivingQuantity = $this->receivingLogRepository->getSkuTotalReceivingQuantity($preReceivingItem->pre_receiving_id, $params['sku_code']);
            if ($totalReceivingQuantity > $preReceivingItem->quantity) {
                $actualReceiving = $this->receivingLogRepository->getActualReceivingById($actualReceivingId);
                // 创建多件异常
                $abnormal = $this->abnormalRepository->createAbnormal(
                    $warehouseId,
                    $userId,
                    Abnormal::RELATION_TYPE_ACTUAL_RECEIVING,
                    $actualReceiving->actual_receiving_number,
                    '',
                    $preReceivingNumber,
                    '',
                    Abnormal::REASON_EXTRA, // 多件
                    Abnormal::STATE_CREATED
                );
                $skus[] = [
                    'sku_code' => $skuCode,
                    'quantity' => $totalReceivingQuantity - $preReceivingItem->quantity,
                ];
                $this->abnormalRepository->createAbnormalItems($abnormal->id, $skus);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('StoreReceivingLogRunner Fail', [
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
            return fail();
        }

        return success();

    }
}
