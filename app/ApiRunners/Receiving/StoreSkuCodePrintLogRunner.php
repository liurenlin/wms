<?php

namespace App\ApiRunners\Receiving;

use App\ApiRunners\ApiRunner;
use App\ApiRunners\Runner;
use App\Repositories\ReceivingLogRepository;
use App\Services\CacheService;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class StoreSkuCodePrintLogRunner extends Runner implements ApiRunner
{
    public $receivingLogRepository;

    public function __construct(ReceivingLogRepository $receivingLogRepository)
    {
        $this->receivingLogRepository = $receivingLogRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $userId = Auth::id();
        $warehouseId = UserService::getWarehouseId($userId);

        $preReceivingId = (int) $request->get('pre_receiving_id');
        $skuCode = $request->get('sku_code');
        $quantity = (int) $request->get('quantity');

        // 限流，每秒钟同一个用户，同一个sku只能打印一次
        $lock = Cache::lock(CacheService::getSkuCodePrintLogKey($userId, $warehouseId, $skuCode, $preReceivingId), 1);
        if (!$lock->get()) {
            return fail(__('receiving.sku_code_print_is_busy'));
        }

        // 生成打印记录
        $res = $this->receivingLogRepository->createSkuCodePrintLog($userId, $warehouseId, $preReceivingId, $skuCode, $quantity);

        if (!$res) {
            $lock->release();
            return fail();
        }
        // 释放锁
        $lock->release();
        return success();
    }
}
