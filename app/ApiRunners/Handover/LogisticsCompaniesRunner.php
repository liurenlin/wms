<?php

namespace App\ApiRunners\Handover;

use App\ApiRunners\ApiRunner;
use App\ApiRunners\Runner;
use App\Repositories\LogisticsCompanyRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class LogisticsCompaniesRunner extends Runner implements ApiRunner
{
    public $logisticsCompanyRepository;

    public function __construct(LogisticsCompanyRepository $logisticsCompanyRepository)
    {
        $this->logisticsCompanyRepository = $logisticsCompanyRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $logisticsCompanies = $this->logisticsCompanyRepository->all([
            'id',
            'company_name as name',
        ]);
        if ($logisticsCompanies->isNotEmpty()) {
            $logisticsCompanies = $logisticsCompanies->toArray();
        } else {
            $logisticsCompanies = [];
        }
        return success(['companies' => $logisticsCompanies]);
    }
}
