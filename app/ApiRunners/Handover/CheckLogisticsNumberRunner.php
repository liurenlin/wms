<?php

namespace App\ApiRunners\Handover;

use App\ApiRunners\ApiRunner;
use App\ApiRunners\Runner;
use App\Models\HandoverItem;
use App\Repositories\OutboundRepository;
use App\Repositories\WeighRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CheckLogisticsNumberRunner extends Runner implements ApiRunner
{
    public $weighRepository;
    public $outboundRepository;

    public function __construct(WeighRepository $weighRepository, OutboundRepository $outboundRepository)
    {
        $this->weighRepository = $weighRepository;
        $this->outboundRepository = $outboundRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $logisticsNumber = $request->get('logistics_number');
        $weigh = $this->weighRepository->getWeighByLogistcsNumber($logisticsNumber);
        if (!$weigh) {
            return fail('Error! Wrong order status, don\'t ship it ');
        }

        // 检查是否存在重复物流单号
        $handoverItemsCount = HandoverItem::query()
            ->where('logistics_number', $logisticsNumber)
            ->count();
        if ($handoverItemsCount) {
            return fail('Error! Duplicated shipping #, don\'t ship it');
        }
        return success();
    }

    private function getStateTip(int $state)
    {
        $tip = 'Unknown';
        switch ($state) {
            case Outbound::STATE_CREATE:
                $tip = 'Created';
                break;
            case Outbound::STATE_FROZEN:
                $tip = 'Frozen';
                break;
            case Outbound::STATE_PICKING_ING:
                $tip = 'Picking';
                break;
            case Outbound::STATE_PICKING_ABNORMAL:
                $tip = 'Picking abnormal';
                break;
            case Outbound::STATE_PICKING_COMPLETED:
                $tip = 'Picking completed';
                break;
            case Outbound::STATE_WEIGHED:
                $tip = 'Weighed';
                break;
            case Outbound::STATE_INTERCEPTED:
                $tip = 'Intercepted';
                break;
            case Outbound::STATE_HANDOVER:
                $tip = 'Handover';
                break;
        }

        return $tip;

    }
}
