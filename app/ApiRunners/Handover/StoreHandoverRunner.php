<?php

namespace App\ApiRunners\Handover;

use App\ApiRunners\ApiRunner;
use App\ApiRunners\Runner;
use App\Consts\CommonConst;
use App\Consts\NumberPrefixConst;
use App\Models\ActualReceiving;
use App\Models\Handover;
use App\Models\HandoverItem;
use App\Models\OpenMessageQueue;
use App\Models\Outbound;
use App\Models\OutboundItem;
use App\Models\PickingTask;
use App\Models\StockLog;
use App\Models\StorageArea;
use App\Models\Weigh;
use App\Repositories\LogisticsCompanyRepository;
use App\Repositories\OutboundRepository;
use App\Repositories\PickingTaskRepository;
use App\Repositories\StockRepository;
use App\Services\MessageService;
use App\Services\UserService;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StoreHandoverRunner extends Runner implements ApiRunner
{
    public $logisticsCompanyRepository;
    public $pickingTaskRepository;
    public $stockRepository;
    public $outboundRepository;

    public function __construct(
        LogisticsCompanyRepository $logisticsCompanyRepository,
        PickingTaskRepository $pickingTaskRepository,
        StockRepository $stockRepository,
        OutboundRepository $outboundRepository
    )
    {
        $this->logisticsCompanyRepository = $logisticsCompanyRepository;
        $this->pickingTaskRepository = $pickingTaskRepository;
        $this->stockRepository = $stockRepository;
        $this->outboundRepository = $outboundRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $userId = Auth::id();
        $warehouseId = UserService::getWarehouseId($userId);
        $logisticsNumbers = json_decode($request->get('logistics_numbers'), JSON_OBJECT_AS_ARRAY);
        // 检查是否都有称重
        foreach ($logisticsNumbers as $logisticsNumber) {
            $weighExist = Weigh::query()
                ->where('warehouse_id', $warehouseId)
                ->whereIn('logistics_number', $logisticsNumber)
                ->exists();
            if (!$weighExist) {
                return fail('Error! Wrong order status, don\'t ship it ');
            }
        }

        // 检查是否出库是否完成
        $outboundsCount = Outbound::query()
            ->whereIn('logistics_number', Arr::pluck($logisticsNumbers, 'logistics_number'))
            ->where('state', Outbound::STATE_WEIGHED)
            ->count();
        if ($outboundsCount != count($logisticsNumbers)) {
            return fail('Error! Wrong order status, don\'t ship it ');
        }

        // 检查是否存在重复物流单号
        $handoverItemsCount = HandoverItem::query()
            ->whereIn('logistics_number', Arr::pluck($logisticsNumbers, 'logistics_number'))
            ->count();
        if ($handoverItemsCount) {
            return fail('Error! Duplicated shipping #, don\'t ship it');
        }

        DB::beginTransaction();
        try {
            // 生成交接单
            $handover = Handover::create([
                'handover_number' => $this->generateHandoverNumber(),
                'logistics_company_id' => $request->get('logistics_company_id'),
                'signature' => $request->get('signature'),
                'user_id' => $userId,
            ]);
            // 生成交接单 item
            foreach ($logisticsNumbers as $logisticsNumber) {
                $weight = Weigh::query()
                    ->where('warehouse_id', $warehouseId)
                    ->where('logistics_number', $logisticsNumber['logistics_number'])
                    ->orderByDesc('created_at')
                    ->first();
                $handoverItem = HandoverItem::create([
                    'handover_id' => $handover->id,
                    'logistics_number' => $logisticsNumber['logistics_number'],
                    'weight' => $weight->weight,
                    'quantity' => $logisticsNumber['quantity'],
                ]);

                $outbound = Outbound::query()
                    ->where('logistics_number', $logisticsNumber['logistics_number'])
                    ->first();
                $this->outboundRepository->updateState(
                    $outbound->id,
                    Outbound::STATE_HANDOVER,
                    $userId,
                    'Handover report number ' . $handover->handover_number
                );

                $outboundItems = OutboundItem::query()
                    ->where('outbound_id', $outbound->id)
                    ->get();

                foreach ($outboundItems as $outboundItem) {
                    // 更新出库暂存区库存
                    $this->stockRepository->updateStock(
                        $warehouseId,
                        $userId,
                        StorageArea::STORAGE_TYPE_OUTBOUND,
                        StockLog::RELATION_TYPE_HANDOVER_ITEM,
                        $handoverItem->id,
                        $outboundItem->sku_code,
                        '',
                        -$outboundItem->quantity
                    );
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('StoreHandover Fail', [
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
            return fail();
        }
        MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_ORDER_STATUS_NOTIFY, ['order_number' => $outbound->order_number]);
        return success(['id' => $handover->id]);
    }

    private function generateHandoverNumber(): string
    {
        $handoverCount = Handover::query()
            ->whereBetween('created_at', [
                Carbon::today()->startOfDay()->timestamp,
                Carbon::today()->endOfDay()->timestamp,
            ])
            ->count();
        $handoverCount = $handoverCount + 1;
        $number = NumberPrefixConst::HANDOVER . date('Ymd') . sprintf('%03s', $handoverCount);
        $exist = Handover::query()
            ->where('handover_number', $number)
            ->count();
        if ($exist) {
            return self::generateHandoverNumber();
        }
        return $number;
    }
}
