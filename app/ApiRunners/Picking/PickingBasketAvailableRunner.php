<?php

namespace App\ApiRunners\Picking;

use App\ApiRunners\ApiRunner;
use App\ApiRunners\Runner;
use App\Models\ReceivingCart;
use App\Repositories\PickingTaskRepository;
use App\Services\CacheService;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PickingBasketAvailableRunner extends Runner implements ApiRunner
{
    public $pickingTaskRepository;

    public function __construct(PickingTaskRepository $pickingTaskRepository)
    {
        $this->pickingTaskRepository = $pickingTaskRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $userId = Auth::id();
        $warehouseId = UserService::getWarehouseId($userId);
        $basketCode = $request->get('basket_code');
        // 拉去用户未完成的拣货任务
        $errorBaskets = $this->pickingTaskRepository->checkBasketsExist($warehouseId, [['code' => $basketCode]]);
        if (!empty($errorBaskets)) {
            return fail('Error! Basket ' . implode(', ', $errorBaskets) . ' is not recorded in the system');
        }
        $errorBaskets = $this->pickingTaskRepository->checkBaskets($warehouseId, [['code' => $basketCode]]);
        if (!empty($errorBaskets)) {
            return fail('Error! Basket ' . implode(', ', $errorBaskets) . (count($errorBaskets) > 1 ? 'are' : 'is') . ' bound to another order');
        }
        return success();

    }
}
