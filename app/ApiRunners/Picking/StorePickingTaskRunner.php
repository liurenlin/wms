<?php

namespace App\ApiRunners\Picking;

use App\ApiRunners\ApiRunner;
use App\ApiRunners\Runner;
use App\Models\OpenMessageQueue;
use App\Models\PickingTaskBasket;
use App\Models\ReceivingCart;
use App\Repositories\OutboundRepository;
use App\Repositories\PickingTaskRepository;
use App\Services\CacheService;
use App\Services\MessageService;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StorePickingTaskRunner extends Runner implements ApiRunner
{
    public $pickingTaskRepository;
    public $outboundRepository;

    public function __construct(
        PickingTaskRepository $pickingTaskRepository,
        OutboundRepository $outboundRepository
    )
    {
        $this->pickingTaskRepository = $pickingTaskRepository;
        $this->outboundRepository = $outboundRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $userId = Auth::id();
        $warehouseId = UserService::getWarehouseId($userId);

        // 检查篮子参数
        $baskets = json_decode($request->get('baskets'), JSON_OBJECT_AS_ARRAY);
        if (empty($baskets)) {
            return fail(__('picking.baskets_empty'));
        }
        $errorBaskets = $this->pickingTaskRepository->checkBasketsExist($warehouseId, $baskets);
        if (!empty($errorBaskets)) {
            return fail('Error! Basket ' . implode(', ', $errorBaskets) . ' is not recorded in the system');
        }
        $errorBaskets = $this->pickingTaskRepository->checkBaskets($warehouseId, $baskets);
        if (!empty($errorBaskets)) {
            return fail('Error! Basket ' . implode(', ', $errorBaskets) . (count($errorBaskets) > 1 ? 'are' : 'is') . ' bound to another order');
        }
        // 排队检查
        $lock = Cache::lock(CacheService::getPickingTaskConfirmKey($warehouseId), 2);
        if (!$lock->get()) {
            return fail(__('picking.picking_busy'));
        }

        // 智能推荐任务
        $suggestPickingTaskItems = $this->pickingTaskRepository->suggestPickingTaskItems($warehouseId, count($baskets));
        if (empty($suggestPickingTaskItems)) {
            return fail(__('picking.no_outbound_available'));
        }

        DB::beginTransaction();
        try {
            // 生成任务清单
            $pickingTask = $this->pickingTaskRepository->createPickingTask($warehouseId, $userId, $baskets, $suggestPickingTaskItems);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $lock->release();
            Log::error('StorePickingTask Fail', [
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
            return fail($e->getMessage());
        }
        $lock->release();
        // 通知订单状态变化
        MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_ORDER_STATUS_NOTIFY, ['picking_task_id' => $pickingTask['id']]);
        // 首个推荐
        return success($pickingTask);

    }
}
