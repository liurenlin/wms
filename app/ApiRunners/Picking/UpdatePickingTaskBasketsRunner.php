<?php

namespace App\ApiRunners\Picking;

use App\ApiRunners\ApiRunner;
use App\ApiRunners\Runner;
use App\Models\PickingTask;
use App\Repositories\PickingTaskRepository;
use App\Services\CacheService;
use App\Services\UserService;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UpdatePickingTaskBasketsRunner extends Runner implements ApiRunner
{
    public $pickingTaskRepository;

    public function __construct(PickingTaskRepository $pickingTaskRepository)
    {
        $this->pickingTaskRepository = $pickingTaskRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $userId = Auth::id();
        $warehouseId = UserService::getWarehouseId($userId);
        $pickingTaskId = (int) $request->input('picking_task_id');

        // 检查拣货任务归属
        $pickingTask = PickingTask::query()->find($pickingTaskId);
        if (!$pickingTask || $pickingTask->user_id != $userId) {
            return fail('Error! This task is assigned to ' . (User::query()->where('id', $pickingTask->user_id)->value('name') ?? ''));
        }

        // 检查篮子参数
        $baskets = json_decode($request->get('baskets'), JSON_OBJECT_AS_ARRAY);
        if (empty($baskets)) {
            return fail(__('picking.baskets_empty'));
        }
        $errorBaskets = $this->pickingTaskRepository->checkBasketsExist($warehouseId, $baskets);
        if (!empty($errorBaskets)) {
            return fail('Error! Basket ' . implode(', ', $errorBaskets) . ' is not recorded in the system');
        }
        $errorBaskets = $this->pickingTaskRepository->checkBaskets($warehouseId, $baskets);
        if (!empty($errorBaskets)) {
            return fail('Error! Basket ' . implode(', ', $errorBaskets) . (count($errorBaskets) > 1 ? 'are' : 'is') . ' bound to another order');
        }

        // 检查任务篮子是否已经绑定
        $pickingTaskBaskets = $this->pickingTaskRepository->getPickingTaskBaskets($pickingTaskId);
        if (empty($pickingTaskBaskets)) {
            return fail(__('picking.picking_task_wrong'));
        }

        $basketNotBoundCount = collect($pickingTaskBaskets)->where('basket_code', '')->count();
        if ($basketNotBoundCount == 0) {
            return fail(__('picking.baskets_already_bound'));
        }
        $basketCount = count($baskets);
        if ($basketNotBoundCount != $basketCount) {
            return fail(__('picking.baskets_wrong'));
        }

        // 排队检查
        $lock = Cache::lock(CacheService::getPickingTaskConfirmKey($warehouseId), 2);
        if (!$lock->get()) {
            return fail(__('picking.picking_busy'));
        }
        DB::beginTransaction();
        try {
            // 绑定篮子
            $taskItems = $this->pickingTaskRepository->boundBaskets($baskets);
            // 更新拣货任务状态
            $this->pickingTaskRepository->updateTaskState($warehouseId, $userId, $pickingTaskId, PickingTask::STATE_IN_PROGRESS);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $lock->release();
            Log::error('StorePickingTask Fail', [
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
            return fail();
        }
        $lock->release();
        // 任务详情

        return success(['task_items' => $taskItems]);

    }
}
