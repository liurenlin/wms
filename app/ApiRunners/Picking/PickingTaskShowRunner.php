<?php

namespace App\ApiRunners\Picking;

use App\ApiRunners\ApiRunner;
use App\ApiRunners\Runner;
use App\Repositories\PickingTaskRepository;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PickingTaskShowRunner extends Runner implements ApiRunner
{
    public $pickingTaskRepository;

    public function __construct(PickingTaskRepository $pickingTaskRepository)
    {
        $this->pickingTaskRepository = $pickingTaskRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $userId = Auth::id();
        $warehouseId = UserService::getWarehouseId($userId);

        $pickingTaskId = (int) $request->get('picking_task_id');

        // 检查任务
        $pickingTask = $this->pickingTaskRepository->getUserPickingTask($pickingTaskId, $warehouseId, $userId);
        if (!$pickingTask) {
            return fail(__('picking.picking_task_wrong'));
        }
        $taskItems = $this->pickingTaskRepository->getPickingTaskItems($pickingTaskId, $pickingTask->task_number);

        // 首个推荐
        return success(['task_items' => $taskItems]);
    }
}
