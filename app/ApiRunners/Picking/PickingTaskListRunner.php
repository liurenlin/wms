<?php

namespace App\ApiRunners\Picking;

use App\ApiRunners\ApiRunner;
use App\ApiRunners\Runner;
use App\Models\ReceivingCart;
use App\Repositories\PickingTaskRepository;
use App\Services\CacheService;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PickingTaskListRunner extends Runner implements ApiRunner
{
    public $pickingTaskRepository;

    public function __construct(PickingTaskRepository $pickingTaskRepository)
    {
        $this->pickingTaskRepository = $pickingTaskRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $userId = Auth::id();
        $warehouseId = UserService::getWarehouseId($userId);
        // 拉去用户未完成的拣货任务
        $tasks = $this->pickingTaskRepository->getUserUnFinishedPickingTasks($warehouseId, $userId);
        return success([
            'list' => $tasks,
        ]);

    }
}
