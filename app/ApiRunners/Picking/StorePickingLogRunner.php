<?php

namespace App\ApiRunners\Picking;

use App\ApiRunners\ApiRunner;
use App\ApiRunners\Runner;
use App\Models\OpenMessageQueue;
use App\Models\Outbound;
use App\Models\PickingTask;
use App\Models\StockLog;
use App\Models\StorageArea;
use App\Repositories\OutboundRepository;
use App\Repositories\PickingTaskRepository;
use App\Repositories\PutawayTaskRepository;
use App\Repositories\StockRepository;
use App\Services\CacheService;
use App\Services\MessageService;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StorePickingLogRunner extends Runner implements ApiRunner
{
    public $pickingTaskRepository;
    public $putawayTaskRepository;
    public $outboundRepository;
    public $stockRepository;

    public function __construct(
        PickingTaskRepository $pickingTaskRepository,
        PutawayTaskRepository $putawayTaskRepository,
        OutboundRepository $outboundRepository,
        StockRepository $stockRepository
    )
    {
        $this->pickingTaskRepository = $pickingTaskRepository;
        $this->putawayTaskRepository = $putawayTaskRepository;
        $this->outboundRepository = $outboundRepository;
        $this->stockRepository = $stockRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $userId = Auth::id();
        $warehouseId = UserService::getWarehouseId($userId);
        $pickingTaskItemId = $request->get('picking_task_item_id');
        $skuCode = $request->get('sku_code');
        $quantity = $request->get('quantity');
        $bin = $request->get('bin');
        $confirmFinished = $request->get('confirm_finished');

        if (!$this->pickingTaskRepository->checkPickingLogParams($warehouseId, $userId, $pickingTaskItemId, $skuCode, $quantity)) {
            return fail(__('picking.picking_log_params_wrong'));
        }

        // 排队检查
        $lock = Cache::lock(CacheService::getPickingLogKey($pickingTaskItemId), 2);
        if (!$lock->get()) {
            return fail(__('receiving.receiving_busy'));
        }

        $pickingTaskBasket = $this->pickingTaskRepository->getPickingTaskBasketByPickingTaskItemId($pickingTaskItemId);
        $outbound = $this->outboundRepository->getOutbound($warehouseId, $pickingTaskBasket->outbound_number);
        $outboundState = $outbound->state;

        DB::beginTransaction();
        try {
            if ($quantity > 0) {
                // 生成日志
                $pickingTaskLog = $this->pickingTaskRepository->createPickingLog($warehouseId, $userId, $pickingTaskItemId, $skuCode, $quantity, $bin);

                // 更新 picking_task_item
                $this->pickingTaskRepository->incrementPickingTaskItemPickingQuantity($pickingTaskItemId, $quantity);

                // 更新出库暂存区库存情况
                $this->stockRepository->updateStock(
                    $warehouseId,
                    $userId,
                    StorageArea::STORAGE_TYPE_PACKAGE,
                    StockLog::RELATION_TYPE_PICKING_LOG,
                    $pickingTaskLog->id,
                    $skuCode,
                    '',
                    $quantity
                );

                // 更新良品库位区库存情况
                $this->stockRepository->updateStock(
                    $warehouseId,
                    $userId,
                    StorageArea::STORAGE_TYPE_PICKING,
                    StockLog::RELATION_TYPE_PICKING_LOG,
                    $pickingTaskLog->id,
                    $skuCode,
                    $bin,
                    -$quantity,
                    -$quantity
                );

                if ($outbound->state == Outbound::STATE_PICKING_TASK_CREATED) {
                    $this->outboundRepository->updateState($outbound->id, Outbound::STATE_PICKING_ING, $userId);
                }

            }

            // 跟新拣货任务
            if ($confirmFinished) {
                $pickingTask = $this->pickingTaskRepository->getPickingTaskByPickingTaskItemId($pickingTaskItemId);
                $this->pickingTaskRepository->updateTaskState($warehouseId, $userId, $pickingTask->id, PickingTask::STATE_DONE);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $lock->release();
            Log::error('StorePickingLog Fail', [
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
            return fail();
        }
        $lock->release();
        if ($outboundState == Outbound::STATE_PICKING_TASK_CREATED) {
            MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_ORDER_STATUS_NOTIFY, ['order_number' => $outbound->order_number]);
        }
        $list[] = [
            'sku_code' => $skuCode,
        ];
        MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_STOCK_NOTIFY, ['warehouse_id' => $warehouseId, 'order_number' => $outbound->order_number, 'list' => $list]);
        return success();

    }
}
