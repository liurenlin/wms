<?php

namespace App\ApiRunners\File;

use App\ApiRunners\ApiRunner;
use App\ApiRunners\Runner;
use App\Services\UploadService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class UploadImageRunner extends Runner implements ApiRunner
{

    public function run(Request $request): JsonResponse
    {
        if (app()->environment() == 'local') {
            return success(['url' => 'https://xxx.png']);
        }
        try {
            $url = UploadService::uploadAbnormalImage($request->get('content'));
            if (!$url) {
                throw new \Exception('UploadService::uploadAbnormalImage Fail');
            }
        } catch (\Exception $e) {
            Log::error('UploadImageRunner Run Fail', [
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
            return fail();
        }

        return success(['url' => $url]);
    }
}
