<?php

namespace App\ApiRunners\Abnormal;

use App\ApiRunners\ApiRunner;
use App\ApiRunners\Runner;
use App\Consts\CommonConst;
use App\Models\Abnormal;
use App\Models\CheckTask;
use App\Models\OpenMessageQueue;
use App\Models\Outbound;
use App\Models\StockLog;
use App\Models\StorageArea;
use App\Repositories\AbnormalRepository;
use App\Repositories\CheckTaskRepository;
use App\Repositories\OutboundRepository;
use App\Repositories\PickingTaskRepository;
use App\Repositories\PutawayTaskRepository;
use App\Repositories\ReceivingLogRepository;
use App\Repositories\ResourceRepository;
use App\Repositories\StockRepository;
use App\Services\MessageService;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StoreRunner extends Runner implements ApiRunner
{
    public $abnormalRepository;
    public $stockRepository;
    public $resourceRepository;
    public $putawayTaskRepository;
    public $pickingTaskRepository;
    public $checkTaskRepository;
    public $outboundRepository;
    public $receivingLogRepository;

    public function __construct(
        AbnormalRepository $abnormalRepository,
        StockRepository $stockRepository,
        ResourceRepository $resourceRepository,
        PutawayTaskRepository $putawayTaskRepository,
        PickingTaskRepository $pickingTaskRepository,
        CheckTaskRepository $checkTaskRepository,
        OutboundRepository $outboundRepository,
        ReceivingLogRepository $receivingLogRepository
    ) {
        $this->abnormalRepository = $abnormalRepository;
        $this->stockRepository = $stockRepository;
        $this->resourceRepository = $resourceRepository;
        $this->putawayTaskRepository = $putawayTaskRepository;
        $this->pickingTaskRepository = $pickingTaskRepository;
        $this->checkTaskRepository = $checkTaskRepository;
        $this->outboundRepository = $outboundRepository;
        $this->receivingLogRepository = $receivingLogRepository;

    }

    public function run(Request $request): JsonResponse
    {
        $userId = Auth::id();
        $warehouseId = UserService::getWarehouseId($userId);
        $relationType = $request->get('relation_type');
        $relationNumber = $request->get('relation_number');
        $skuCode = (string)$request->get('sku_code');
        $abnormalCode = (string)$request->get('abnormal_code');
        $reason = (string)$request->get('reason');
        $image = (string)$request->get('image');
        $quantity = (int)$request->get('quantity');
        $bin = $request->get('bin', '');
        $basketCode = $request->get('basket_code', '');

        $preReceivingNumber = '';
        $outboundNumber = '';

        if ($relationType == Abnormal::RELATION_TYPE_ACTUAL_RECEIVING) {
            if (in_array($reason, [
                    Abnormal::REASON_BROKEN,
                    Abnormal::REASON_STAINS,
                    Abnormal::REASON_COLOR_SHADING,
                ]) && !$skuCode) {
                return fail('The sku code cannot be empty');
            }
            $actualReceiving = $this->receivingLogRepository->getActualReceivingByNumber($relationNumber);
            $preReceiving = $this->receivingLogRepository->getPreReceivingById($actualReceiving->pre_receiving_id);
            $preReceivingNumber = $preReceiving->pre_receiving_number;
        }

        if ($relationType == Abnormal::RELATION_TYPE_PUTAWAY) {
            $putawayTask = $this->putawayTaskRepository->getPutawayTaskByNumber($relationNumber);
            $putawayTaskItem = $this->putawayTaskRepository->getPutawayTaskItemBySkuCode($putawayTask->id, $skuCode);
            if (!$putawayTaskItem) {
                $reason = Abnormal::REASON_WRONG;
            } else {
                if ($quantity) {
                    $reason = Abnormal::REASON_MISSING;
                    $quantity = $putawayTaskItem->receiving_quantity - $putawayTaskItem->putaway_quantity;
                } else {
                    $reason = Abnormal::REASON_EXTRA;
                }
            }
            if (
                in_array($reason, [Abnormal::REASON_WRONG, Abnormal::REASON_EXTRA])
                && !$this->abnormalRepository->checkAbnormalBin($warehouseId, $bin)
            ) {
                return fail(__('abnormal.not_abnormal_bin'));
            }
        }

        if ($relationType == Abnormal::RELATION_TYPE_CHECK) {
            $checkTask = $this->checkTaskRepository->getCheckTaskByNumber($relationNumber);
            $outboundNumber = $checkTask->outbound_number;
            if ($skuCode) {
                $reason = Abnormal::REASON_FAILED;
            } else {
                $reason = Abnormal::REASON_DEFECTIVE;
            }
        }

        if (!$quantity) {
            $quantity = 1;
        }

        if ($relationType == Abnormal::RELATION_TYPE_PICKING) {
            if (!$basketCode) {
                return fail('Err! Basket is needed!');
            }
            // 出库单号
            $pickingTask = $this->pickingTaskRepository->getPickingTaskByNumber($relationNumber);
            $pickingTaskBasket = $this->pickingTaskRepository->getPickingTaskBasket($pickingTask->id, $basketCode);
            $outboundNumber = $pickingTaskBasket->outbound_number;
        }

        $stockNotify = [];
        $outbound = null;

        DB::beginTransaction();
        try {
            $resourceId = 0;
            if ($image) {
                $resource = $this->resourceRepository->createResource($image);
                $resourceId = $resource->id;
            }

            $abnormal = null;

            // 拣货异常特殊处理
            if ($relationType == Abnormal::RELATION_TYPE_PICKING) {
                $quantity = 1;
                // 矫正库位
                $stock = $this->stockRepository->getStockByBin($warehouseId, $bin);
                if ($stock && $stock->sku_code == $skuCode) {
                    // 减少 $quantity 量
                    $updateQuantity = 0;
                    $updateFrozenQuantity = 0;
                    $updateUnfrozenQuantity = 0;
                    if ($stock->unfrozen_quantity >= $quantity) {
                        $updateQuantity = -$quantity;
                        $updateUnfrozenQuantity = -$quantity;
                    } else {
                        $updateQuantity = -$quantity;
                        $updateFrozenQuantity = -$quantity;
                    }

                    $this->stockRepository->updateStock(
                        $warehouseId,
                        $userId,
                        StorageArea::STORAGE_TYPE_PICKING,
                        StockLog::RELATION_TYPE_PICKING,
                        $pickingTask->id,
                        $skuCode,
                        $bin,
                        $updateQuantity,
                        $updateFrozenQuantity,
                        $updateUnfrozenQuantity
                    );
                    $stockNotify[] = [
                        'sku_code' => $skuCode,
                    ];
                }
                // 检查是否有相同篮子的未关闭的拣货异常
                $abnormal = Abnormal::query()
                    ->where('relation_type', $relationType)
                    ->where('relation_number', $relationNumber)
                    ->where('basket_code', $basketCode)
                    ->whereNotIn('state', [
                        Abnormal::STATE_CLOSED,
                        Abnormal::STATE_COMPLETED,
                    ])
                    ->first();
            }
            if (!$abnormal) {
                // 生成异常单
                $abnormal = $this->abnormalRepository->createAbnormal(
                    $warehouseId,
                    $userId,
                    $relationType,
                    $relationNumber,
                    $abnormalCode,
                    $preReceivingNumber,
                    $outboundNumber,
                    $reason,
                    Abnormal::STATE_CREATED,
                    $resourceId,
                    $basketCode
                );
            }

            if (
                in_array($abnormal->abnormal_type, [
                    Abnormal::TYPE_RECEIVING_EXTRA,
                    Abnormal::TYPE_RECEIVING_DEFECTIVE,
                    Abnormal::TYPE_RECEIVING_WRONG,
                    Abnormal::TYPE_PUTAWAY_EXTRA,
                    Abnormal::TYPE_PUTAWAY_MISSING,
                    Abnormal::TYPE_PUTAWAY_WRONG,
                ])
                && $skuCode
            ) {
                // 添加异常单 item
                $skus[] = [
                    'sku_code' => $skuCode,
                    'quantity' => $quantity,
                ];
                $this->abnormalRepository->createAbnormalItems($abnormal->id, $skus);
                switch ($abnormal->abnormal_type) {
                    case Abnormal::TYPE_RECEIVING_DEFECTIVE:
                        $this->stockRepository->updateStock(
                            $warehouseId,
                            $userId,
                            StorageArea::STORAGE_TYPE_ABNORMAL,
                            StockLog::RELATION_TYPE_ABNORMAL,
                            $abnormal->id,
                            $skuCode,
                            '',
                            $quantity
                        );
                        break;
                    case Abnormal::TYPE_PUTAWAY_MISSING:
                        // 减少收货区库存
                        $this->stockRepository->updateStock(
                            $warehouseId,
                            $userId,
                            StorageArea::STORAGE_TYPE_RECEIVING,
                            StockLog::RELATION_TYPE_ABNORMAL,
                            $abnormal->id,
                            $skuCode,
                            '',
                            -$quantity
                        );
                        // 更新收货车
                        $this->putawayTaskRepository->decreaseReceivingCartItem(
                            $warehouseId,
                            $putawayTask->receiving_cart_code,
                            $skuCode,
                            $quantity
                        );
                        break;
                    case Abnormal::TYPE_PUTAWAY_EXTRA:
                    case Abnormal::TYPE_PUTAWAY_WRONG:
                        // 不知道 SKU 或者 SKU 不在收货单中
                        // 增加异常去库存
                        $this->stockRepository->updateStock(
                            $warehouseId,
                            $userId,
                            StorageArea::STORAGE_TYPE_ABNORMAL,
                            StockLog::RELATION_TYPE_ABNORMAL,
                            $abnormal->id,
                            $skuCode,
                            $bin,
                            $quantity
                        );
                        break;
                }
            }

            if (in_array($abnormal->abnormal_type, [
                Abnormal::TYPE_PICKING_MISSING,
                Abnormal::TYPE_CHECKING_DEFECTIVE,
                Abnormal::TYPE_CHECKING_FAILED,
                Abnormal::TYPE_ORDER_INTERCEPTED,
            ])) {
                switch ($abnormal->abnormal_type) {
                    case Abnormal::TYPE_PICKING_MISSING:
                        // 添加异常单 item
                        $skus[] = [
                            'bin' => $bin,
                            'sku_code' => $skuCode,
                            'missing_quantity' => $quantity,
                        ];
                        $this->abnormalRepository->createPickingMissingAbnormalItems($abnormal->id, $skus);
                        break;
                    case Abnormal::TYPE_CHECKING_DEFECTIVE:
                    case Abnormal::TYPE_CHECKING_FAILED:
                        $checkTaskItems = $this->checkTaskRepository->getCheckTaskItems($checkTask->id);
                        // 添加异常单 item
                        $skus = collect($checkTaskItems)->transform(function ($item) {
                            return [
                                'sku_code' => $item['sku_code'],
                                'quantity' => $item['quantity'],
                            ];
                        })->toArray();
                        $this->abnormalRepository->createAbnormalItems($abnormal->id, $skus);
                        foreach ($skus as $sku) {
                            $this->stockRepository->updateStock(
                                $warehouseId,
                                $userId,
                                StorageArea::STORAGE_TYPE_PACKAGE,
                                StockLog::RELATION_TYPE_ABNORMAL,
                                $abnormal->id,
                                $sku['sku_code'],
                                '',
                                -$sku['quantity']
                            );
                            $this->stockRepository->updateStock(
                                $warehouseId,
                                $userId,
                                StorageArea::STORAGE_TYPE_ABNORMAL,
                                StockLog::RELATION_TYPE_ABNORMAL,
                                $abnormal->id,
                                $sku['sku_code'],
                                '',
                                $sku['quantity']
                            );
                        }
                        $this->checkTaskRepository->updateState($checkTask->id, CheckTask::STATE_ABNORMAL);
                        // 更新出库单状态
                        $outbound = $this->outboundRepository->getOutbound($checkTask->warehouse_id,
                            $checkTask->outbound_number);
                        $this->outboundRepository->updateState($outbound->id, Outbound::STATE_CHECK_ABNORMAL);
                        break;
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('StoreAbnormal Fail', [
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
            return fail();
        }
        if (!empty($stockNotify)) {
            MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_STOCK_NOTIFY, ['warehouse_id' => $warehouseId, 'list' => $stockNotify]);
        }
        if ($outbound) {
            MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_ORDER_STATUS_NOTIFY, ['order_number' => $outbound->order_number]);
        }

        return success();
    }
}
