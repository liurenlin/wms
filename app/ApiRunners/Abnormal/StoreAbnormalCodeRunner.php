<?php

namespace App\ApiRunners\Abnormal;

use App\ApiRunners\ApiRunner;
use App\ApiRunners\Runner;
use App\Repositories\AbnormalRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class StoreAbnormalCodeRunner extends Runner implements ApiRunner
{
    public $abnormalRepository;

    public function __construct(AbnormalRepository $abnormalRepository)
    {
        $this->abnormalRepository = $abnormalRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $code = $this->abnormalRepository->generateAbnormalCode();

        return success([
            'code' => $code,
        ]);
    }
}
