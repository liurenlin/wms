<?php

namespace App\ApiRunners;

use Illuminate\Http\Request;

interface ApiRunner
{

    public function run(Request $request);
}
