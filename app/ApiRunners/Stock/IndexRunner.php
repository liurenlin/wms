<?php

namespace App\ApiRunners\Stock;

use App\ApiRunners\ApiRunner;
use App\ApiRunners\Runner;
use App\Repositories\StockRepository;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IndexRunner extends Runner implements ApiRunner
{
    public $stockRepository;

    public function __construct(StockRepository $stockRepository)
    {
        $this->stockRepository = $stockRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $userId = Auth::id();
        $warehouseId = UserService::getWarehouseId($userId);

        if (!$request->get('bin') && !$request->get('sku_code')) {
            return fail(__('common.fail'));
        }
        $list = [];
        if ($request->get('bin') && !$request->get('sku_code')) {
            $list = $this->stockRepository->getStocksByBin($warehouseId, $request->get('bin'));
        } elseif ($request->get('sku_code') && !$request->get('bin')) {
            $list = $this->stockRepository->getStocksBySkuCode($warehouseId, $request->get('sku_code'));
        } elseif ($request->get('bin') && $request->get('sku_code')) {
            $list = $this->stockRepository->getStocksByBinAndSkuCode($warehouseId, $request->get('bin'), $request->get('sku_code'));
        }
        return success(['list' => $list]);
    }
}
