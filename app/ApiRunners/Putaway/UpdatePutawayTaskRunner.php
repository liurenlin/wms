<?php

namespace App\ApiRunners\Putaway;

use App\ApiRunners\ApiRunner;
use App\ApiRunners\Runner;
use App\Models\PutawayTask;
use App\Models\ReceivingCart;
use App\Repositories\PutawayTaskRepository;
use App\Repositories\ReceivingLogRepository;
use App\Services\CacheService;
use App\Services\UserService;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UpdatePutawayTaskRunner extends Runner implements ApiRunner
{
    public $putawayTaskRepository;
    public $receivingLogRepository;

    public function __construct(PutawayTaskRepository $putawayTaskRepository, ReceivingLogRepository $receivingLogRepository)
    {
        $this->putawayTaskRepository = $putawayTaskRepository;
        $this->receivingLogRepository = $receivingLogRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $userId = Auth::id();
        $putawayTaskId = (int) $request->input('putaway_task_id');
        $state = (int) $request->get('state');
        // 检查车子使用情况
        $putawayTask =  $this->putawayTaskRepository->getPutawayTask($putawayTaskId);
        if (!$putawayTask || $putawayTask->user_id != $userId) {
            return fail(__('putaway.task_not_exist'));
        }
        if ($putawayTask->state == PutawayTask::STATE_FINISHED) {
            return success();
        }
        if (!$this->putawayTaskRepository->isPutawayTaskCompleted($putawayTaskId)) {
            return fail(__('putaway.receiving_cart_busy'));
        }

        DB::beginTransaction();
        try {
            // 更新上架任务状态
            $this->putawayTaskRepository->updatePutawayTaskState($putawayTaskId, $state);
            // 释放入库车
            $this->putawayTaskRepository->releaseReceivingCart($putawayTask->receiving_cart_code);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('UpdatePutawayTaskRunner Fail', [
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
            return fail();
        }
        return success();

    }
}
