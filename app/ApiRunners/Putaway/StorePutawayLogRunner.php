<?php

namespace App\ApiRunners\Putaway;

use App\ApiRunners\ApiRunner;
use App\ApiRunners\Runner;
use App\Consts\CommonConst;
use App\Events\OmsOrderOutEvent;
use App\Events\OmsShelfOnEvent;
use App\Models\OpenMessageQueue;
use App\Models\PutawayTask;
use App\Models\ReceivingLog;
use App\Models\StockLog;
use App\Models\StorageArea;
use App\Repositories\OutboundRepository;
use App\Repositories\PutawayTaskRepository;
use App\Repositories\ReceivingLogRepository;
use App\Repositories\StockRepository;
use App\Repositories\StorageLocationRepository;
use App\Services\MessageService;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StorePutawayLogRunner extends Runner implements ApiRunner
{
    public $putawayTaskRepository;
    public $receivingLogRepository;
    public $stockRepository;
    public $storageLocationRepository;
    public $outboundRepository;

    public function __construct(
        PutawayTaskRepository $putawayTaskRepository,
        ReceivingLogRepository $receivingLogRepository,
        StockRepository $stockRepository,
        StorageLocationRepository $storageLocationRepository,
        OutboundRepository $outboundRepository
    ) {
        $this->putawayTaskRepository = $putawayTaskRepository;
        $this->receivingLogRepository = $receivingLogRepository;
        $this->stockRepository = $stockRepository;
        $this->storageLocationRepository = $storageLocationRepository;
        $this->outboundRepository = $outboundRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $userId = Auth::id();
        $warehouseId = UserService::getWarehouseId($userId);
        $putawayTaskId = (int) $request->get('putaway_task_id');
        $receivingCartCode = $request->get('receiving_cart_code');
        $skuCode = $request->get('sku_code');
        $quantity = (int) $request->get('quantity');
        $bin = (string) $request->get('bin');

        // 检查库位在不在良品区
        $storageLocation = $this->storageLocationRepository->getStockStorageLocation($warehouseId, $bin);
        if (!$storageLocation) {
            return fail(__('putaway.storage_location_wrong'));
        }

        // 检查库位时候被其他 sku 占用
        $stock = $this->stockRepository->getStockByBin($warehouseId, $bin);
        if ($stock && $stock->quantity > 0 && $stock->sku_code != $skuCode) {
            return fail('Error! Bin is occupied by ' . $stock->sku_code . ', please use another one.');
        }
        // 检查是否还有未入库的 sku
        $putawayTask =  $this->putawayTaskRepository->getPutawayTask($putawayTaskId);
        if ($putawayTask->state == PutawayTask::STATE_FINISHED) {
            return fail(__('putaway.receiving_finished'));
        }

        // 检查入库是否超出收货数量
        $putawayTaskItem= $this->putawayTaskRepository->getPutawayTaskItemBySkuCode($putawayTaskId, $skuCode);
        if (!$putawayTaskItem) {
            return fail(__('putaway.wrong_sku'));
        }
        if ($quantity > ($putawayTaskItem->receiving_quantity - $putawayTaskItem->putaway_quantity)) {
            return fail('Error! Qty should no more than ' . ($putawayTaskItem->receiving_quantity - $putawayTaskItem->putaway_quantity) . '.');
        }

        DB::beginTransaction();
        try {
            // 生成入库日志
            $putawayLog = $this->putawayTaskRepository->createPutawayLog($warehouseId, $userId, $putawayTaskId, $skuCode, $quantity, $bin);

            // 更新入库任务详情
            $this->putawayTaskRepository->updatePutawayTaskItem($putawayTaskId, $skuCode, $bin, $quantity);

            // 更新收货暂存区库存情况
            $this->stockRepository->updateStock(
                $warehouseId,
                $userId,
                StorageArea::STORAGE_TYPE_RECEIVING,
                StockLog::RELATION_TYPE_PUTAWAY_LOG,
                $putawayLog->id,
                $skuCode,
                '',
                -$quantity
            );

            // 更新良品库位区库存情况
            $this->stockRepository->updateStock(
                $warehouseId,
                $userId,
                StorageArea::STORAGE_TYPE_PICKING,
                StockLog::RELATION_TYPE_PUTAWAY_LOG,
                $putawayLog->id,
                $skuCode,
                $bin,
                $quantity,
                0,
                $quantity
            );

            // 减少车子内容货物
            $this->putawayTaskRepository->decreaseReceivingCartItem($warehouseId, $receivingCartCode, $skuCode, $quantity);

            // 更新车子状态是否为空闲
            $this->putawayTaskRepository->checkReceivingCartState($warehouseId, $receivingCartCode);

            // 撤销库位推荐 lock
            $this->putawayTaskRepository->unLockAndUnEmptyStorageLocationSuggest($warehouseId, $bin);

            // 更新锁库
            $this->outboundRepository->frozenOutboundItems($warehouseId, $skuCode);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('StorePutawayLog Fail', [
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
            return fail();
        }

        try {
            $latestReceivingLog = $this->receivingLogRepository->getLatestLog($warehouseId, $receivingCartCode, $skuCode);
            $preReceiving = $this->receivingLogRepository->getPreReceivingById($latestReceivingLog->pre_receiving_id);
            MessageService::inQueue(OpenMessageQueue::MESSAGE_TYPE_SHELF_ON, [
                'sku_code' => $skuCode,
                'quantity' => $quantity,
                'task_number' => $putawayTask->task_number,
                'pre_receiving_number' => $preReceiving->pre_receiving_number
            ]);
        } catch (\Exception $e) {
            Log::error('OmsShelfOnEvent Fail', [
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
        }

        return success();

    }
}
