<?php

namespace App\ApiRunners\Putaway;

use App\ApiRunners\ApiRunner;
use App\ApiRunners\Runner;
use App\Models\ReceivingCart;
use App\Repositories\PutawayTaskRepository;
use App\Repositories\ReceivingLogRepository;
use App\Services\CacheService;
use App\Services\UserService;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StorePutawayTaskRunner extends Runner implements ApiRunner
{
    public $putawayTaskRepository;
    public $receivingLogRepository;

    public function __construct(PutawayTaskRepository $putawayTaskRepository, ReceivingLogRepository $receivingLogRepository)
    {
        $this->putawayTaskRepository = $putawayTaskRepository;
        $this->receivingLogRepository = $receivingLogRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $userId = Auth::id();
        $warehouseId = UserService::getWarehouseId($userId);
        $receivingCartCode = $request->get('receiving_cart_code');
        // 检查车子使用情况
        $putawayTask = $this->putawayTaskRepository->getPutawayTaskByReceivingCartCode($warehouseId, $receivingCartCode);
        if ($putawayTask) {
            if ($putawayTask->user_id != $userId) {
                $putawayTaskUser = User::find($putawayTask->user_id);
                return fail('Error! ' . $putawayTaskUser->name . ' is doing the putaway task, please use another cart.');
            }
            // 取任务
            $skus = $this->putawayTaskRepository->getPutawayTaskItems($putawayTask->id, $putawayTask->task_number);
            return success([
                'skus' => $skus,
                'id' => $putawayTask->id,
                'task_number' => $putawayTask->task_number,
            ]);
        }

        // 检查车中是否有商品
        $isReceivingCartEmpty = $this->putawayTaskRepository->isReceivingCartEmpty($warehouseId, $receivingCartCode);
        if ($isReceivingCartEmpty) {
            return fail(__('putaway.receiving_cart_empty'));
        }

        // 排队检查
        $lock = Cache::lock(CacheService::getPutawayTaskConfirmKey($receivingCartCode), 2);
        if (!$lock->get()) {
            return fail(__('putaway.putaway_busy'));
        }

        DB::beginTransaction();
        try {
            // 生成任务清单
            $putawayTask = $this->putawayTaskRepository->createPutawayTask($warehouseId, $userId, $receivingCartCode);
            if (!$putawayTask) {
                DB::rollBack();
                $lock->release();
                return fail('Error! Not enough available bin locations for the task');
            }
            // 更新车子使用状态
            $this->receivingLogRepository->updateReceivingCartState($receivingCartCode, ReceivingCart::STATE_BUSY);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $lock->release();
            Log::error('StorePutawayTask Fail', [
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
            return fail();
        }
        $lock->release();
        // 首个推荐
        $skus = $this->putawayTaskRepository->getPutawayTaskItems($putawayTask->id, $putawayTask->task_number);
        return success([
            'skus' => $skus,
            'id' => $putawayTask->id,
            'task_number' => $putawayTask->task_number,
        ]);

    }
}
