<?php

namespace App\ApiRunners\User;

use App\ApiRunners\ApiRunner;
use App\ApiRunners\Runner;
use App\Repositories\UserRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class LoginRunner extends Runner implements ApiRunner
{
    public $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function run(Request $request): JsonResponse
    {
        // 校验登录
        $credentials = $request->only(['name', 'password']);

        if ($token = auth('api')->attempt($credentials)) {
            $user = auth('api')->user();
            return success([
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'token' => $token,
                'warehouses' => $this->userRepository->getUserWarehouses($user->id),
            ]);
        } else {
            return fail(__('auth.login.fail'));
        }


    }
}
