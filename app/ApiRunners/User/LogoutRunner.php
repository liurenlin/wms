<?php

namespace App\ApiRunners\User;

use App\ApiRunners\ApiRunner;
use App\ApiRunners\Runner;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class LogoutRunner extends Runner implements ApiRunner
{
    public function run(Request $request): JsonResponse
    {
        // 校验登录
        auth()->logout();
        return success();
    }
}
