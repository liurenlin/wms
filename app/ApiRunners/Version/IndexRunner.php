<?php

namespace App\ApiRunners\Version;

use App\ApiRunners\ApiRunner;
use App\ApiRunners\Runner;
use App\Repositories\AppVersionRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class IndexRunner extends Runner implements ApiRunner
{
    public $appVersionRepository;

    public function __construct(AppVersionRepository $appVersionRepository)
    {
        $this->appVersionRepository = $appVersionRepository;
    }

    public function run(Request $request): JsonResponse
    {
        $platform = $request->get('platform');
        switch ($platform) {
            case 'ios':
                $platform = 1;
                break;
            case 'android':
                $platform = 2;
                break;
            default:
                $platform = 2;
        }
        if (empty($request->get('version'))) {
            return fail();
        }
        $versions = $this->appVersionRepository->getVersions($platform, $request->get('version'));
        return success(['versions' => $versions]);
    }
}
