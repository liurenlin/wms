<?php

if (!function_exists('success')) {
    function success(
        array $data = [],
        string $message = \App\Consts\ResponseConst::MESSAGE_SUCCESS
    ): \Illuminate\Http\JsonResponse
    {
        $responseData = [
            'code' => \App\Consts\ResponseConst::CODE_SUCCESS,
            'message' => $message  == \App\Consts\ResponseConst::MESSAGE_SUCCESS ? __('common.success') : $message,
        ];
        if (!empty($data)) {
            $responseData['data'] = $data;
        }
        return response()->json($responseData);
    }
}

if (!function_exists('fail')) {
    function fail(
        string $message = \App\Consts\ResponseConst::MESSAGE_FAIL,
        int $code = \App\Consts\ResponseConst::CODE_FAIL,
        array $data = []
    ): \Illuminate\Http\JsonResponse
    {
        $responseData = [
            'code' => \App\Consts\ResponseConst::CODE_FAIL,
            'message' => $message  == \App\Consts\ResponseConst::MESSAGE_FAIL ? __('common.fail') : $message,
        ];
        if (!empty($data)) {
            $responseData['data'] = $data;
        }
        return response()->json($responseData);
    }
}
