<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\MessageHandlingEvent' => [
            'App\Listeners\MessageHandlingListener',
        ],
        'App\Events\OmsInWareEvent' => [
            'App\Listeners\OmsInWareListener',
        ],
        'App\Events\OmsOrderOutEvent' => [
            'App\Listeners\OmsOrderOutListener',
        ],
        'App\Events\OmsShelfOnEvent' => [
            'App\Listeners\OmsShelfOnListener',
        ],
        'App\Events\OmsStockNotifyEvent' => [
            'App\Listeners\OmsStockNotifyListener',
        ],
        'App\Events\OmsOrderStatusNotifyEvent' => [
            'App\Listeners\OmsOrderStatusNotifyListener',
        ],
        'App\Events\OmsOrderCancelResultNotifyEvent' => [
            'App\Listeners\OmsOrderCancelResultNotifyListener',
        ],
        'App\Events\OmsPreReceivingNotifyEvent' => [
            'App\Listeners\OmsPreReceivingNotifyListener',
        ],
        'App\Events\OmsSyncProductSkuEvent' => [
            'App\Listeners\OmsSyncProductSkuListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
