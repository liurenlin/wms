<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(\App\Repositories\UserRepository::class, \App\Repositories\UserRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PreReceivingRepository::class, \App\Repositories\PreReceivingRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ReceivingLogRepository::class, \App\Repositories\ReceivingLogRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PutawayTaskRepository::class, \App\Repositories\PutawayTaskRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PickingTaskRepository::class, \App\Repositories\PickingTaskRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CheckTaskRepository::class, \App\Repositories\CheckTaskRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AbnormalRepository::class, \App\Repositories\AbnormalRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\WeighRepository::class, \App\Repositories\WeighRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\LogisticsCompanyRepository::class, \App\Repositories\LogisticsCompanyRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PermissionRepository::class, \App\Repositories\PermissionRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\RoleRepository::class, \App\Repositories\RoleRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\OutboundRepository::class, \App\Repositories\OutboundRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\HandoverRepository::class, \App\Repositories\HandoverRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\StockRepository::class, \App\Repositories\StockRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProductRepository::class, \App\Repositories\ProductRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\StorageLocationRepository::class, \App\Repositories\StorageLocationRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AppVersionRepository::class, \App\Repositories\AppVersionRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\OpenMessageQueueRepository::class, \App\Repositories\OpenMessageQueueRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PickingTaskBasketRepository::class, \App\Repositories\PickingTaskBasketRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ResourceRepository::class, \App\Repositories\ResourceRepositoryEloquent::class);
        //:end-bindings:
    }
}
