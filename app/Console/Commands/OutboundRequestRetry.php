<?php

namespace App\Console\Commands;

use App\Models\OutboundRequestLog;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class OutboundRequestRetry extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:outbound-request-retry';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // 每次取 10 条
        $outboundRequestLogs = OutboundRequestLog::query()
            ->where('state', OutboundRequestLog::STATE_EXCEPTION)
            ->where('retry_time', '<', 3)
            ->latest('id')
            ->limit(10)
            ->get();
        foreach ($outboundRequestLogs as $outboundRequestLog) {
            $tmpOutboundRequestLog = OutboundRequestLog::query()->find($outboundRequestLog->id);
            $tmpOutboundRequestLog->retry_time = $tmpOutboundRequestLog->retry_time + 1;
            try {
                $http = new Client();
                if ($outboundRequestLog == 'POST') {
                    $response = $http->post($outboundRequestLog->uri, [
                        'headers' => json_decode($outboundRequestLog->headers, JSON_OBJECT_AS_ARRAY),
                        'body' => json_decode($outboundRequestLog->input, JSON_OBJECT_AS_ARRAY),
                    ]);
                    $statusCode = $response->getStatusCode();
                    $responseContents = $response->getBody()->getContents();
                    $tmpOutboundRequestLog->response_body = $responseContents;

                    $contents = json_decode($responseContents, JSON_OBJECT_AS_ARRAY);
                    if ($statusCode != 200 || !$contents['success']) {
                        $tmpOutboundRequestLog->state = OutboundRequestLog::STATE_FAIL;
                    } else {
                        $tmpOutboundRequestLog->state = OutboundRequestLog::STATE_SUCCESS;
                    }
                }
            } catch (\Exception $e) {
                $tmpOutboundRequestLog->state = OutboundRequestLog::STATE_ABANDONED;
                Log::error('Command OutboundRequestRetry Fail', [
                    'exception' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ]);
            }
            $tmpOutboundRequestLog->save();
        }
    }
}
