<?php

namespace App\Console\Commands;

use App\Consts\CommonConst;
use App\Models\Outbound;
use App\Models\OutboundItem;
use App\Models\OutboundProcess;
use App\Models\Product;
use App\Models\Warehouse;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class OutboundMigrate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:outbound-migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Outbound Migrate';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $warehouseId = CommonConst::WAREHOUSE_ID;
        DB::table('pow_orders')
            ->whereNull('deleted_at')
            ->orderBy('id')
            ->chunk(500, function ($orders) use ($warehouseId) {
                foreach ($orders as $order) {
                    $shipping = DB::table('pow_shippings')
                        ->where('id', $order->shipping_id)
                        ->where('order_id', $order->id)
                        ->first();
                    $outbound = Outbound::create([
                        'warehouse_id' => $warehouseId,
                        'outbound_number' => $this->generateOutboundNumber($order->created_at),
                        'outbound_type' => Outbound::TYPE_ORDER,
                        'order_number' => $order->order_code,
                        'order_created_at' => Carbon::parse($order->created_at)->timestamp,
                        'currency' => Str::lower($order->currency_code),
                        'total_amount' => $order->order_price,
                        'freight' => $order->freight,
                        'receiving_country' => $order->receiver_country_code,
                        'receiving_region' => $order->receiver_region,
                        'receiving_state' => $order->receiver_state,
                        'receiving_city' => $order->receiver_city,
                        'receiving_district' => $order->receiver_region,
                        'receiving_address' => $order->receiver_address_1,
                        'receiving_postcode' => $order->receiver_postcode,
                        'receiving_cellphone' => $order->receiver_phone,
                        'receiving_user_name' => $order->receiver_first_name . ' ' . $order->receiver_last_name,
                        'receiving_first_name' => $order->receiver_first_name,
                        'receiving_last_name' => $order->receiver_last_name,
                        'logistics_company_id' =>  $shipping ? 1 : 0,
                        'logistics_number' => $shipping ? $shipping->tracking_number : '',
                        'is_need_intercept' => 0,
                        'state' => Outbound::STATE_CREATED,
                        'created_at' => Carbon::parse($order->created_at)->timestamp,
                        'updated_at' => Carbon::parse($order->updated_at)->timestamp,
                    ]);

                    $lineItems = DB::table('pow_line_items')
                        ->where('order_id', $order->id)
                        ->whereNull('deleted_at')
                        ->get();

                    foreach ($lineItems as $lineItem) {
                        OutboundItem::create([
                            'outbound_id' => $outbound->id,
                            'sku_code' => $lineItem->sku_code,
                            'quantity' => $lineItem->sku_quantity,
                            'created_at' => Carbon::parse($lineItem->created_at)->timestamp,
                            'updated_at' => Carbon::parse($lineItem->updated_at)->timestamp,
                        ]);
                    }
                    // 关联 process
                    OutboundProcess::create([
                        'outbound_id' => $outbound->id,
                        'state_after' => Outbound::STATE_CREATED,
                        'content' => 'Outbound order number ' . $outbound->outbound_number,
                        'created_at' => Carbon::parse($lineItem->created_at)->timestamp,
                        'updated_at' => Carbon::parse($lineItem->created_at)->timestamp,
                    ]);
                }
            });

        $this->info('outbound migrate finished');
    }

    private function generateOutboundNumber(string $createdAt)
    {
        $date = Carbon::parse($createdAt)->format('Ymd');

        $count = Outbound::query()->whereBetween('created_at', [
            Carbon::createFromDate($date)->startOfDay()->timestamp,
            Carbon::createFromDate($date)->endOfDay()->timestamp
        ])->count();

        $count = $count + 1;

        return 'OUT' . $date . sprintf('%04d', $count);
    }
}
