<?php

namespace App\Console\Commands;

use App\Consts\CommonConst;
use App\Models\PreReceiving;
use App\Models\PreReceivingItem;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PreReceivingMigrate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:pre-receiving-migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pre-receiving Migrate';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $warehouseId = CommonConst::WAREHOUSE_ID;
        DB::table('pow_purchases')
            ->whereNull('deleted_at')
            ->orderBy('id')
            ->chunk(500, function ($purchases) use ($warehouseId) {
                foreach ($purchases as $purchase) {

                    $preReceiving = PreReceiving::create([
                        'warehouse_id' => $warehouseId,
                        'pre_receiving_number' => $this->generatePreReceivingNumber($purchase->created_at),
                        'state' => 0,
                        'third_party_order_type' => 1, // 采购单类型
                        'third_party_order_number' => $purchase->purchase_code,
                        'third_party_order_created_at' => Carbon::parse($purchase->completed_at)->timestamp,
                        'owner_id' => 0,
                        'created_at' => Carbon::parse($purchase->created_at)->timestamp,
                        'updated_at' => Carbon::parse($purchase->updated_at)->timestamp,
                    ]);

                    $purchaseItems = DB::table('pow_purchase_items')
                        ->where('purchase_id', $purchase->id)
                        ->whereNull('deleted_at')
                        ->get();

                    foreach ($purchaseItems as $purchaseItem) {
                        PreReceivingItem::create([
                            'pre_receiving_id' => $preReceiving->id,
                            'sku_code' => $purchaseItem->sku_code,
                            'quantity' => (int) $purchaseItem->quantity,
                            'created_at' => Carbon::parse($purchaseItem->created_at)->timestamp,
                            'updated_at' => Carbon::parse($purchaseItem->updated_at)->timestamp,
                        ]);
                    }
                }
            });

        $this->info('pre_receiving migrate finished');

    }

    private function generatePreReceivingNumber(string $createdAt)
    {
        $date = Carbon::parse($createdAt)->format('Ymd');

        $count = PreReceiving::query()->whereBetween('created_at', [
            Carbon::createFromDate($date)->startOfDay()->timestamp,
            Carbon::createFromDate($date)->endOfDay()->timestamp
        ])->count();

        $count = $count + 1;

        return 'PRE' . $date . sprintf('%04d', $count);
    }
}
