<?php

namespace App\Console\Commands;

use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductSku;
use App\Models\ProductSkuImage;
use App\Models\Resource;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ProductMigrate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:product-migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Product Migration';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('pow_products')
            ->orderBy('id')
            ->chunk(500, function ($products) {
                foreach ($products as $product) {
                    // product
                    $newProduct = Product::create([
                        'product_code' => $product->product_code,
                        'product_name' => $product->product_name,
                    ]);

                    // resource & product sku image
                    $productImages = DB::table('pow_product_images')
                        ->where('product_id', $product->id)
                        ->whereNull('deleted_at')
                        ->get();
                    if ($productImages->isEmpty()) {
                        $productImages = DB::table('pow_product_images')
                            ->where('product_id', $product->id)
                            ->get();
                    }
                    $newResourceIds = [];
                    foreach ($productImages as $productImage) {
                        $resource = Resource::query()
                            ->create([
                                'url' => $productImage->image_url,
                            ]);
                        $newResourceIds[] = $resource->id;
                    }

                    $time = time();

                    // product images
                    $productImageInserts = [];
                    foreach ($newResourceIds as $newResourceId) {
                        $productImageInserts[] = [
                            'product_id' => $newProduct->id,
                            'resource_id' => $newResourceId,
                            'created_at' => $time,
                            'updated_at' => $time,
                        ];
                    }
                    if (!empty($productImageInserts)) {
                        ProductImage::query()->insert($productImageInserts);
                    }

                    // product sku
                    $productSkus = DB::table('pow_product_skus')
                        ->where('product_id', $product->id)
                        ->get();
                    foreach ($productSkus as $productSku) {
                        $newProductSku = ProductSku::create([
                            'product_id' => $newProduct->id,
                            'sku_code' => $productSku->sku_code,
                            'sku_name' => $productSku->sku_name,
                            'sku_attribute' => $productSku->sku_attribute,
                            'weight' => $productSku->weight,
                            'width' => $productSku->width,
                            'height' => $productSku->height,
                            'length' => $productSku->length,
                        ]);

                        $productSkuImageInserts = [];
                        foreach ($newResourceIds as $newResourceId) {
                            $productSkuImageInserts[] = [
                                'product_sku_id' => $newProductSku->id,
                                'resource_id' => $newResourceId,
                                'created_at' => $time,
                                'updated_at' => $time,
                            ];
                        }
                        if (!empty($productSkuImageInserts)) {
                            ProductSkuImage::query()->insert($productSkuImageInserts);
                        }
                    }
                }
            });

        $this->info('product migrate finished');

    }
}
