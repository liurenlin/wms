<?php

namespace App\Services;

use App\Models\Abnormal;
use App\Models\ActualReceivingItem;
use App\User;
use Carbon\Carbon;

class ReceivingService extends Service
{
    public function formatPreReceivingItems($preReceiving): array
    {
        $items = $preReceiving->items;
        if ($items->isEmpty()) {
            $data = [];
        } else {
            foreach ($items as $item) {
                $receivingQuantity = ActualReceivingItem::getReceivingQuantity($preReceiving, $item->sku_code);
                $formatItem[] = [
                    'id' => $item->id,
                    'sku_code' => $item->sku_code,
                    'quantity' => $item->quantity,
                    'receiving_quantity' => $receivingQuantity
                ];
            }
            $data = $formatItem;
        }
        return $data;
    }

    public function formatActualReceiving($actualReceivings): array
    {
        if ($actualReceivings->isEmpty()) {
            return [];
        }
        foreach ($actualReceivings as $actualReceiving) {
            $data[] = [
                'id' => $actualReceiving->id,
                'actual_receiving_number' => $actualReceiving->actual_receiving_number,
                'created_at' => $actualReceiving->created_at->format('Y-m-d H:i:s'),
                'completed_at' => $actualReceiving->finished_at ? Carbon::createFromTimestamp($actualReceiving->finished_at)->toDateTimeString() : '',
                'operator' => User::query()->where('id', $actualReceiving->user_id)->value('name') ?? '',
            ];
        }
        return $data;
    }

    public function formatActualReceivingItems($actualReceiving): array
    {
        $items = $actualReceiving->items;
        if ($items->isEmpty()) {
            $data = [];
        } else {
            foreach ($items as $item) {
                $receivingQuantity = ActualReceivingItem::getReceivingQuantity($actualReceiving->preReceiving,
                    $item->sku_code);
                $formatItem[] = [
                    'id' => $item->id,
                    'sku' => [
                        'code' => $item->sku_code
                    ],
                    'quantity' => $item->receiving_quantity,
                    'receiving_quantity' => $receivingQuantity
                ];
            }
            $data = $formatItem;
        }
        return $data;
    }

    public function formatActualReceivingLogs($actualReceiving): array
    {
        $receivingLogs = $actualReceiving->receivingLogs()->where('actual_receiving_id', $actualReceiving->id)->get('*');
        if ($receivingLogs->isEmpty()) {
            $data = [];
        } else {
            foreach ($receivingLogs as $receivingLog) {
                $receivingQuantity = ActualReceivingItem::getReceivingQuantity($actualReceiving->preReceiving,
                    $receivingLog->sku_code);
                $formatItem[] = [
                    'id' => $receivingLog->id,
                    'sku' => [
                        'code' => $receivingLog->sku_code
                    ],
                    'quantity' => $receivingQuantity,
                    'received_at' => date($receivingLog->created_at),
                    'receiving_cart_code' => $receivingLog->receiving_cart_code,
                    'operator' => isset($receivingLog->operator) ? $receivingLog->operator->name : ''
                ];
            }
            $data = $formatItem;
        }
        return $data;
    }

    public function formatActualReceivingAbnormal($actualReceiving): array
    {
        $actualReceivingAbnormals = $actualReceiving->abnormals()
            ->where('relation_type', Abnormal::RELATION_TYPE_ACTUAL_RECEIVING)
            ->where('relation_number', $actualReceiving->actual_receiving_number)
            ->get('*');
        if ($actualReceivingAbnormals->isEmpty()) {
            $data = [];
        } else {
            foreach ($actualReceivingAbnormals as $actualReceivingAbnormal) {
                $formatItem[] = [
                    'id' => $actualReceivingAbnormal->id,
                    'abnormal_number' => $actualReceivingAbnormal->abnormal_number,
                    'created_at' => date($actualReceivingAbnormal->created_at),
                    'type' => $actualReceivingAbnormal->relation_type,
                    'state' => $actualReceivingAbnormal->state,
                    'operator' => isset($actualReceivingAbnormal->operator) ? $actualReceivingAbnormal->operator->name : '',
                ];
            }
            $data = $formatItem;
        }
        return $data;
    }
}
