<?php
namespace App\Services;


use App\Models\Warehouse;

class WarehouseService
{
    public function generateWarehouseCode($country){
        $num = Warehouse::where('country', $country)
            ->count();
        return $country . sprintf('%04s', $num);
    }

}