<?php

namespace App\Services;

use Illuminate\Support\Facades\Cache;

class UserService extends Service
{
    public static function setWarehouseId(int $userId, int $warehouseId): bool
    {
        return Cache::put(CacheService::getUserWarehouseIdKey($userId), $warehouseId);
    }

    public static function getWarehouseId(int $userId): int
    {
        return (int) Cache::get(CacheService::getUserWarehouseIdKey($userId));
    }
}
