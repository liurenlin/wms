<?php

namespace App\Services;

use App\Models\ReceivingCart;
use Carbon\Carbon;

class ReceivingCartService
{
    protected $prefix = 'REC-';

    public function batchAddCarts($warehouseId, $num)
    {

        $latestReceivingCart = ReceivingCart::query()
            ->latest('id')
            ->first();
        if (!$latestReceivingCart) {
            $startCodeNum = 0;
        } else {
            $temp = substr($latestReceivingCart->cart_code, -3);;
            $startCodeNum = (int) $temp;
        }
        $carts = [];
        $time = Carbon::now()->getTimestamp();
        $startCodeNum += 1;
        for ($i = 0; $i < $num; $i++) {
            if ($startCodeNum < 100) {
                $code = sprintf('%03s', $startCodeNum);
            }
            $cart = [
                'warehouse_id' => $warehouseId,
                'cart_code' => $this->prefix . $code,
                'created_at' => $time,
                'updated_at' => $time,
            ];
            $carts[] = $cart;
            $startCodeNum++;
        }
        ReceivingCart::insert($carts);
        return true;
    }
}
