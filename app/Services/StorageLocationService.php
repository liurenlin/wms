<?php

namespace App\Services;

use App\Models\StorageArea;
use App\Models\StorageLocation;
use App\Models\Warehouse;

class StorageLocationService
{
    public function batchAddLocations(
        Warehouse $warehouse,
        StorageArea $area,
        $pathStart,
        $pathEnd,
        $shelfQty,
        $layerQty,
        $binQty
    )
    {
        $prefix = $area->area_code;
        for ($pathCode = $pathStart; $pathCode <= $pathEnd; $pathCode++) {
            for ($shelfCode = 1; $shelfCode <= $shelfQty; $shelfCode++) {
                for ($layerCode = 0; $layerCode < $layerQty; $layerCode++) {
                    for ($binCode = 1; $binCode <= $binQty; $binCode++) {
                        $code = $prefix . sprintf('%02s', $pathCode)
                            . '-' . sprintf('%02s', $shelfCode) . '-'
                            . $layerCode . $binCode;
                        $key = [
                            'warehouse_id' => $warehouse->id,
                            'storage_location_code' => $code,
                            'deleted_at' => null,
                        ];
                        $update = [
                            'storage_area_code' => $area->area_code,
                            'storage_type' => $area->storage_type,
                            'is_empty' => StorageLocation::IS_EMPTY_TRUE,
                        ];
                        StorageLocation::firstOrCreate($key, $update);
                    }
                }
            }
        }
        return true;
    }

}
