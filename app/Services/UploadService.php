<?php

namespace App\Services;

use Illuminate\Http\UploadedFile;

class UploadService extends Service
{
    public static function uploadAbnormalImage(UploadedFile $file): string
    {
        $service = new GoogleCloudStorageService();
        return $service->uploadImageToCdn($file, 'abnormal-images/');
    }


}
