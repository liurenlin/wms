<?php

namespace App\Services;

class CacheService
{
    const CACHE_PRE_USER_WAREHOUSE_ID = 'user_warehouse_id';
    const CACHE_PRE_RECEIVING_TASK_CONFIRM = 'receiving_task_confirm';
    const CACHE_PRE_SKU_CODE_PRINT_LOG = 'sku_code_print_log';
    const CACHE_PUTAWAY_TASK_CONFIRM = 'putaway_task_confirm';
    const CACHE_PICKING_TASK_CONFIRM = 'picking_task_confirm';
    const CACHE_ABNORMAL_CODE_COUNT = 'abnormal_code_count';
    const CACHE_OUTBOUND_SYNC = 'outbound_sync';
    const CACHE_PICKING_LOG = 'picking_log';

    public static function getUserWarehouseIdKey(int $userId): string
    {
        return self::CACHE_PRE_USER_WAREHOUSE_ID . ':' . $userId;
    }

    public static function getReceivingTaskConfirmKey(string $preReceivingNumber): string
    {
        return self::CACHE_PRE_RECEIVING_TASK_CONFIRM . ':' . $preReceivingNumber;
    }

    public static function getSkuCodePrintLogKey(int $userId, int $warehouseId, string $skuCode, int $preReceivingId): string
    {
        return self::CACHE_PRE_SKU_CODE_PRINT_LOG . ':' . $userId . ':' . $warehouseId . ':' . $skuCode . ':' .$preReceivingId;
    }

    public static function getPutawayTaskConfirmKey(string $receivingCartCode): string
    {
        return self::CACHE_PUTAWAY_TASK_CONFIRM . ':' .$receivingCartCode;
    }

    public static function getPickingTaskConfirmKey(int $warehouseId): string
    {
        return self::CACHE_PICKING_TASK_CONFIRM . ':' . $warehouseId;
    }

    public static function getAbnormalCodeCountKey(string $date): string
    {
        return self::CACHE_ABNORMAL_CODE_COUNT . ':' . $date;
    }

    public static function getOutboundSyncKey(): string
    {
        return self::CACHE_OUTBOUND_SYNC;
    }

    public static function getPickingLogKey(int $pickingTaskItemId): string
    {
        return self::CACHE_PICKING_LOG . ':' . $pickingTaskItemId;
    }
}
