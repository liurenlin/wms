<?php

namespace App\Services;

use App\Models\ActualReceiving;
use App\Models\ReceivingLog;
use App\User;

class PutawayService extends Service
{
    public function formatPutAwayTaskItems($putawayTask): array
    {
        $items = $putawayTask->items;
        if ($items->isEmpty()) {
            $data = [];
        } else {
            foreach ($items as $item) {
                $formatItem[] = [
                    'id' => $item->id,
                    'sku_code' => $item->sku_code,
                    'quantity' => $item->putaway_quantity,
                    'bin' => $item->putaway_bin,
                    'time' => date($item->created_at)
                ];
            }
            $data = $formatItem;
        }
        return $data;
    }

    public function formatPutAwayTaskSkuItems($putawayTask): array
    {
        $items = $putawayTask->items;
        if ($items->isEmpty()) {
            $data = [];
        } else {
            foreach ($items as $item) {
                $formatItem[] = [
                    'id' => $item->id,
                    'sku_code' => $item->sku_code,
                    'quantity' => $item->receiving_quantity,
                    'bin' => $item->suggest_bin,
                    'time' => date($item->created_at)
                ];
            }
            $data = $formatItem;
        }
        return $data;
    }

    public function formatPutAwayCartItems($putawayCart): array
    {
        $items = $putawayCart->items;
        if ($items->isEmpty()) {
            return [];
        }
        $receivingCartItemIds = $items->pluck('id')->toArray();
        return ReceivingLog::query()
            ->whereIn('receiving_cart_item_id', $receivingCartItemIds)
            ->get()
            ->transform(function ($item) {
                $actualReceivng = ActualReceiving::query()->find($item->actual_receiving_id);
                return [
                    'sku_code' => $item->sku_code,
                    'quantity' => $item->quantity,
                    'actual_receiving' => $actualReceivng ? [
                        'id' => $actualReceivng->id,
                        'actual_receiving_number' => $actualReceivng->actual_receiving_number,
                    ] : [],
                    'created_at' => $item->created_at->format('Y-m-d H:i:s'),
                    'operator' => $actualReceivng ? (User::query()->where('id', $actualReceivng->user_id)->value('name') ?? '') : '',
                ];

            })
            ->toArray();
    }

    public function formatPutAwayTaskAbnormals($putawayTask): array
    {
        return [];
    }
}
