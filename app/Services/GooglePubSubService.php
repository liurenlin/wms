<?php

namespace App\Services;

use App\Events\MessageHandlingEvent;
use Google\Cloud\PubSub\PubSubClient;
use Illuminate\Support\Facades\Log;

class GooglePubSubService extends Service
{
    private $client;

    public function __construct()
    {
        putenv('GOOGLE_APPLICATION_CREDENTIALS=' . config('services.google.pubsub_credentials'));
        $this->client = new PubSubClient();
    }

    public function publish(string $topicName, string $data = '', array $attributes = []): array
    {
        $topic = $this->client->topic($topicName);
        foreach ($attributes as $key => $attribute) {
            $attributes[$key] = (string) $attribute;
        }
        $publishData = [
            'data' => $data,
        ];
        if (!empty($attributes)) {
            $publishData['attributes'] = $attributes;
        }
        return $topic->publish($publishData);
    }

    public function pull(string $subscription): bool
    {
        $subscription = $this->client->subscription($subscription);
        $messages = $subscription->pull([
            'maxMessages' => 50, // TODO config
        ]);

        foreach ($messages as $message) {

            // 调用处理事件
            try {
                event(new MessageHandlingEvent($message->id(), $message->data(), $message->attributes()));
                $subscription->acknowledge($message); // 确认消息
            } catch (\Exception $e) {
                Log::error('MessageHandlingEvent Fail', [
                    'exception' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ]);
            }

        }
        return true;
    }

}
