<?php

namespace App\Services;

use App\Models\OpenApp;
use Illuminate\Support\Facades\Log;

class Sign
{
    public static function generateSign($params)
    {
        $openApp = OpenApp::query()
            ->where('app_id', $params['app_id'])
            ->first();

        $params['app_secret'] = $openApp->app_secret;

        //排序
        try {
            ksort($params, SORT_STRING);
            $str = http_build_query($params);
            return sha1($str);
        } catch (Exception $e) {
            Log::error('Sign generateSign Fail', [
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
            return '';
        }
    }


}
