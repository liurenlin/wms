<?php

namespace App\Services;

use App\Models\Basket;
use Carbon\Carbon;

class PickingBasketService
{
    protected $prefix = 'BAS-';

    public function batchAddBaskets($warehouseId, $num)
    {
        $latestBasket = Basket::query()
            ->latest('id')
            ->first();
        if (!$latestBasket) {
            $startCodeNum = 0;
        } else {
            $temp = substr($latestBasket->basket_code, -3);;
            $startCodeNum = (int) $temp;
        }
        $baskets = [];
        $time = Carbon::now()->getTimestamp();
        $startCodeNum += 1;
        for ($i = 0; $i < $num; $i++) {
            $code = $startCodeNum;
            if ($startCodeNum > 999) {
                return false;
            }
            if ($startCodeNum < 100) {
                $code = sprintf('%03s', $startCodeNum);
            }
            $basket = [
                'warehouse_id' => $warehouseId,
                'basket_code' => $this->prefix . $code,
                'created_at' => $time,
                'updated_at' => $time,
            ];
            $baskets[] = $basket;
            $startCodeNum++;
        }
        Basket::insert($baskets);
        return true;
    }
}
