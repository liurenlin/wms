<?php

namespace App\Services;

use App\Models\Permission;
use App\Models\UserPermission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PermissionService extends Service
{
    public static function isUserRequestAllowed(int $userId, Request $request): bool
    {
        $routeName = $request->route()->getName();
        $routeUri = $request->route()->uri();
        $method = $request->method();
        Log::info($routeName);
        Log::info($routeUri);
        Log::info($method);

        $permission = Permission::query()
            ->when($routeName, function ($query) use ($routeName) {
                $query->where('route_name', $routeName);
            })
            ->where('route_uri', $routeUri)
            ->where('request_method', $method)
            ->first();
        if (!$permission) {
            return false;
        }
        return UserPermission::query()
            ->where('user_id', $userId)
            ->where('permission_id', $permission->id)
            ->exists();
    }
}
