<?php

namespace App\Services\Clients;

use App\Models\OutboundRequestLog;
use GuzzleHttp\Client as HttpClient;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class OmsClient extends Client
{
    const CLIENT_TAG = 'oms';
    const CANCEL_STATE_SUCCESS = 1;
    const CANCEL_STATE_FAIL = 2;

    public $host;

    public function __construct()
    {
        $this->host = config('services.oms.host');
    }


    public function checkOrderShipped(string $orderNumber): bool
    {
        if (app()->runningUnitTests()) {
            return true;
        }
        if (empty($orderNumber)) {
            return false;
        }
        $http = new HttpClient();
        $response = $http->get($this->host . '/api/order_status_check/' . $orderNumber);
        if ($response->getStatusCode() != 200) {
            return false;
        }
        $content = json_decode($response->getBody()->getContents(), JSON_OBJECT_AS_ARRAY);
        Log::debug('checkOrderShipped', $content);
        if (isset($content['data']['status']) && $content['data']['status'] == false) {
            return false;
        }
        return true;
    }

    public function inWare(
        string $thirdPartyOrderNumber,
        string $preReceivingNumber,
        array $skus
    ): bool
    {
        if (app()->runningUnitTests()) {
            return true;
        }
        $headers = [
            'Content-Type' => 'application/json',
            'method' => 'InWare',
        ];
        $list = [];
        foreach ($skus as $sku) {
            $list[] = [
                'skuCode' => $sku['code'],
                'skuName' => $sku['name'],
                'quantity' => $sku['quantity'],
                'unDefectiveQuantity' => $sku['quantity'],
                'defectiveQuantity' => 0,
                'abnormalNumber' => '',
                'abnormalType' => 0,
            ];
        }
        $body = [
            'thirdPartOrderNumber' => $thirdPartyOrderNumber,
            'PreReceivingNumber' => $preReceivingNumber,
            'list' => $list,
        ];

        $uri = $this->host . '/callback/bukitowms/api';

        $response  = $this->runPost('inWare', $uri, $headers, $body);
        if (!$response || empty($response)) {
            return false;
        }
        return true;
    }

    public function orderOut(string $orderNumber, string $weight): bool
    {
        if (app()->runningUnitTests()) {
            return true;
        }
        $headers = [
            'Content-Type' => 'application/json',
            'method' => 'OrderOut',
        ];
        $body = [
            'orderNo' => $orderNumber,
            'weight' => $weight,
        ];

        $uri = $this->host . '/callback/bukitowms/api';

        $response  = $this->runPost('orderOut', $uri, $headers, $body);
        if (!$response || empty($response)) {
            return false;
        }
        return true;
    }

    public function shelfOn(string $skuCode, int $quantity, string $taskNumber, string $preReceivingNumber): bool
    {
        if (app()->runningUnitTests()) {
            return true;
        }
        $headers = [
            'Content-Type' => 'application/json',
            'method' => 'ShelfOn',
        ];
        $body = [
            'skuCode' => $skuCode,
            'quantity' => $quantity,
            'taskNumber' => $taskNumber,
            'PreReceivingNumber' => $preReceivingNumber,
            'abnormalNumber' => '',
            'abnormalType' => 0,
        ];

        $uri = $this->host . '/callback/bukitowms/api';

        $response  = $this->runPost('shelfOn', $uri, $headers, $body);
        if (!$response || empty($response)) {
            return false;
        }
        return true;
    }

    /**
     * @param  array  $list
     * [
     *   [
     *     "sku_code" => "SKU0001",
     *     "quantity" => 2,
     *     "frozen_quantity" => 0,
     *     "bin" => "BIN0001"
     *   ]
     * ]
     * @param  string  $orderNumber
     * @param  string  $purchaseNumber
     * @return bool
     */
    public function stockNotify(array $list, string $orderNumber = '', string $purchaseNumber = ''): bool
    {
        if (app()->runningUnitTests()) {
            return true;
        }
        $headers = [
            'Content-Type' => 'application/json',
            'method' => 'StockNotify',
        ];
        $body = [
            'order_number' => $orderNumber,
            'purchase_number' => $purchaseNumber,
            'list' => $list,
        ];

        $uri = $this->host . '/callback/bukitowms/api';

        $response  = $this->runPost('stockNotify', $uri, $headers, $body);
        if (!$response || empty($response)) {
            return false;
        }
        return true;
    }

    public function orderStatusNotify(string $orderNumber, int $state): bool
    {
        if (app()->runningUnitTests()) {
            return true;
        }
        $headers = [
            'Content-Type' => 'application/json',
            'method' => 'OrderStatusNotify',
        ];
        $body = [
            'order_number' => $orderNumber,
            'state' => $state,
        ];

        $uri = $this->host . '/callback/bukitowms/api';

        $response  = $this->runPost('orderStatusNotify', $uri, $headers, $body);
        if (!$response || empty($response)) {
            return false;
        }
        return true;
    }

    public function orderCancelResultNotify(string $orderNumber, int $state): bool
    {
        if (app()->runningUnitTests()) {
            return true;
        }
        $headers = [
            'Content-Type' => 'application/json',
            'method' => 'OrderCancelResultNotify',
        ];
        $body = [
            'order_number' => $orderNumber,
            'state' => $state,
        ];

        $uri = $this->host . '/callback/bukitowms/api';

        $response  = $this->runPost('orderCancelResultNotify', $uri, $headers, $body);
        if (!$response || empty($response)) {
            return false;
        }
        return true;
    }

    public function preReceivingNotify(string $preReceivingNumber, string $thirdPartyOrderNumber): bool
    {
        if (app()->runningUnitTests()) {
            return true;
        }
        $headers = [
            'Content-Type' => 'application/json',
            'method' => 'BindPurchase',
        ];
        $body = [
            'thirdPartOrderNumber' => $thirdPartyOrderNumber,
            'preReceivingNumber' => $preReceivingNumber,
        ];

        $uri = $this->host . '/callback/bukitowms/api';

        $response  = $this->runPost('preReceivingNotify', $uri, $headers, $body);
        if (!$response || empty($response)) {
            return false;
        }
        return true;
    }

    public function syncProductSku(string $skuCode, float $weight, float $length, float $width, float $height): bool
    {
        if (app()->runningUnitTests()) {
            return true;
        }
        $headers = [
            'Content-Type' => 'application/json',
            'method' => 'SyncProductSku',
        ];
        $body = [
            'skuCode' => $skuCode,
            'weight' => $weight,
            'length' => $length,
            'width' => $width,
            'height' => $height,
        ];

        $uri = $this->host . '/callback/bukitowms/api';

        $response  = $this->runPost('SyncProductSku', $uri, $headers, $body);
        if (!$response || empty($response)) {
            return false;
        }
        return true;
    }

    /**
     * @param  string  $function
     * @param  string  $uri
     * @param  array  $headers
     * @param  array  $body
     * @return array|null
     */
    private function runPost(string $function, string $uri, array $headers, array $body): ?array
    {
        $traceId = Str::uuid()->toString();
        $headers['request-id'] = $traceId;
        $outboundRequestLog = OutboundRequestLog::query()
            ->create([
                'trace_id' => $traceId,
                'client' => self::CLIENT_TAG,
                'uri' => $uri,
                'method' => 'POST',
                'headers' => json_encode($headers),
                'input' => json_encode($body),
            ]);
        try {
            $http = new HttpClient();
            $response = $http->post($uri, [
                'headers' => $headers,
                'json' => $body,
            ]);
            $statusCode = $response->getStatusCode();
            $responseContents = $response->getBody()->getContents();
            $outboundRequestLog->response_body = $responseContents;
            $contents = json_decode($responseContents, JSON_OBJECT_AS_ARRAY);

            if ($statusCode != 200 || !$contents['success']) {
                $outboundRequestLog->state = OutboundRequestLog::STATE_FAIL;
                $outboundRequestLog->save();
            } else {
                $outboundRequestLog->state = OutboundRequestLog::STATE_SUCCESS;
                $outboundRequestLog->save();
            }
            return $contents;
        } catch (\Exception $e) {
            Log::error('OmsClient ' . $function . ' Fail', [
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
            $outboundRequestLog->state = OutboundRequestLog::STATE_EXCEPTION;
            $outboundRequestLog->save();
            return false;
        }
    }
}
