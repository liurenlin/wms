<?php

namespace App\Services;

use App\Models\StorageArea;

class StorageAreaService
{
    public function createStorageArea($type)
    {
        $oldAreas = StorageArea::query()
            ->where('storage_type', $type)
            ->latest()
            ->get();
        $code = 'A';
        foreach ($oldAreas as $oldArea) {
            $suffixCode = $oldArea->area_code;
            $code = (++$suffixCode);
            break;
        }
        while (
            StorageArea::query()
            ->where('area_code', $code)
            ->count() > 0
        ) {
            $code++;
        }
        $area = [
            'storage_type' => $type,
            'area_code' => $code,
        ];
        $area = StorageArea::create($area);
        return $area;
    }
}
