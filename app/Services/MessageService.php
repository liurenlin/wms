<?php

namespace App\Services;

use App\Repositories\OpenMessageQueueRepository;

class MessageService extends Service
{
    public static function inQueue(string $messageType, array $payload = [], array $attributes = []): bool
    {
        $data = [
            'message_type' => $messageType,
            'payload' => $payload,
        ];
        foreach ($attributes as $key => $attribute) {
            $attributes[$key] = (string) $attribute;
        }
        $openMessageQueue = app(OpenMessageQueueRepository::class)->createOpenMessageQueue($messageType, $payload, $attributes);
        if (!$openMessageQueue) {
            return false;
        }
        if (app()->environment() != 'local' && !app()->runningUnitTests()) {
            $publishRes = self::publishMessage(config('services.google.topic'), json_encode($data), $attributes);
        } else {
            $publishRes = [
                'messageIds' => [
                    date('YmdHis') . rand(10000, 99999)
                ],
            ];
        }
        if (isset($publishRes['messageIds'])) {
            $openMessageQueue->message_id = $publishRes['messageIds'][0];
            $openMessageQueue->save();
        }
        return true;
    }


    public static function publishMessage(string $topicName, string $data, array $attributes = []): array
    {
        $pubSubService = new GooglePubSubService();
        return $pubSubService->publish($topicName, $data, $attributes);
    }

    public static function pullMessage($subscription): bool
    {
        $pubSubService = new GooglePubSubService();
        return $pubSubService->pull($subscription);

    }

}
