<?php

namespace App\Services;

use Google\Cloud\Storage\StorageClient;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class GoogleCloudStorageService extends Service
{
    private $client;

    public function __construct()
    {
        putenv('GOOGLE_APPLICATION_CREDENTIALS=' . config('services.google.storage_credentials'));
        $this->client = new StorageClient();
    }


    private function getBucketName()
    {
        $bucketName = config('services.google.bucket_name');
        if (in_array(substr($bucketName, 0, 1), ['/', '\\'])) {
            $bucketName = substr($bucketName, 1);
        }
        return $bucketName;
    }

    public function uploadImageToCdn(UploadedFile $file, $folder)
    {
        return $this->uploadFile(
            $file,
            $folder,
            [
                'Cache-Control'=>'public, max-age=31536000',
            ],
            'publicRead'
        );
    }

    public function uploadFile(UploadedFile $file, string $folder, array $metadata = [], string $predefinedAcl = ''): string
    {
        $folder = Str::finish($folder, '/');
        $extension = $file->extension();
        $filePathName = $file->getPathname();
        $file = fopen($filePathName, 'r');
        $bucketName = $this->getBucketName();
        $fileName = $folder . date('Ymd') . '/'
            . md5(time() . hash_file('md5', $filePathName)) . '.' . $extension;
        $bucket = $this->client->bucket($bucketName);
        $res =$bucket->upload($file, [
            'name' => $fileName,
            'metadata'=> $metadata,
            'predefinedAcl' => $predefinedAcl,
        ]);
        return 'https://' . $bucketName . '/' . $fileName;
    }
}
