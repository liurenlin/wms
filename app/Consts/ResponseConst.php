<?php

namespace App\Consts;

class ResponseConst
{
    const CODE_SUCCESS = 200;
    const CODE_FAIL = 400;
    const CODE_UNAUTHORIZED = 401;
    const CODE_ERROR = 500;

    const MESSAGE_SUCCESS = 'success';
    const MESSAGE_FAIL = 'fail';

}
