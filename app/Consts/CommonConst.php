<?php

namespace App\Consts;

class CommonConst
{
    const WAREHOUSE_ID = 1;

    const DEFAULT_PAGE = 0;
    const DEFAULT_PER_PAGE = 20;

    const PICKING_TASK_QUANTITY_PER_TIME = 9;
    const PICKING_TASK_SKU_MAX_QUANTITY = 80;

}
