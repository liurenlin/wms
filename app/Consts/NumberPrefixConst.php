<?php

namespace App\Consts;

class NumberPrefixConst
{
    const ACTUAL_RECEIVING = 'ACT';
    const PICKING = 'PIC';
    const PUTAWAY = 'PUT';
    const CHECK = 'CHE';
    const ABNORMAL = 'ABN';
    const ABNORMAL_CODE = 'ABNC';
    const PRE_RECEIVING = 'PRE';
    const OUTBOUND = 'OUT';
    const HANDOVER = 'HAN';
}
