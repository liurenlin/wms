<?php
namespace App\Consts;


class StorageTypeConst
{

    const STORAGE_TYPE_CONST_ABNORMAL = 5;

    const STORAGE_TYPE_CONST_PICKING = 2;

    const STORAGE_TYPE_CONST_DETECTIVE = 6;
    
    public static function getTypes()
    {
        return [
            self::STORAGE_TYPE_CONST_ABNORMAL,
            self::STORAGE_TYPE_CONST_PICKING,
            self::STORAGE_TYPE_CONST_DETECTIVE,
        ];
    }
}
