<?php
namespace App\Exceptions;

use Exception;


class BaseException extends Exception
{
    //HTTP状态码
    public $httpCode = 400;

    // 错误具体信息
    public $message = 'error param';

    //自定义错误码
    public $code = 10000;

    public function __construct($param = [])
    {
        if (!is_array($param)) {
            return;
        }
        if (array_key_exists('code', $param)) {
            $this->code = $param['code'];
        }
        if (array_key_exists('message', $param)) {
            $this->message = $param['message'];
        }
        if (array_key_exists('httpCode', $param)) {
            $this->httpCode = $param['httpCode'];
        }
    }

}