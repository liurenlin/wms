<?php
namespace App\Exceptions;


class ParameterException extends BaseException
{
    //HTTP状态码
    public $httpCode = 200;

    // 错误具体信息
    public $message = 'Parameter Invalid!';

    //自定义错误码
    public $code = 10003;

}