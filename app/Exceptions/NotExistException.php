<?php
namespace App\Exceptions;


class NotExistException extends BaseException
{
    //HTTP状态码
    public $httpCode = 200;

    // 错误具体信息
    public $message = 'object not exist!';

    //自定义错误码
    public $code = 10002;

}