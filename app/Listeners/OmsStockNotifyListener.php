<?php

namespace App\Listeners;

use App\Events\OmsStockNotifyEvent;
use App\Repositories\StockRepository;
use App\Services\Clients\OmsClient;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class OmsStockNotifyListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OmsStockNotifyEvent  $event
     * @return void
     */
    public function handle(OmsStockNotifyEvent $event)
    {
        $list = app(StockRepository::class)->getStocksBySkus($event->warehouseId, $event->list);
        $omsClient = new OmsClient();

        return $omsClient->stockNotify($list, $event->orderNumber, $event->purchaseNumber);
    }
}
