<?php

namespace App\Listeners;

use App\Events\OmsInWareEvent;
use App\Models\ActualReceiving;
use App\Models\ActualReceivingItem;
use App\Models\PreReceiving;
use App\Models\PreReceivingItem;
use App\Models\ProductSku;
use App\Services\Clients\OmsClient;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class OmsInWareListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OmsInWareEvent  $event
     * @return void
     */
    public function handle(OmsInWareEvent $event)
    {
        $list = [];
        $actualReceivingItems = ActualReceivingItem::query()
            ->where('actual_receiving_id', $event->actualReceivingId)
            ->get();

        foreach ($actualReceivingItems as $actualReceivingItem) {
            $list[] = [
                'code' => $actualReceivingItem->sku_code,
                'quantity' => $actualReceivingItem->receiving_quantity,
                'name' => ProductSku::query()->where('sku_code', $actualReceivingItem->sku_code)->value('sku_name'),
            ];
        }
        if (empty($list)) {
            return false;
        }

        $actualReceiving = ActualReceiving::query()->find($event->actualReceivingId);
        $preReceiving = PreReceiving::query()->find($actualReceiving->pre_receiving_id);

        $omsClient = new OmsClient();
        $omsClient->inWare(
            $preReceiving->third_party_order_number,
            $preReceiving->pre_receiving_number,
            $list
        );
        return true;
    }
}
