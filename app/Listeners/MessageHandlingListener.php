<?php

namespace App\Listeners;

use App\Events\MessageHandlingEvent;
use App\Events\OmsInWareEvent;
use App\Events\OmsOrderCancelResultNotifyEvent;
use App\Events\OmsOrderOutEvent;
use App\Events\OmsOrderStatusNotifyEvent;
use App\Events\OmsPreReceivingNotifyEvent;
use App\Events\OmsShelfOnEvent;
use App\Events\OmsStockNotifyEvent;
use App\Events\OmsSyncProductSkuEvent;
use App\Models\OpenMessageQueue;
use App\Models\Outbound;
use App\Models\PickingTaskBasket;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;

class MessageHandlingListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MessageHandlingEvent  $event
     * @return void
     */
    public function handle(MessageHandlingEvent $event)
    {
        // 查询 message 是否存在
        $message = OpenMessageQueue::query()
            ->where('message_id', (string) $event->messageId)
            ->first();
        if (!$message) {
            return false;
        }

        if ($message->state == OpenMessageQueue::STATE_DONE) {
            return true;
        }
        // 根据内容选择合适的接口
        $res = $this->done($message->message_type, json_decode($event->data, JSON_OBJECT_AS_ARRAY), $event->attributes);
        if (!$res) {
            $message->state = OpenMessageQueue::STATE_ERROR;
            $message->save();
            return false;
        }
        $message->state = OpenMessageQueue::STATE_DONE;
        $message->save();
        return true;
    }

    private function done(string $messageType, array $data = [], array $attributes = []): bool
    {
        if (in_array($messageType, [
            OpenMessageQueue::MESSAGE_TYPE_STORE_OUTBOUND,
            OpenMessageQueue::MESSAGE_TYPE_STORE_PRE_RECEIVING,
            OpenMessageQueue::MESSAGE_TYPE_BATCH_STORE_PRODUCT,
            OpenMessageQueue::MESSAGE_TYPE_BATCH_STORE_SKU,
        ])) {
            // request
            $request = new Request();
            foreach ($data['payload'] as $key => $val) {
                $request->offsetSet($key, $val);
            }
            switch ($messageType) {
                case OpenMessageQueue::MESSAGE_TYPE_STORE_OUTBOUND:
                    $runner = app(\App\OpenRunners\Outbound\StoreRunner::class);
                    break;
                case OpenMessageQueue::MESSAGE_TYPE_STORE_PRE_RECEIVING:
                    $runner = app(\App\OpenRunners\PreReceiving\StoreRunner::class);
                    break;
                case OpenMessageQueue::MESSAGE_TYPE_BATCH_STORE_PRODUCT:
                    $runner = app(\App\OpenRunners\Product\BatchStoreRunner::class);
                    break;
                case OpenMessageQueue::MESSAGE_TYPE_BATCH_STORE_SKU:
                    $runner = app(\App\OpenRunners\Product\BatchStoreSkuRunner::class);
                    break;
            }
            $response = $runner->run($request)->getOriginalContent();
            if ($response['code'] == 200) {
                return true;
            } else {
                return false;
            }

        }

        if (in_array($messageType, [
            OpenMessageQueue::MESSAGE_TYPE_IN_WARE,
            OpenMessageQueue::MESSAGE_TYPE_ORDER_OUT,
            OpenMessageQueue::MESSAGE_TYPE_SHELF_ON,
            OpenMessageQueue::MESSAGE_TYPE_ORDER_STATUS_NOTIFY,
            OpenMessageQueue::MESSAGE_TYPE_ORDER_CANCEL_RESULT_NOTIFY,
            OpenMessageQueue::MESSAGE_TYPE_STOCK_NOTIFY,
            OpenMessageQueue::MESSAGE_TYPE_PRE_RECEIVING_NOTIFY,
            OpenMessageQueue::MESSAGE_TYPE_SYNC_PRODUCT_SKU,
        ])) {
            $res = false;
            switch ($messageType) {
                case OpenMessageQueue::MESSAGE_TYPE_IN_WARE:
                    $res = event(new OmsInWareEvent($data['payload']['actual_receiving_id']));
                    break;
                case OpenMessageQueue::MESSAGE_TYPE_ORDER_OUT:
                    $res = event(new OmsOrderOutEvent($data['payload']['outbound_number']));
                    break;
                case OpenMessageQueue::MESSAGE_TYPE_SHELF_ON:
                    $res = event(new OmsShelfOnEvent(
                        $data['payload']['sku_code'],
                        $data['payload']['quantity'],
                        $data['payload']['task_number'],
                        $data['payload']['pre_receiving_number']
                    ));
                    break;
                case OpenMessageQueue::MESSAGE_TYPE_ORDER_STATUS_NOTIFY:
                    if (isset($data['payload']['picking_task_id'])) {
                        $pickingTaskBaskets = PickingTaskBasket::query()
                            ->where('picking_task_id', $data['payload']['picking_task_id'])
                            ->get();
                        foreach ($pickingTaskBaskets as $pickingTaskBasket) {
                            $outbound = Outbound::query()
                                ->where('outbound_number', $pickingTaskBasket->outbound_number)
                                ->first();
                            $res = event(new OmsOrderStatusNotifyEvent($outbound->order_number, $outbound->state));
                        }
                    } else {
                        $outbound = Outbound::query()
                            ->where('order_number', $data['payload']['order_number'])
                            ->latest('id')
                            ->first();
                        $res = event(new OmsOrderStatusNotifyEvent($outbound->order_number, $outbound->state));
                    }
                    break;
                case OpenMessageQueue::MESSAGE_TYPE_ORDER_CANCEL_RESULT_NOTIFY:
                    $res = event(new OmsOrderCancelResultNotifyEvent($data['payload']['order_number'], $data['payload']['state']));
                    break;
                case OpenMessageQueue::MESSAGE_TYPE_STOCK_NOTIFY:
                    $res = event(new OmsStockNotifyEvent($data['payload']['warehouse_id'], $data['payload']['list'], $data['payload']['order_number'] ?? '', $data['payload']['purchase_number'] ?? ''));
                    break;
                case OpenMessageQueue::MESSAGE_TYPE_PRE_RECEIVING_NOTIFY:
                    $res = event(new OmsPreReceivingNotifyEvent($data['payload']['pre_receiving_number'], $data['payload']['third_party_order_number']));
                    break;
                case OpenMessageQueue::MESSAGE_TYPE_SYNC_PRODUCT_SKU:
                    $res = event(new OmsSyncProductSkuEvent($data['payload']['sku_code']));
                    break;
            }
            if ($res) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
}
