<?php

namespace App\Listeners;

use App\Events\OmsShelfOnEvent;
use App\Services\Clients\OmsClient;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class OmsShelfOnListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OmsShelfOnEvent  $event
     * @return void
     */
    public function handle(OmsShelfOnEvent $event)
    {
        $omsClient = new OmsClient();
        $omsClient->shelfOn(
            $event->skuCode,
            $event->quantity,
            $event->putawayTaskNumber,
            $event->preReceivingNumber
        );
        return true;
    }
}
