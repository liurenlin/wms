<?php

namespace App\Listeners;

use App\Events\OmsOrderOutEvent;
use App\Models\Outbound;
use App\Models\Weigh;
use App\Services\Clients\OmsClient;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class OmsOrderOutListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OmsOrderOutEvent  $event
     * @return void
     */
    public function handle(OmsOrderOutEvent $event)
    {
        $outbound = Outbound::query()->where('outbound_number', $event->outboundNumber)->first();
        if (!$outbound) {
            return false;
        }
        // 称重记录
        $weigh = Weigh::query()
            ->where('outbound_number', $outbound->outbound_number)
            ->first();
        if (!$weigh) {
            return false;
        }
        $omsClient = new OmsClient();
        $omsClient->orderOut(
            $outbound->order_number,
            (string) $weigh->weight
        );
        return true;
    }
}
