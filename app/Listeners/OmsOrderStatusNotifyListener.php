<?php

namespace App\Listeners;

use App\Events\OmsOrderStatusNotifyEvent;
use App\Models\OutboundRequestLog;
use App\Services\Clients\OmsClient;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class OmsOrderStatusNotifyListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OmsOrderStatusNotifyEvent  $event
     * @return void
     */
    public function handle(OmsOrderStatusNotifyEvent $event)
    {
        $omsClient = new OmsClient();
        return $omsClient->orderStatusNotify($event->orderNumber, $event->state);

    }
}
