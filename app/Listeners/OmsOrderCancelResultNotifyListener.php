<?php

namespace App\Listeners;

use App\Events\OmsOrderCancelResultNotifyEvent;
use App\Services\Clients\OmsClient;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class OmsOrderCancelResultNotifyListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OmsOrderCancelResultNotifyEvent  $event
     * @return void
     */
    public function handle(OmsOrderCancelResultNotifyEvent $event)
    {
        $omsClient = new OmsClient();
        return $omsClient->orderCancelResultNotify($event->orderNumber, $event->state);
    }
}
