<?php

namespace App\Listeners;

use App\Events\OmsPreReceivingNotifyEvent;
use App\Services\Clients\OmsClient;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class OmsPreReceivingNotifyListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OmsPreReceivingNotifyEvent  $event
     * @return void
     */
    public function handle(OmsPreReceivingNotifyEvent $event)
    {
        $omsClient = new OmsClient();
        return $omsClient->preReceivingNotify($event->preReceivingNumber, $event->thirdPartyOrderNumber);
    }
}
