<?php

namespace App\Listeners;

use App\Events\OmsSyncProductSkuEvent;
use App\Models\ProductSku;
use App\Services\Clients\OmsClient;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class OmsSyncProductSkuListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OmsSyncProductSkuEvent  $event
     * @return void
     */
    public function handle(OmsSyncProductSkuEvent $event)
    {
        $sku = ProductSku::query()->where('sku_code', $event->skuCode)->first();
        if (!$sku) {
            return true;
        }
        $omsClient = new OmsClient();
        return $omsClient->syncProductSku(
            $sku->sku_code,
            (float) $sku->weight,
            (float) $sku->length,
            (float) $sku->width,
            (float) $sku->height
        );
    }
}
