<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OmsOrderCancelResultNotifyEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $orderNumber;
    public $state;

    /**
     * Create a new event instance.
     *
     * @param  string  $orderNumber
     * @param  int  $state
     */
    public function __construct(string $orderNumber, int $state)
    {
        $this->orderNumber = $orderNumber;
        $this->state = $state;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
