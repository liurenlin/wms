<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OmsStockNotifyEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $warehouseId;
    public $list;
    public $orderNumber;
    public $purchaseNumber;

    /**
     * Create a new event instance.
     *
     * @param  int  $warehouseId
     * @param  array  $list
     * @param  string  $orderNumber
     * @param  string  $purchaseNumber
     */
    public function __construct(int $warehouseId, array $list, string $orderNumber = '', string $purchaseNumber = '')
    {
        $this->warehouseId = $warehouseId;
        $this->list = $list;
        $this->orderNumber = $orderNumber;
        $this->purchaseNumber = $purchaseNumber;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
