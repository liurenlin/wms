<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OmsInWareEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $actualReceivingId;

    /**
     * Create a new event instance.
     * @param  int  $preReceivingId
     */
    public function __construct(int $actualReceivingId)
    {
        $this->actualReceivingId = $actualReceivingId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
