<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OmsPreReceivingNotifyEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $preReceivingNumber;
    public $thirdPartyOrderNumber;

    /**
     * Create a new event instance.
     *
     * @param  string  $preReceivingNumber
     * @param  string  $thirdPartyOrderNumber
     */
    public function __construct(string $preReceivingNumber, string $thirdPartyOrderNumber)
    {
        $this->preReceivingNumber = $preReceivingNumber;
        $this->thirdPartyOrderNumber = $thirdPartyOrderNumber;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
