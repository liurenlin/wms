<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OmsShelfOnEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $skuCode;
    public $quantity;
    public $putawayTaskNumber;
    public $preReceivingNumber;

    /**
     * Create a new event instance.
     *
     * @param  string  $skuCode
     * @param  int  $quantity
     * @param  string  $putawayTaskNumber
     * @param  string  $preReceivingNumber
     */
    public function __construct(string $skuCode, int $quantity, string $putawayTaskNumber = '', string $preReceivingNumber = '')
    {
        $this->skuCode = $skuCode;
        $this->quantity = $quantity;
        $this->putawayTaskNumber = $putawayTaskNumber;
        $this->preReceivingNumber = $preReceivingNumber;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
