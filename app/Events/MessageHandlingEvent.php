<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class MessageHandlingEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $messageId;
    public $data;
    public $attributes;

    /**
     * Create a new event instance.
     *
     * @param  string  $messageId
     * @param  string  $data
     * @param  array  $attributes
     */
    public function __construct(string $messageId, string $data, array $attributes)
    {
        $this->messageId = $messageId;
        $this->data = $data;
        $this->attributes = $attributes;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
